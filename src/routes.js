import React from 'react';


const OtherSamplePage = React.lazy(() => import('./Demo/Other/SamplePage'));
//const OtherSamplePage = React.lazy(() => import('./admin/login/index'));

const FavoritePage = React.lazy(() => import('./favorite/index'));


////////////////////////////////////////////////////  setup-data

const setupTranSAP = React.lazy(() => import('./setup-data/setup-data-transfer-sap/setup-data-transfer-sap-main'));
const setupAdvanceSet = React.lazy(() => import('./setup-data/setup-data-adv-set/setup-data-adv-set-main'));
const setupUploadPDF = React.lazy(() => import('./setup-data/setup-data-upload-pdf/upload-pdf-main'));
const setupCustomerMaster = React.lazy(() => import('./setup-data/setup-data-customer/customer-master-main'));
const setupGeneralMaster = React.lazy(() => import('./setup-data/setup-data-general/general-master-main'));
const setupCFRMaster = React.lazy(() => import('./setup-data/setup-data-cfr/crf-master-main'));
const setupSCORMETRICSMASTER = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-metrics-main'));



//setup-data-transfer-sap
const customerToCar = React.lazy(() => import('./setup-data/setup-data-transfer-sap/customer-to-car'));
const transfertosap = React.lazy(() => import('./setup-data/setup-data-transfer-sap/transfer-to-sap'));
const dntransfer = React.lazy(() => import('./setup-data/setup-data-transfer-sap/dn-transfer'));
const interfacend = React.lazy(() => import('./setup-data/setup-data-transfer-sap/interface-nd'));
const uploadpa = React.lazy(() => import('./setup-data/setup-data-transfer-sap/upload-pa'));
const deptforspecial = React.lazy(() => import('./setup-data/setup-data-transfer-sap/dept-for-special'));
const cario = React.lazy(() => import('./setup-data/setup-data-transfer-sap/car-i-o'));

//setup-data-adv-set

const prepareNewProduct = React.lazy(() => import('./setup-data/setup-data-adv-set/prepare-new-product'));
const productco = React.lazy(() => import('./setup-data/setup-data-adv-set/product-co'));
const fromproductadv = React.lazy(() => import('./setup-data/setup-data-adv-set/from-product-adv'));
const advanceset = React.lazy(() => import('./setup-data/setup-data-adv-set/advance-set'));
const projectpricemaster = React.lazy(() => import('./setup-data/setup-data-adv-set/project-price-master'));
const tradertd = React.lazy(() => import('./setup-data/setup-data-adv-set/trader-td'));
const groupproductadv = React.lazy(() => import('./setup-data/setup-data-adv-set/group-product-adv'));
const cfrquotarange = React.lazy(() => import('./setup-data/setup-data-adv-set/cfr-quota-range'));
const improvecfrquota = React.lazy(() => import('./setup-data/setup-data-adv-set/improve-cfr-quota'));
const quotationproject = React.lazy(() => import('./setup-data/setup-data-adv-set/quotation-project'));

//setup-data-upload-pdf
const uploadpdfforcg = React.lazy(() => import('./setup-data/setup-data-upload-pdf/upload-pdf-for-cg'));
const importemisaf = React.lazy(() => import('./setup-data/setup-data-upload-pdf/import-emis-af'));
const uploadpdfforsys = React.lazy(() => import('./setup-data/setup-data-upload-pdf/upload-pdf-for-sys'));

//setup-data-customer
const customermaster = React.lazy(() => import('./setup-data/setup-data-customer/customer-master'));
const shipmentport = React.lazy(() => import('./setup-data/setup-data-customer/shipment-port'));
const tradermaster = React.lazy(() => import('./setup-data/setup-data-customer/trader-master'));
const adnfrabrucator = React.lazy(() => import('./setup-data/setup-data-customer/adn-frabrucator'));

//setup-data-general
const termconditionmaster = React.lazy(() => import('./setup-data/setup-data-general/term-condition-master'));
const paymenttermmaster = React.lazy(() => import('./setup-data/setup-data-general/payment-term-master'));
const advancetypesetup = React.lazy(() => import('./setup-data/setup-data-general/advance-type-setup'));
const surchargetypesetup = React.lazy(() => import('./setup-data/setup-data-general/surcharge-type-setup'));
const projecttypemaster = React.lazy(() => import('./setup-data/setup-data-general/project-type-master'));
const exportadvancedset = React.lazy(() => import('./setup-data/setup-data-general/export-advanced-set'));
const bankmaster = React.lazy(() => import('./setup-data/setup-data-general/bank-master'));
const applparameter = React.lazy(() => import('./setup-data/setup-data-general/appl-parameter'));
const rebatetypemaster = React.lazy(() => import('./setup-data/setup-data-general/rebate-type-master'));
const generalmaster = React.lazy(() => import('./setup-data/setup-data-general/general-master'));
const countryforexport = React.lazy(() => import('./setup-data/setup-data-general/country-for-export'));

//setup-data-cfr
const producttype = React.lazy(() => import('./setup-data/setup-data-cfr/product-type'));
const shippingcompany = React.lazy(() => import('./setup-data/setup-data-cfr/shipping-company'));

//setup-data-scor-metrics
const scormatrixcontrol = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-matrix-control'));
const scorcd = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-cd'));
const scorclaimdata = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-claim-data'));
const scorifreason = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-if-reason'));
const scorunorder = React.lazy(() => import('./setup-data/setup-data-scor-metrics/scor-unorder'));


////////////////////////////////////////////////////  PN-Master
//#region PN-Master
//PN-Master => Product
const ProductMain = React.lazy(() => import('./pn-master/product/product-main'));
const ProductCodeOpenningMS126 = React.lazy(() => import('./pn-master/product/ProductCodeOpenningMS126'));
const ConfirmOpenProductSBC = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSBC'));
const ConfirmOpenProductSSK = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSK'));
const ConfirmOpenProductSSR2 = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSR2'));
const ConfirmOpenProductSGT = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSGT'));
const ConfirmOpenProductSSJ = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSJ'));
const ConfirmOpenProductSSR1 = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSR1'));
const ItemMastFGSTOCK = React.lazy(() => import('./pn-master/product/ItemMastFGSTOCK'));

//PN-Master => Product Master
const ProductMasterMain = React.lazy(() => import('./pn-master/product-master/product-master-main'));
const GradeMaster = React.lazy(() => import('./pn-master/product-master/GradeMaster'));
const GroupVcCost = React.lazy(() => import('./pn-master/product-master/GroupVcCost'));
const GroupVcMaster = React.lazy(() => import('./pn-master/product-master/GroupVcMaster'));
const PnStatus = React.lazy(() => import('./pn-master/product-master/PnStatus'));
const PnStatusSize = React.lazy(() => import('./pn-master/product-master/PnStatusSize'));
const SalesOrgChannelNewPn = React.lazy(() => import('./pn-master/product-master/SalesOrgChannelNewPn'));
const SectionHierarchyMaster = React.lazy(() => import('./pn-master/product-master/SectionHierarchyMaster'));
const SectionMaster = React.lazy(() => import('./pn-master/product-master/SectionMaster'));
const SizeMaster = React.lazy(() => import('./pn-master/product-master/SizeMaster'));
const StandardGrade = React.lazy(() => import('./pn-master/product-master/StandardGrade'));
const StandardMaster = React.lazy(() => import('./pn-master/product-master/StandardMaster'));
const StorageNewPn = React.lazy(() => import('./pn-master/product-master/StorageNewPn'));

//PN-Master => Other Master
const OtherMasterMain = React.lazy(() => import('./pn-master/other-master/other-master-main'));
const PclLicence = React.lazy(() => import('./pn-master/other-master/PclLicence'));
const NominalSizeHarmonize = React.lazy(() => import('./pn-master/other-master/NominalSizeHarmonize'));
const SectionHarmonize = React.lazy(() => import('./pn-master/other-master/SectionHarmonize'));
const PclLicenceDesc = React.lazy(() => import('./pn-master/other-master/PclLicenceDesc'));

//PN-Master => Production Plan
const ProductionPlanMain = React.lazy(() => import('./pn-master/production-plan/production-plan-main'));
const ExportYearlyProductionPlan = React.lazy(() => import('./pn-master/production-plan/ExportYearlyProductionPlan'));
const DomesticYearlyProductionPlan = React.lazy(() => import('./pn-master/production-plan/DomesticYearlyProductionPlan'));
//#endregion

////////////////////////////////////////////////////  Authorizes
//#region 
//User & Authorize
const UserAuthMain = React.lazy(() => import('./authorizes/user-authorize/authorizes-main'));
const CustomerPassword = React.lazy(() => import('./authorizes/user-authorize/customer-user-password'));
const SysPassword = React.lazy(() => import('./authorizes/user-authorize/sys-user-password'));
const UserMaster = React.lazy(() => import('./authorizes/user-authorize/user-master'));

//e-mail
const EmailMain = React.lazy(() => import('./authorizes/e-mail/email-main'));
const AutoEmail = React.lazy(() => import('./authorizes/e-mail/auto-email'));
const RegionEmail = React.lazy(() => import('./authorizes/e-mail/region-email'));

//set-screen
const SetScreenMain = React.lazy(() => import('./authorizes/set-screen/set-screen-main'));
const EorGroupSetup = React.lazy(() => import('./authorizes/set-screen/eor-group-setup'));
const EorPage = React.lazy(() => import('./authorizes/set-screen/eor-page'));
const MenuApplication = React.lazy(() => import('./authorizes/set-screen/menu-application'));
const MenuGroup = React.lazy(() => import('./authorizes/set-screen/menu-group'));
//#endregion

////////////////////////////////////////////////////  PROJECT
//#region 
//order-confirm
const OrderMain = React.lazy(() => import('./project/project-order-confirm/project-order-confirm-main'));
const chkOrder = React.lazy(() => import('./project/project-order-confirm/check-project-order'));
const invoiceOrder = React.lazy(() => import('./project/project-order-confirm/project-invoice'));
const OrderConf = React.lazy(() => import('./project/project-order-confirm/project-order-confirm'));
const showOrder = React.lazy(() => import('./project/project-order-confirm/show-order-confirm'));

//quotation
const QuotaMain = React.lazy(() => import('./project/quotation/project-quotation-main'));
const AppvQuo = React.lazy(() => import('./project/quotation/approve-project-quotation'));
const Chk1 = React.lazy(() => import('./project/quotation/check-1-project-quotation'));
const Chk2 = React.lazy(() => import('./project/quotation/check-2-project-quotation'));
const invoiceQuo = React.lazy(() => import('./project/quotation/project-quotation-invoice'));
const ProjectQuta = React.lazy(() => import('./project/quotation/project-quotation'));

//receiving
const receivingMain = React.lazy(() => import('./project/project-receiving/project-receiving-main'));
const CarPro = React.lazy(() => import('./project/project-receiving/receiving-car-process'));
const CarSet = React.lazy(() => import('./project/project-receiving/receiving-car-setting'));
const EstPro = React.lazy(() => import('./project/project-receiving/est-shipment-project'));
const RepPro = React.lazy(() => import('./project/project-receiving/report-quotation-project'));
//#endregion

////////////////////////////////////////////////////  EXPORT
//#region 
//order-confirm
const ExpOrderConfirmMain = React.lazy(() => import('./export/order-confirm/order-confirm-main'));
const ExpApproveOrder = React.lazy(() => import('./export/order-confirm/approve-order'));
const ExpCheckOrder = React.lazy(() => import('./export/order-confirm/check-order'));
const ExpExportInvice = React.lazy(() => import('./export/order-confirm/export-invoice'));
const ExpExportOrder = React.lazy(() => import('./export/order-confirm/export-order'));

//quotation
const ExpQuotationMain = React.lazy(() => import('./export/quotation/quotation-main'));
const ExpExportQuotation = React.lazy(() => import('./export/quotation/export-quotation'));
const ExpChheck1ExportQuotation = React.lazy(() => import('./export/quotation/check-1-export-quotation'));
const ExpChheck2ExportQuotation = React.lazy(() => import('./export/quotation/check-2-export-quotation'));
const ExpApproveExportQuotation = React.lazy(() => import('./export/quotation/approve-export-quotation'));
const ExpInquiryProcess = React.lazy(() => import('./export/quotation/inquiry-process'));
const ExpUploadExport = React.lazy(() => import('./export/quotation/upload-export'));

//export-process
const ExpExportProcessMain = React.lazy(() => import('./export/export-process/export-process-main'));
const ExpCheckLow = React.lazy(() => import('./export/export-process/check-low'));
const ExpApproveLow1 = React.lazy(() => import('./export/export-process/approve-low-1'));
const ExpApproveLow2 = React.lazy(() => import('./export/export-process/approve-low-2'));

//export-report
const ExpReportMain = React.lazy(() => import('./export/export-report/export-report-main'));
const ExpReportOrder = React.lazy(() => import('./export/export-report/export-report-order'));
//#endregion

////////////////////////////////////////////////////  CUT BEAM
//CUS-BEAM => Cut Process
const CutProcessMain = React.lazy(() => import('./cut-beam/cut-process/cut-process-main'));
const SteelCutBill = React.lazy(() => import('./cut-beam/cut-process/SteelCutBill'));
const SteelCuttingFinished = React.lazy(() => import('./cut-beam/cut-process/SteelCuttingFinished'));
const SteelCuttingInvoice = React.lazy(() => import('./cut-beam/cut-process/SteelCuttingInvoice'));
const WaitingToPrintSteelCutBill = React.lazy(() => import('./cut-beam/cut-process/WaitingToPrintSteelCutBill'));
const SteelReceiveCustomer = React.lazy(() => import('./cut-beam/cut-process/SteelReceiveCustomer'));
const DeliveryBill = React.lazy(() => import('./cut-beam/cut-process/DeliveryBill'));
const ApproveCutBill = React.lazy(() => import('./cut-beam/cut-process/ApproveCutBill'));

//CUS-BEAM => Memo for Cut
const MemoForCut = React.lazy(() => import('./cut-beam/memo-for-cut/memo-for-cut-main'));
const RequstToCutProductBill = React.lazy(() => import('./cut-beam/memo-for-cut/RequstToCutProductBill'));
const ApproveToCutProduct = React.lazy(() => import('./cut-beam/memo-for-cut/ApproveToCutProduct'));

//CUS-BEAM => After Billing Process 
const AfterBillingProcessMain = React.lazy(() => import('./cut-beam/after-billing-process/after-billing-process-main'));
const DataBillDueDate = React.lazy(() => import('./cut-beam/after-billing-process/DataBillDueDate'));
const WaybillData = React.lazy(() => import('./cut-beam/after-billing-process/WaybillData'));
const ImportDataBillDueDate = React.lazy(() => import('./cut-beam/after-billing-process/ImportDataBillDueDate'));
const ReceiptInformation = React.lazy(() => import('./cut-beam/after-billing-process/ReceiptInformation'));

//CUS-BEAM => Report
const ReportMain = React.lazy(() => import('./cut-beam/cut-beam-report/report-main'));
const ReportFromCustomer = React.lazy(() => import('./cut-beam/cut-beam-report/ReportFromCustomer'));
const ReportCuttingBill = React.lazy(() => import('./cut-beam/cut-beam-report/ReportCuttingBill'));
const ReportReceivedFromCut = React.lazy(() => import('./cut-beam/cut-beam-report/ReportReceivedFromCut'));
const ReportCashForCutting = React.lazy(() => import('./cut-beam/cut-beam-report/ReportCashForCutting'));
//#endregion


const domesticMain = React.lazy(() => import('./domestic/domestic-order-confirm/domestic-order-main'));
const checkdomesticorderconfirmMain = React.lazy(() => import('./domestic/domestic-order-confirm/check-domestic-order-confirm'));
const CustomerPosaptext = React.lazy(() => import('./domestic/domestic-order-confirm/customer-po-sap-text'));
const domesticdomesticorderconfirmMain = React.lazy(() => import('./domestic/domestic-order-confirm/domestic-domestic-order-confirm'));
const domesticinvoiceMain = React.lazy(() => import('./domestic/domestic-order-confirm/domestic-invoice'));
const domesticorderconfirmshowMain = React.lazy(() => import('./domestic/domestic-order-confirm/domestic-order-confirm-show'));
const domesticposaptextMain = React.lazy(() => import('./domestic/domestic-order-confirm/domestic-po-sap-text'));
const manageorderfilloldMain = React.lazy(() => import('./domestic/domestic-order-confirm/manage-order-fill-old'));


const domesticDlcMain = React.lazy(() => import('./domestic/domestic-dlc/domestic-dlc-main'));
const approvescdlcMain = React.lazy(() => import('./domestic/domestic-dlc/approve-sc-dlc'));
const checkscdlcMain = React.lazy(() => import('./domestic/domestic-dlc/check-sc-dlc'));
const scdlcdomesticMain = React.lazy(() => import('./domestic/domestic-dlc/sc-dlc-domestic'));


const domesticquotationMain = React.lazy(() => import('./domestic/domestic-quotation/index'));
const approvedomesticquotationMain = React.lazy(() => import('./domestic/domestic-quotation/approve-domestic-quotation'));
const check1domesticquotationMain = React.lazy(() => import('./domestic/domestic-quotation/check1-domestic-quotation'));
const check2domesticquotationMain = React.lazy(() => import('./domestic/domestic-quotation/check2-domestic-quotation'));
const csapprovedomesticquotationMain = React.lazy(() => import('./domestic/domestic-quotation/cs-approve-domestic-quotation'));
const csapproveexportquotationMain = React.lazy(() => import('./domestic/domestic-quotation/cs-approve-export-quotation'));
const domesticdomestquotationMain = React.lazy(() => import('./domestic/domestic-quotation/domestic-domest-quotation'));
const renewalcertificatepossibilityMain = React.lazy(() => import('./domestic/domestic-quotation/renewal-certificate-possibility'));


const domesticOrderMain = React.lazy(() => import('./domestic/domestic-order/domestic-order-main'));
const approveCutOffMain = React.lazy(() => import('./domestic/domestic-order/approve-cut-off-ticket'));
const arrangeacarspecifyDPMain = React.lazy(() => import('./domestic/domestic-order/arrange-a-car-specify-DP-number'));
const arrangetransportationssjMain = React.lazy(() => import('./domestic/domestic-order/arrange-transportation-ssj'));
const createapprovalrequestMain = React.lazy(() => import('./domestic/domestic-order/create-approval-request'));
const createcararrangementMain = React.lazy(() => import('./domestic/domestic-order/create-car-arrangement'));
const createcheckthepaymentsummarySYSMain = React.lazy(() => import('./domestic/domestic-order/create-check-the-payment-summary-SYS'));
const createpurchaseorderSYSMain = React.lazy(() => import('./domestic/domestic-order/create-purchase-order-SYS'));
const deliveryscheduleMain = React.lazy(() => import('./domestic/domestic-order/delivery-schedule'));
const domesticarrangeacarMain = React.lazy(() => import('./domestic/domestic-order/domestic-arrange-a-car'));
const domesticarrangetransportMain = React.lazy(() => import('./domestic/domestic-order/domestic-arrange-transport'));
const estshipmentdoMain = React.lazy(() => import('./domestic/domestic-order/est.shipment-do'));
const ESTshipmentdooldMain = React.lazy(() => import('./domestic/domestic-order/EST.shipment-do.old'));
const purchaseorderMain = React.lazy(() => import('./domestic/domestic-order/purchase-order'));
const salesORG0180Main = React.lazy(() => import('./domestic/domestic-order/sales-ORG-0180'));
const summaryofpaymentMain = React.lazy(() => import('./domestic/domestic-order/summary-of-payment'));

//const xxx = React.lazy(() => import('./authorizes/xxx'));

//#region
const advancedocmain = React.lazy(() => import('./report/advance-order/advance-order-main'));
const summarycustomer = React.lazy(() => import('./report/advance-order/summarycustomer'));
const summaryproduct = React.lazy(() => import('./report/advance-order/summaryproduct'));
const summarybyproduct = React.lazy(() => import('./report/advance-order/summarybyproduct'));
const summarybycustomer = React.lazy(() => import('./report/advance-order/summarybycustomer'));

const confirmedmain = React.lazy(() => import('./report/confirmed-order/confirmed-order-main'));
const confirmbycustumer = React.lazy(() => import('./report/confirmed-order/confirmbycustumer'));
const confirmordersap = React.lazy(() => import('./report/confirmed-order/confirmordersap'));
const confirmsummarycustumer = React.lazy(() => import('./report/confirmed-order/summary-order'));
const confirmsummaryproduct = React.lazy(() => import('./report/confirmed-order/confirmsummaryproduct'));
const confirmreportorder = React.lazy(() => import('./report/confirmed-order/confirmreportorder'));

const reportdocmain = React.lazy(() => import('./report/report-order/report-order-main'));
const reportordercutsys = React.lazy(() => import('./report/report-order/reportordercutsys'));
const reportorderoff = React.lazy(() => import('./report/report-order/reportorderoff'));
const reportorderproduct = React.lazy(() => import('./report/report-order/reportorderproduct'));
const reportordersales = React.lazy(() => import('./report/report-order/reportordersales'));
const reportordersys = React.lazy(() => import('./report/report-order/reportordersys'));

const reportstockdocmain = React.lazy(() => import('./report/report-stock/report-stock-main'));
const reportstocksap = React.lazy(() => import('./report/report-stock/reportstocksap'));

const sysdocmain = React.lazy(() => import('./report/sys-order/sys-order-main'));
const reportorderallsys = React.lazy(() => import('./report/sys-order/reportorderallsys'));
const reportordersatatussys = React.lazy(() => import('./report/sys-order/reportordersatatussys'));
const reportorderrece = React.lazy(() => import('./report/sys-order/reportorderrece'));
//#endregion

////////////////////////////////////////////////////  EXPORT DOC
//#region 
//setup-data
const setupdatoMain = React.lazy(() => import('./export-doc/setup-data-export/setup-data-main'));
const adminpage = React.lazy(() => import('./export-doc/setup-data-export/admin-page'));
const endcustomeremail = React.lazy(() => import('./export-doc/setup-data-export/end-customer-email'));
const orderemail = React.lazy(() => import('./export-doc/setup-data-export/order-email'));
const maillog = React.lazy(() => import('./export-doc/setup-data-export/mail-log'));
const opdata = React.lazy(() => import('./export-doc/setup-data-export/op-data'));
const customeremail = React.lazy(() => import('./export-doc/setup-data-export/customer-email'));
const odsSetup = React.lazy(() => import('./export-doc/setup-data-export/ods-setup'));
const odsreportauthorize = React.lazy(() => import('./export-doc/setup-data-export/ods-report-authorize'));
const scrapreturnmaster = React.lazy(() => import('./export-doc/setup-data-export/scrap-return-master'));
const inlandcost = React.lazy(() => import('./export-doc/setup-data-export/in-land-cost'));

//schedule-shipment
const scheduleMain = React.lazy(() => import('./export-doc/schedule-shipment/schedule-shipment-main'));
const scheduleshipmentpage = React.lazy(() => import('./export-doc/schedule-shipment/schedule-shipment-page'));
const vesselagent = React.lazy(() => import('./export-doc/schedule-shipment/vessel-agent'));
const shipmentperiod = React.lazy(() => import('./export-doc/schedule-shipment/shipment-period'));
const svessel = React.lazy(() => import('./export-doc/schedule-shipment/svessel'));
const vesseltype = React.lazy(() => import('./export-doc/schedule-shipment/vessel-type'));
const listrebatebyorder = React.lazy(() => import('./export-doc/schedule-shipment/list-rebate-byorder'));

//operation
const operationMain = React.lazy(() => import('./export-doc/operation/operation-main'));
const printinvexportorder = React.lazy(() => import('./export-doc/operation/print-inv-export-order'));
const moveordertoods = React.lazy(() => import('./export-doc/operation/moveorder-to-ods'));
const bebencertfrom = React.lazy(() => import('./export-doc/operation/be-ben-cert-from'));
const operationTaxation = React.lazy(() => import('./export-doc/operation/operation-taxation'));
const estshippment = React.lazy(() => import('./export-doc/operation/est-shippment'));
const orderdiscrepancy = React.lazy(() => import('./export-doc/operation/order-discrepancy'));
const printinvexportordshipment = React.lazy(() => import('./export-doc/operation/print-inv-export-ord-shipment'));
const reditdocument = React.lazy(() => import('./export-doc/operation/redit-document'));
const operationNumber = React.lazy(() => import('./export-doc/operation/operation-number'));
const updatepaymentstatus = React.lazy(() => import('./export-doc/operation/update-payment-status'));
const paymentreceipt = React.lazy(() => import('./export-doc/operation/payment-receipt'));
const shortshipment = React.lazy(() => import('./export-doc/operation/short-shipment'));

//ods
const osdMain = React.lazy(() => import('./export-doc/export-ods/ods-main'));
const importdhl = React.lazy(() => import('./export-doc/export-ods/import-dhl'));
const bankdhl = React.lazy(() => import('./export-doc/export-ods/bank-dhl'));
const dhlmatching = React.lazy(() => import('./export-doc/export-ods/dhl-matching'));
const otherdoctype = React.lazy(() => import('./export-doc/export-ods/other-doc-type'));

//reporting
const reportingMain = React.lazy(() => import('./export-doc/reporting/reporting-main'));
const odsreporting = React.lazy(() => import('./export-doc/reporting/ods-reporting'));
const accountreceivablereport = React.lazy(() => import('./export-doc/reporting/account-receivable-report'));
const summaryfob = React.lazy(() => import('./export-doc/reporting/summary-fob'));
//#endregion



const routes = [

   
{ path: '/export-doc/setup-data-export/setup-data-main', exact: true, name: 'export doc', component: setupdatoMain },
{ path: '/export-doc/setup-data-export/admin-page', exact: true, name: 'ADMIN PAGE', component: adminpage },
{ path: '/export-doc/setup-data-export/end-customer-email', exact: true, name: 'END CUSTOMER EMAIL', component: endcustomeremail },
{ path: '/export-doc/setup-data-export/order-email', exact: true, name: 'ods setup', component: orderemail },
{ path: '/export-doc/setup-data-export/mail-log', exact: true, name: 'MAIL LOG', component: maillog },
{ path: '/export-doc/setup-data-export/op-data', exact: true, name: 'ods setup', component: opdata },
{ path: '/export-doc/setup-data-export/customer-email', exact: true, name: 'CUSTOMER EMAIL', component: customeremail },
{ path: '/export-doc/setup-data-export/ods-setup', exact: true, name: 'ODS SETUP', component: odsSetup },
{ path: '/export-doc/setup-data-export/ods-report-authorize', exact: true, name: 'ods setup', component: odsreportauthorize },
{ path: '/export-doc/setup-data-export/scrap-return-master', exact: true, name: 'ods setup', component: scrapreturnmaster },
{ path: '/export-doc/setup-data-export/in-land-cost', exact: true, name: 'ods setup', component: inlandcost },


{ path: '/export-doc/schedule-shipment/schedule-shipment-main', exact: true, name: 'export doc', component: scheduleMain },
{ path: '/export-doc/schedule-shipment/schedule-shipment-page', exact: true, name: 'ods setup', component: scheduleshipmentpage },
{ path: '/export-doc/schedule-shipment/vessel-agent', exact: true, name: 'ods setup', component: vesselagent },
{ path: '/export-doc/schedule-shipment/shipment-period', exact: true, name: 'ods setup', component: shipmentperiod },
{ path: '/export-doc/schedule-shipment/svessel', exact: true, name: 'ods setup', component: svessel },
{ path: '/export-doc/schedule-shipment/vessel-type', exact: true, name: 'ods setup', component: vesseltype },
{ path: '/export-doc/schedule-shipment/list-rebate-byorder', exact: true, name: 'ods setup', component: listrebatebyorder },


{ path: '/export-doc/operation/operation-main', exact: true, name: 'export doc', component: operationMain },
{ path: '/export-doc/operation/print-inv-export-order', exact: true, name: 'ods setup', component: printinvexportorder },
{ path: '/export-doc/operation/moveorder-to-ods', exact: true, name: 'OPERATION', component: moveordertoods },
{ path: '/export-doc/operation/be-ben-cert-from', exact: true, name: 'ods setup', component:bebencertfrom },
{ path: '/export-doc/operation/operation-taxation', exact: true, name: 'ods setup', component:operationTaxation },
{ path: '/export-doc/operation/est-shippment', exact: true, name: 'ods setup', component:estshippment },
{ path: '/export-doc/operation/order-discrepancy', exact: true, name: 'ods setup', component:orderdiscrepancy },
{ path: '/export-doc/operation/print-inv-export-ord-shipment', exact: true, name: 'ods setup', component:printinvexportordshipment },
{ path: '/export-doc/operation/redit-document', exact: true, name: 'ods setup', component:reditdocument },
{ path: '/export-doc/operation/operation-number', exact: true, name: 'ods setup', component:operationNumber },
{ path: '/export-doc/operation/update-payment-status', exact: true, name: 'ods setup', component:updatepaymentstatus },
{ path: '/export-doc/operation/payment-receipt', exact: true, name: 'ods setup', component:paymentreceipt },
{ path: '/export-doc/operation/short-shipment', exact: true, name: 'ods setup', component:shortshipment },


{ path: '/export-doc/export-ods/ods-main', exact: true, name: 'export doc', component: osdMain },
{ path: '/export-doc/export-ods/import-dhl', exact: true, name: 'ods setup', component:importdhl },
{ path: '/export-doc/export-ods/bank-dhl', exact: true, name: 'ods setup', component:bankdhl },
{ path: '/export-doc/export-ods/dhl-matching', exact: true, name: 'ods setup', component:dhlmatching },
{ path: '/export-doc/export-ods/other-doc-type', exact: true, name: 'ods setup', component:otherdoctype },


{ path: '/export-doc/reporting/reporting-main', exact: true, name: 'export doc', component: reportingMain },
{ path: '/export-doc/reporting/ods-reporting', exact: true, name: 'ods setup', component:odsreporting },
{ path: '/export-doc/reporting/account-receivable-report', exact: true, name: 'ods setup', component:accountreceivablereport },
{ path: '/export-doc/reporting/summary-fob', exact: true, name: 'ods setup', component:summaryfob },

   //{ path: '/xxx', exact: true, name: 'ORDER CONFIRM', component: xxx },
//setup data
   //setup-data-transfer-sap
   { path: '/setup-data/setup-data-transfer-sap/setup-data-transfer-sap-main', exact: true, name: 'Transfer to SAP', component: setupTranSAP },   
   { path: '/setup-data/setup-data-transfer-sap/customer-to-car', exact: true, name: 'Transfer to SAP', component: customerToCar },
   { path: '/setup-data/setup-data-transfer-sap/transfer-to-sap', exact: true, name: 'Transfer to SAP', component: transfertosap },
   { path: '/setup-data/setup-data-transfer-sap/dn-transfer', exact: true, name: 'Transfer to SAP', component: dntransfer },
   { path: '/setup-data/setup-data-transfer-sap/interface-nd', exact: true, name: 'Transfer to SAP', component: interfacend },
   { path: '/setup-data/setup-data-transfer-sap/upload-pa', exact: true, name: 'Transfer to SAP', component: uploadpa },
   { path: '/setup-data/setup-data-transfer-sap/dept-for-special', exact: true, name: 'Transfer to SAP', component: deptforspecial },
   { path: '/setup-data/setup-data-transfer-sap/car-i-o', exact: true, name: 'Transfer to SAP', component: cario },
   
   //setup-data-adv-set
   { path: '/setup-data/setup-data-adv-set/setup-data-adv-set-main', exact: true, name: 'Advance Set', component: setupAdvanceSet },
   { path: '/setup-data/setup-data-adv-set/prepare-new-product', exact: true, name: 'Advance Set', component: prepareNewProduct },
   { path: '/setup-data/setup-data-adv-set/product-co', exact: true, name: 'Advance Set', component: productco },
   { path: '/setup-data/setup-data-adv-set/from-product-adv', exact: true, name: 'Advance Set', component: fromproductadv },
   { path: '/setup-data/setup-data-adv-set/advance-set', exact: true, name: 'Advance Set', component: advanceset },
   { path: '/setup-data/setup-data-adv-set/project-price-master', exact: true, name: 'Advance Set', component: projectpricemaster },
   { path: '/setup-data/setup-data-adv-set/trader-td', exact: true, name: 'Advance Set', component: tradertd },
   { path: '/setup-data/setup-data-adv-set/group-product-adv', exact: true, name: 'Advance Set', component: groupproductadv },
   { path: '/setup-data/setup-data-adv-set/cfr-quota-range', exact: true, name: 'Advance Set', component: cfrquotarange },
   { path: '/setup-data/setup-data-adv-set/improve-cfr-quota', exact: true, name: 'Advance Set', component: improvecfrquota },
   { path: '/setup-data/setup-data-adv-set/quotation-project', exact: true, name: 'Advance Set', component: quotationproject },

   //setup-data-upload-pdf
   { path: '/setup-data/setup-data-upload-pdf/upload-pdf-main', exact: true, name: 'Upload PDF', component: setupUploadPDF },
   { path: '/setup-data/setup-data-upload-pdf/upload-pdf-for-cg', exact: true, name: 'Upload PDF', component: uploadpdfforcg },
   { path: '/setup-data/setup-data-upload-pdf/import-emis-af', exact: true, name: 'Upload PDF', component: importemisaf },
   { path: '/setup-data/setup-data-upload-pdf/upload-pdf-for-sys', exact: true, name: 'Upload PDF', component: uploadpdfforsys },

   //setup-data-customer
   { path: '/setup-data/setup-data-customer/customer-master-main', exact: true, name: 'Customer Master', component: setupCustomerMaster },
   { path: '/setup-data/setup-data-customer/customer-master', exact: true, name: 'Customer Master', component: customermaster },
   { path: '/setup-data/setup-data-customer/shipment-port', exact: true, name: 'Customer Master', component: shipmentport },
   { path: '/setup-data/setup-data-customer/trader-master', exact: true, name: 'Customer Master', component: tradermaster },
   { path: '/setup-data/setup-data-customer/adn-frabrucator', exact: true, name: 'Customer Master', component: adnfrabrucator },

   //setup-data-general
   { path: '/setup-data/setup-data-general/general-master-main', exact: true, name: 'General Master', component: setupGeneralMaster },
   { path: '/setup-data/setup-data-general/term-condition-master', exact: true, name: 'General Master', component: termconditionmaster },
   { path: '/setup-data/setup-data-general/payment-term-master', exact: true, name: 'General Master', component: paymenttermmaster },
   { path: '/setup-data/setup-data-general/advance-type-setup', exact: true, name: 'General Master', component: advancetypesetup },
   { path: '/setup-data/setup-data-general/surcharge-type-setup', exact: true, name: 'General Master', component: surchargetypesetup },
   { path: '/setup-data/setup-data-general/project-type-master', exact: true, name: 'General Master', component: projecttypemaster },
   { path: '/setup-data/setup-data-general/export-advanced-set', exact: true, name: 'General Master', component: exportadvancedset },
   { path: '/setup-data/setup-data-general/bank-master', exact: true, name: 'General Master', component: bankmaster },
   { path: '/setup-data/setup-data-general/appl-parameter', exact: true, name: 'General Master', component: applparameter },
   { path: '/setup-data/setup-data-general/rebate-type-master', exact: true, name: 'General Master', component: rebatetypemaster },
   { path: '/setup-data/setup-data-general/general-master', exact: true, name: 'General Master', component: generalmaster },
   { path: '/setup-data/setup-data-general/country-for-export', exact: true, name: 'General Master', component: countryforexport },

   //setup-data-cfr
   { path: '/setup-data/setup-data-cfr/crf-master-main', exact: true, name: 'CFR Master', component: setupCFRMaster },
   { path: '/setup-data/setup-data-cfr/product-type', exact: true, name: 'CFR Master', component: producttype },
   { path: '/setup-data/setup-data-cfr/shipping-company', exact: true, name: 'CFR Master', component: shippingcompany },

   //setup-data-scor-metrics
   { path: '/setup-data/setup-data-scor-metrics/scor-metrics-main', exact: true, name: 'SCOR METRICS MASTER', component: setupSCORMETRICSMASTER },
   { path: '/setup-data/setup-data-scor-metrics/scor-matrix-control', exact: true, name: 'SCOR METRICS MASTER', component: scormatrixcontrol },
   { path: '/setup-data/setup-data-scor-metrics/scor-cd', exact: true, name: 'SCOR METRICS MASTER', component: scorcd },
   { path: '/setup-data/setup-data-scor-metrics/scor-claim-data', exact: true, name: 'SCOR METRICS MASTER', component: scorclaimdata },
   { path: '/setup-data/setup-data-scor-metrics/scor-if-reason', exact: true, name: 'SCOR METRICS MASTER', component: scorifreason },
   { path: '/setup-data/setup-data-scor-metrics/scor-unorder', exact: true, name: 'SCOR METRICS MASTER', component: scorunorder },


   
   { path: '/report/advance-order/advance-order-main', exact: true, name: 'ADVANCE ORDER', component: advancedocmain },
   { path: '/report/advance-order/summarycustomer', exact: true, name: 'REPORT', component: summarycustomer },
   { path: '/report/advance-order/summaryproduct', exact: true, name: 'REPORT', component: summaryproduct },
   { path: '/report/advance-order/summarybyproduct', exact: true, name: 'REPORT', component: summarybyproduct },
   { path: '/report/advance-order/summarybycustomer', exact: true, name: 'REPORT', component: summarybycustomer },

   { path: '/report/confirmed-order/confirmed-order-main', exact: true, name: 'CONFIRMED ORDER', component: confirmedmain },
   { path: '/report/confirmed-order/confirmbycustumer', exact: true, name: 'CONFIRMED ORDER', component: confirmbycustumer },
   { path: '/report/confirmed-order/confirmordersap', exact: true, name: 'CONFIRMED ORDER', component: confirmordersap },
   { path: '/report/confirmed-order/summary-order', exact: true, name: 'CONFIRMED ORDER', component: confirmsummarycustumer },
   { path: '/report/confirmed-order/confirmsummaryproduct', exact: true, name: 'CONFIRMED ORDER', component: confirmsummaryproduct },
   { path: '/report/confirmed-order/confirmreportorder', exact: true, name: 'CONFIRMED ORDER', component: confirmreportorder },

   { path: '/report/report-order/report-order-main', exact: true, name: 'REPORT', component: reportdocmain }, 
   { path: '/report/report-order/reportordercutsys', exact: true, name: 'REPORT', component: reportordercutsys },
   { path: '/report/report-order/reportorderoff', exact: true, name: 'REPORT', component: reportorderoff },
   { path: '/report/report-order/reportorderproduct', exact: true, name: 'REPORT', component: reportorderproduct },
   { path: '/report/report-order/reportordersales', exact: true, name: 'REPORT', component: reportordersales },
   { path: '/report/report-order/reportordersys', exact: true, name: 'REPORT', component: reportordersys },

   { path: '/report/report-stock/report-stock-main', exact: true, name: 'REPORT', component: reportstockdocmain }, 
   { path: '/report/report-stock/reportstocksap', exact: true, name: 'REPORT', component: reportstocksap }, 

   { path: '/report/sys-order/sys-order-main', exact: true, name: 'REPORT', component: sysdocmain },
   { path: '/report/sys-order/reportorderallsys', exact: true, name: 'REPORT', component: reportorderallsys },
   { path: '/report/sys-order/reportordersatatussys', exact: true, name: 'REPORT', component: reportordersatatussys },
   { path: '/report/sys-order/reportorderrece', exact: true, name: 'REPORT', component: reportorderrece },



   { path: '/domestic/domestic-order/domestic-order-main', exact: true, name: 'ORDER CONFIRM', component: domesticOrderMain },
   { path: '/domestic/domestic-order/approve-cut-off-ticket', exact: true, name: 'ORDER CONFIRM', component: approveCutOffMain },
   { path: '/domestic/domestic-order/arrange-a-car-specify-DP-number', exact: true, name: 'ORDER CONFIRM', component: arrangeacarspecifyDPMain },
   { path: '/domestic/domestic-order/arrange-transportation-ssj', exact: true, name: 'ORDER CONFIRM', component: arrangetransportationssjMain },
   { path: '/domestic/domestic-order/create-approval-request', exact: true, name: 'ORDER CONFIRM', component:createapprovalrequestMain },
   { path: '/domestic/domestic-order/create-car-arrangement', exact: true, name: 'ORDER CONFIRM', component: createcararrangementMain },
   { path: '/domestic/domestic-order/create-check-the-payment-summary-SYS', exact: true, name: 'ORDER CONFIRM', component:createcheckthepaymentsummarySYSMain },
   { path: '/domestic/domestic-order/create-purchase-order-SYS', exact: true, name: 'ORDER CONFIRM', component:createpurchaseorderSYSMain },
   { path: '/domestic/domestic-order/delivery-schedule', exact: true, name: 'ORDER CONFIRM', component:deliveryscheduleMain },
   { path: '/domestic/domestic-order/domestic-arrange-a-car', exact: true, name: 'ORDER CONFIRM', component:domesticarrangeacarMain },
   { path: '/domestic/domestic-order/domestic-arrange-transport', exact: true, name: 'ORDER CONFIRM', component:domesticarrangetransportMain },
   { path: '/domestic/domestic-order/est.shipment-do', exact: true, name: 'ORDER CONFIRM', component:estshipmentdoMain },
   { path: '/domestic/domestic-order/EST.shipment-do.old', exact: true, name: 'ORDER CONFIRM', component:ESTshipmentdooldMain },
   { path: '/domestic/domestic-order/purchase-order', exact: true, name: 'ORDER CONFIRM', component:purchaseorderMain },
   { path: '/domestic/domestic-order/sales-ORG-0180', exact: true, name: 'ORDER CONFIRM', component:salesORG0180Main },
   { path: '/domestic/domestic-order/summary-of-payment', exact: true, name: 'ORDER CONFIRM', component:summaryofpaymentMain },


   { path: '/domestic/domestic-order-confirm/domestic-order-confirm-main', exact: true, name: 'DOMESTIC', component: domesticMain },
   { path: '/domestic/domestic-order-confirm/check-domestic-order-confirm', exact: true, name: 'ORDER CONFIRM', component: checkdomesticorderconfirmMain },
   { path: '/domestic/domestic-order-confirm/customer-po-sap-text', exact: true, name: 'ORDER CONFIRM', component: CustomerPosaptext },
   { path: '/domestic/domestic-order-confirm/domestic-domestic-order-confirm', exact: true, name: 'ORDER CONFIRM', component:domesticdomesticorderconfirmMain },
   { path: '/domestic/domestic-order-confirm/domestic-invoice', exact: true, name: 'ORDER CONFIRM', component:domesticinvoiceMain },
   { path: '/domestic/domestic-order-confirm/domestic-order-confirm-show', exact: true, name: 'ORDER CONFIRM', component: domesticorderconfirmshowMain },
   { path: '/domestic/domestic-order-confirm/domestic-po-sap-text', exact: true, name: 'ORDER CONFIRM', component:domesticposaptextMain },
   { path: '/domestic/domestic-order-confirm/manage-order-fill-old', exact: true, name: 'ORDER CONFIRM', component:manageorderfilloldMain },

   
   
   { path: '/domestic/domestic-dlc/domestic-dlc-main', exact: true, name: 'ORDER CONFIRM', component: domesticDlcMain },
   { path: '/domestic/domestic-dlc/approve-sc-dlc', exact: true, name: 'ORDER CONFIRM', component: approvescdlcMain },
   { path: '/domestic/domestic-dlc/check-sc-dlc', exact: true, name: 'ORDER CONFIRM', component: checkscdlcMain },
   { path: '/domestic/domestic-dlc/sc-dlc-domestic', exact: true, name: 'ORDER CONFIRM', component:scdlcdomesticMain },


   { path: '/domestic/domestic-quotation', exact: true, name: 'ORDER CONFIRM', component: domesticquotationMain },
   { path: '/domestic/domestic-quotation/approve-domestic-quotation', exact: true, name: 'ORDER CONFIRM', component: approvedomesticquotationMain },
   { path: '/domestic/domestic-quotation/check1-domestic-quotation', exact: true, name: 'ORDER CONFIRM', component: check1domesticquotationMain },
   { path: '/domestic/domestic-quotation/check2-domestic-quotation', exact: true, name: 'ORDER CONFIRM', component:check2domesticquotationMain },
   { path: '/domestic/domestic-quotation/cs-approve-domestic-quotation', exact: true, name: 'ORDER CONFIRM', component:csapprovedomesticquotationMain },
   { path: '/domestic/domestic-quotation/cs-approve-export-quotation', exact: true, name: 'ORDER CONFIRM', component: csapproveexportquotationMain },
   { path: '/domestic/domestic-quotation/domestic-domest-quotation', exact: true, name: 'ORDER CONFIRM', component:domesticdomestquotationMain },
   { path: '/domestic/domestic-quotation/renewal-certificate-possibility', exact: true, name: 'ORDER CONFIRM', component:renewalcertificatepossibilityMain },






   // { path: '/sample-page', exact: true, name: 'Sample Page', component: OtherSamplePage },
   { path: '/sample-page', exact: true, name: 'Login', component: OtherSamplePage },
   { path: '/favorite', exact: true, name: 'Favorite', component: FavoritePage },

   //#region  PN-Master
   //PN-Master => Product
   { path: '/pn-master/product/product-main', exact: true, name: 'Product', component: ProductMain },
   { path: '/pn-master/product/ProductCodeOpenningMS126', exact: true, name: 'ใบเปิดรหัสสินค้า (MS126-1)', component: ProductCodeOpenningMS126 },
   { path: '/pn-master/product/ConfirmOpenProductSBC', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สบช)', component: ConfirmOpenProductSBC },
   { path: '/pn-master/product/ConfirmOpenProductSSK', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสค)', component: ConfirmOpenProductSSK },
   { path: '/pn-master/product/ConfirmOpenProductSSR2', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสร2)', component: ConfirmOpenProductSSR2 },
   { path: '/pn-master/product/ConfirmOpenProductSGT', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สกธ)', component: ConfirmOpenProductSGT },
   { path: '/pn-master/product/ConfirmOpenProductSSJ', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสจ)', component: ConfirmOpenProductSSJ },
   { path: '/pn-master/product/ConfirmOpenProductSSR1', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสร1)', component: ConfirmOpenProductSSR1 },
   { path: '/pn-master/product/ItemMastFGSTOCK', exact: true, name: 'ITEM MAST (FGSTOCK)', component: ItemMastFGSTOCK },

   //PN-Master => Product Master
   { path: '/pn-master/product-master/product-master-main', exact: true, name: 'Product Master', component: ProductMasterMain },
   { path: '/pn-master/product-master/SectionMaster', exact: true, name: 'Section Master', component: SectionMaster },
   { path: '/pn-master/product-master/GradeMaster', exact: true, name: 'Grade Master', component: GradeMaster },
   { path: '/pn-master/product-master/GroupVcCost', exact: true, name: 'Group VC Cost', component: GroupVcCost },
   { path: '/pn-master/product-master/GroupVcMaster', exact: true, name: 'Group VC Master', component: GroupVcMaster },
   { path: '/pn-master/product-master/PnStatus', exact: true, name: 'PN Status', component: PnStatus },
   { path: '/pn-master/product-master/PnStatusSize', exact: true, name: 'PN Status Size', component: PnStatusSize },
   { path: '/pn-master/product-master/SalesOrgChannelNewPn', exact: true, name: 'Sales ORG Channel New Pn', component: SalesOrgChannelNewPn },
   { path: '/pn-master/product-master/SectionHierarchyMaster', exact: true, name: 'Section Hierarchy Master', component: SectionHierarchyMaster },
   { path: '/pn-master/product-master/SizeMaster', exact: true, name: 'Size Master', component: SizeMaster },
   { path: '/pn-master/product-master/StandardGrade', exact: true, name: 'Standard Grade', component: StandardGrade },
   { path: '/pn-master/product-master/StandardMaster', exact: true, name: 'Standard Master', component: StandardMaster },
   { path: '/pn-master/product-master/StorageNewPn', exact: true, name: 'Storage New PN', component: StorageNewPn },

   //PN-Master => Other Master
   { path: '/pn-master/other-master/other-master-main', exact: true, name: 'Other Master', component: OtherMasterMain },
   { path: '/pn-master/other-master/PclLicence', exact: true, name: 'PCL Licence', component: PclLicence },
   { path: '/pn-master/other-master/NominalSizeHarmonize', exact: true, name: 'Nominal Size Harmonize', component: NominalSizeHarmonize },
   { path: '/pn-master/other-master/SectionHarmonize', exact: true, name: 'Section Harmonize', component: SectionHarmonize },
   { path: '/pn-master/other-master/PclLicenceDesc', exact: true, name: 'PCL Licence Description', component: PclLicenceDesc },

   //PN-Master => Production Plan
   { path: '/pn-master/production-plan/production-plan-main', exact: true, name: 'Production Plan', component: ProductionPlanMain },
   { path: '/pn-master/production-plan/ExportYearlyProductionPlan', exact: true, name: 'Export Yearly Production Plan', component: ExportYearlyProductionPlan },
   { path: '/pn-master/production-plan/DomesticYearlyProductionPlan', exact: true, name: 'Domestic Yearly Production Plan', component: DomesticYearlyProductionPlan },
   //#endregion

   //#region  Authorize
   //User & Authorize
   { path: '/authorizes/user-authorize/authorizes-main', exact: true, name: 'User & Authorize', component: UserAuthMain },
   { path: '/authorizes/user-authorize/customer-user-password', exact: true, name: "CUSTOMER'S USER & PASSWORD", component: CustomerPassword },
   { path: '/authorizes/user-authorize/sys-user-password', exact: true, name: 'SYS USER & PASSWORD', component: SysPassword },
   { path: '/authorizes/user-authorize/user-master', exact: true, name: 'USER MASTER', component: UserMaster },
   //e-mail
   { path: '/authorizes/e-mail/email-main', exact: true, name: 'E-MAIL', component: EmailMain },
   { path: '/authorizes/e-mail/auto-email', exact: true, name: 'AUTO E-MAIL', component: AutoEmail },
   { path: '/authorizes/e-mail/region-email', exact: true, name: 'REGION E-MAIL', component: RegionEmail },
   //set-screen
   { path: '/authorizes/set-screen/set-screen-main', exact: true, name: 'SET SCREEN', component: SetScreenMain },
   { path: '/authorizes/set-screen/eor-group-setup', exact: true, name: "EOR GROUP'S SETUP", component: EorGroupSetup },
   { path: '/authorizes/set-screen/eor-page', exact: true, name: 'EOR PAGE', component: EorPage },
   { path: '/authorizes/set-screen/menu-application', exact: true, name: 'MENU APPLICATION', component: MenuApplication },
   { path: '/authorizes/set-screen/menu-group', exact: true, name: 'MENU GROUP', component: MenuGroup },
   //#endregion

  //#region PROJECT
   //order-confirm
   { path: '/project/project-order-confirm/project-order-confirm-main', exact: true, name: 'Order Confirm', component: OrderMain },
   { path: '/project/project-order-confirm/project-order-confirm', exact: true, name: 'PROJECT ORDER CONFIRM', component: OrderConf },
   { path: '/project/project-order-confirm/check-project-order', exact: true, name: 'ตรวจสอบ PROJECT ORDER CONFIRM', component: chkOrder },
   { path: '/project/project-order-confirm/show-order-confirm', exact: true, name: 'แสดง PROJECT ORDER CONFIRM', component: showOrder },
   { path: '/project/project-order-confirm/project-invoice', exact: true, name: 'ใบแจ้งหนี้ (INVOICE)', component: invoiceOrder },
   //quotation   
   { path: '/project/quotation/project-quotation-main', exact: true, name: 'Quotation', component: QuotaMain },
   { path: '/project/quotation/approve-project-quotation', exact: true, name: 'APPROVE PROJECT QUOTATION', component: AppvQuo },
   { path: '/project/quotation/check-1-project-quotation', exact: true, name: 'CHECK 1 PROJECT QUOTATION', component: Chk1 },
   { path: '/project/quotation/check-2-project-quotation', exact: true, name: 'CHECK 2 PROJECT QUOTATION', component: Chk2 },
   { path: '/project/quotation/project-quotation-invoice', exact: true, name: 'ใบสรุปยอดใบแจ้งหนี้', component: invoiceQuo },
   { path: '/project/quotation/project-quotation', exact: true, name: 'PROJECT QUOTATION', component: ProjectQuta },
   //receiving
   { path: '/project/project-receiving/project-receiving-main', exact: true, name: 'Receiving', component: receivingMain },
   { path: '/project/project-receiving/receiving-car-process', exact: true, name: 'กระบวนการออกใบจัดรถ', component: CarPro },
   { path: '/project/project-receiving/receiving-car-setting', exact: true, name: 'ใบจัดรถขนส่ง (SYS)', component: CarSet },
   { path: '/project/project-receiving/est-shipment-project', exact: true, name: 'EST.SHIPMENT PRJ.', component: EstPro },
   { path: '/project/project-receiving/report-quotation-project', exact: true, name: 'รายงานสรุป QUOTATION PROJECT', component: RepPro },
   //#endregion

   /*
   //#region PROJECT
   //order-confirm
   { path: '/export/order-confirm/', exact: true, name: 'Order Confirm', component: ExpMain },
   { path: '/export/order-confirm/approve-order', exact: true, name: 'EXPORT ORDER CONFIRMED', component: ExpApvOrder },
   { path: '/export/order-confirm/check-order', exact: true, name: 'CHECK EXPORT ORDER CONFIRM', component: ExpChkOrder },
   { path: '/export/order-confirm/export-invoice', exact: true, name: 'EXPORT INVOICE', component: ExpInv },
   { path: '/export/order-confirm/export-order', exact: true, name: 'APPROVE EXPORT ORDER CONFIRM', component: ExpOrder },
*/

   //#region Export
   //order-confirm
   { path: '/export/order-confirm/order-confirm-main', exact: true, name: 'ORDER CONFIRM', component: ExpOrderConfirmMain },
   { path: '/export/order-confirm/approve-order', exact: true, name: 'APPROVE EXPORT ORDER CONFIRM', component: ExpApproveOrder },
   { path: '/export/order-confirm/export-order', exact: true, name: 'EXPORT ORDER CONFIRMED', component: ExpExportOrder },
   { path: '/export/order-confirm/check-order', exact: true, name: 'CHECK EXPORT ORDER CONFIRM', component: ExpCheckOrder },
   { path: '/export/order-confirm/export-invoice', exact: true, name: 'EXPORT INVOICE', component: ExpExportInvice },

   //quotation
   { path: '/export/quotation/quotation-main', exact: true, name: 'QUOTATION', component: ExpQuotationMain },
   { path: '/export/quotation/export-quotation', exact: true, name: 'EXPORT QUOTATION', component: ExpExportQuotation },
   { path: '/export/quotation/check-1-export-quotation', exact: true, name: 'CHECK 1 EXPORT QUOTATION', component: ExpChheck1ExportQuotation },
   { path: '/export/quotation/check-2-export-quotation', exact: true, name: 'CHECK 2 EXPORT QUOTATION', component: ExpChheck2ExportQuotation },
   { path: '/export/quotation/approve-export-quotation', exact: true, name: 'APPROVE EXPORT QUOTATION', component: ExpApproveExportQuotation },
   { path: '/export/quotation/inquiry-process', exact: true, name: 'INQUIRY IN PROCESS', component: ExpInquiryProcess },
   { path: '/export/quotation/upload-export', exact: true, name: 'UPLOAD EXPORT ORDER', component: ExpUploadExport },

   //export-process
   { path: '/export/export-process/export-process-main', exact: true, name: 'EXPORT PROCESS', component: ExpExportProcessMain },
   { path: '/export/export-process/check-low', exact: true, name: 'CHECK LOW PRICE', component: ExpCheckLow },
   { path: '/export/export-process/approve-low-1', exact: true, name: 'APPROVE LOW PRICE #1', component: ExpApproveLow1 },
   { path: '/export/export-process/approve-low-2', exact: true, name: 'APPROVE LOW PRICE #2', component: ExpApproveLow2 },

   //report
   { path: '/export/export-report/export-report-main', exact: true, name: 'REPORT', component: ExpReportMain },
   { path: '/export/export-report/export-report-order', exact: true, name: 'REPORT ORDER BY SHIPMENT', component: ExpReportOrder },
   //#endregion

   //Create new path to Cut Beam
   //CUt-BEAM => Cut Process
   { path: '/cut-beam/cut-process/cut-process-main', exact: true, name: 'Cut Process', component: CutProcessMain },
   { path: '/cut-beam/cut-process/SteelCutBill', exact: true, name: 'ใบแจ้งตัดเหล็ก (SYS)', component: SteelCutBill },
   { path: '/cut-beam/cut-process/SteelCuttingFinished', exact: true, name: 'ใบแจ้งตัดเหล็ก เพื่อ KEY เหล็กตัดเสร็จ', component: SteelCuttingFinished },
   { path: '/cut-beam/cut-process/SteelCuttingInvoice', exact: true, name: 'ใบแจ้งค่าตัดเหล็ก', component: SteelCuttingInvoice },
   { path: '/cut-beam/cut-process/WaitingToPrintSteelCutBill', exact: true, name: 'รอพิมพ์ใบแจ้งหนี้ค่าตัดเหล็ก', component: WaitingToPrintSteelCutBill },
   { path: '/cut-beam/cut-process/SteelReceiveCustomer', exact: true, name: 'ใบแจ้งตัดเหล็ก เพื่อ KEY รับเหล็กลูกค้า', component: SteelReceiveCustomer },
   { path: '/cut-beam/cut-process/DeliveryBill', exact: true, name: 'ใบส่งของ', component: DeliveryBill },
   { path: '/cut-beam/cut-process/ApproveCutBill', exact: true, name: 'อนุมัติใบแจ้งหนี้ค่าตัดเหล็ก', component: ApproveCutBill },

   //CUS-BEAM => Mwmo For CUT
   { path: '/cut-beam/memo-for-cut/memo-for-cut-main', exact: true, name: 'Memo for CUT', component: MemoForCut },
   { path: '/cut-beam/memo-for-cut/RequstToCutProductBill', exact: true, name: 'ใบขออนุมัติตัดสินค้า', component: RequstToCutProductBill },
   { path: '/cut-beam/memo-for-cut/ApproveToCutProduct', exact: true, name: 'อนุมัติ ใบขออนุมัติตัดสินค้า', component: ApproveToCutProduct },

   //CUS-BEAM => After Billing Process
   { path: '/cut-beam/after-billing-process/after-billing-process-main', exact: true, name: 'After Billing Process', component: AfterBillingProcessMain },
   { path: '/cut-beam/after-billing-process/DataBillDueDate', exact: true, name: 'ข้อมูล BILL DUE DATE', component: DataBillDueDate },
   { path: '/cut-beam/after-billing-process/WaybillData', exact: true, name: 'ข้อมูล ใบชำระ OCW', component: WaybillData },
   { path: '/cut-beam/after-billing-process/ImportDataBillDueDate', exact: true, name: 'นำเข้าข้อมูล BILL DUE DATE', component: ImportDataBillDueDate },
   { path: '/cut-beam/after-billing-process/ReceiptInformation', exact: true, name: 'ข้อมูล ใบนำส่ง', component: ReceiptInformation },

   //CUS-BEAM => Report
   { path: '/cut-beam/cut-beam-report/report-main', exact: true, name: 'Report', component: ReportMain },
   { path: '/cut-beam/cut-beam-report/ReportFromCustomer', exact: true, name: 'รายงานรับสินค้าจากลูกค้า', component: ReportFromCustomer },
   { path: '/cut-beam/cut-beam-report/ReportCuttingBill', exact: true, name: 'รายงานใบแจ้งหนี้ค่าตัด', component: ReportCuttingBill },
   { path: '/cut-beam/cut-beam-report/ReportReceivedFromCut', exact: true, name: 'รายงานรับสินค้าจากการตัด', component: ReportReceivedFromCut },
   { path: '/cut-beam/cut-beam-report/ReportCashForCutting', exact: true, name: 'รายงานรับเงินค่าตัดเหล็ก', component: ReportCashForCutting },
];

export default routes;