import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import Aux from "../../hoc/_Aux";
import MainCard from "../../App/components/MainCard";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            //{ "data": "UpdateDate", render: function (data, type, row) { return data; } }
            {
                sortable: false,
                className: "text-center",
                "data": "UpdateDate", "render": function (data, type, row) {
                    return '<button type="button" class="btnEdit btn btn-icon btn-outline-secondary btn-rounded btn-warring waves-effect waves-light"><span class="feather icon-upload text-info"></span></button>'
                }
            },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}
function supportedSelectHandler() {
    alert(1)
}
class webpage extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ isModal: true });
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else if (type === "Page") {
            this.setState({ isModalPage: true });
            this.setState({ setTitleModal: "MENU PAGE" });
        } else if (type === "Group") {
            this.setState({ isModalGroup: true });
            this.setState({ setTitleModal: "EOR.GROUP" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>
                <Row>
                    <Col>
                    <Modal backdrop="static" size="xl" show={this.state.isModalPage} onHide={() => this.setState({ isModalPage: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={5}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>APPLICATION</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">All</option><option value="22">-Application-22-</option><option value="06">AUTHORIZES</option><option value="05">CUT BEAM</option><option value="02">DOMESTIC</option><option value="04">EXPORT</option><option value="08">EXPORT DOC</option><option value="17">MARKETING</option><option value="14">OPERATION</option><option value="11">OPERATON</option><option value="07">PN MASTER</option><option value="19">PRODUCT GRP</option><option value="03">PROJECT</option><option value="21">REPAIR</option><option value="09">REPORT</option><option value="12">REPORT</option><option value="15">REPORT</option><option value="18">ROLLING MILL</option><option value="20">SAMPLING INSPECT</option><option value="10">SETUP</option><option value="16">SETUP</option><option value="01">SETUP DATA</option><option value="13">SETUP DATA</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={5}>PAGE NO</Form.Label>
                                            <Col sm={7}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>PAGE's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={5}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>APP+GROUP</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">All</option><option value="01_01">01-01 = SETUP DATA-Transfer to SAP</option><option value="01_02">01-02 = SETUP DATA-Advance Set</option><option value="01_04">01-04 = SETUP DATA-Product Master</option><option value="01_05">01-05 = SETUP DATA-Customer Master</option><option value="01_06">01-06 = SETUP DATA-Domestic Process</option><option value="01_07">01-07 = SETUP DATA-Export Process</option><option value="01_08">01-08 = SETUP DATA-General Master</option><option value="01_09">01-09 = SETUP DATA-วัสดุส่งเสริมการขาย</option><option value="01_10">01-10 = SETUP DATA-CFR Master</option><option value="01_11">01-11 = SETUP DATA-Upload PDF</option><option value="01_12">01-12 = SETUP DATA-SCOR METRICS MASTER</option><option value="02_01">02-01 = DOMESTIC-Quotation</option><option value="02_02">02-02 = DOMESTIC-DLC</option><option value="02_03">02-03 = DOMESTIC-Process</option><option value="02_04">02-04 = DOMESTIC-Report</option><option value="02_05">02-05 = DOMESTIC-การรับสินค้า</option><option value="02_06">02-06 = DOMESTIC-Order Confirm</option><option value="03_01">03-01 = PROJECT-Quotation</option><option value="03_02">03-02 = PROJECT-Order Confirm</option><option value="03_03">03-03 = PROJECT-การรับสินค้า</option><option value="03_04">03-04 = PROJECT-PROJECT TRACK</option><option value="03_05">03-05 = PROJECT-PROJECT TRACK - REPORT</option><option value="04_01">04-01 = EXPORT-Order Confirm</option><option value="04_02">04-02 = EXPORT-Export Process</option><option value="04_03">04-03 = EXPORT-Quotation</option><option value="04_04">04-04 = EXPORT-REPORT</option><option value="05_01">05-01 = CUT BEAM-CUT Process</option><option value="05_03">05-03 = CUT BEAM-Report</option><option value="05_04">05-04 = CUT BEAM-After Billing Process</option><option value="05_05">05-05 = CUT BEAM-CUT General Master</option><option value="05_06">05-06 = CUT BEAM-Memo for CUT</option><option value="06_03">06-03 = AUTHORIZES-User &amp; Authorize</option><option value="06_04">06-04 = AUTHORIZES-E-MAIL</option><option value="06_05">06-05 = AUTHORIZES-SET SCREEN</option><option value="07_01">07-01 = PN MASTER-PRODUCT</option><option value="07_02">07-02 = PN MASTER-CUSTOMER</option><option value="07_03">07-03 = PN MASTER-OTHER MASTER</option><option value="07_04">07-04 = PN MASTER-PRODUCT MASTER</option><option value="07_05">07-05 = PN MASTER-PRODUCTION PLAN</option><option value="08_01">08-01 = EXPORT DOC-SETUP DATA</option><option value="08_02">08-02 = EXPORT DOC-REPORTING</option><option value="08_03">08-03 = EXPORT DOC-OPERATION</option><option value="08_04">08-04 = EXPORT DOC-ODS</option><option value="08_05">08-05 = EXPORT DOC-SCHEDULE SHIPMENT</option><option value="09_01">09-01 = REPORT-ADVANCE ORDER</option><option value="09_02">09-02 = REPORT-CONFIRMED ORDER</option><option value="09_03">09-03 = REPORT-แจ้งยอดรับ</option><option value="09_04">09-04 = REPORT-รายงาน STOCK</option><option value="09_05">09-05 = REPORT-รายงานการรับสินค้า</option><option value="10_01">10-01 = SETUP-SETUP DATA</option><option value="10_02">10-02 = SETUP-PROCESS</option><option value="10_03">10-03 = SETUP--NEW GROUP NAME-</option><option value="11_03">11-03 = OPERATON-OPERATION</option><option value="12_04">12-04 = REPORT-REPORT</option><option value="13_01">13-01 = SETUP DATA-GENERAL</option><option value="13_02">13-02 = SETUP DATA-PROCESS</option><option value="14_01">14-01 = OPERATION-GENERAL</option><option value="14_02">14-02 = OPERATION-PROCESS</option><option value="15_01">15-01 = REPORT-REPORT</option><option value="16_01">16-01 = SETUP-GENERAL</option><option value="17_01">17-01 = MARKETING-GENERAL</option><option value="18_01">18-01 = ROLLING MILL-GENERAL</option><option value="19_02">19-02 = PRODUCT GRP-GENERAL</option><option value="20_01">20-01 = SAMPLING INSPECT-GENERAL</option><option value="21_01">21-01 = REPAIR-GENERAL</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={5}>GROUP NO</Form.Label>
                                            <Col sm={7}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>GROUP</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={8}>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>TYPE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">All</option><option value="D">Data Form</option><option value="R">Report</option><option value="B">Button</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="tb-group" /></th>
                                            <th>#</th>
                                            <th>APPLICATION</th>
                                            <th>GROUP</th>
                                            <th>PAGE NO</th>
                                            <th>PAGE'S NAME</th>
                                            <th>SEQ</th>
                                            <th>TYPE</th>
                                            <th>WND</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colSpan="9" className="text-center">
                                                <label > No Data.</label>
                                            </td>

                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalPage: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal id="modelGroup" backdrop="static" size="xl" show={this.state.isModalGroup} onHide={() => this.setState({ isModalGroup: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>GROUP NO.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>EOR.GROUP's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>


                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>APPL.NO.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>EOR.APPL's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>


                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelGrp" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="tb-group" /></th>
                                            <th>#</th>
                                            <th>CODE</th>
                                            <th>EOR.APPL'S NAME</th>
                                            <th>CODE</th>
                                            <th>EOR.GROUP'S NAME</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colSpan="6" className="text-center">
                                                <label > No Data.</label>
                                            </td>

                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalGroup: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>


                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Login's Name</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Password</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Sale.Code</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> User's Name</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Company</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">E-Mail Addr.</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ODS Sign Name</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ODS Sign Short	</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> Mobile No</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Tel.No</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Fax.No</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Expire Day(s)</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Warning Day(s)</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">PSI.PLANT</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="NONE">None</option>
                                                    <option value="SYS1">SYS1</option>
                                                    <option value="SYS2">SYS2</option>
                                                    <option value="SYSA">SYS1-2</option>
                                                    <option value="SR">SR</option>
                                                    <option value="MKT">MKT</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <hr />


                                <Col sm={12}>
                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" defaultActiveKey="group-auth" className=" form-control-file">

                                            <Tab eventKey="group-auth" title="GROUP AUTHORIZED">
                                                <Form.Group as={Row}>
                                                    <Col className="email-card">
                                                        <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                                    </Col>
                                                    <Col className="text-right" sm>
                                                        <Button size="sm" variant="success" className="mr-1" >ADD NEW</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Group")}>เพิ่มรายการ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-user" /></th>
                                                            <th>#</th>
                                                            <th>APPL.NO</th>
                                                            <th>APPLICATION</th>
                                                            <th>GROUP NO.</th>
                                                            <th>GROUP NAME</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="6" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="page-auth" title="PAGE AUTHORIZED" >
                                                <Form.Group as={Row}>
                                                    <Col className="email-card">
                                                        <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                                    </Col>
                                                    <Col className="text-right" sm>
                                                        <Button size="sm" variant="success" className="mr-1" >ADD NEW</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Page")}>เพิ่มรายการ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Table ref="tbl" striped hover responsive bordered id="user">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-user" /></th>
                                                            <th>#</th>
                                                            <th>PAGE.NO</th>
                                                            <th>PAGE NAME</th>
                                                            <th>MENU APPLICATION</th>
                                                            <th>MENU GROUP</th>
                                                            <th>TYPE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="7" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                        </Tabs>
                                    </Form.Group>
                                </Col>
                                <Row>
                                    <Col sm={1}>
                                        <Form.Label className="col-sm-4"></Form.Label>
                                    </Col>
                                    <Col sm={11}>
                                        <div className="ml-sm-4">
                                            <input type="checkbox" className="ml-sm-2 mr-1" />
                                            <Form.Label className="text-c-red"> CANCELED</Form.Label>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={1}>
                                        <Form.Label className="col-sm-4"></Form.Label>
                                    </Col>
                                    <Col sm={11}>
                                        <div className="ml-sm-4">
                                            <input type="checkbox" className="ml-sm-2 mr-1" />
                                            <Form.Label> User must change password at next logon</Form.Label>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={1}>
                                        <Form.Label className="col-sm-4"></Form.Label>
                                    </Col>
                                    <Col sm={11}>
                                        <div className="ml-sm-4">
                                            <input type="checkbox" className="ml-sm-2 mr-1" />
                                            <Form.Label> Password never expires</Form.Label>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">CREATED BY</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> CREATED DATE	</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" disabled value=" 19-Jan-15" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">UPDATED BY</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">UPDATED DATE</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" disabled value="19-Jan-15 07:26 " />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">SYSTEM.CODE</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" disabled value="161" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save</Button>
                                <Button variant="success">SAVE AS</Button>

                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>USER LOGIN</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>USER NAME</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>COMPANY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>MOBILE NO</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>E-MAIL</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PLANT for PSI</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="NONE">None</option><option value="SYS1">SYS1</option><option value="SYS2">SYS2</option><option value="SR">SR</option><option value="MKT">MKT</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SALE.CD.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>STATUS</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="SIZE MASTER">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    <Button size="sm" variant="success" >SET ACTIVE</Button>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>LOGIN NAME</th>
                                        <th>NAME (ENG)</th>
                                        <th>NAME (THAI)</th>
                                        <th>COMPANY</th>
                                        <th>MOBILE NO</th>
                                        <th>E-MAIL</th>
                                        <th>PLANT PSI</th>
                                        <th>วันที่สร้าง</th>
                                        <th>UPLOAD</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default webpage;
