import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Advance Set </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/prepare-new-product">
                                            PREPARE NEW PRODUCT (MS126D) AE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/product-co">
                                            เปิดเสนอราคา สั่งซื้อล่วงหน้า
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/from-product-adv">
                                            เอกสารประกอบการสั่งซื้อล่วงหน้า (ADV)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/advance-set">
                                            ADVANCE SET สินค้าชุดสุดคุ้ม
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/project-price-master">
                                            PROJECT PRICE MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/trader-td">
                                            เปิดเสนอราคา สั่งซื้อล่วงหน้า (TRADER)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/group-product-adv">
                                            กำหนดกลุ่มการขายสินค้า ADV
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/cfr-quota-range">
                                            CFR QUOTA.RANGE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/improve-cfr-quota">
                                            ปรับปรุง CFR QUOTA
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-adv-set/quotation-project">
                                            กำหนดกลุ่มสินค้า สำหรับ QUOTATION PROJECT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;