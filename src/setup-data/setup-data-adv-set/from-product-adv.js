import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl ,Tab ,Tabs} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";
import MainCard from "../../App/components/MainCard";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id","render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class webpage extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Year</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Month</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">No</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">From Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">To Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Advance type</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Order Type</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Validity From</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">To</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Produce Plan</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                </Row>
                                
                                <br /><br />
                                <hr />
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>ใบเสนอราคาสั่งซื้อล่วงหน้า</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Created</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Source</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Created Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Destination</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>D:\wwwroot\WebNEOrder\WebNEOrder\PDF\Quotation\</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Update</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Remark.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">New PDF File</Form.Label>
                                            <Col sm={8}>
                                                <input type="file" className="form-control-file my-xl-1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <br /><br />
                                <hr />
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option> โปรแกรมส่งเสริมการขาย</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Created</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Source</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Created Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Destination</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>D:\wwwroot\WebNEOrder\WebNEOrder\PDF\Quotation\</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Update</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Remark.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">New PDF File</Form.Label>
                                            <Col sm={8}>
                                                <input type="file" className="form-control-file my-xl-1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <br /><br />
                                <hr />
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>แผนการเปิดเสนอราคาสั่งซื้อล่วงหน้า</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Created</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Source</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Created Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Destination</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>D:\wwwroot\WebNEOrder\WebNEOrder\PDF\Quotation\</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Update</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Remark.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">New PDF File</Form.Label>
                                            <Col sm={8}>
                                                <input type="file" className="form-control-file my-xl-1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <br /><br />
                                <hr />
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>แผนการผลิต</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Created</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Source</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Created Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Destination</Form.Label>
                                            <Col sm={8}>
                                                <p className="form-control-file"><option>D:\wwwroot\WebNEOrder\WebNEOrder\PDF\Quotation\</option></p>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">User Update</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Remark.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">New PDF File</Form.Label>
                                            <Col sm={8}>
                                                <input type="file" className="form-control-file my-xl-1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                        <Form.Label className="col-sm-4">Update Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                            
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="success">Upload</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                        <Row>
                            <Col sm={12}>
                                <Form>
                                <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ADV.Year</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ADV.Month</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ADV.รอบที่</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ADV.Type</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="NONE">None</option><option value="0001">ADV SCG Dealer</option><option value="0006">ADV SCG Project</option><option value="0009">ADV Sheet Pile</option><option value="0007">ADV Small Section</option><option value="0002">ADV SYS Dealer</option><option value="0004">ADV SYS Project</option><option value="0012">DL Customer USE</option><option value="0011">DL Make Stock</option><option value="0016">-NAME-</option><option value="0010">Package สุดคุ้ม</option><option value="0008">Quick ADV</option><option value="0014">SCG สินค้าพิเศษ</option><option value="0005">SYS Export</option><option value="0003">SYS Project</option><option value="0013">SYS สินค้าพิเศษ</option><option value="0015">สินค้าพิเศษสต๊อก</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Order Type</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="A">Advance</option><option value="N">Project</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="NONE">None</option><option value="B">ส.ขบ.</option><option value="C">ส.ตท.</option><option value="A">ส.ผจ.</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Status</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>หมายเหตุ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        
                                    </Form.Group>
                                    <br />
                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control as="select">
                                                            <option value={0}>Create Date</option>
                                                            <option value={1}>Update Date</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 1 ||
                                                    this.state.selectDate === 2 ||
                                                    this.state.selectDate === 3 ||
                                                    this.state.selectDate === 4 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 5 ||
                                                    this.state.selectDate === 6 ||
                                                    this.state.selectDate === 7 ||
                                                    this.state.selectDate === 8 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>From</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 9 ||
                                                    this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 11 ||
                                                    this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>At Month</option>
                                                        <option value={6}>Between Month</option>
                                                        <option value={7}>More than</option>
                                                        <option value={8}>More than or equal</option>
                                                        <option value={9}>At Quater</option>
                                                        <option value={10}>Between Quater</option>
                                                        <option value={11}>At Year</option>
                                                        <option value={12}>Between Year</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>To</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>More than</option>
                                                            <option value={6}>More than or equal</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                        {
                                                this.state.selectValue === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > SEARCH </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="เอกสารประกอบการสั่งซื้อล่วงหน้า (ADV)">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info"/></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" >SET ACTIVE</Button>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    <Button size="sm" variant="success" onClick={e => this.setShowModal(e, "Create")}>Add size.</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className="ml-sm-4" id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>ORDER TYPE</th>
                                        <th>Yr-Mth-##</th>
                                        <th>FromDate</th>
                                        <th>ToDate</th>
                                        <th>ADV.TYPE</th>
                                        <th>วันที่แก้ไข</th>
                                        <th>ใบเสนอราคา</th>
                                        <th>โปรแกรมส่งเสริมการขาย</th>
                                        <th>แผนการเปิดเสนอราคาล่วงหน้า</th>
                                        <th>แผนการผลิต</th>
                                        <th>FUNC.</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default webpage;
