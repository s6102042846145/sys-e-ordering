import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl ,Tab ,Tabs} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";
import MainCard from "../../App/components/MainCard";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id","render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class webpage extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">MST.Year</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">MST.Month</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="B">ส.ขบ.</option><option value="C">ส.ตท.</option><option value="A">ส.ผจ.</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">From Date</Form.Label>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">To</Form.Label>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">MST.SET ID.</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Base Grade</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="SS400">SS400</option><option value="SM520">SM520</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Base M</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-file"><option value="06000">6.0</option><option value="09000">9.0</option><option value="12000">12.0</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> +ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" value="0.00" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">REF.NO.</Form.Label>
                                            <Col sm={8}>
                                            <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={8}>
                                                <Button size="sm" variant="primary" className="mr-sm-1 wid-100">PREVIEW</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> หมายเหตุ.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                
                                <br /><br />
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right col-sm-7" sm>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-100">SAVE</Button>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-150">SAVE AS</Button>
                                    </Col>
                                </Form.Group>

                                <Col sm={12}>
                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" className="form-control-file w-100">
                                            <Tab eventKey="prodsize" title="PROD.SIZE">
                                                <Row className="mt-2">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Col className="col-sm-8" sm>
                                                                <div className="form-check-inline">
                                                                    <Button size="sm" variant="danger" className="mr-sm-1 wid-100 h-25px">ลบรายการ</Button>
                                                                </div>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="mt--4px text-right">
                                                                <Button size="sm" variant="primary" className="mr-1">SEARCH</Button>
                                                                <Button size="sm" variant="success" className="mr-1">บันทึก</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>STD.DIM</th>
                                                            <th>SEC</th>
                                                            <th>NOMINAL SIZE</th>
                                                            <th>WEIGHTKG./M.</th>
                                                            <th>PRICE 12M</th>
                                                            <th>REMARK.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="12" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>

                                            <Tab eventKey="addmore" title="บวกเพิ่ม">
                                                <Row className="mt-2">
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>PRJ.TYPE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="NONE">None</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>PRJ.TYPE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">ALL</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>TYPE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">ALL</option><option value="STRUCT">โครงสร้างราคา</option><option value="GRRNG">เกรดพเศษ</option><option value="LNRNG">ความยาวพเศษ</option><option value="SBRNG">SUB-SERIES</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row className="mt-2">
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>GRADE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">ALL</option><option value="ABS_D">ABS - D</option><option value="ABS_DH32">ABS - DH32</option><option value="ABS_DH36">ABS - DH36</option><option value="ABS_DH40">ABS - DH40</option><option value="AS/NZS_AS/NZS 3679.1-250">AS/NZS - AS/NZS 3679.1-250</option><option value="AS/NZS_AS/NZS 3679.1-300">AS/NZS - AS/NZS 3679.1-300</option><option value="AS/NZS_AS/NZS 3679.1-300L0">AS/NZS - AS/NZS 3679.1-300L0</option><option value="AS/NZS_AS/NZS 3679.1-300S0">AS/NZS - AS/NZS 3679.1-300S0</option><option value="AS/NZS_AS/NZS 3679.1-350">AS/NZS - AS/NZS 3679.1-350</option><option value="AS/NZS_AS/NZS 3679.1-350S0">AS/NZS - AS/NZS 3679.1-350S0</option><option value="ASTM_A36">ASTM - A36</option><option value="ASTM_A36/SS400">ASTM - A36/SS400</option><option value="ASTM2_A992">ASTM2 - A992</option><option value="ASTM3_A572-Gr.50">ASTM3 - A572-Gr.50</option><option value="ASTM3_A572-GR.60">ASTM3 - A572-GR.60</option><option value="ASTM6_A992/A572G50">ASTM6 - A992/A572G50</option><option value="BS/EN_43A">BS/EN - 43A</option><option value="BS/EN_43B">BS/EN - 43B</option><option value="BS/EN_43C">BS/EN - 43C</option><option value="BS/EN_50B">BS/EN - 50B</option><option value="BS/EN_50C">BS/EN - 50C</option><option value="BS/EN_55C">BS/EN - 55C</option><option value="BS/EN2_355D">BS/EN2 - 355D</option><option value="BS/EN2_S235J0">BS/EN2 - S235J0</option><option value="BS/EN2_S235JR">BS/EN2 - S235JR</option><option value="BS/EN2_S275J0">BS/EN2 - S275J0</option><option value="BS/EN2_S275J2">BS/EN2 - S275J2</option><option value="BS/EN2_S275J2G3">BS/EN2 - S275J2G3</option><option value="BS/EN2_S275JR">BS/EN2 - S275JR</option><option value="BS/EN2_S355J0">BS/EN2 - S355J0</option><option value="BS/EN2_S355J2">BS/EN2 - S355J2</option><option value="BS/EN2_S355J2G3">BS/EN2 - S355J2G3</option><option value="BS/EN2_S355JR">BS/EN2 - S355JR</option><option value="BS/EN2_S450J0">BS/EN2 - S450J0</option><option value="BS/EN3_43A/S275JR">BS/EN3 - 43A/S275JR</option><option value="BS/EN3_50B/S355JR">BS/EN3 - 50B/S355JR</option><option value="BS/EN4_S240GP">BS/EN4 - S240GP</option><option value="BS/EN4_S270GP">BS/EN4 - S270GP</option><option value="BS/EN4_S320GP">BS/EN4 - S320GP</option><option value="BS/EN4_S355GP">BS/EN4 - S355GP</option><option value="BS/EN4_S390GP">BS/EN4 - S390GP</option><option value="BS/EN4_S430GP">BS/EN4 - S430GP</option><option value="CNS_SN400YB/SN400B">CNS - SN400YB/SN400B</option><option value="DIN_ST44-2">DIN - ST44-2</option><option value="DIN_ST50-2">DIN - ST50-2</option><option value="DIN_ST52-3">DIN - ST52-3</option><option value="GB706_8.8">GB706 - 8.8</option><option value="GBT714_3SP">GBT714 - 3SP</option><option value="GBT714_Q235qD">GBT714 - Q235qD</option><option value="GOST_3SP">GOST - 3SP</option><option value="GOST_5SP">GOST - 5SP</option><option value="JIS_SS400">JIS - SS400</option><option value="JIS_SS400/SM400">JIS - SS400/SM400</option><option value="JIS_SS490">JIS - SS490</option><option value="JIS_SS540">JIS - SS540</option><option value="JIS1_SM400B">JIS1 - SM400B</option><option value="JIS1_SM490A">JIS1 - SM490A</option><option value="JIS1_SM490B">JIS1 - SM490B</option><option value="JIS1_SM490YA">JIS1 - SM490YA</option><option value="JIS1_SM490YB">JIS1 - SM490YB</option><option value="JIS1_SM520B">JIS1 - SM520B</option><option value="JIS1_SM520C">JIS1 - SM520C</option><option value="JISA5528_SY295">JISA5528 - SY295</option><option value="JISA5528_SY390">JISA5528 - SY390</option><option value="JISA5528-2012_SY295:2012">JISA5528-2012 - SY295:2012</option><option value="JISA5528-2012_SY390:2012">JISA5528-2012 - SY390:2012</option><option value="MS1490_SW275A">MS1490 - SW275A</option><option value="MSEN_S235JR">MSEN - S235JR</option><option value="MSEN_S275J0">MSEN - S275J0</option><option value="MSEN_S275J2">MSEN - S275J2</option><option value="MSEN_S275J2G3">MSEN - S275J2G3</option><option value="MSEN_S275JR">MSEN - S275JR</option><option value="MSEN_S355J0">MSEN - S355J0</option><option value="MSEN_S355J2">MSEN - S355J2</option><option value="MSEN_S355J2G3">MSEN - S355J2G3</option><option value="MSEN_S355JR">MSEN - S355JR</option><option value="MSEN_S450J0">MSEN - S450J0</option><option value="SNI-C_BJ P 41">SNI-C - BJ P 41</option><option value="SNI-C_BJ P 50">SNI-C - BJ P 50</option><option value="SNI-C_BJ P 55">SNI-C - BJ P 55</option><option value="SNI-H_BJ PHC 400">SNI-H - BJ PHC 400</option><option value="SNI-H_BJ PHC 490">SNI-H - BJ PHC 490</option><option value="SNI-H_BJ PHC 540">SNI-H - BJ PHC 540</option><option value="SNI-I_BJ P 41">SNI-I - BJ P 41</option><option value="SNI-I_BJ P 50">SNI-I - BJ P 50</option><option value="SNI-I_BJ P 55">SNI-I - BJ P 55</option><option value="SNI-L_BJ P 41">SNI-L - BJ P 41</option><option value="SNI-L_BJ P 50">SNI-L - BJ P 50</option><option value="SNI-L_BJ P 55">SNI-L - BJ P 55</option><option value="SNI-WF_BJ P 41">SNI-WF - BJ P 41</option><option value="SNI-WF_BJ P 50">SNI-WF - BJ P 50</option><option value="SNI-WF_BJ P 55">SNI-WF - BJ P 55</option><option value="TIS_3SP">TIS - 3SP</option><option value="TIS_3SP">TIS - 3SP</option><option value="TIS_43A">TIS - 43A</option><option value="TIS_43A">TIS - 43A</option><option value="TIS_43A">TIS - 43A</option><option value="TIS_43A">TIS - 43A</option><option value="TIS_SM400">TIS - SM400</option><option value="TIS_SM490">TIS - SM490</option><option value="TIS_SM520">TIS - SM520</option><option value="TIS_SM570">TIS - SM570</option><option value="TIS_SS400/SM400">TIS - SS400/SM400</option><option value="TIS_SS490">TIS - SS490</option><option value="TIS_SS540">TIS - SS540</option><option value="TIS1390_SY295">TIS1390 - SY295</option><option value="TIS1390_SY390">TIS1390 - SY390</option><option value="TIS2015_SM400">TIS2015 - SM400</option><option value="TIS2015_SM490">TIS2015 - SM490</option><option value="TIS2015_SM520">TIS2015 - SM520</option><option value="TIS2015_SM570">TIS2015 - SM570</option><option value="TIS2015_SS400">TIS2015 - SS400</option><option value="TIS2015_SS490">TIS2015 - SS490</option><option value="TIS2015_SS540">TIS2015 - SS540</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>

                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <br />
                                
                                                <Row className="mt-2">
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="mt--4px text-right">
                                                                <a>Range Ton 	:<input type="text" className="" value="0.000"/></a>
                                                                <Button size="sm" variant="primary" className="mr-1">เพิ่ม ขั้นบวกเพิ่มโครงสร้างราคา</Button>
                                                                <Button size="sm" variant="primary" className="mr-1">เพิ่ม ขั้นบวกเพิ่ม GRADE พิเศษ</Button>
                                                                <Button size="sm" variant="primary" className="mr-1">เพิ่ม ขั้นบวกเพิ่มความยาวพิเศษ</Button>
                                                                <Button size="sm" variant="primary" >ค้นหา</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row className="mt-2">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Col className="col-sm-8" sm>
                                                                <div className="form-check-inline">
                                                                    <Button size="sm" variant="danger" className="mr-sm-1 wid-100 h-25px">ลบรายการ</Button>
                                                                </div>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>

                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="mt--4px text-right">
                                                                <Button size="sm" variant="success" className="mr-1">เพิ่ม ขั้นบวกเพิ่มSUB SERIES</Button>
                                                                <Button size="sm" variant="success" className="mr-1">บันทึก</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>

                                                <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>PRJ.TYPE</th>
                                                            <th>TYPE</th>
                                                            <th>RANGE TON</th>
                                                            <th>บวกเพิ่มTIS</th>
                                                            <th>บวกเพิ่มNON-TIS</th>
                                                            <th>GRADE</th>
                                                            <th>REMARK.-</th>
                                                            <th>UPDATE DATE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="12" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>


                                        </Tabs>

                                        <Row className="mt-2 w-100">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CREATED BY</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-file" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CREATED DATE</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="Date" className="form-control-file" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CANCEL</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="checkbox" className="my-sm-2" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row className="mt-2 w-100">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>UPDATED BY</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-file" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>UPDATED DATE</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="Date" className="form-control-file" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-file" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                    </Form.Group>  
                                </Col>

                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                        <Row>
                            <Col sm={12}>
                                <Form>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>MST.Year</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>MST.Month</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>MST.Type</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="NONE">None</option><option value="0001">ADV SCG Dealer</option><option value="0006">ADV SCG Project</option><option value="0009">ADV Sheet Pile</option><option value="0007">ADV Small Section</option><option value="0002">ADV SYS Dealer</option><option value="0004">ADV SYS Project</option><option value="0012">DL Customer USE</option><option value="0011">DL Make Stock</option><option value="0016">-NAME-</option><option value="0010">Package สุดคุ้ม</option><option value="0008">Quick ADV</option><option value="0014">SCG สินค้าพิเศษ</option><option value="0005">SYS Export</option><option value="0003">SYS Project</option><option value="0013">SYS สินค้าพิเศษ</option><option value="0015">สินค้าพิเศษสต๊อก</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Status</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>หมายเหตุ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <br />
                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control as="select">
                                                            <option value={0}>Create Date</option>
                                                            <option value={1}>Update Date</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 1 ||
                                                    this.state.selectDate === 2 ||
                                                    this.state.selectDate === 3 ||
                                                    this.state.selectDate === 4 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 5 ||
                                                    this.state.selectDate === 6 ||
                                                    this.state.selectDate === 7 ||
                                                    this.state.selectDate === 8 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>From</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 9 ||
                                                    this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 11 ||
                                                    this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>At Month</option>
                                                        <option value={6}>Between Month</option>
                                                        <option value={7}>More than</option>
                                                        <option value={8}>More than or equal</option>
                                                        <option value={9}>At Quater</option>
                                                        <option value={10}>Between Quater</option>
                                                        <option value={11}>At Year</option>
                                                        <option value={12}>Between Year</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>To</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>More than</option>
                                                            <option value={6}>More than or equal</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                        {
                                                this.state.selectValue === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > SEARCH </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="PROJECT PRICE MASTER">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info"/></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" >SET ACTIVE</Button>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    <Button size="sm" variant="success" onClick={e => this.setShowModal(e, "Create")}>Add new adv.set.</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>YR-MTH-##</th>
                                        <th>FROM DATE</th>
                                        <th>TO DATE</th>
                                        <th>หมายเหตุ.-</th>
                                        <th>วันที่แก้ไข</th>
                                        <th>สร้าง AOW</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default webpage;
