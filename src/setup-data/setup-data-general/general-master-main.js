import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> General Master </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/term-condition-master">
                                            TERM & CONDITION MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/payment-term-master" >
                                            PAYMENT TERM MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/advance-type-setup" >
                                            ADVANCE TYPE SETUP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/surcharge-type-setup" >
                                            SURCHARGE TYPE SETUP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/project-type-master" >
                                            PROJECT TYPE MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/export-advanced-set" >
                                            EXPORT ADVANCED SET
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/bank-master" >
                                            BANK MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/appl-parameter" >
                                            APPL.PARAMETER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/rebate-type-master" >
                                            REBATE TYPE MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/general-master" >
                                            อำนาจดำเนินการ
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-general/country-for-export" >
                                            COUNTRY FOR EXPORT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;