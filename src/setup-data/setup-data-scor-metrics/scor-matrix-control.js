import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";
import MainCard from "../../App/components/MainCard";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id","render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class webpage extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Label className="input-group-prepend text-behance">for CODE</Form.Label>
                                <hr />
                                
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                        <Row>
                            <Col sm={12}>
                                <Form>
                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SIZE STD.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="ASTM2003">ASTM A6/A6M:2003</option><option value="ABS">ASTM A6/A6M:2003</option><option value="BS">BS EN 10034:1993</option><option value="BS3">BS EN 10279:2000</option><option value="BS1">BSEN 10056-1:1999</option><option value="EURO">EN 53-1962,EN 19-1957</option><option value="GB706">GB/T 706-2008</option><option value="GOST">GOST 380-94</option><option value="JISA5528-2012">JIS A5528 : 2012</option><option value="JISA5528">JIS A5528-2006</option><option value="JIS1990">JIS G3192-1990</option><option value="JIS1994">JIS G3192-1994</option><option value="SNI-C">SNI-C : SNI 07-0052-2006</option><option value="SNI-H">SNI-H : SNI 2610-2011</option><option value="SNI-I">SNI-I : SNI 07-0329-2005</option><option value="SNI-L">SNI-L : SNI 07-2054-2006</option><option value="SNI-WF">SNI-WF : SNI 07-7178-2006</option><option value="TIS">TIS 1227:1996</option><option value="TIS/JIS">TIS 1227:1996/JIS G3192:1990</option><option value="TIS2015">TIS 1227:2558 (2015)</option><option value="TIS1390-2539">TIS1390-2539 (1996)</option><option value="TIS1390">TIS1390-2560 (2017)</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SECTION</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="B">B-BLOOM</option><option value="C">C-CHANNEL</option><option value="H">H-H-BEAM</option><option value="I">I-I-BEAM</option><option value="L">L-ANGLE</option><option value="M">M-MODULAR</option><option value="P">P-H-PILE</option><option value="R">R-RAIL</option><option value="S">S-SHEET-PILE</option><option value="T">T-CUT-BEAM</option><option value="V">V-HVA</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Size ID</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>NOMINAL SIZE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="100 UC">100 UC</option><option value="100X10">100X10</option><option value="100X100">100X100</option><option value="100X12">100X12</option><option value="100X13">100X13</option><option value="100X150">100X150</option><option value="100X200">100X200</option><option value="100X50">100X50</option><option value="100X7">100X7</option><option value="102X102">102X102</option><option value="120X10">120X10</option><option value="120X8">120X8</option><option value="122X175">122X175</option><option value="125CT">125CT</option><option value="125X125">125X125</option><option value="125X175">125X175</option><option value="125X250">125X250</option><option value="125X65">125X65</option><option value="127X127">127X127</option><option value="130X10">130X10</option><option value="130X12">130X12</option><option value="130X13">130X13</option><option value="130X14">130X14</option><option value="130X15">130X15</option><option value="130X16">130X16</option><option value="130X8">130X8</option><option value="130X9">130X9</option><option value="147X200">147X200</option><option value="148X100">148X100</option><option value="149X201">149X201</option><option value="150 PFC">150 PFC</option><option value="150 UB">150 UB</option><option value="150 UC">150 UC</option><option value="150X10">150X10</option><option value="150X100">150X100</option><option value="150X12">150X12</option><option value="150X13">150X13</option><option value="150X14">150X14</option><option value="150X15">150X15</option><option value="150X150">150X150</option><option value="150X16">150X16</option><option value="150X18">150X18</option><option value="150X19">150X19</option><option value="150X200">150X200</option><option value="150X300">150X300</option><option value="150X75">150X75</option><option value="152X102">152X102</option><option value="152X152">152X152</option><option value="152X25.4">152X25.4</option><option value="155CT">155CT</option><option value="168X249">168X249</option><option value="170X250">170X250</option><option value="175X12">175X12</option><option value="175X15">175X15</option><option value="175X175">175X175</option><option value="175X250">175X250</option><option value="175X350">175X350</option><option value="178X171">178X171</option><option value="180 PFC">180 PFC</option><option value="180 UB">180 UB</option><option value="180X75">180X75</option><option value="194X150">194X150</option><option value="195X300">195X300</option><option value="198X199">198X199</option><option value="200 PFC">200 PFC</option><option value="200 UB">200 UB</option><option value="200 UC">200 UC</option><option value="200X100">200X100</option><option value="200X13">200X13</option><option value="200X15">200X15</option><option value="200X150">200X150</option><option value="200X16">200X16</option><option value="200X18">200X18</option><option value="200X20">200X20</option><option value="200X200">200X200</option><option value="200X204">200X204</option><option value="200X22">200X22</option><option value="200X23">200X23</option><option value="200X25">200X25</option><option value="200X300">200X300</option><option value="200X350">200X350</option><option value="200X80">200X80</option><option value="200X90">200X90</option><option value="203X102">203X102</option><option value="203X133">203X133</option><option value="203X19">203X19</option><option value="203X203">203X203</option><option value="203X25.4">203X25.4</option><option value="203X28">203X28</option><option value="203X28.6">203X28.6</option><option value="205CT">205CT</option><option value="220X300">220X300</option><option value="225X200">225X200</option><option value="225X300">225X300</option><option value="228.5X191">228.5X191</option><option value="230 PFC">230 PFC</option><option value="230CT">230CT</option><option value="244X175">244X175</option><option value="244X252">244X252</option><option value="244X300">244X300</option><option value="250 PFC">250 PFC</option><option value="250 UB">250 UB</option><option value="250 UC">250 UC</option><option value="250X125">250X125</option><option value="250X125H">250X125H</option><option value="250X125L">250X125L</option><option value="250X175">250X175</option><option value="250X200">250X200</option><option value="250X22">250X22</option><option value="250X23">250X23</option><option value="250X24">250X24</option><option value="250X25">250X25</option><option value="250X250">250X250</option><option value="250X26">250X26</option><option value="250X27">250X27</option><option value="250X28">250X28</option><option value="250X35">250X35</option><option value="250X90">250X90</option><option value="250X90H">250X90H</option><option value="250X90L">250X90L</option><option value="254X102">254X102</option><option value="254X120">254X120</option><option value="254X146">254X146</option><option value="254X254">254X254</option><option value="265CT">265CT</option><option value="267X210">267X210</option><option value="294X200">294X200</option><option value="294X300">294X300</option><option value="294X302">294X302</option><option value="298X149">298X149</option><option value="298X199">298X199</option><option value="300 PFC">300 PFC</option><option value="300X150">300X150</option><option value="300X150H">300X150H</option><option value="300X150L">300X150L</option><option value="300X150M">300X150M</option><option value="300X200">300X200</option><option value="300X300">300X300</option><option value="300X305">300X305</option><option value="300X90H">300X90H</option><option value="300X90L">300X90L</option><option value="300X90M">300X90M</option><option value="304X301">304X301</option><option value="305CT">305CT</option><option value="305X102">305X102</option><option value="305X165">305X165</option><option value="305X203">305X203</option><option value="305X305">305X305</option><option value="310 UB">310 UB</option><option value="310 UC">310 UC</option><option value="338X351">338X351</option><option value="33X11 1/2">33X11 1/2</option><option value="340X250">340X250</option><option value="344X354">344X354</option><option value="346X174">346X174</option><option value="346X300">346X300</option><option value="350X150H">350X150H</option><option value="350X150L">350X150L</option><option value="350X175">350X175</option><option value="350X250">350X250</option><option value="350X300">350X300</option><option value="350X350">350X350</option><option value="350X357">350X357</option><option value="356X127">356X127</option><option value="356X171">356X171</option><option value="356X254">356X254</option><option value="356X368">356X368</option><option value="356X406">356X406</option><option value="358X172">358X172</option><option value="360 UB">360 UB</option><option value="375X394">375X394</option><option value="380X100">380X100</option><option value="380X100H">380X100H</option><option value="380X100L">380X100L</option><option value="380X395">380X395</option><option value="390X300">390X300</option><option value="394X405">394X405</option><option value="400X125">400X125</option><option value="400X150">400X150</option><option value="400X150H">400X150H</option><option value="400X150L">400X150L</option><option value="400X170">400X170</option><option value="400X200">400X200</option><option value="400X300">400X300</option><option value="400X400">400X400</option><option value="400X408">400X408</option><option value="406X140">406X140</option><option value="406X178">406X178</option><option value="410 UB">410 UB</option><option value="414X405">414X405</option><option value="440X300">440X300</option><option value="450X175">450X175</option><option value="450X175H">450X175H</option><option value="450X175L">450X175L</option><option value="450X200">450X200</option><option value="450X300">450X300</option><option value="450X550">450X550</option><option value="457X152">457X152</option><option value="457X190">457X190</option><option value="457X191">457X191</option><option value="460 UB">460 UB</option><option value="488X300">488X300</option><option value="496X199">496X199</option><option value="500X200">500X200</option><option value="500X300">500X300</option><option value="506X201">506X201</option><option value="50X100">50X100</option><option value="530 UB">530 UB</option><option value="533X165">533X165</option><option value="533X210">533X210</option><option value="588X300">588X300</option><option value="594X302">594X302</option><option value="600X190H">600X190H</option><option value="600X190L">600X190L</option><option value="600X200">600X200</option><option value="600X300">600X300</option><option value="610 UB">610 UB</option><option value="610X178">610X178</option><option value="610X229">610X229</option><option value="610X305">610X305</option><option value="610X324">610X324</option><option value="62.5X125">62.5X125</option><option value="686X254">686X254</option><option value="700X300">700X300</option><option value="75X100">75X100</option><option value="75X150">75X150</option><option value="75X75">75X75</option><option value="762X267">762X267</option><option value="800X300">800X300</option><option value="808X302">808X302</option><option value="838X292">838X292</option><option value="87.5X175">87.5X175</option><option value="8X8">8X8</option><option value="900X300">900X300</option><option value="97X150">97X150</option><option value="A1">A1</option><option value="A2">A2</option><option value="A3">A3</option><option value="A4">A4</option><option value="B&amp;N">B&amp;N</option><option value="B&amp;NM10">B&amp;NM10</option><option value="B0">B0</option><option value="B1">B1</option><option value="B11">B11</option><option value="B2">B2</option><option value="B21">B21</option><option value="B3">B3</option><option value="B31">B31</option><option value="B3M">B3M</option><option value="B4">B4</option><option value="B41">B41</option><option value="C1">C1</option><option value="C11">C11</option><option value="C1R">C1R</option><option value="C1S">C1S</option>
                                                        <option value="C2">C2</option><option value="C21">C21</option><option value="C2R">C2R</option><option value="C2S">C2S</option><option value="HE 100 A">HE 100 A</option><option value="HE 100 B">HE 100 B</option><option value="HE 160 A">HE 160 A</option><option value="HE 160 B">HE 160 B</option><option value="HE 180 A">HE 180 A</option><option value="HE 180 B">HE 180 B</option><option value="HE 200 A">HE 200 A</option><option value="HE 200 B">HE 200 B</option><option value="HE 220 A">HE 220 A</option><option value="HE 220 B">HE 220 B</option><option value="HE 260 A">HE 260 A</option><option value="HE 260 B">HE 260 B</option><option value="HE 300 A">HE 300 A</option><option value="HE 300 B">HE 300 B</option><option value="HE 320 A">HE 320 A</option><option value="HE 320 B">HE 320 B</option><option value="HE 360 A">HE 360 A</option><option value="HE 360 B">HE 360 B</option><option value="HE 400 A">HE 400 A</option><option value="HE 400 B">HE 400 B</option><option value="HE 450 A">HE 450 A</option><option value="HE 450 B">HE 450 B</option><option value="HE 500 A">HE 500 A</option><option value="HE 500 B">HE 500 B</option><option value="HE 600 A">HE 600 A</option><option value="HE 600 B">HE 600 B</option><option value="IPE 100">IPE 100</option><option value="IPE 180">IPE 180</option><option value="IPE 200">IPE 200</option><option value="IPE 240">IPE 240</option><option value="IPE 270">IPE 270</option><option value="IPE 300">IPE 300</option><option value="IPE 330">IPE 330</option><option value="IPE 360">IPE 360</option><option value="IPE 400">IPE 400</option><option value="IPE 450">IPE 450</option><option value="IPE 500">IPE 500</option><option value="IPE 550">IPE 550</option><option value="IPE 600">IPE 600</option><option value="IPE550">IPE550</option><option value="IPEA 100">IPEA 100</option><option value="IPEA 180">IPEA 180</option><option value="IPEA 300">IPEA 300</option><option value="IPEAA 100">IPEAA 100</option><option value="IPEAA 180">IPEAA 180</option><option value="IPEAA 200">IPEAA 200</option><option value="IPEO 300">IPEO 300</option><option value="P1">P1</option><option value="RB1">RB1</option><option value="RB2">RB2</option><option value="RB3">RB3</option><option value="RB4">RB4</option><option value="SB1L">SB1L</option><option value="SB1R">SB1R</option><option value="SB2L">SB2L</option><option value="SB2R">SB2R</option><option value="SB3L">SB3L</option><option value="SB3R">SB3R</option><option value="SB4L">SB4L</option><option value="SB4R">SB4R</option><option value="SP1">SP1</option><option value="ST1">ST1</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>DIM.DESC</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ACT.DIM</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />    
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>NOMINAL SIZE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SERIES</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="Y">MAIN SERIES</option><option value="N">SUB SERIES</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>STATUS</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <br />
                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control as="select">
                                                            <option value={0}>Create Date</option>
                                                            <option value={1}>Update Date</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 1 ||
                                                    this.state.selectDate === 2 ||
                                                    this.state.selectDate === 3 ||
                                                    this.state.selectDate === 4 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 5 ||
                                                    this.state.selectDate === 6 ||
                                                    this.state.selectDate === 7 ||
                                                    this.state.selectDate === 8 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>From</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 9 ||
                                                    this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 11 ||
                                                    this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>At Month</option>
                                                        <option value={6}>Between Month</option>
                                                        <option value={7}>More than</option>
                                                        <option value={8}>More than or equal</option>
                                                        <option value={9}>At Quater</option>
                                                        <option value={10}>Between Quater</option>
                                                        <option value={11}>At Year</option>
                                                        <option value={12}>Between Year</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>To</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>More than</option>
                                                            <option value={6}>More than or equal</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                        {
                                                this.state.selectValue === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > SEARCH </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="SIZE MASTER">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info"/></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" >SET ACTIVE</Button>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    {/* <Button size="sm" variant="primary" >Remove</Button> */}
                                    <Button size="sm" variant="success" >Add new adv.set.</Button>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>YEAR - MONTH</th>
                                        <th>ITEM(S)</th>
                                        <th>RUN TIME</th>
                                        <th>ITEM(S)</th>
                                        <th>RUN TIME</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default webpage;
