import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Transfer to SAP </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                    <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/customer-to-car">
                                    ใบจัดรถขนส่ง (ลูกค้า)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/transfer-to-sap">
                                            TRANSFER ORDER TO SAP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/interface-nd" >
                                            INTERFACE DN
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/dept-for-special" >
                                            DEPT FOR SPECIAL REQUEST
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/dn-transfer" >
                                            DN TRANSFER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/upload-pa" >
                                            UPLOAD PA REPORT FROM SAP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/setup-data/setup-data-transfer-sap/car-i-o" >
                                            CAR I/O
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;