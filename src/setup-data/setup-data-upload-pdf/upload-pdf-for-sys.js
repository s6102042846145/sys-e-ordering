import React from 'react';
import {
    Row, Col
} from 'react-bootstrap';
import Aux from "../../hoc/_Aux";

class NoPage extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col className="text-center">
                        <h1 className=" text-c-red">ERROR PAGE.</h1>
                    </Col>
                </Row >
            </Aux >
        );
    }
}

export default NoPage;
