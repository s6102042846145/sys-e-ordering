import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Upload PDF </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-upload-pdf/upload-pdf-for-cg">
                                            UPLOAD PDF FOR CUSTOMER GROUP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-upload-pdf/import-emis-af" >
                                            IMPORT EMIS AUTO FILE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Nav.Link className='list-group-item list-group-item-action' href="/setup-data/setup-data-upload-pdf/upload-pdf-for-sys" >
                                            UPLOAD PDF FOR CUSTOMER (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>



                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;