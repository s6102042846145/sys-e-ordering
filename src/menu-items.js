export default {
    items: [
        {
            id: 'support',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-support',
            children: [
                {
                    id: 'favorite',
                    title: 'Favorite',
                    type: 'item',
                    url: '/favorite',
                    classes: 'nav-item',
                    icon: 'feather icon-home'
                },
                { //SETUP DATA
                    id: 'setup-data',
                    title: 'SETUP DATA',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    menu: 'setup-data',
                    children: [
                        {
                            id: 'setup-data-transfer-sap',
                            title: 'Transfer to SAP',
                            type: 'item',
                            url: '/setup-data/setup-data-transfer-sap/setup-data-transfer-sap-main'
                        },
                        {
                            id: 'setup-data-transfer-sap-main',
                            title: 'Transfer to SAP',
                            type: 'submain',
                            submenu: 'setup-data-transfer-sap',
                            url: '/setup-data/setup-data-transfer-sap/setup-data-transfer-sap-main',
                            children: [
                                
                                {
                                    id: 'customer-to-car',
                                    title: 'ใบจัดรถขนส่ง (ลูกค้า)',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/customer-to-car'
                                },
                                {
                                    id: 'transfer-to-sap',
                                    title: 'TRANSFER ORDER TO SAP',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/transfer-to-sap'
                                },
                                {
                                    id: 'interface-nd',
                                    title: 'INTERFACE DN',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/interface-nd'
                                },{
                                    id: 'dept-for-special',
                                    title: 'DEPT FOR SPECIAL REQUEST',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/dept-for-special'
                                },
                                {
                                    id: 'dn-transfer',
                                    title: 'DN TRANSFER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/dn-transfer'
                                },{
                                    id: 'upload-pa',
                                    title: 'UPLOAD PA REPORT FROM SAP',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/upload-pa'
                                },
                                {
                                    id: 'car-i-o',
                                    title: 'CAR I/O',
                                    type: 'item',
                                    url: '/setup-data/setup-data-transfer-sap/car-i-o'
                                },
                            ]
                        },
                        {
                            id: 'setup-data-adv-set',
                            title: 'Advance Set',
                            type: 'item',
                            url: '/setup-data/setup-data-adv-set/setup-data-adv-set-main'
                        },
                        {
                            id: 'setup-data-adv-set-main',
                            title: 'Advance Set',
                            type: 'submain',
                            submenu: 'setup-data-adv-set',
                            url: '/setup-data/setup-data-adv-set/setup-data-adv-set-main',
                            children: [
                                {
                                    id: 'product-co',
                                    title: 'เปิดเสนอราคา สั่งซื้อล่วงหน้า',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/product-co'
                                },
                                {
                                    id: 'from-product-adv',
                                    title: 'เอกสารประกอบการสั่งซื้อล่วงหน้า (ADV)',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/from-product-adv'
                                },{
                                    id: 'advance-set',
                                    title: 'ADVANCE SET สินค้าชุดสุดคุ้ม',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/advance-set'
                                },{
                                    id: 'project-price-master',
                                    title: 'PROJECT PRICE MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/project-price-master'
                                },{
                                    id: 'trader-td',
                                    title: 'เปิดเสนอราคา สั่งซื้อล่วงหน้า (TRADER)',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/trader-td'
                                },{
                                    id: 'group-product-adv',
                                    title: 'กำหนดกลุ่มการขายสินค้า ADV',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/group-product-adv'
                                },{
                                    id: 'cfr-quota-range',
                                    title: 'CFR QUOTA.RANGE',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/cfr-quota-range'
                                },{
                                    id: 'improve-cfr-quota',
                                    title: 'ปรับปรุง CFR QUOTA',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/improve-cfr-quota'
                                },{
                                    id: 'quotation-project',
                                    title: ' กำหนดกลุ่มสินค้า สำหรับ QUOTATION PROJECT',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/quotation-project'
                                },{
                                    id: 'prepare-new-product',
                                    title: 'PREPARE NEW PRODUCT (MS126D) AE',
                                    type: 'item',
                                    url: '/setup-data/setup-data-adv-set/prepare-new-product'
                                },
                            ]
                        },
                        {
                            id: 'upload-pdf',
                            title: 'Upload PDF',
                            type: 'item',
                            url: '/setup-data/setup-data-upload-pdf/setup-data-adv-set-main'
                        },
                        {
                            id: 'upload-pdf-main',
                            title: 'Upload PDF',
                            type: 'submain',
                            submenu: 'upload-pdf',
                            url: '/setup-data/setup-data-upload-pdf/setup-data-adv-set-main',
                            children: [
                                {
                                    id: 'upload-pdf-for-cg',
                                    title: 'UPLOAD PDF FOR CUSTOMER GROUP',
                                    type: 'item',
                                    url: '/setup-data/setup-data-upload-pdf/upload-pdf-for-cg'
                                },{
                                    id: 'upload-pdf-for-sys',
                                    title: 'UPLOAD PDF FOR CUSTOMER (SYS)',
                                    type: 'item',
                                    url: '/setup-data/setup-data-upload-pdf/upload-pdf-for-sys'
                                },{
                                    id: 'import-emis-af',
                                    title: 'IMPORT EMIS AUTO FILE',
                                    type: 'item',
                                    url: '/setup-data/setup-data-upload-pdf/import-emis-af'
                                },
                            ]
                        },
                        {
                            id: 'customer-master-page',
                            title: 'Customer Master',
                            type: 'item',
                            url: '/setup-data/setup-data-customer/customer-master-main'
                        },
                        {
                            id: 'customer-master-main',
                            title: 'Customer Master',
                            type: 'submain',
                            submenu: 'customer-master-page',
                            url: '/setup-data/setup-data-customer/customer-master-main',
                            children: [
                                {
                                    id: 'customer-master',
                                    title: 'CUSTOMER MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-customer/customer-master'
                                },{
                                    id: 'trader-master',
                                    title: 'TRADER MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-customer/trader-master'
                                },{
                                    id: 'shipment-port',
                                    title: 'SHIPMENT PORT',
                                    type: 'item',
                                    url: '/setup-data/setup-data-customer/shipment-port'
                                },{
                                    id: 'adn-frabrucator',
                                    title: 'เพิ่มชื่อ FRABRICATOR',
                                    type: 'item',
                                    url: '/setup-data/setup-data-customer/adn-frabrucator'
                                },
                            ]
                        },
                        {
                            id: 'general-master-page',
                            title: 'General Master',
                            type: 'item',
                            url: '/setup-data/setup-data-general/general-master-main'
                        },
                        {
                            id: 'general-master-main',
                            title: 'General Master',
                            type: 'submain',
                            submenu: 'general-master-page',
                            url: '/setup-data/setup-data-general/general-master-main',
                            children: [
                                {
                                    id: 'term-condition-master',
                                    title: 'TERM & CONDITION MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/term-condition-master'
                                },{
                                    id: 'bank-master',
                                    title: 'BANK MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/bank-master'
                                },{
                                    id: 'payment-term-master',
                                    title: 'PAYMENT TERM MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/payment-term-master'
                                },{
                                    id: 'appl-parameter',
                                    title: 'APPL.PARAMETER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/appl-parameter'
                                },{
                                    id: 'advance-type-setup',
                                    title: 'ADVANCE TYPE SETUP',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/advance-type-setup'
                                },{
                                    id: 'rebate-type-master',
                                    title: 'REBATE TYPE MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/rebate-type-master'
                                },{
                                    id: 'surcharge-type-setup',
                                    title: 'SURCHARGE TYPE SETUP',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/surcharge-type-setup'
                                },{
                                    id: 'shipping-company',
                                    title: 'อำนาจดำเนินการ',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/shipping-company'
                                },{
                                    id: 'project-type-master',
                                    title: 'PROJECT TYPE MASTER',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/project-type-master'
                                },{
                                    id: 'country-for-export',
                                    title: 'COUNTRY FOR EXPORT',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/country-for-export'
                                },{
                                    id: 'export-advanced-set',
                                    title: 'EXPORT ADVANCED SET',
                                    type: 'item',
                                    url: '/setup-data/setup-data-general/export-advanced-set'
                                }
                            ]
                        },
                        {
                            id: 'crf-master',
                            title: 'CFR Master',
                            type: 'item',
                            url: '/setup-data/setup-data-cfr/crf-master-main'
                        },
                        {
                            id: 'crf-master-main',
                            title: 'CFR Master',
                            type: 'submain',
                            submenu: 'crf-master',
                            url: '/setup-data/setup-data-cfr/crf-master-main',
                            children: [
                                {
                                    id: 'product-type',
                                    title: 'ประเภทรถขนส่ง',
                                    type: 'item',
                                    url: '/setup-data/setup-data-cfr/product-type'
                                },{
                                    id: 'shipping-company',
                                    title: 'บริษัทรับจ้างขนส่ง',
                                    type: 'item',
                                    url: '/setup-data/setup-data-cfr/shipping-company'
                                }
                            ]
                        },
                        {
                            id: 'scor-metrics',
                            title: 'SCOR METRICS MASTER',
                            type: 'item',
                            url: '/setup-data/setup-data-scor-metrics/scor-metrics-main'
                        },
                        {
                            id: 'scor-metrics-main',
                            title: 'SCOR METRICS MASTER',
                            type: 'submain',
                            submenu: 'scor-metrics',
                            url: '/setup-data/setup-data-scor-metrics/scor-metrics-main',
                            children: [
                                {
                                    id: 'scor-matrix-control',
                                    title: 'SCOR MATRIX CONTROL',
                                    type: 'item',
                                    url: '/setup-data/setup-data-scor-metrics/scor-matrix-control'
                                },{
                                    id: 'scor-if-reason',
                                    title: 'SCOR -  IF REASON',
                                    type: 'item',
                                    url: '/setup-data/setup-data-scor-metrics/scor-if-reason'
                                }, {
                                    id: 'scor-cd',
                                    title: 'SCOR -  CD ใบลดหนี้/เพิ่มหนี้',
                                    type: 'item',
                                    url: '/setup-data/setup-data-scor-metrics/scor-cd'
                                },{
                                    id: 'scor-unorder',
                                    title: 'SCOR -  รายการ ORDER ที่ปฏิเสธการส่ง',
                                    type: 'item',
                                    url: '/setup-data/setup-data-scor-metrics/scor-unorder'
                                }, {
                                    id: 'scor-claim-data',
                                    title: 'SCOR-CLAIM DATA',
                                    type: 'item',
                                    url: '/setup-data/setup-data-scor-metrics/scor-claim-data'
                                }
                            ]
                        },
                    ]
                }
                ,
                { //DOMESTIC
                    id: 'domestic',
                    title: 'DOMESTIC',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    menu: 'domestic',
                    children: [
                        {
                            id: 'domestic-order-confirm',
                            title: 'ORDER CONFIRM',
                            type: 'item',
                            url: '/domestic/domestic-order-confirm/domestic-order-confirm-main'
                        },
                        {
                            id: 'domestic-order-confirm-main',
                            title: 'ORDER CONFIRM',
                            type: 'submain',
                            submenu: 'domestic-order-confirm',
                            url: '/domestic/domestic-order-confirm/domestic-order-confirm-main',
                            children: [
                                {
                                    id: 'domestic-invoice',
                                    title: 'ใบสั่งซื้อล่วงหน้า',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/domestic-invoice'
                                },
                                {
                                    id: 'domestic-domestic-order-confirm',
                                    title: 'DOMESTIC ORDER CONFIRM',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/domestic-domestic-order-confirm'
                                },
                                {
                                    id: 'check-domestic-order-confirm',
                                    title: 'ตรวจสอบ DOMESTIC ORDER CONFIRM',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/check-domestic-order-confirm'
                                },
                                {
                                    id: 'domestic-po-sap-text',
                                    title: 'PO SAP TEXT',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/domestic-po-sap-text'
                                },
                                {
                                    id: 'manage-order-fill-old',
                                    title: 'MANAGE ORDER FILL เดิม',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/manage-order-fill-old'
                                },
                                {
                                    id: 'domestic-order-confirm-show',
                                    title: 'แสดง DOMESTIC ORDER CONFIRM',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/domestic-order-confirm-show'
                                },
                                {
                                    id: 'customer-po-sap-text',
                                    title: 'PO SAP TEXT (ลูกค้า)',
                                    type: 'item',
                                    url: '/domestic/domestic-order-confirm/customer-po-sap-text'
                                }
                            ]
                        },
                        {
                            id: 'domestic-order',
                            title: 'การรับสินค้า',
                            type: 'item',
                            url: '/domestic/domestic-order/domestic-order-main'
                        },
                        {
                            id: 'domestic-order-main',
                            title: 'การรับสินค้า',
                            type: 'submain',
                            submenu: 'domestic-order',
                            url: '/domestic/domestic-order/domestic-order-main',
                            children: [
                                {
                                    id: 'create-approval-request',
                                    title: 'สร้างใบขอนุมัติตั๋วตัดจ่ายล่วงหน้า',
                                    type: 'item',
                                    url: '/domestic/domestic-order/create-approval-request'
                                }, {
                                    id: 'create-car-arrangement',
                                    title: 'สร้างใบจัดรถ',
                                    type: 'item',
                                    url: '/domestic/domestic-order/create-car-arrangement'
                                }, {
                                    id: 'create-purchase-order-SYS',
                                    title: 'สร้างใบสั่งซื้อ (SYS)',
                                    type: 'item',
                                    url: '/domestic/domestic-order/create-purchase-order-SYS'
                                }, {
                                    id: 'create-check-the-payment-summary-SYS',
                                    title: 'สร้าง ใบสรุปยอดจ่ายเช็ค (SYS)',
                                    type: 'item',
                                    //url: '/domestic/domestic-order/PnStatusSize'
                                    url: '/domestic/domestic-order/create-check-the-payment-summary-SYS'
                                }, {
                                    id: 'arrange-a-car-specify-DP-number',
                                    title: 'ใบจัดรถ (กำหนดเลข DP)',
                                    type: 'item',
                                    //url: '/domestic/domestic-order/PnStatusSize'
                                    url: '/domestic/domestic-order/arrange-a-car-specify-DP-number'
                                }, {
                                    id: 'sales-ORG-0180',
                                    title: 'ใบจัดรถ (กำหนดเลข DP) SALES ORG 0180',
                                    type: 'item',
                                    url: '/domestic/domestic-order/sales-ORG-0180'
                                }, {
                                    id: 'est.shipment-do',
                                    title: 'EST.SHIPMENT DO.',
                                    type: 'item',
                                    url: '/domestic/domestic-order/est.shipment-do'
                                }, {
                                    id: 'arrange-transportation-ssj',
                                    title: 'จัดการใบจัดรถขนส่ง สสจ',
                                    type: 'item',
                                    url: '/domestic/domestic-order/arrange-transportation-ssj'
                                }, {
                                    id: 'approve-cut-off-ticket',
                                    title: 'ใบขอนุมัติตั๋วตัดฝาก',
                                    type: 'item',
                                    url: '/domestic/domestic-order/approve-cut-off-ticket'
                                }, {
                                    id: 'domestic-arrange-transport',
                                    title: 'ใบจัดรถขนส่ง (SYS)',
                                    type: 'item',
                                    url: '/domestic/domestic-order/domestic-arrange-transport'
                                }, {
                                    id: 'purchase-order',
                                    title: 'ใบสั่งซื้อ (SYS)',
                                    type: 'item',
                                    url: '/domestic/domestic-order/purchase-order'
                                }, {
                                    id: 'summary-of-payment',
                                    title: 'สรุปยอดจ่ายเช็ค',
                                    type: 'item',
                                    url: '/domestic/domestic-order/summary-of-payment'
                                }, {
                                    id: 'delivery-schedule',
                                    title: 'ตารางจัดรถส่งให้ (SYS)',
                                    type: 'item',
                                    url: '/domestic/domestic-order/delivery-schedule'
                                }, {
                                    id: 'domestic-arrange-a-car',
                                    title: 'ใบจัดรถ (กำหนดเลข DP) CFR',
                                    type: 'item',
                                    url: '/domestic/domestic-order/domestic-arrange-a-car'
                                }, {
                                    id: 'EST.shipment-do.old',
                                    title: 'EST.SHIPMENT DO.OLD',
                                    type: 'item',
                                    url: '/domestic/domestic-order/EST.shipment-do.old'
                                }
                            ]
                        },
                        {
                            id: 'domestic-dlc',
                            title: 'DLC',
                            type: 'item',
                            url: '/domestic/domestic-dlc/domestic-dlc-main'
                        },
                        {
                            id: 'domestic-dlc-main',
                            title: 'DLC',
                            type: 'submain',
                            submenu: 'domestic-dlc',
                            url: '/domestic/domestic-dlc/domestic-dlc-main',
                            children: [
                                {
                                    id: 'sc-dlc-domestic',
                                    title: 'SC DLC (DOMESTIC)',
                                    type: 'item',
                                    url: '/domestic/domestic-dlc/sc-dlc-domestic'
                                }, {
                                    id: 'check-sc-dlc',
                                    title: 'ตรวจสอบ SC DLC (DOMESTIC)',
                                    type: 'item',
                                    url: '/domestic/domestic-dlc/check-sc-dlc'
                                }, {
                                    id: 'approve-sc-dlc',
                                    title: 'อนุมัติ SC DLC (DOMESTIC)',
                                    type: 'item',
                                    url: '/domestic/domestic-dlc/approve-sc-dlc'
                                }
                            ]
                        },
                        {
                            id: 'domestic-quotation',
                            title: 'QUOTATION',
                            type: 'item',
                            url: '/domestic/domestic-quotation'
                        },
                        {
                            id: 'domestic-quotation-main',
                            title: 'QUOTATION',
                            type: 'submain',
                            submenu: 'domestic-quotation',
                            url: '/domestic/domestic-quotation',
                            children: [
                                {
                                    id: 'domestic-domest-quotation',
                                    title: 'DOMESTIC QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/domestic-domest-quotation'
                                }, {
                                    id: 'check1-domestic-quotation',
                                    title: 'CHECK 1 DOMESTIC QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/check1-domestic-quotation'
                                }, {
                                    id: 'check2-domestic-quotation',
                                    title: 'CHECK 2 DOMESTIC QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/check2-domestic-quotation'
                                }, {
                                    id: 'approve-domestic-quotation',
                                    title: 'APPROVE DOMESTIC QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/approve-domestic-quotation'
                                }, {
                                    id: 'cs-approve-domestic-quotation',
                                    title: 'CS APPROVE DOMESTIC QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/cs-approve-domestic-quotation'
                                }, {
                                    id: 'cs-approve-export-quotation',
                                    title: 'CS APPROVE EXPORT QUOTATION',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/cs-approve-export-quotation'
                                }, {
                                    id: 'renewal-certificate-possibility',
                                    title: 'ใบขอต่ออายุ POSSIBILITY',
                                    type: 'item',
                                    url: '/domestic/domestic-quotation/renewal-certificate-possibility'
                                }
                            ]
                        },

                    ]
                },
                { //PROJECT
                    id: 'project',
                    title: 'PROJECT',
                    type: 'collapse',
                    icon: 'feather icon-box',
                    menu: 'project',
                    children: [
                        {
                            id: 'project-order-confirm',
                            title: 'ORDER CONFIRM',
                            type: 'item',
                            url: '/project/project-order-confirm/project-order-confirm-main'
                        },
                        {
                            id: 'project-order-confirm-main',
                            title: 'ORDER CONFIRM',
                            type: 'submain',
                            submenu: 'project-order-confirm',
                            url: '/project/project-order-confirm/project-order-confirm-main',
                            children: [
                                {
                                    id: 'project-order-confirm',
                                    title: 'PROJECT ORDER CONFIRM',
                                    type: 'item',
                                    url: '/project/project-order-confirm/project-order-confirm'
                                },{
                                    id: 'check-project-order',
                                    title: 'ตรวจสอบ PROJECT ORDER CONFIRM',
                                    type: 'item',
                                    url: '/project/project-order-confirm/check-project-order'
                                },{
                                    id: 'show-order-confirm',
                                    title: 'แสดง PROJECT ORDER CONFIRM',
                                    type: 'item',
                                    url: '/project/project-order-confirm/show-order-confirm'
                                },{
                                    id: 'project-invoice',
                                    title: 'ใบแจ้งหนี้ (INVOICE)',
                                    type: 'item',
                                    url: '/project/project-order-confirm/project-invoice'
                                }
                            ]
                        },
                        {
                            id: 'project-quotation',
                            title: 'QUOTATION',
                            type: 'item',
                            url: '/project/quotation/project-quotation-main'
                        },{
                            id: 'project-quotation-main',
                            title: 'QUOTATION',
                            type: 'submain',
                            submenu: 'project-quotation',
                            url: '/project/quotation/project-quotation-main',
                            children: [
                                {
                                    id: 'project-project-quotation',
                                    title: 'PROJECT QUOTATION',
                                    type: 'item',
                                    url: '/project/quotation/project-quotation'
                                },{
                                    id: 'check-1-project-quotation',
                                    title: 'CHECK 1 PROJECT QUOTATION',
                                    type: 'item',
                                    url: '/project/quotation/check-1-project-quotation'
                                },{
                                    id: 'check-2-project-quotation',
                                    title: 'CHECK 2 PROJECT QUOTATION',
                                    type: 'item',
                                    url: '/project/quotation/check-2-project-quotation'
                                },{
                                    id: 'approve-project-quotation',
                                    title: 'APPROVE PROJECT QUOTATION',
                                    type: 'item',
                                    url: '/project/quotation/approve-project-quotation'
                                },{
                                    id: 'project-quotation-invoice',
                                    title: 'ใบสรุปยอดใบแจ้งหนี้',
                                    type: 'item',
                                    url: '/project/quotation/project-quotation-invoice'
                                }
                            ]
                        },
                        {
                            id: 'project-receiving',
                            title: 'การรับสินค้า',
                            type: 'item',
                            url: '/project/project-receiving/project-receiving-main'
                        },{
                            id: 'project-receiving-main',
                            title: 'การรับสินค้า',
                            type: 'submain',
                            submenu: 'project-receiving',
                            url: '/project/project-receiving/project-receiving-main',
                            children: [
                                {
                                    id: 'receiving-car-setting',
                                    title: 'ใบจัดรถขนส่ง (SYS)',
                                    type: 'item',
                                    url: '/project/project-receiving/receiving-car-setting'
                                },{
                                    id: 'report-quotation-project',
                                    title: 'รายงานสรุป QUOTATION PROJECT',
                                    type: 'item',
                                    url: '/project/project-receiving/report-quotation-project'
                                },{
                                    id: 'receiving-car-process',
                                    title: 'กระบวนการออกใบจัดรถ',
                                    type: 'item',
                                    url: '/project/project-receiving/receiving-car-process'
                                },{
                                    id: 'est-shipment-project',
                                    title: 'EST.SHIPMENT PRJ.',
                                    type: 'item',
                                    url: '/project/project-receiving/est-shipment-project'
                                }
                            ]
                        }
                    ]
                }                
                ,
                { //export
                    id: 'export',
                    title: 'EXPORT',
                    type: 'collapse',
                    icon: 'feather icon-clipboard',
                    menu: 'export',
                    children: [
                        {
                            id: 'order-confirm',
                            title: 'ORDER CONFIRM',
                            type: 'item',
                            url: '/export/order-confirm/order-confirm-main'
                        },
                        {
                            id: 'export',
                            title: 'ORDER CONFIRM',
                            type: 'submain',
                            submenu: 'order-confirm',
                            url: '/export/order-confirm/order-confirm-main',
                            children: [
                                {
                                    id: 'approve-order',
                                    title: 'APPROVE EXPORT ORDER CONFIRM',
                                    type: 'item',
                                    url: '/export/order-confirm/approve-order'
                                },
                                {
                                    id: 'export-order',
                                    title: 'EXPORT ORDER CONFIRMED',
                                    type: 'item',
                                    url: '/export/order-confirm/export-order'
                                },
                                {
                                    id: 'check-order',
                                    title: 'CHECK EXPORT ORDER CONFIRM',
                                    type: 'item',
                                    url: '/export/order-confirm/check-order'
                                },
                                {
                                    id: 'export-invoice',
                                    title: 'EXPORT INVOICE',
                                    type: 'item',
                                    url: '/export/order-confirm/export-invoice'
                                }
                            ]
                        },
                        {
                            id: 'quotation',
                            title: 'QUOTATION',
                            type: 'item',
                            url: '/export/quotation/quotation-main'
                        },
                        {
                            id: 'quotation',
                            title: 'QUOTATION',
                            type: 'submain',
                            submenu: 'quotation',
                            url: '/export/quotation/quotation-main',
                            children: [
                                {
                                    id: 'export-quotation',
                                    title: 'EXPORT QUOTATION',
                                    type: 'item',
                                    url: '/export/quotation/export-quotation'
                                },
                                {
                                    id: 'check-1-export-quotation',
                                    title: 'CHECK 1 EXPORT QUOTATION',
                                    type: 'item',
                                    url: '/export/quotation/check-1-export-quotation'
                                },
                                {
                                    id: 'check-2-export-quotation',
                                    title: 'CHECK 2 EXPORT QUOTATION',
                                    type: 'item',
                                    url: '/export/quotation/check-2-export-quotation'
                                },
                                {
                                    id: 'approve-export-quotation',
                                    title: 'APPROVE EXPORT QUOTATION',
                                    type: 'item',
                                    url: '/export/quotation/approve-export-quotation'
                                },
                                {
                                    id: 'inquiry-process',
                                    title: 'INQUIRY IN PROCESS',
                                    type: 'item',
                                    url: '/export/quotation/inquiry-process'
                                },
                                {
                                    id: 'upload-export',
                                    title: 'UPLOAD EXPORT ORDER',
                                    type: 'item',
                                    url: '/export/quotation/upload-export'
                                }
                            ]
                        },
                        {
                            id: 'export-process',
                            title: 'EXPORT PROCESS',
                            type: 'item',
                            url: '/export/export-process/export-process-main'
                        },
                        {
                            id: 'export-process',
                            title: 'EXPORT PROCESS',
                            type: 'submain',
                            submenu: 'export-process',
                            url: '/export/export-process/export-process-main',
                            children: [
                                {
                                    id: 'check-low',
                                    title: 'CHECK LOW PRICE',
                                    type: 'item',
                                    url: '/export/export-process/check-low'
                                },
                                {
                                    id: 'approve-low-1',
                                    title: 'APPROVE LOW PRICE #1',
                                    type: 'item',
                                    url: '/export/export-process/approve-low-1'
                                }
                                ,
                                {
                                    id: 'approve-low-2',
                                    title: 'APPROVE LOW PRICE #2',
                                    type: 'item',
                                    url: '/export/export-process/approve-low-2'
                                }
                            ]
                        },
                        {
                            id: 'export-report',
                            title: 'REPORT',
                            type: 'item',
                            url: '/export/export-report/export-report-main'
                        },
                        {
                            id: 'export-report-main',
                            title: 'REPORT',
                            type: 'submain',
                            submenu: 'export-report',
                            url: '/export/export-report/export-report-main',
                            children: [
                                {
                                    id: 'export-report-order',
                                    title: 'REPORT ORDER BY SHIPMENT',
                                    type: 'item',
                                    url: '/export/export-report/export-report-order'
                                }
                            ]
                        }
                    ]
                },
                { //cut beam
                    id: 'cut-beam',
                    title: 'CUT BEAM',
                    type: 'collapse',
                    icon: 'feather icon-scissors',
                    menu: 'cut-beam',
                    children: [
                        {
                            id: 'cut-process',
                            title: 'CUT Process',
                            type: 'item',
                            url: '/cut-beam/cut-process/cut-process-main'
                        },
                        {
                            id: 'cut-process-main',
                            title: 'CUT Process',
                            type: 'submain',
                            submenu: 'cut-process',
                            url: '/cut-beam/cut-process/cut-process-main',
                            children: [
                                {
                                    id: 'steel-cut-bill',
                                    title: 'ใบแจ้งตัดเหล็ก (SYS)',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/SteelCutBill'
                                },
                                {
                                    id: 'steel-receive-customer',
                                    title: 'ใบแจ้งตัดเหล็กเพื่อ KEY รับเหล็กลูกค้า',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/SteelReceiveCustomer'
                                },
                                {
                                    id: 'cut-notice',
                                    title: 'ใบแจ้งตัดเหล็กเพื่อ KEY เหล็กตัดเสร็จ',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/SteelCuttingFinished'
                                },
                                {
                                    id: 'delivery-bill',
                                    title: 'ใบส่งของ',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/DeliveryBill'
                                },
                                {
                                    id: 'steel-cutting-invoice',
                                    title: 'ใบแจ้งหนี้ค่าตัดเหล็ก',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/SteelCuttingInvoice'
                                },
                                {
                                    id: 'approve-cut-bill',
                                    title: 'อนุมัตืใบแจ้งหนี้ค่าตัดเหล็ก',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/ApproveCutBill'
                                },
                                {
                                    id: 'waiting-to-print-steel-cut-bill',
                                    title: 'รอพิมพ์ใบแจ้งหนี้ค่าตัดเหล็ก',
                                    type: 'item',
                                    url: '/cut-beam/cut-process/WaitingToPrintSteelCutBill'
                                }
                            ]
                        },
                        {
                            id: 'memo-for-cut',
                            title: 'Memo for CUT',
                            type: 'item',
                            url: '/cut-beam/memo-for-cut/memo-for-cut-main'
                        },
                        {
                            id: 'memo-for-cut-main',
                            title: 'Memo for CUT',
                            type: 'submain',
                            submenu: 'memo-for-cut',
                            url: '/cut-beam/memo-for-cut/memo-for-cut-main',
                            children: [
                                {
                                    id: 'requst-to-product-bill',
                                    title: 'ใบขออนุมัติตัดสินค้า',
                                    type: 'item',
                                    url: '/cut-beam/memo-for-cut/RequstToCutProductBill'
                                },
                                {
                                    id: 'approve-to-cut-product',
                                    title: 'อนุมัติ ใบขออนุมัติตัดสินค้า',
                                    type: 'item',
                                    url: '/cut-beam/memo-for-cut/ApproveToCutProduct'
                                }
                            ]
                        },
                        {
                            id: 'after-billing-process',
                            title: 'After Billing Process',
                            type: 'item',
                            url: '/cut-beam/after-billing-process/after-billing-process-main'
                        },
                        {
                            id: 'after-billing-process-main',
                            title: 'After Billing Process',
                            type: 'submain',
                            submenu: 'after-billing-process',
                            url: '/cut-beam/after-billing-process/after-billing-process-main',
                            children: [
                                {
                                    id: 'import-data-bill-due-date',
                                    title: 'นำเข้าข้อมูล BILL DUE DATE',
                                    type: 'item',
                                    url: '/cut-beam/after-billing-process/ImportDataBillDueDate'
                                },
                                {
                                    id: 'data-bill-due-date',
                                    title: 'ข้อมูล BILL DUE DATE',
                                    type: 'item',
                                    url: '/cut-beam/after-billing-process/DataBillDueDate'
                                },
                                {
                                    id: 'receipt-information',
                                    title: 'ข้อมูลใบรับชำระ OCW',
                                    type: 'item',
                                    url: '/cut-beam/after-billing-process/ReceiptInformation'
                                },
                                {
                                    id: 'way-bill-data',
                                    title: 'ข้อมูลใบนำส่ง',
                                    type: 'item',
                                    url: '/cut-beam/after-billing-process/WaybillData'
                                }
                            ]
                        },
                        {
                            id: 'cut-beam-report',
                            title: 'Report',
                            type: 'item',
                            url: '/cut-beam/cut-beam-report/report-main'
                        },
                        {
                            id: 'report-main',
                            title: 'Report',
                            type: 'submain',
                            submenu: 'cut-beam-report',
                            url: '/cut-beam/cut-beam-report/report-main',
                            children: [
                                {
                                    id: 'report-from-customer',
                                    title: 'รายงานรับสินค้าจากลูกค้า',
                                    type: 'item',
                                    url: '/cut-beam/cut-beam-report/ReportFromCustomer'
                                },
                                {
                                    id: 'report-received-from-cut',
                                    title: 'รายงานรับสินค้าจากการตัด',
                                    type: 'item',
                                    url: '/cut-beam/cut-beam-report/ReportReceivedFromCut'
                                },
                                {
                                    id: 'report-cutting-bill',
                                    title: 'รายงานใบแจ้งหนี้ค่าตัด',
                                    type: 'item',
                                    url: '/cut-beam/cut-beam-report/ReportCuttingBill'
                                },
                                {
                                    id: 'report-cash-for-cutting',
                                    title: 'รายงานรับเงินค่าตัดเหล็ก',
                                    type: 'item',
                                    url: '/cut-beam/cut-beam-report/ReportCashForCutting'
                                }
                            ]
                        }
                    ]
                },
                
                { //EXPORT DOC
                    id: 'export-doc',
                    title: 'EXPORT DOC',
                    type: 'collapse',
                    icon: 'feather icon-pie-chart',
                    menu: 'export-doc',
                    children: [
                        {
                            id: 'doc-setup-data',
                            title: 'SETUP DATA',
                            type: 'item',
                            url: '/export-doc/setup-data-export/setup-data-main'
                        },
                        {
                            id: 'doc-setup-data-main',
                            title: 'SETUP DATA',
                            type: 'submain',
                            submenu: 'export-doc',
                            url: '/export-doc/setup-data-export/setup-data-main',
                            children: [
                                {
                                    id: 'admin-page',
                                    title: 'ADMIN PAGE',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/admin-page'
                                },
                                {
                                    id: 'customer-email',
                                    title: 'CUSTOMER EMAIL',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/customer-email'
                                },
                                {
                                    id: 'end-customer-email',
                                    title: 'END CUSTOMER EMAIL',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/end-customer-email'
                                },
                                {
                                    id: 'ods-setup',
                                    title: 'ODS SETUP',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/ods-setup'
                                },
                                {
                                    id: 'order-email',
                                    title: 'SEND ORDER EMAIL',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/order-email'
                                },
                                {
                                    id: 'ods-report-authorize',
                                    title: 'ODS REPORT AUTHORIZE',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/ods-report-authorize'
                                },
                                {
                                    id: 'mail-log',
                                    title: 'MAIL LOG',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/mail-log'
                                },
                                {
                                    id: 'scrap-return-master',
                                    title: 'SCRAP RETURN MASTER',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/scrap-return-master'
                                },
                                {
                                    id: 'op-data',
                                    title: 'OP DATA',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/op-data'
                                },
                                {
                                    id: 'in-land-cost',
                                    title: 'IN-LAND COST',
                                    type: 'item',
                                    url: '/export-doc/setup-data-export/in-land-cost'
                                },
                            ]
                        },
                        {
                            id: 'schedule-shipment',
                            title: 'SCHEDULE SHIPMENT',
                            type: 'item',
                            url: '/export-doc/schedule-shipment/schedule-shipment-main'
                        },
                        {
                            id: 'schedule-shipment-main',
                            title: 'SCHEDULE SHIPMENT',
                            type: 'submain',
                            submenu: 'schedule-shipment',
                            url: '/export-doc/schedule-shipment/schedule-shipment-main',
                            children: [
                                {
                                    id: 'schedule-shipment-page',
                                    title: 'SCHEDULE SHIPMENT',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/schedule-shipment-page'
                                },{
                                    id: 'svessel',
                                    title: 'VESSEL',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/svessel'
                                },{
                                    id: 'vessel-agent',
                                    title: 'VESSEL AGENT',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/vessel-agent'
                                },{
                                    id: 'vessel-type',
                                    title: 'VESSEL TYPE',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/vessel-type'
                                },{
                                    id: 'shipment-period',
                                    title: 'SHIPMENT PERIOD',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/shipment-period'
                                },{
                                    id: 'list-rebate-byorder',
                                    title: 'LIST REBATE BY ORDER',
                                    type: 'item',
                                    url: '/export-doc/schedule-shipment/list-rebate-byorder'
                                },
                            ]
                        },
                        {
                            id: 'operation',
                            title: 'OPERATION',
                            type: 'item',
                            url: '/export-doc/operation/operation-main'
                        },
                        {
                            id: 'operation-main',
                            title: 'OPERATION',
                            type: 'submain',
                            submenu: 'operation',
                            url: '/export-doc/operation/operation-main',
                            children: [
                                {
                                    id: 'print-inv-export-order',
                                    title: 'PRINT INV# EXPORT ORDER',
                                    type: 'item',
                                    url: '/export-doc/operation/print-inv-export-order'
                                },{
                                    id: 'print-inv-export-ord-shipment',
                                    title: 'PRINT INV# EXPORT ORD.BY SHIPMENT',
                                    type: 'item',
                                    url: '/export-doc/operation/print-inv-export-ord-shipment'
                                },{
                                    id: 'moveorder-to-ods',
                                    title: 'MOVE ORDER TO ODS',
                                    type: 'item',
                                    url: '/export-doc/operation/moveorder-to-ods'
                                },{
                                    id: 'redit-document',
                                    title: 'CREDIT DOCUMENT',
                                    type: 'item',
                                    url: '/export-doc/operation/redit-document'
                                },{
                                    id: 'be-ben-cert-from',
                                    title: 'BE/BEN.CERT.FROM',
                                    type: 'item',
                                    url: '/export-doc/operation/be-ben-cert-from'
                                },{
                                    id: 'operation-number',
                                    title: 'กำหนดเลขที่ใบขน',
                                    type: 'item',
                                    url: '/export-doc/operation/operation-number'
                                },{
                                    id: 'operation-taxation',
                                    title: 'ใบขอรับเงินชดเชยค่าภาษีอากร',
                                    type: 'item',
                                    url: '/export-doc/operation/operation-taxation'
                                },{
                                    id: 'update-payment-status',
                                    title: 'UPDATE PAYMENT STATUS',
                                    type: 'item',
                                    url: '/export-doc/operation/update-payment-status'
                                },{
                                    id: 'payment-receipt',
                                    title: 'ใบรับชำระเงิน',
                                    type: 'item',
                                    url: '/export-doc/operation/payment-receipt'
                                },{
                                    id: 'est-shippment',
                                    title: 'EST.SHIPPMENT',
                                    type: 'item',
                                    url: '/export-doc/operation/est-shippment'
                                },{
                                    id: 'short-shipment',
                                    title: 'SHORT SHIPMENT',
                                    type: 'item',
                                    url: '/export-doc/operation/short-shipment'
                                },{
                                    id: 'order-discrepancy',
                                    title: 'ORDER DISCREPANCY',
                                    type: 'item',
                                    url: '/export-doc/operation/order-discrepancy'
                                },
                            ]
                        },
                        {
                            id: 'export-ods',
                            title: 'ODS',
                            type: 'item',
                            url: '/export-doc/export-ods/ods-main'
                        },
                        {
                            id: 'export-ods-main',
                            title: 'ODS',
                            type: 'submain',
                            submenu: 'export-ods',
                            url: '/export-doc/export-ods/ods-main',
                            children: [
                                {
                                    id: 'export-import-dhl',
                                    title: 'IMPORT DHL',
                                    type: 'item',
                                    url: '/export-doc/export-ods/import-dhl'
                                }, {
                                    id: 'export-dhl-matching',
                                    title: 'DHL MATCHING',
                                    type: 'item',
                                    url: '/export-doc/export-ods/dhl-matching'
                                }, {
                                    id: 'export-bank-dhl',
                                    title: 'BANK DHL',
                                    type: 'item',
                                    url: '/export-doc/export-ods/bank-dhl'
                                }, {
                                    id: 'export-other-doc-type',
                                    title: 'OTHER DOC.TYPE',
                                    type: 'item',
                                    url: '/export-doc/export-ods/other-doc-type'
                                },
                            ]
                        },
                        {
                            id: 'reporting',
                            title: 'REPORTING',
                            type: 'item',
                            url: '/export-doc/reporting/reporting-main'
                        },
                        {
                            id: 'reporting-main',
                            title: 'REPORTING',
                            type: 'submain',
                            submenu: 'reporting',
                            url: '/export-doc/reporting/reporting-main',
                            children: [
                                {
                                    id: 'ods-reporting',
                                    title: 'ODS REPORTING',
                                    type: 'item',
                                    url: '/export-doc/reporting/ods-reporting'
                                }, {
                                    id: 'summary-fob',
                                    title: 'SUMMARY FOB',
                                    type: 'item',
                                    url: '/export-doc/reporting/summary-fob'
                                }, {
                                    id: 'account-receivable-report',
                                    title: 'ACCOUNT RECEIVABLE REPORT',
                                    type: 'item',
                                    url: '/export-doc/reporting/account-receivable-report'
                                },
                            ]
                        }
                    ]
                },
                
                { //cut beam
                    id: 'report',
                    title: 'REPORT',
                    type: 'collapse',
                    icon: 'feather icon-pie-chart',
                    menu: 'report',
                    children: [
                        {
                            id: 'advance-order',
                            title: 'ADVANCE ORDER',
                            type: 'item',
                            url: '/report/advance-order/advance-order-main'
                        },
                        {
                            id: 'advance-order-main',
                            title: 'ADVANCE ORDER',
                            type: 'submain',
                            submenu: 'advance-order',
                            url: '/report/advance-order/advance-order-main',
                            children: [
                                {
                                    id: 'summarybycustomer',
                                    title: 'SUMMARY ADV.ORDER BY CUSTOMER',
                                    type: 'item',
                                    url: '/report/advance-order/summarybycustomer'
                                },
                                {
                                    id: 'summarycustomer',
                                    title: 'SUMMARY ADV.ORDER BY CUSTOMER',
                                    type: 'item',
                                    url: '/report/advance-order/summarycustomer'
                                },
                                {
                                    id: 'summarybyproduct',
                                    title: 'SUMMARY ADV.ORDER BY PRODUCT',
                                    type: 'item',
                                    url: '/report/advance-order/summarybyproduct'
                                },
                                {
                                    id: 'summaryproduct',
                                    title: 'SUMMARY ADV.ORDER BY PRODUCT',
                                    type: 'item',
                                    url: '/report/advance-order/summaryproduct'
                                },
                            ]
                        },
                        {
                            id: 'confirmed-order',
                            title: 'CONFIRMED ORDER',
                            type: 'item',
                            url: '/report/confirmed-order/confirmed-order-main'
                        },
                        {
                            id: 'confirmed-order-main',
                            title: 'CONFIRMED ORDER',
                            type: 'submain',
                            submenu: 'confirmed-order',
                            url: '/report/confirmed-order/confirmed-order-main',
                            children: [
                                {
                                    id: 'confirmbycustumer',
                                    title: 'SUMMARY ORDER CONFIRMED BY CUSTOMER',
                                    type: 'item',
                                    url: '/report/confirmed-order/confirmbycustumer'
                                },
                                {
                                    id: 'summary-order',
                                    title: 'SUMMARY ORDER CONFIRMED BY PRODUCT',
                                    type: 'item',
                                    url: '/report/confirmed-order/summary-order'
                                },
                                {
                                    id: 'confirmsummaryproduct',
                                    title: 'SUMMARY ORDER CONFIRMED BY PRODUCT',
                                    type: 'item',
                                    url: '/report/confirmed-order/confirmsummaryproduct'
                                },
                                {
                                    id: 'confirmreportorder',
                                    title: 'รายงานติดตามสถานะ ORDER',
                                    type: 'item',
                                    url: '/report/confirmed-order/confirmreportorder'
                                },
                                {
                                    id: 'confirmordersap',
                                    title: 'ตรวจสอบ ORDER - SAP',
                                    type: 'item',
                                    url: '/report/confirmed-order/confirmordersap'
                                },
                            ]
                        },
                        {
                            id: 'sys-order',
                            title: 'แจ้งยอดรับ',
                            type: 'item',
                            url: '/report/sys-order/sys-order-main'
                        },
                        {
                            id: 'sys-order-main',
                            title: 'แจ้งยอดรับ',
                            type: 'submain',
                            submenu: 'sys-order',
                            url: '/report/sys-order/sys-order-main',
                            children: [
                                {
                                    id: 'reportordersatatussys',
                                    title: 'รายงานติดตามสถานะ ORDER (SYS)',
                                    type: 'item',
                                    url: '/report/sys-order/reportordersatatussys'
                                },
                                {
                                    id: 'reportorderrece',
                                    title: 'รายงานแจ้งยอดรับสินค้า (ทั้งหมด) (SYS)',
                                    type: 'item',
                                    url: '/report/sys-order/reportorderrece'
                                },
                                {
                                    id: 'reportorderallsys',
                                    title: 'รายงานแจ้งยอดรับสินค้า (SYS)',
                                    type: 'item',
                                    url: '/report/sys-order/reportorderallsys'
                                },
                            ]
                        },
                        {
                            id: 'report-order',
                            title: 'รายงานการรับสินค้า',
                            type: 'item',
                            url: '/report/report-order/report-order-main'
                        },
                        {
                            id: 'report-order-main',
                            title: 'รายงานการรับสินค้า',
                            type: 'submain',
                            submenu: 'report-order',
                            url: '/report/report-order/report-order-main',
                            children: [
                                {
                                    id: 'reportordersys',
                                    title: 'รายการแจ้งรับสินค้า (SYS)',
                                    type: 'item',
                                    url: '/report/report-order/reportordersys'
                                },
                                {
                                    id: 'reportordersales',
                                    title: 'DAILY SALES',
                                    type: 'item',
                                    url: '/report/report-order/reportordersales'
                                },
                                {
                                    id: 'reportorderproduct',
                                    title: 'รายงานสรุปการรับสินค้าตามใบจัดรถ SYS',
                                    type: 'item',
                                    url: '/report/report-order/reportorderproduct'
                                },
                                {
                                    id: 'reportorderoff',
                                    title: 'รายงานใบจัดรถที่ไม่ออกตั๋ว',
                                    type: 'item',
                                    url: '/report/report-order/reportorderoff'
                                },
                                {
                                    id: 'reportordercutsys',
                                    title: 'รายงาน รายการตั๋วตัดฝาก SYS',
                                    type: 'item',
                                    url: '/report/report-order/reportordercutsys'
                                },
                            ]
                        },
                        {
                            id: 'report-stock',
                            title: 'รายงาน STOCK',
                            type: 'item',
                            url: '/report/report-stock/report-stock-main'
                        },
                        {
                            id: 'report-stock-main',
                            title: 'รายงาน STOCK',
                            type: 'submain',
                            submenu: 'report-stock',
                            url: '/report/report-stock/report-stock-main',
                            children: [
                                {
                                    id: 'reportstocksap',
                                    title: 'รายงานติดตามสถานะ ORDER (SYS)',
                                    type: 'item',
                                    url: '/report/report-stock/reportstocksap'
                                },
                            ]
                        }
                    ]
                },
                { //pn master
                    id: 'pn-master',
                    title: 'PN MASTER',
                    type: 'collapse',
                    icon: 'feather icon-book',
                    menu: 'pn-master',
                    children: [
                        {
                            id: 'product',
                            title: 'PRODUCT',
                            type: 'item',
                            url: '/pn-master/product/product-main'
                        },
                        {
                            id: 'product',
                            title: 'PRODUCT',
                            type: 'submain',
                            submenu: 'product',
                            url: '/pn-master/product/product-main',
                            children: [
                                {
                                    id: 'item-mast-fg-stock',
                                    title: 'ITEM_MAST (FGSTOCK)',
                                    type: 'item',
                                    url: '/pn-master/product/ItemMastFGSTOCK'
                                },
                                {
                                    id: 'ProductCodeOpenningMS126',
                                    title: 'ใบเปิดรหัสสินค้า (MS126-1)',
                                    type: 'item',
                                    url: '/pn-master/product/ProductCodeOpenningMS126'
                                },
                                {
                                    id: 'ConfirmOpenProductSBC',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สบช)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSBC'
                                },
                                {
                                    id: 'ConfirmOpenProductSSK',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สสค)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSSK'
                                },
                                {
                                    id: 'ConfirmOpenProductSSR2',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สสร2)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSSR2'
                                },
                                {
                                    id: 'ConfirmOpenProductSGT',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สกธ)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSGT'
                                },
                                {
                                    id: 'ConfirmOpenProductSSJ',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สสจ)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSSJ'
                                },
                                {
                                    id: 'ConfirmOpenProductSSR1',
                                    title: 'ยืนยันเปิดรหัสสินค้า (สสร1)',
                                    type: 'item',
                                    url: '/pn-master/product/ConfirmOpenProductSSR1'
                                }
                            ]
                        },
                        {
                            id: 'product-master',
                            title: 'PRODUCT MASTER',
                            type: 'item',
                            url: '/pn-master/product-master/product-master-main'
                        },
                        {
                            id: 'product-master',
                            title: 'PRODUCT MASTER',
                            type: 'submain',
                            submenu: 'product-master',
                            url: '/pn-master/product-master/product-master-main',
                            children: [
                                {
                                    id: 'section-master',
                                    title: 'SECTION MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/SectionMaster'
                                }, {
                                    id: 'size-master',
                                    title: 'SIZE MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/SizeMaster'
                                }, {
                                    id: 'grade-master',
                                    title: 'GRADE MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/GradeMaster'
                                }, {
                                    id: 'pn-status',
                                    title: 'PN STATUS',
                                    type: 'item',
                                    //url: '/pn-master/product-master/PnStatusSize'
                                    url: '/pn-master/product-master/PnStatus'
                                }, {
                                    id: 'pn-status-size',
                                    title: 'PN STATUS-SIZE.STD-OLD CODE',
                                    type: 'item',
                                    //url: '/pn-master/product-master/PnStatusSize'
                                    url: '/pn-master/product-master/PnStatusSize'
                                }, {
                                    id: 'storage-new-pn',
                                    title: 'STORAGE NEW PN',
                                    type: 'item',
                                    url: '/pn-master/product-master/StorageNewPn'
                                }, {
                                    id: 'sales-org-channel-new-pn',
                                    title: 'SALES ORG CHANNEL NEW PN',
                                    type: 'item',
                                    url: '/pn-master/product-master/SalesOrgChannelNewPn'
                                }, {
                                    id: 'section-hierarchy-master',
                                    title: 'SECTION HIERARCHY MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/SectionHierarchyMaster'
                                }, {
                                    id: 'standard-master',
                                    title: 'STANDARD MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/StandardMaster'
                                }, {
                                    id: 'standard-grade',
                                    title: 'STANDARD - GRADE',
                                    type: 'item',
                                    url: '/pn-master/product-master/StandardGrade'
                                }, {
                                    id: 'pn-status',
                                    title: 'PN STATUS-SIZE.STD-OLD CODE',
                                    type: 'item',
                                    url: '/pn-master/product-master/PNStatus'
                                }, {
                                    id: 'group-vc-master',
                                    title: 'GROUP VC MASTER',
                                    type: 'item',
                                    url: '/pn-master/product-master/GroupVcMaster'
                                }, {
                                    id: 'group-vc-cost',
                                    title: 'GROUP-VC COST',
                                    type: 'item',
                                    url: '/pn-master/product-master/GroupVcCost'
                                },
                            ]
                        },
                        {
                            id: 'other-master',
                            title: 'OTHER MASTER',
                            type: 'item',
                            url: '/pn-master/other-master/other-master-main'
                        },
                        {
                            id: 'other-master',
                            title: 'ORTER MASTER',
                            type: 'submain',
                            submenu: 'other-master',
                            url: '/pn-master/other-master/other-master-main',
                            children: [
                                {
                                    id: 'pcl-licence',
                                    title: 'PCL LICENCE (MY)',
                                    type: 'item',
                                    url: '/pn-master/other-master/PclLicence'
                                }, {
                                    id: 'nominal-size-harmonize',
                                    title: 'NOMINAL SIZE-HARMONIZE',
                                    type: 'item',
                                    url: '/pn-master/other-master/NominalSizeHarmonize'
                                }, {
                                    id: 'section-harmonize',
                                    title: 'SECTION-HARMONIZE',
                                    type: 'item',
                                    url: '/pn-master/other-master/SectionHarmonize'
                                }
                            ]
                        },
                        {
                            id: 'production-plan',
                            title: 'PRODUCTION PLAN',
                            type: 'item',
                            url: '/pn-master/production-plan/production-plan-main'
                        },
                        {
                            id: 'production-plan',
                            title: 'PRODUCTION PLAN',
                            type: 'submain',
                            submenu: 'production-plan',
                            url: '/pn-master/production-plan/production-plan-main',
                            children: [
                                {
                                    id: 'ExportYearlyProductionPlan',
                                    title: 'EXPORT YEARLY PRODUCTION PLAN',
                                    type: 'item',
                                    url: '/pn-master/production-plan/ExportYearlyProductionPlan'
                                }, {
                                    id: 'section-harmonize',
                                    title: 'SECTION-HARMONIZE',
                                    type: 'item',
                                    url: '/pn-master/production-plan/DomesticYearlyProductionPlan'
                                }
                            ]
                        },

                    ]
                },
                {
                    id: 'authorizes',
                    title: 'AUTHORIZES',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    menu: 'authorizes',
                    children: [
                        {
                            id: 'user-authorize',
                            title: 'USER & AUTHORIZE',
                            type: 'item',
                            url: '/authorizes/user-authorize/authorizes-main'
                        },
                        {
                            id: 'user-authorize',
                            title: 'USER & AUTHORIZE',
                            type: 'submain',
                            submenu: 'user-authorize',
                            url: '/authorizes/user-authorize/authorizes-main',
                            children: [
                                {
                                    id: 'sys-user-password',
                                    title: 'SYS USER & PASSWORD',
                                    type: 'item',
                                    url: '/authorizes/user-authorize/sys-user-password'
                                }, {
                                    id: 'user-master',
                                    title: 'USER MASTER',
                                    type: 'item',
                                    url: '/authorizes/user-authorize/user-master'
                                }, {
                                    id: 'customer-user-password',
                                    title: "CUSTOMER'S USER & PASSWORD",
                                    type: 'item',
                                    url: '/authorizes/user-authorize/customer-user-password'
                                }
                            ]
                        },
                        /*
                        {
                            id: 'xxx',
                            title: 'xxx',
                            type: 'item',
                            url: '/xxx'
                        },*/
                        {
                            id: 'e-mail',
                            title: 'E-MAIL',
                            type: 'item',
                            url: '/authorizes/e-mail/email-main'
                        },
                        {
                            id: 'e-mail',
                            title: 'E-MAIL',
                            type: 'submain',
                            submenu: 'e-mail',
                            url: '/authorizes/e-mail/email-main',
                            children: [
                                {
                                    id: 'auto-email',
                                    title: 'AUTO E-MAIL',
                                    type: 'item',
                                    url: '/authorizes/e-mail/auto-email'
                                }, {
                                    id: 'region-email',
                                    title: 'REGION E-MAIL',
                                    type: 'item',
                                    url: '/authorizes/e-mail/region-email'
                                }
                            ]
                        }
                        ,
                        {
                            id: 'set-screen',
                            title: 'SET SCREEN',
                            type: 'item',
                            url: '/authorizes/set-screen/set-screen-main'
                        },
                        {
                            id: 'set-screen',
                            title: 'SET SCREEN',
                            type: 'submain',
                            submenu: 'set-screen',
                            url: '/authorizes/set-screen/set-screen-main',
                            children: [
                                {
                                    id: 'eor-group-setup',
                                    title: "EOR GROUP'S SETUP",
                                    type: 'item',
                                    url: '/authorizes/set-screen/eor-group-setup'
                                }, {
                                    id: 'eor-page',
                                    title: 'EOR PAGE',
                                    type: 'item',
                                    url: '/authorizes/set-screen/eor-page'
                                }, {
                                    id: 'menu-application',
                                    title: 'MENU APPLICATION',
                                    type: 'item',
                                    url: '/authorizes/set-screen/menu-application'
                                }, {
                                    id: 'menu-group',
                                    title: 'MENU GROUP',
                                    type: 'item',
                                    url: '/authorizes/set-screen/menu-group'
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}