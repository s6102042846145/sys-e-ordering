export default {
    items: [
        {
            id: 'support',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-support',
            children: [
                {
                    id: 'favorite',
                    title: 'Favorite',
                    type: 'item',
                    url: '/favorite',
                    classes: 'nav-item',
                    icon: 'feather icon-home'
                },
                {
                    id: 'project',
                    title: 'PROJECT',
                    type: 'collapse',
                    icon: 'feather icon-box',
                    children: [
                        {
                            id: 'order-confirm',
                            title: 'ORDER CONFIRM',
                            type: 'item',
                            url: '/project/order-confirm'
                        },
                        {
                            id: 'quotation',
                            title: 'QUOTATION',
                            type: 'item',
                            url: '/project/quotation'
                        }
                        ,
                        {
                            id: 'receiving',
                            title: 'การรับสินค้า',
                            type: 'item',
                            url: '/project/receiving'
                        }
                    ]
                },
                {
                    id: 'pn-master',
                    title: 'PN MASTER',
                    type: 'collapse',
                    icon: 'feather icon-book',
                    children: [
                        
                        {
                            id: 'product',
                            title: 'PRODUCT',
                            type: 'item',
                            url: '/pn-master/product/product-main'
                        },
                        {
                            id: 'product-master',
                            title: 'PRODUCT MASTER',
                            type: 'item',
                            url: '/pn-master/product-master'
                        },
                        {
                            id: 'orter-master',
                            title: 'OTHER MASTER',
                            type: 'item',
                            url: '/pn-master/other-master'
                        },
                        {
                            id: 'product-plan',
                            title: 'PRODUCTION PLAN',
                            type: 'item',
                            url: '/pn-master/production-plan'
                        },
                    ]
                },
                {
                    id: 'authorizes',
                    title: 'AUTHORIZES',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    children: [
                        {
                            id: 'authorize',
                            title: 'USER & AUTHORIZE',
                            type: 'item',
                            url: '/authorizes/user-authorize'
                        },
                        {
                            id: 'e-mail',
                            title: 'E-MAIL',
                            type: 'item',
                            url: '/authorizes/e-mail'
                        }
                        ,
                        {
                            id: 'set-screen',
                            title: 'SET SCREEN',
                            type: 'item',
                            url: '/authorizes/set-screen'
                        }
                    ]
                }
            ]
        }
    ]
}