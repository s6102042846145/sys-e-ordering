import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> ORDER CONFIRM </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/domestic-invoice">
                                        ใบสั่งซื้อล่วงหน้า
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/check-domestic-order-confirm" >
                                        ตรวจสอบ DOMESTIC ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                      


                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/manage-order-fill-old" >
                                        MANAGE ORDER FILL เดิม
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/domestic-domestic-order-confirm" >
                                        DOMESTIC ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/domestic-po-sap-text" >
                                        PO SAP TEXT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/domestic-order-confirm-show" >
                                        แสดง DOMESTIC ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-order-confirm/customer-po-sap-text" >
                                        PO SAP TEXT (ลูกค้า)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;