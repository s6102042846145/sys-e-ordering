import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";



import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "Customer" })
        } else {
            this.setState({ setTitleModal: "Customer" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
               
                <Row>
                    <Col>
                    <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SALES ORG.CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CHANNEL CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>COUNTRY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CUST.CODE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>

                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GROUP</th>
                                                <th>COUNTRY</th>
                                                <th>CODE</th>
                                                <th>NAME</th>
                                                <th>ABBRV.NAME</th>
                                                <th>NAME (ENG.)</th>
                                                <th>ABBRV.NAME (ENG.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="cursor" onClick={() => this.setState({ isModal: false })}>
                                                <td >1</td>
                                                <td >0180_10&nbsp;</td>
                                                <td >TH
                                                    &nbsp;-&nbsp;Thailand
                                                    &nbsp;</td>
                                                <td >3001574&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>
                        
                        <MainCard isOption title="SEARCH">
                        <Row>
                            <Col sm={12}>
                                <Form>
                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Order Year.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Order Month.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>รอบที่</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">All</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>เลขที่ใบจัดรถ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ORDER NO.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>PO.REF</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ทะเบียนรถ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อคนขับรถ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ประเภทเอกสาร</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="P">ตั๋วจัดจ่ายล่วงหน้า</option><option value="S">ธรรมดา</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>สถานที่รับสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="4951">BDC-บ้านบึง</option><option value="4941">SOL-SYS Solution</option><option value="4921">SR-ศรีราชา</option><option value="4911">SYS1-ระยอง1</option><option value="4931">SYS2-ระยอง2</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">All</option><option value="CFR">CFR - Costs and freight</option><option value="EXW">EXW - Ex works</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>รหัส ซื้อสำหรับ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อ ซื้อสำหรับ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="NONE">None</option><option value="3001906"> C.R. สยามภัณฑ์</option><option value="3001806"> โพคคาอินดัสตรี้ส์</option><option value="3001895"> ค.เคหะภัณฑ์</option><option value="3001456"> ช.โลหะกิจ</option><option value="3001468"> ช.พานิช</option><option value="3001619"> บี.เค.เค.</option><option value="3001942"> ลี้ไทยมุ้ย</option><option value="3001037">14 ซีเมนต์</option><option value="3007200">2S เมทัล</option><option value="3001020">39 แมททีเรียล</option><option value="3005232">CMC</option><option value="3005243">CORUS</option><option value="T000036">GLOBAL ALLIANZ(UK)</option><option value="3005303">KAWASHO</option><option value="3005246">MARUBENI</option><option value="3005248">METAL ONE HK</option><option value="3005309">MITSUI</option><option value="3002017">MITSUI JP</option><option value="T000037">MITSUI MY</option><option value="3005310">MITSUI SG</option><option value="3005311">NEWCO</option><option value="3005312">NOMURA</option><option value="3005330">SCT VN</option><option value="3005336">SHIMIZU</option><option value="3005320">STARPOLY</option><option value="3005242">STEMCOR</option><option value="3005317">TRADE ARBED</option><option value="9999991">WTP</option><option value="3001641">เกรียงกิจ (ภาคใต้)</option><option value="3001730">เกรียงกิจแผ่นพื้น</option><option value="3001736">เกรียงกิจผลิตภัณฑ์คอ</option><option value="3001873">เกรียงสินวัฒนา</option><option value="3012060">เกษตรผล</option><option value="3014260">เกษม</option><option value="3001665">เกียงเซ้ง</option><option value="3001358">เกียรติวุฒิเจริญชัย</option><option value="3001061">เขื่อนเจ้าพระยา</option><option value="3001932">เค พี เอส อลูมินั่ม</option><option value="3001626">เค.เค.ซี.วัสดุภัณฑ์ค</option><option value="3001929">เค.เอ็ม.ซี.อลูมิเนีย</option><option value="3001593">เค.เอส.เอ็นจิเนียริ่</option><option value="3001600">เค.คอมเมอร์เชียล แอน</option><option value="3001635">เค.บี.อิฐทนไฟ</option><option value="3001153">เค.วี.พี.เซ็นเตอร์</option><option value="3001792">เคนโซ่ (ประเทศไทย)</option><option value="3000969">เครื่องก่อสร้าง</option><option value="3004355">เคหะเซนเตอร์</option><option value="3001002">เคหะเทพทวี</option><option value="3001743">เคหะภัณฑ์มาร์เก็ตติ้</option><option value="3001556">เงินขุมทอง</option><option value="3001822">เจ แอนด์ เอ สตีล</option><option value="3001716">เจ แอนด์ เอ อุตสาหกร</option><option value="3001367">เจ็งฮงค้าไม้</option><option value="3001301">เจตนะธรรมค้าไม้</option><option value="3001120">เจริญเคหะ</option><option value="3001079">เจริญโสภณ</option><option value="3001122">เจริญใหญ่พาณิชย์</option><option value="3001495">เจริญไทยปัตตานี</option><option value="3001221">เจริญกิจพานิช</option><option value="3000984">เจริญชัยค้าไม้</option><option value="3001417">เจริญธุรกิจ</option><option value="3001169">เจริญผลท่าม่วงก่อสร้</option><option value="3001909">เจริญผลสุขภัณฑ์</option><option value="3001307">เจริญพัฒนา</option><option value="3001426">เจริญพันธ์</option><option value="3001455">เจริญพันธ์-พงศ์พันธ์</option><option value="3001195">เจริญภัณฑ์พาณิชย์</option><option value="3001164">เจริญภัณฑ์สามชุก</option><option value="3001896">เจริญสุขภัณฑ์กำแพงเพ</option><option value="3001889">เจษฎ์พัฒนา</option><option value="3001425">เจียบเซ้งทุ่งสง</option><option value="3001111">เจียฮะฮวด</option><option value="3001347">เฉลิมชัยพานิช'เลย'</option><option value="3020471">เชียงใหม่</option><option value="3001282">เชียงใหม่เพ็ญภัณฑ์</option><option value="3001279">เชียงใหม่ชัยวัฒน์</option><option value="3001281">เชียงใหม่ตังอู๋เซ้ง</option><option value="3001284">เชียงใหม่ธาราภัณฑ์</option><option value="3001897">เชียงใหม่ลำปางปาเก้ส</option><option value="3001280">เชียงใหม่วีระกิจ</option><option value="3001277">เชียงใหม่วีระพานิช</option><option value="3001899">เชียงใหม่อริยกิจ</option><option value="3001288">เชียงคำกิจเสรี</option><option value="3001265">เชียงรายเอกชัยค้าไม้</option><option value="3001268">เชียงรายกิจชัยเจริญ</option><option value="3001772">เชียงรายคอนกรีต</option><option value="3001267">เชียงรายพิริยะกิจ</option><option value="3001433">เชียรใหญ่</option><option value="3001006">เซ่งเฮงฮวดวัสดุก่อสร</option><option value="3016606">เซ็นเตอร์รุ่งเรือง</option><option value="3001805">เซ็นจูรี่คราฟท์</option><option value="3001342">เซ็นจูรี่บึงกาฬ</option><option value="3001758">เซ็นทรัลคอนกรีต</option><option value="3001916">เซ็นทรัลสุขภัณฑ์</option><option value="3001599">เซอร์คิวล่าร์ ดีเวลอ</option><option value="3001216">เซี้ยงวัสดุก่อสร้าง</option><option value="3001885">เดคคอร์มาร์ท</option><option value="3001496">เด่นไทย</option><option value="3001591">เด่นชัยสุรินทร์</option><option value="3001738">เด็มโก้</option><option value="3001686">เดอะโมเดอร์นกรุ๊ปเรี</option><option value="3001357">เต็งจิตติ</option><option value="3001368">เตียไคฮวด เคหะภัณฑ์</option><option value="3001391">เตียคุนฮะร้อยเอ็ด</option><option value="3001291">เตียตังฮง</option><option value="3001855">เตียอิ้วเลี้ยง ก.ม.9</option><option value="3012500">เตียฮงฮะ</option><option value="3005514">เตียฮงฮะ-O</option><option value="3001620">เทคฟอร์ม</option><option value="3001226">เทพวรชัย</option><option value="3001259">เทิงทวีทรัพย์</option><option value="3001720">เทียบทอง</option><option value="3001038">เทียมเอียะ</option><option value="3023132">เทียรประเสริฐ</option><option value="3001602">เบ็ญจา</option><option value="3018025">เบสท์ สตีล</option><option value="3014641">เบสท์ สตีล Re-export</option><option value="3001862">เปงฮอง</option><option value="3006031">เป็นเอก</option><option value="3001057">เป็นเอกสิงห์บุรี</option><option value="3000997">เพชรเกษมซีเมนต์</option><option value="3001184">เพชรเอเชียค้าวัตถุ</option><option value="3001177">เพชรบุรีวัชรกิจ</option><option value="3001183">เพชรพัฒนาวัสดุก่อสร้</option><option value="3001203">เพ็ชรพิชัย</option><option value="3001737">เพาเวอร์-พี</option><option value="3001076">เพิ่มทรัพย์</option><option value="3007091">เพื่อนบ้านเพื่อนคุณ</option><option value="3001671">เฟิสท์ ฟังค์ชั่น</option><option value="3001820">เมเจอร์อุตสาหกรรมไทย</option><option value="3001351">เมโทร (2533)</option><option value="3001218">เม่งฮงหลี</option><option value="3001445">เม้งฮวดนครศรีฯ</option><option value="3001135">เมตตาค้าไม้</option><option value="3001668">เมทัลลิค ดีไซน์</option><option value="3001015">เมืองทองค้าวัสดุก่อส</option><option value="3001657">เรพแพคคอนสตรัคชั่น</option><option value="3001363">เรืองสุขพาณิชย์</option><option value="3001865">เรือนขวัญ เซรามิค</option><option value="3001406">เล้าจิ้นเงี้ยบ</option><option value="3001876">เวิลด์ ฮาร์ดแวร์</option><option value="3001266">เวียงพานทวีภัณฑ์</option><option value="3001119">เสมียนเหงี่ยม</option><option value="000005">เสริมสิริ</option><option value="3001913">เสริมสุขภัณฑ์สุไหงโก</option><option value="3031513">เหล็กดี</option><option value="3017451">เหล็กทรัพย์โสภณ</option><option value="3001029">เอ เอ็ม เสรี กรุ๊ป 1</option><option value="3001484">เอ.สยามกรุ๊ปหาดใหญ่</option><option value="3001139">เอเซียค้าไม้และวัตถุ</option><option value="3001739">เอเซียสตีลแอนด์ไพล์</option><option value="3000999">เอเอ็นซี ค้าวัตถุก่อ</option><option value="3001790">เอแอนด์จี เอ็นจิเนีย</option><option value="3001127">เอกภัณฑ์ซีเมนต์</option><option value="3001886">เอกสิทธิ์ภัณฑ์</option><option value="3001733">เอนจิเนียริ่งคอนกรีต</option><option value="3001043">เอ็นทีเอ็น อินเตอร์เ</option><option value="3001651">เอ็ม.เอส.อาร์.กรุ๊ป</option><option value="3001580">เอ็ม.ซี.คอนสตรัคชั่น</option><option value="3001727">เอ็มแพคแผ่นคอนกรีต</option><option value="3001678">เอ็มจีเทคกรุ๊ฟ</option><option value="3001841">เอส แอล ค้าวัสดุ</option><option value="3001506">เอส.ยูฟรีเทรด</option><option value="3001925">เอส.วาย เซรามิคส์</option><option value="3001837">เอสวี.เทอรัชโซ่ 1999</option><option value="3001459">เอี้ยท่งฮวด</option><option value="3001234">เอี๊ยบคิมง้วนจั่น</option><option value="3001110">เอี๊ยบซิ้นเชียง</option><option value="3001200">เอี่ยมฮวดเส็งเสียงกี</option><option value="3001097">เอี๊ยวเซ่งฮวด</option><option value="3001356">เอี่ยวหลีจั่น</option><option value="3001085">เอี๊ยะเฮงหลีค้าวัสดุ</option><option value="3001718">เฮงเจริญโลหะภัณฑ์</option><option value="3001308">เฮงทวี โฮมเซ็นเตอร์</option><option value="3000956">เฮงย่งเซ้ง</option><option value="3000974">เฮงย่งสูงหลานหลวง</option><option value="3001691">เฮอริเทจสโตนแวร์</option><option value="3001449">เฮียบหิ้นพาณิชย์</option><option value="3001557">แกมมอน ดีเวลลอปเมนท์</option><option value="3001863">แกรนด์ โฮมมาร์ท</option><option value="3001880">แกรนด์ โฮมมาร์ท</option><option value="3001333">แจ่มดุสิตค้าไม้</option><option value="3001559">แซมคอน</option><option value="3001937">แทรนดาร์</option><option value="3001662">แปซิฟิกและโอเรียน</option><option value="3000955">แพนกรุงเทพ</option><option value="3001241">แพร่ก่อสร้าง</option><option value="3001474">แม่ขรีค้าวัสดุ</option><option value="3001262">แม่จันค้าวัสดุ</option><option value="3001606">แมทคอน</option><option value="3001721">แมนสตีลแฟคทอรี่</option><option value="3001924">แมนสุขภัณฑ์</option><option value="3001933">แม่น้ำมิทอลซัพพลาย</option><option value="3001285">แม่ฮ่องสอนก่อสร้าง</option><option value="3001683">แลนด์แอนด์เฮ้าส์</option><option value="3001187">แสงเจริญรวมวัสดุ</option><option value="3001130">แสงเพชรวิศวภัณฑ์ มหา</option><option value="3001751">แสงไทยกรุ๊ป</option><option value="3001264">แสงไพบูลย์เชียงราย</option><option value="3001453">แสงชัยพาณิชย์</option><option value="3001486">แสงชัยหาดใหญ่</option><option value="3013386">แสงทองโฮมมาร์ท</option><option value="3005787">แสงทองวัฒนกิจ</option><option value="3001750">แสงทิพย์</option><option value="3001469">แสงพาณิชย์</option><option value="3001214">แสงศิริบริการ</option><option value="3000960">แสงอรุณสยาม</option><option value="3001160">แสงอู่ทองเคหภัณฑ์</option><option value="3001601">แสตนคอน</option><option value="3001904">แอคทีฟ เซรามิค เซ็นเ</option><option value="3001807">แอดวานซ์ เซรามิค อิน</option><option value="3001936">แอ็ดว๊านซ์กล๊าส</option><option value="3001667">แอ๊ดวานซ์ซิลลิ่ง</option><option value="3001596">แอล แทป เอ็นเตอร์ไพร</option><option value="3001709">แอลเอสอุตสาหกรรม</option><option value="3001310">โคราชแสงชัยภัณฑ์ เอส</option><option value="3001852">โคราชแสงทวีคลังกระจก</option><option value="3001297">โคราชรวมวัสดุ</option><option value="3001194">โง้วง่วนไถ่</option><option value="3001191">โง้วง่วนฮะนครสวรรค์</option><option value="3001100">โง้วจิ้นเชียง</option><option value="3001341">โง้วจี่เฮง</option><option value="3006749">โชคบุญมา</option><option value="3005979">โชคบูญมา</option><option value="3001062">โชคพนากิจวัสดุภัณฑ์</option><option value="3001399">โชคพูลผลคอนกรีต</option><option value="3001180">โชควิชัยค้าวัสดุ</option><option value="3001714">โชคสุขใจ</option><option value="3001664">โตเคนอินทีเรียร์แอนด</option><option value="3000982">โต้วฮั่วเฮง</option><option value="3001639">โนโวปลาสต์</option><option value="000009">โปรเจค</option><option value="3001905">โพธิ์ทองเซรามิค-สุขภ</option><option value="3001868">โมเดิร์นบาธ</option><option value="3001649">โรแยลไทล์ฟิกซ์</option><option value="3001711">โรแยลคลีฟ บีช โฮเต็ล</option><option value="3001502">โรงไม้เอี๊ยะง้วนตันห</option><option value="3001497">โรงไม้ทุเรียนยะลา</option><option value="3001046">โรงค้าไม้กือหลี</option><option value="3001064">โรซาร์เทรดดิ้ง</option><option value="3001956">โลหะเจริญ</option><option value="000007">โลหะไพศาล</option><option value="3001331">โลหะกิจอุดร</option><option value="3001926">โสภณพรรณ</option><option value="3001710">โอเอไอ พร็อพเพอร์ตี้</option><option value="3001927">โอฬารอลูมินั่ม</option><option value="3006196">โฮมมาร์ท</option><option value="3001124">โฮมวัสดุภัณฑ์</option><option value="3001742">ไกรกาบแก้ววัสดุภัณฑ์</option><option value="3000985">ไชยมงคล โฮมมาร์ท</option><option value="3000952">ไชยฮวดค้าวัสด</option><option value="3001423">ไชยาค้าวัสดุ</option><option value="3001466">ไถ่เชียงวัสดุก่อสร้า</option><option value="3020858">ไทเกอร์</option><option value="3025830">ไทเกอร์</option><option value="3001566">ไทพิพัฒน์</option><option value="3001919">ไทยเซรามิค สุขภัณฑ์</option><option value="3001661">ไทยเรืองอุตสาหกรรม</option><option value="3001623">ไทยแอ๊ดวานด์ แพนเนล</option><option value="3001213">ไทยโชคดี</option><option value="3001818">ไทยไวร์โพรดัคท์</option><option value="3013794">ไทยถาวรพาณิชย์ฯแพร่</option><option value="3001844">ไทยทอง อีสเทิร์น อลู</option><option value="3001847">ไทยทองอีสเทิร์น อลูม</option><option value="3001848">ไทยทองอีสเทิร์นอลูมิ</option><option value="3001131">ไทยนำวัสดุก่อสร้าง</option><option value="3001755">ไทยนิปปอนคอนกรีต</option><option value="3034642">ไทยนิยม สตีล</option><option value="3001179">ไทยนิยมเทรดดิ้ง</option><option value="3001747">ไทยประดับภัณฑ์</option><option value="3001148">ไทยประสิทธิ์เคหะภัณฑ</option><option value="3001231">ไทยพิทักษ์</option><option value="3001539">ไทยพิพัฒน์</option><option value="3001318">ไทยพิพัฒน์วัสดุก่อสร</option><option value="3001624">ไทยพีค่อนและอุตสาหกร</option><option value="3001359">ไทยรุ่งเรืองรวมวัสดุ</option><option value="3001235">ไทยวัฒนาแม่สอด</option><option value="3001860">ไทยวิวัฒน์สุขภัณฑ์</option><option value="3001706">ไทยศิริอาร์ตไชน่า</option><option value="3001337">ไทยศุภการอุดรธานี</option><option value="3001422">ไทยสงวนค้าวัสดุ</option><option value="3001374">ไทยสงวนอุทุมพรก่อสร้</option><option value="3001040">ไทยสมบูรณ์หินกอง</option><option value="3001713">ไทยสะเปเชียลไวร์</option><option value="3001819">ไทยสินเหล็ก</option><option value="3001494">ไทยสิริปัตตานี</option><option value="3001410">ไทยอุดมค้าไม้และก่อส</option><option value="3001500">ไทยอุปกรณ์ก่อสร้างสุ</option><option value="3001815">ไทยฮั่วฮง</option><option value="3000987">ไทยฮุค</option><option value="3008441">ไทยฮุค</option><option value="3001839">ไพโรจน์ สุขภัณฑ์</option><option value="3001321">ไพรัชเคหภัณฑ์</option><option value="3001697">ไฟน์ อาร์ต เซรามิค</option><option value="3001054">ไม้ไทยลพบุรี</option><option value="3001604">ไมวาน</option><option value="3001138">ไม้อ้อมน้อยพาณิชย์</option><option value="3001672">ไอ.เอฟ.เดคคอเรชั่น</option><option value="3001835">ไฮเทคไทยคอนกรีต</option><option value="3001225">ก โลหะกิจ</option><option value="3001845">ก.กระจกอลูมินั่มซัพพ</option><option value="3001077">ก.ทวีทรัพย์</option><option value="3001109">ก.ภัทร</option><option value="3001795">กนกเฟอร์นิเจอร์ แอนด</option><option value="3001091">กบินทร์ ส.ทวีภัณฑ์</option><option value="3001794">กรีนแอคส์</option><option value="3000957">กรุงเกษมวัสดุภัณฑ์</option><option value="3001628">กรุงเทพคอนกรีต</option><option value="3000964">กรุงเทพง่วนเฮงล้ง</option><option value="3001814">กรุงเทพตาปูและลวด</option><option value="3001856">กรุงเทพพัฒนภัณฑ์</option><option value="3001756">กรุงเทพหินอ่อนเทียม</option><option value="3001567">กรุงธนเอนยิเนียร์</option><option value="3001798">กรุ๊ป 67 ซัพพลายส์</option><option value="3001075">กฤษณ์พัฒนา</option><option value="3001348">กลศรีวัสดุก่อสร้าง</option><option value="3001499">กวงเล้ง</option><option value="3006245">กวงหลี</option><option value="3001212">กวางเต็กล้ง</option><option value="3001824">กวีวรรณผลิตภัณฑ์คอนก</option><option value="3001168">กอวัฒนาท่าเรือ</option><option value="3001481">กังส์แสงศรีสวัสดิ์</option><option value="3001892">กังส์แสงสุขภัณฑ์</option><option value="3001669">กัลป์เกษม คอนสตรัคชั</option><option value="3001393">กาฬสินธุ์ก่อสร้าง</option><option value="3022912">กำแพงเพชรง่วนฮงหลี</option><option value="3001617">กำจรกิจ</option><option value="3001610">กำจรกิจก่อสร้าง</option><option value="3005890">กิจเจริญ</option><option value="3001384">กิจเจริญอุบล</option><option value="3001074">กิจเพิ่มพูลค้าวัสดุก</option><option value="3001571">กิจการร่วมค้า SC</option><option value="3001614">กิจการร่วมค้า บีซีเค</option><option value="3001581">กิจการร่วมค้า อินเตอ</option><option value="3001621">กิจการร่วมค้ารัชภณ</option><option value="3001380">กิจตรงอุบล</option><option value="3001376">กิจทวีสิน(แซเยี้ยง)</option><option value="3005934">กิจภัณฑ์</option><option value="3013487">กิจรุ่งเรืองค้าเหล็ก</option><option value="3001175">กิจวัฒนาราชบุรี</option><option value="3000990">กิจศิริซีเมนต์</option><option value="3006098">กิจสยาม</option><option value="3000959">กิจสินพัฒนา</option><option value="3001762">กิตติพงษ์ไมน์นิ่ง</option><option value="3001411">กิตติภัณฑ์เทรดดิ้ง</option><option value="3000986">กิตภัณฑ์</option><option value="3001059">กิติไพศาล</option><option value="3001222">กิมซ่งหลี</option><option value="3001941">กิมฮงเส็ง</option><option value="3001592">กู้ชัย</option><option value="3001255">ขจรเกียรติ</option><option value="3001598">ขจรการโยธาหาดใหญ่</option><option value="3001327">ขอนแก่นชัยวิวัฒน์</option><option value="3001328">ขอนแก่นทวีภัณฑ์วัสดุ</option><option value="3001290">คงสงวน</option><option value="3001698">คราวน์ เซรามิคส์</option><option value="3001802">ครีเอทีฟ คร๊าฟ (ประเ</option><option value="3001398">คลังไม้สกล</option><option value="3001145">คลังซีเมนต์</option><option value="3001684">ควอลิตี้เฮ้าส์</option><option value="3001731">คอนกรีตไลน์</option><option value="3001780">คอนกรีตชวนอยู่มั่นคง</option><option value="3001067">คอนสแมทวัสดุภัณฑ์</option><option value="3001575">คอนส์อินเตอร์เทรด</option><option value="3001699">คาสเดย์</option><option value="3001702">คาสเดย์(ขอนแก่น)</option><option value="3001350">คุณศิริ</option><option value="3001113">คูเซี้ยงล้ง</option><option value="3001257">คูเส็งกี่</option><option value="3001475">คูขุดค้าวัสดุก่อสร้า</option><option value="3001371">คูย่งฮวด (1995)</option><option value="3001215">งามศิลป์วัสดุ</option><option value="3001871">จงเจริญสุขภัณฑ์</option><option value="3001769">จงรักษ์ก่อสร้าง</option><option value="3001156">จวนสงวนพานิชนครปฐม</option><option value="3008436">จอมทอง</option><option value="3001300">จอหอค้าวัสดุก่อสร้าง</option><option value="3001316">จัตุรัสรุ่งเรืองพาณิ</option><option value="3001373">จันทร์มณีเลิศก่อสร้า</option><option value="3001352">จั่วเซ้งยโสธร</option><option value="3001157">จำหน่ายวัตถุก่อสร้าง</option><option value="3007454">จำหน่ายวัตถุก่อสร้าง</option><option value="3001274">จินทนาพร</option><option value="3001188">จิรเคหะภัณฑ์ ซินฮั้ว</option><option value="3001017">จิระเคหะภัณฑ์</option><option value="3001239">จิวเฮงเส็ง</option><option value="3001403">จุรีภัณฑ์ค้าวัสดุ</option><option value="3001392">ฉั่วจั่วกี่ร้อยเอ็ด</option><option value="3001003">ฉิมพลีค้าวัสดุก่อสร้</option><option value="3026006">ช.ม.วิทยา</option><option value="3013614">ช.รุ่งโรจน์</option><option value="3001537">ช.วีระวิศวการ</option><option value="3001101">ช.วุฒิกร</option><option value="3000961">ช.สมเจริญ</option><option value="3000994">ช.ฮกเซ้งวัสดุก่อสร้า</option><option value="3001415">ชนะพาณิชย์</option><option value="3000992">ชลทรัพย์วัสดุก่อสร้า</option><option value="3001105">ชลบุรีเกียรติเจริญ</option><option value="3023403">ชลบุรีไทยสวัสดิ์</option><option value="3001096">ชลบุรีอึ้งย่งล้ง</option><option value="3001293">ชลรุ่งเรืองนครราชสีม</option><option value="3001573">ชลัมเบอร์เจอร์โอเวอร</option><option value="3001088">ชลีทิพย์ซีเมนต์</option><option value="3001361">ชวลิตรวมวัสด</option><option value="3001396">ชัยเจริญ</option><option value="3001081">ชัยเจริญแปดริ้ว</option><option value="3001068">ชัยทวีโชคค้าวัสดุก่อ</option><option value="3001561">ชัยนันท์ค้าวัตถุก่อส</option><option value="3001060">ชัยพฤกษ์ค้าวัสดุก่อส</option><option value="3001311">ชัยภูมิกิตติพาณิชย์</option><option value="3001589">ชัยภูมิย่งเซ่งฮวด</option><option value="3001314">ชัยภูมิรุ่งเรือง</option><option value="3001016">ชัยวัฒน์ค้าซีเมนต์</option><option value="3001834">ชัยวัฒนา วัสดุภัณฑ์</option><option value="3001416">ชัยสมพรค้าวัสด</option><option value="3038217">ชัยสุภณ</option><option value="3001136">ช้างหยก</option><option value="3001230">ชาตรีพัฒนภัณฑ์</option><option value="3001248">ชินสุขเสริม</option><option value="3001470">ชีย้งเฮง</option><option value="3001286">ชื่นชูไพรก่อสร้าง</option><option value="3001220">ชุณหทัยพิษณุโลก</option><option value="3001178">ชุณห์วัฒนา</option><option value="000004">ชุมเสริม</option><option value="3001326">ชุมแพก่อสร้าง</option><option value="3001261">ชุมแสงชัย</option><option value="3016465">ชุมพรตั้งฮวดหลี</option><option value="3001467">ชุมพรรวมวัสดุก่อสร้า</option><option value="3001247">ชูชีพสุโกศล</option><option value="3001302">ซ่งจี่ค้าไม้</option><option value="3001607">ซวอพส์</option><option value="3001583">ซอยล์กรีต เทคโนโลยี</option><option value="3001694">ซันเซอรา</option><option value="3001803">ซันวาเซอรามิค</option><option value="3001745">ซิกม่าคอนกรีต</option><option value="3017180">ซิตี้ สตีล</option><option value="3001397">ซินเซิน</option><option value="3001701">ซิ่นเอี่ยวเซรามิคศิล</option><option value="3001401">ซินซินสกลนคร</option><option value="3001695">ซินหยิงพ๊อตเตอรี่</option><option value="3001752">ซี</option><option value="3001877">ซี เอส เอ กรุ๊ป</option><option value="000006">ซี.เอส.เอช.</option><option value="3001603">ซี.อี.เอส.</option><option value="3039627">ซีเคเอสที</option><option value="3001146">ซีเมนต์ไทยเจริญ</option><option value="3001741">ซี-โพส</option><option value="3001746">ซีคอน จก</option><option value="3001615">ซีวิลเอนจีเนียริง</option><option value="3001448">ซุ่นเชียงโคกกลอย</option><option value="3001167">ซุ่นเส็งก่อสร้าง</option><option value="3001366">ซุ่ยเฮงเส็ง</option><option value="3001878">ซุ่ยเฮงเส็งเซรามิค</option><option value="3001744">ซูมิโตโมอีเล็คตริกซิ</option><option value="3001791">ณัฐกิจอลูมิเนียม</option><option value="3001260">ดวงแสงทอง</option><option value="3001810">ดัลไมสัน (ประเทศไทย)</option><option value="3001013">ดำรงค์วัฒนาค้าวัสดุ</option><option value="3001921">ดี.พี.เซรามิคส์</option><option value="3045176">ดีเค</option><option value="3028537">ดีเดย์ โลหะกิจ</option><option value="3001132">ดีโก้ วัสดุภัณฑ์</option><option value="3001162">ต.แสงอุปกรณ์สุพรรณบุ</option><option value="3001427">ต.ไทยเจริญการค้านครศ</option><option value="3001023">ต.รุ่งเรืองสิน</option><option value="3004294">ต.วัฒนาเทรด</option><option value="3001402">ต.วานิชวัสดุภัณฑ์</option><option value="3000973">ต.วิวัฒน์ค้าวัสดุภัณ</option><option value="3001479">ตงเฮงหาดใหญ่</option><option value="3001804">ตงฟง เซรามิค</option><option value="3001771">ตระกูลวิศวกร</option><option value="3000958">ตระกูลสุขค้าซีเมนต์</option><option value="3001125">ตราดเกษมศานติ์ค้าวัส</option><option value="3000951">ตรีเพชร</option><option value="3001452">ตรีพันธ์พาณิชย์</option><option value="3001930">ตอยงเกียรติกลาส</option><option value="3001171">ตั้งเจี๊ยเฮง</option><option value="3001197">ตั้งเล่งเส็งตาคลี</option><option value="3001329">ตั้งกิมเซ้งอุดร</option><option value="3001123">ตั้งง่วนเซ้งตราด</option><option value="3001102">ตันติวัฒน์</option><option value="3001233">ตากค้าซิเมนต์</option><option value="3001198">ตาคลีวัสดุ</option><option value="3001199">ตึกน้ำเงิน</option><option value="3028963">ตึกน้ำเงินสตีล</option><option value="3001447">ทรงชัยวัสดุภัณฑ์</option><option value="3001140">ทรวงพาณิชย์</option><option value="3001154">ทรัพย์ทวีวัฒน์</option><option value="3013856">ทรัพย์นิยม</option><option value="3018004">ทรัพย์นิยม</option><option value="3001729">ทรัพย์มณีคอนสตรัคชั่</option><option value="3001269">ทรัพย์สินรุ่งเรืองพา</option><option value="3001313">ทรายทองก่อสร้าง</option><option value="3001283">ทวิภัณฑ์ฮอด</option><option value="3001377">ทวีกิจเดชอุดม</option><option value="3001369">ทวีกิจก่อสร้างสุรินท</option><option value="3001935">ทวีกิจค้าไม้</option><option value="3001634">ทวีคูณคอนกรีต</option><option value="3001915">ทวีรุ่งยนตรกิจ (สตึก</option><option value="3001063">ทวีวัฒนาอุทัยธานี</option><option value="3001823">ทองทรัพย์เจริญอุตสาห</option><option value="3001084">ทองทวีซีเมนต์</option><option value="3001108">ทองทิตต์เจริญ</option><option value="3001692">ทองประเสริฐศิลป์ชัย</option><option value="3001779">ทักษิณคอนกรีต</option><option value="3001644">ทักษิณคอนกรีต (มหาชน</option><option value="3001645">ทักษิณคอนกรีต (มหาชน</option><option value="3001646">ทักษิณคอนกรีต (มหาชน</option><option value="3001647">ทักษิณคอนกรีต (มหาชน</option><option value="3001648">ทักษิณคอนกรีต (มหาชน</option><option value="3001850">ทักษิณทาชัย</option><option value="3003842">ทักษิณา</option><option value="3001450">ท่าเรือคอนกรีต</option><option value="3001343">ท่าบ่อค้าปูนซิเมนต์</option><option value="3001344">ท่าบ่อวิเชียรการค้า</option><option value="3001558">ทิพากร</option><option value="3001190">ที ซี คอนกรีตแลนด์</option><option value="3001732">ที.ซี.ซี.คอนกรีตแอนด</option><option value="3001722">ที.พี.เอ็น.อินดัสเตร</option><option value="3001643">ที.พี.ซี. คอนกรีต โป</option><option value="3001778">ที.พี.ซี.คอนกรีต</option><option value="3007503">ทีเอ็มที</option><option value="3001560">ธ เชี่ยวเชิงชล</option><option value="3001922">ธงแสง</option><option value="3001133">ธงชัยโฮมมาร์ท</option><option value="3001555">ธนไพสรรก่อสร้าง</option><option value="3001767">ธนพรรษ์คอนกรีต</option><option value="3001881">ธนรุ่งวัสดุภัณฑ์</option><option value="3001800">ธนา แอนด์ นา</option><option value="3001107">ธนาภัณฑ์ค้าวัสดุ</option><option value="3001788">ธนาวรรณ 3 วัสดุภัณฑ์</option><option value="3001853">ธนาวรรณ 3 วัสดุภัณฑ์</option><option value="3001676">ธนาศิศ เทรดดิ้ง</option><option value="3001951">ธนาสาร</option><option value="3001143">ธราธรวัสดุก่อสร้าง</option><option value="3001462">ธวัชวัตถุก่อสร้าง</option><option value="3000983">ธัญญโรจน์ค้าวัสดุ</option><option value="3001754">ธำรงค์ชัย</option><option value="3001011">ธำรงวัสดุภัณฑ์</option><option value="3007460">ธีรภาดา</option><option value="3001078">ธีระกิจค้าวัสดุ</option><option value="3001446">ธีระพลพานิชพังงา</option><option value="3001193">นครเมืองพล</option><option value="3001151">นครชัยศรี</option><option value="3001595">นครธนาสิน</option><option value="3001458">นครพาณิชย์ตรัง</option><option value="3001437">นครภักดีวัสดุก่อสร้า</option><option value="3001304">นครราชสีมาแสงไทยก่อส</option><option value="3001436">นครวรพันธ์</option><option value="3001192">นครสวรรค์เต็กเซ่งฮง</option><option value="3001627">นครหลวงวัตถุก่อสร้าง</option><option value="3001622">นครหลวงวัสดุภัณฑ์</option><option value="3001434">นครอึ้งฮั่วหลี</option><option value="3001910">นครินทร์เซรามิค</option><option value="3001010">นนท์ง้วนเฮงล้ง</option><option value="3001039">นนทวรรณ</option><option value="3003640">นพเก้าโฮมมาร์ท</option><option value="3001372">นพเก้ารุ่งโรจน์</option><option value="3001278">นพดลพานิช</option><option value="3001036">นพปฎล</option><option value="3001569">นพวงศ์ก่อสร้าง</option><option value="3001071">นภาภัณฑ์ ซัพพลาย</option><option value="3001504">นราไทยเจริญ</option><option value="3001501">นราธิวาสสาครินทร์พาน</option><option value="3001024">นวการค้าวัสดุก่อสร้า</option><option value="3000966">นวชัยค้าวัสดุก่อสร้า</option><option value="3001939">นอริตาเก้ (สยาม)</option><option value="3012409">นางรอง</option><option value="3001364">นางรองเคหะภัณฑ์</option><option value="3001246">น่านทรัพย์ทวี</option><option value="3001493">น้ำใส ปัตตานี</option><option value="3001320">น้ำพองพัฒน์วัสดุก่อส</option><option value="3001477">นำศิลป์</option><option value="3001717">นิตา อินเตอร์เทรด</option><option value="3001630">นิปปอนฮูมคอนกรีต</option><option value="3012148">บ.จังหย่งเฮงเส็ง จก.</option><option value="3041816">บ.ยิ่งเจริญสระบุรี</option><option value="3007063">บ.วัฒนาโฮมเซ็นเตอร์</option><option value="3001705">บมจ.เครื่องสุขภัณฑ์อ</option><option value="3001809">บมจ.เครื่องสุขภัณฑ์อ</option><option value="3001632">บมจ.เยนเนอรัล เอนยิเ</option><option value="3001687">บมจ.ธารารมณ์ เอ็นเตอ</option><option value="3001650">บมจ.มหพันธ์ไฟเบอร์ซี</option><option value="3001715">บมจ.อูช่าสยามสตีลอิน</option><option value="3001940">บริบูรณ์พานิช</option><option value="3001129">บ้วนล้งค้าวัสดุภัณฑ์</option><option value="3000981">บ้วนฮงฮวดพาณิชยการ</option><option value="3001891">บางกอกเซรามิค</option><option value="3001712">บางกอกลวดเชื่อม</option><option value="3001725">บางกอกอินดัสเตรียลบอ</option><option value="3001831">บางปะอินเสาเข็มคอนกร</option><option value="3001072">บางพลีซิเมนต์</option><option value="3001859">บ๊าธรูมเซนเตอร์</option><option value="3001594">บ้านแพงกรุ๊ป</option><option value="3001025">บ้านแพนวิสาหกิจ</option><option value="3001861">บ้านและวัสดุภัณฑ์</option><option value="3001172">บ้านโป่งกิตติภัณฑ์</option><option value="3012178">บ้านโป่งค้าเหล็ก</option><option value="3001173">บ้านโป่งซีเมนต์</option><option value="3001116">บ้านฉางสามเจริญ</option><option value="3001115">บ้านดี</option><option value="3001170">บ้านบ่อค้าไม้</option><option value="3001332">บ้านผือโพธิ์ทองวัสด</option><option value="3001888">บ้านสุขภัณฑ์และกระเบ</option><option value="3001099">บ้านอำเภอเทรดดิ้ง</option><option value="3001893">บิ๊กโฮมสุขภัณฑ์</option><option value="3001707">บิ๊กซี ซูเปอร์เซ็นเต</option><option value="3001796">บิลดิ้ง เอ็นเวลลอป</option><option value="3001660">บี.แอล.เอ็ม กรุ๊ป</option><option value="3001564">บี.บอย.เบลล์.คอนสตรั</option><option value="3001570">บีเจ เซอร์วิส อินเตอ</option><option value="3001586">บีอาร์ ก่อสร้าง</option><option value="3001770">บุญเลิศคอนกรีต</option><option value="3001323">บุญโพธิ์</option><option value="3001294">บุญไทยพาณิชย์ซีเมนต์</option><option value="3001089">บุญไทยวัสดุก่อสร้าง</option><option value="3001298">บุญชัยวัสดุก่อสร้าง</option><option value="3001883">บุญดีสุขภัณฑ์</option><option value="3001872">บุญถาวรเซรามิค(รังสิ</option><option value="3001874">บุญถาวรเซรามิค(รังสิ</option><option value="3001875">บุญถาวรเซรามิค(รังสิ</option><option value="3001903">บุญถาวรเซรามิค(รังสิ</option><option value="3001908">บุญถาวรเซรามิค(รังสิ</option><option value="3001879">บุญถาวรเซรามิค(รัชดา</option><option value="3001118">บุญทวีวัสดุ</option><option value="3001053">บุญธรรมการค้า</option><option value="3001237">บุญบันดาลการค้า</option><option value="3001121">บุญมี โฮม มาร์ท</option><option value="3001309">บุญศิริโชค</option><option value="3001766">บุนยดิลก 1992</option><option value="3001365">บุรีรัมย์นำโชค</option><option value="3001471">บุลกุลวรรธน์</option><option value="3001579">ป.สุวิมล</option><option value="3001825">ปทุมธานี คอนกรีต</option><option value="3001829">ปทุมพื้นสำเร็จรูป</option><option value="3001021">ปทุมรุ่งถาวรค้าวัสดุ</option><option value="3001158">ประเสริฐภัณฑ์</option><option value="3001204">ประไพค้าไม้</option><option value="3001240">ประกายวิเชียร</option><option value="3001704">ประพาฬ เซอรามิก้า</option><option value="3001420">ประภาวิวัฒน์ค้าวัสดุ</option><option value="3001565">ประยูรวิศว์การช่าง</option><option value="3000995">ประสงค์กิจซิเมนต์</option><option value="3001186">ปราณบุรีวัสดุภัณฑ์</option><option value="3001578">ปรีดาก่อสร้าง</option><option value="3006089">ปัญญาวัสดุก่อสร้าง</option><option value="3001490">ปัตตานีเจริญสถาปัตย์</option><option value="3001799">ปากเกร็ดอลูมิเนียม</option><option value="3001001">ปากลัดค้าวัสดุก่อสร้</option><option value="3001353">ปิยกรณ์วัสดุก่อสร้าง</option><option value="3001217">ปีนังค้าวัสดุ</option><option value="3001049">ปึงเกียงเส็งค้าวัสดุ</option><option value="3001073">ปู่เจ้าวัสดุภัณฑ์</option><option value="3001773">ปูนเชียงราย</option><option value="3001444">ปูนเมืองใต้นคร</option><option value="3000953">ปูนวัฒนา</option><option value="3001395">ผดุงพาณิชย์</option><option value="3001689">ผลเจริญ</option><option value="3001166">พ.ประสิทธิ์ค้าไม้และ</option><option value="3001292">พ.พัฒนาสุวรรณ</option><option value="3001086">พ.วิศวภัณฑ์</option><option value="3001028">พงษ์ศักดิ์ไทย</option><option value="3001489">พงษ์สินโฮลดิ้ง</option><option value="3001487">พงษ์สินค้าวัสดุ</option><option value="3001554">พจน์รุ่งโรจน์</option><option value="3001080">พนมซิเมนต์</option><option value="3001165">พนาโรจน์</option><option value="3001907">พรชัยนคร</option><option value="3000954">พรพุทธิไชย</option><option value="3000972">พรวัฒนา พี.นุกูร</option><option value="3001093">พรสมบูรณ์ค้าวัสดุก่อ</option><option value="3001572">พร้อมมิตร เอส เอ เอฟ</option><option value="3001763">พระประแดงคอนกรีต</option><option value="3001811">พรีไซส์สตีล แอนด์</option><option value="3001832">พหลโยธินคอนกรีตอุตสา</option><option value="3001289">พะเยานาทีทอง</option><option value="3000978">พัฒนาการค้าไม้-เสาเข</option><option value="3017016">พัฒนาดีโฮมมาร์ท</option><option value="3001473">พัทลุงสิทธิชัยวัสดุก</option><option value="3001775">พันแสนคอนกรีต</option><option value="3001900">พาณิชย์วิบูลย์เชียงใ</option><option value="3001094">พานทอง</option><option value="3001205">พิจิตรนวกิจ</option><option value="3001666">พิชญ กรุ๊ป</option><option value="3001315">พิชัยค้าไม้ชัยภูมิ</option><option value="3001407">พิชัยค้าผลิตภัณฑ์ก่อ</option><option value="3001505">พิชัยอุปกรณ์ก่อสร้าง</option><option value="3000975">พิบูลย์ไทยพาณิชย์</option><option value="3001287">พิบูลย์กิจวัสดุ</option><option value="3001898">พิษณุโลกลิ้มเซ่งฮวด</option><option value="3001843">พี เทอรัซโซ</option><option value="3001653">พี.เค.ทรัพย์ไพศาล</option><option value="3001867">พี.เอช.สุขภัณฑ์</option><option value="3001027">พี.เอ็น.</option><option value="3001740">พี.เอ็น.ซี.คอนกรีต</option><option value="3001866">พี.พี.เซรามิค(ลี้คุน</option><option value="3001827">พีบีแอล กรุ๊ป</option><option value="3001636">พีระมิดคอนกรีต</option><option value="3001485">พีระวัฒน์</option><option value="3000993">พูนทรัพย์ค้าวัสดุก่อ</option><option value="3001134">พูนพรศรี</option><option value="3001082">พูนศิริวัฒนา</option><option value="3001414">ฟ้าทวีพร</option><option value="3001244">ฟิลลิปส์โฮมโปรดักท์</option><option value="3001830">ภัทรไพล์ลิ่ง</option><option value="3001007">ภัทรชัยซิเมนต์</option><option value="3001482">ภาคใต้วัสดุภัณฑ์</option><option value="3001492">ภาคีวัสดุก่อสร้าง</option><option value="3001801">ภาณุแลนด์ดีเวลลอปเมน</option><option value="3001451">ภูเก็ตพงศ์ธรพาณิชย์</option><option value="3001236">ม.บุญทำพานิช</option><option value="3001637">มงคลกิจคอนกรีต</option><option value="3001918">มหาชัยเซรามิค</option><option value="3001812">มหาวิทยาลัยเกษมบัณฑิ</option><option value="3001472">มังกรทองพัทลุงวัสดุก</option><option value="3001306">มานิตย์ค้าวัสดุก่อสร</option><option value="3001317">มาวินค้าไม้ 1991</option><option value="3001312">มิตรภาพภูเขียว</option><option value="3000967">มิตรอภัย</option><option value="3000971">มีนบุรีซิเมนต์ไทย</option><option value="3001826">มีนบุรีอุตสาหกรรมคอน</option><option value="3001776">มุกดาหารคอนกรีตอุตสา</option><option value="3001355">มุกดาหารซิเมนต์</option><option value="3001322">ยงชัยพาณิชย์</option><option value="3001765">ยงยุทธคอนกรีตผสมเสร็</option><option value="3001258">ย่งหลีเส็ง(1994)</option><option value="3006071">ย่งฮวดวัสดุก่อสร้าง</option><option value="3001339">ยงฮวดหลีก่อสร้าง</option><option value="3001816">ยรรยงโลหะการ</option><option value="3001498">ยะลาย่งฮวด</option><option value="3027377">ยิ่งเจริญ โฮมมาร์ท</option><option value="3013795">ยิ่งเจริญปากช่อง</option><option value="3006397">ยิ่งเจริญพาณิชย์</option><option value="3001938">ยิ่งเจริญวัสดุก่อสร้</option><option value="3007400">ยิ่งเจริญสระบุรีสตีล</option><option value="3001147">ยินดีเคหะภัณฑ์</option><option value="3001734">ยู แอนด์ โอ</option><option value="3001305">ยูเนียนค้าไม้ค้าวัตถ</option><option value="3001808">ยูเอ็มไอ-เลาเฟน</option><option value="3001633">ยูไนเต็ดคอนสตรัคชั่น</option><option value="3001612">ยูนิคเอ็นจิเนียริ่ง</option><option value="3001618">ยูนิคเอ็นจิเนียริ่ง</option><option value="3001638">ยูบาว</option><option value="3001749">ยูบาว</option><option value="3001753">ร่มเกล้าคอนกรีตอัดแร</option><option value="3001421">รวมกิจเจริญ</option><option value="3001735">รวมค้าวัสดุคอนกรีตอั</option><option value="3001838">รวมชัยค้าวัสดุ</option><option value="3001159">รวมชัยสุพรรณบุรี</option><option value="3001012">รวมซีเมนต์ไทย</option><option value="3001430">รวีภัณฑ์</option><option value="3001189">ร่อนทองชัย</option><option value="3001390">ร้อยเอ็ดชัยวัฒนพานิช</option><option value="3001389">ร้อยเอ็ดรุ่งเรือง</option><option value="3001319">ระดมเคหะกิจ</option><option value="3001114">ระยองเคหะภัณฑ์</option><option value="3001783">ระยองโอเลฟินส์</option><option value="3001625">ระยองไวร์ อินดัสตรีส</option><option value="3001535">ระยองภาคินทร์</option><option value="3001095">รังศิริเคหะภัณฑ์</option><option value="3001652">รังสิยาอินเตอร์เนชั่</option><option value="3001931">รัชดาศูนย์รวมวัสดุ</option><option value="3001270">รัชตภัณฑ์ลำพูน</option><option value="3001106">รัตนประดิษฐ์ค้าไม้ศร</option><option value="3001584">รับเหมาไม้แบบและเหล็</option><option value="3000991">รางไผ่</option><option value="3001176">ราชบุรีซิเมนต์</option><option value="3001696">ราชาเซรามิค</option><option value="3001597">ราชาอุตสาหกรรม</option><option value="3005363">รามคำแหง โฮมมาร์ท</option><option value="3001675">รุ่งเกียรติทรัพย์</option><option value="3001842">รุ่งเจริญสินซิเมนต์</option><option value="3001413">รุ่งเรือง</option><option value="3001263">รุ่งเรืองการค้าพาน</option><option value="3000980">รุ่งกิจวัสดุการสร้าง</option><option value="3001018">ล.ไชยฮวดวัสดุภัณฑ์</option><option value="3001163">ล.ฮั่วฮวดเส็งค้าไม้</option><option value="3001052">ลพบุรีค้าวัตถุก่อสร้</option><option value="3001055">ลพบุรีจิระพันธ์ค้าไม</option><option value="3001296">ล้อเฮงเส็งง้วนค้าไม้</option><option value="3001491">ละงูค้าวัสดุ</option><option value="3001680">ลากัวร์เทค</option><option value="3000988">ลาดพร้าววัสดุภัณฑ์</option><option value="3012636">ลำปาง</option><option value="3001256">ลำปางเตียวเชียงล้ง</option><option value="3019008">ลำปางเหลี่ยงฮะเฮง</option><option value="3001793">ลิบเจริญอลูมิเนียม</option><option value="3001424">ลิ่มเฮงหลี</option><option value="3001463">ลิ้มกวงฮวด</option><option value="3005981">ลิ้มง้วนฮวด</option><option value="3001152">ลิ้มพัฒนาดีซีเมนต์.</option><option value="3001050">ลิ้มย่งฮงล้ง</option><option value="3001708">ลี้ย่งติ่ง</option><option value="3001149">ว.ณรงค์ชัย</option><option value="3001443">ว.อรุณพันธ์</option><option value="3005371">วนาวัฒน์วัสดุ</option><option value="3001663">วรนันท์ครีเอทีฟ</option><option value="3001252">วรรณจักรลำปาง</option><option value="3001141">วรวัฒน์การสร้าง</option><option value="3001026">วรวิทย์วัสดุก่อสร้าง</option><option value="3001150">ว่องวัชรินทร์</option><option value="3001360">วอวิศววัสดุ</option><option value="3001442">วัฒนไพบูลย์ค้า</option><option value="3001846">วัฒนชัย (1993)</option><option value="3001207">วัฒนชัยวัสดุหล่มสัก</option><option value="3001219">วัฒนากิจวัสดุ</option><option value="3001828">วัสดุภัณฑ์คอนกรีต (โ</option><option value="3001019">วานิชธนกุลคอนสตรัคชั</option><option value="3001383">วารินโชคชัยโลหะกิจ</option><option value="3001728">วิเศษคอนกรีต</option><option value="3001048">วิเศษวัฒนาค้าวัสด</option><option value="3001253">วิชัยเถินโลหะธุรกิจ</option><option value="3001631">วิน คอนกรีต</option><option value="3001864">วิบูลย์จันทร์(กุ่ยยู</option><option value="3001117">วิบูลย์พานิชจันทบุรี</option><option value="3001759">วิรัชคอนกรีต</option><option value="3001409">วิริยะระนอง</option><option value="3000962">วิรุฬห์กิจ</option><option value="3000963">วิวัฒน์ภัณฑ์พาณิชย์</option><option value="3001849">วิวัฒน์อลูมิเนียม แอ</option><option value="3001387">วิสิทธิ์ก่อสร้าง(พยั</option><option value="3001325">วิสุทธิชัย</option><option value="3001890">วี.เค เซรามิค</option><option value="3032041">วี.พี.สตีล</option><option value="3001858">วี.วิตตานนท์</option><option value="3001208">วี.สถาปัตย์</option><option value="3023402">วีซีเอสเอเชีย</option><option value="3001405">วุฒิชัยเจริญนครพนม 1</option><option value="3001917">ศ.ถาวรสุขภัณฑ์เซ็นเต</option><option value="3001748">ศ.ศิริถาวรค้าวัสดุ</option><option value="3001210">ศรีเทพก่อสร้าง</option><option value="3001000">ศรีเมืองวัสดุ</option><option value="3001902">ศรีทองวัสดุภัณฑ์</option><option value="3001432">ศรีธรรมราชลำเลียง</option><option value="3001008">ศรีบุญชัย</option><option value="3001454">ศรีผ่องพาณิชย์</option><option value="3001585">ศรียุทธศักดิ์</option><option value="3001098">ศรีราชาสิริพานิช</option><option value="3001128">ศรีสมพร</option><option value="3001375">ศรีสะเกษซีหลี</option><option value="3001324">ศรีสูรย์</option><option value="3001761">ศรีอยุธยาคอนกรีต</option><option value="3001272">ศักดิ์ชัยลำพูน</option><option value="3001945">ศิริกุล</option><option value="3001428">ศิริภัณฑ์ซิเมนต์</option><option value="3001185">ศิริภัณฑ์บางสะพาน</option><option value="3005425">ศิริมหาชัย โฮม</option><option value="3001588">ศิริวิทยาภัณฑ์ 1995</option><option value="3000965">ศิริสมบัติค้าวัสดุก่</option><option value="3001354">ศิวาวุธวัสดุก่อสร้าง</option><option value="3001238">ส.เคหะภัณฑ์</option><option value="3006197">ส.เจริญพงษ์</option><option value="3001673">ส.ไพบูลย์โลหะกิจ</option><option value="3001884">ส.ไมตรีภัณฑ์เฮ้าส์ซิ</option><option value="3001912">ส.ตรีวิศวกิจเคหะภัณฑ</option><option value="3001394">ส.ทวีสินกาฬสินธุ์</option><option value="3001229">ส.พงษ์เจริญการค้า</option><option value="3001250">ส.รุ่งเรืองน่าน</option><option value="3001787">ส.วรรณพล</option><option value="3001092">ส.วัสดุก่อสร้าง</option><option value="3026816">ส.วิไลสตีล</option><option value="3001576">ส.สหมิตรวัฒนาก่อสร้า</option><option value="3001670">ส.อินทีเรียร์</option><option value="3001679">สแควร์อินเตอร์เนชั่น</option><option value="3001654">สโนว์เซ็ม เพ้นท์</option><option value="3001103">ส่งเสริมสกุลฮ้อ</option><option value="3001033">สงวนไทยพาณิชย์ สระบุ</option><option value="3000977">สงวนพงษ์วัสดุก่อสร้า</option><option value="3000968">สถาพรวัฒนา</option><option value="3001440">สมชัยค้าวัสด</option><option value="3001923">สมบูรณ์ไพศาล</option><option value="3001047">สมบูรณ์พาณิชย์อ่างทอ</option><option value="3001295">สมพงษ์ค้าวัสดุก่อสร้</option><option value="3015696">สมาร์ท</option><option value="3001065">สมุทรพัฒนา</option><option value="3001478">สมุทรสาร</option><option value="3001693">สยาม โทซู เซรามิกส์</option><option value="3001760">สยาม ซิสเต็ม บิลท์</option><option value="3001757">สยามคอนกรีต (1988)</option><option value="3001782">สยามคูโบต้าอุตสาหกรร</option><option value="3001784">สยามบรรจุภัณฑ์อุตสาห</option><option value="3001642">สยามผลิตภัณฑ์คอนกรีต</option><option value="3001014">สยามพรวัสดุ</option><option value="3001781">สยามฟอเรสทรี</option><option value="3001786">สยามฟูรูกาวา</option><option value="3001461">สยามภัณฑ์วัตถุก่อสร้</option><option value="3001346">สยามภัทร์</option><option value="3001789">สยามยิปซั่มบอร์ด</option><option value="3001813">สยามสติลซินดิเกต</option><option value="3001854">สยามสตีล แอนด์ ไอออน</option><option value="3001682">สยามสินธร</option><option value="3001685">สยามสินธร แลนด์</option><option value="3001797">สยามอิมเมจ ดีเวลลอปเ</option><option value="3001613">สรรพ์พัฒน์</option><option value="3001032">สระบุรีค้าวัตถุก่อสร</option><option value="3001041">สระบุรีศุภภัณฑ์</option><option value="3001227">สวรรคโลกกิจดำรงค์</option><option value="3001400">สว่างวิโรจน์โลหะกิจ</option><option value="3001658">สหโชติอุตสาหกรรมคอนก</option><option value="3001202">สหไม้ไทยค้าวัสดุ</option><option value="3001034">สหกิจทวีภัณฑ์</option><option value="3001035">สหชัยธุรกิจก่อสร้าง</option><option value="3001870">สหประสุข</option><option value="3001723">สหพรเจริญสตีล</option><option value="3001431">สหพานิชจันดี</option><option value="3001031">สหพานิชพระพุทธบาท</option><option value="3031784">สหมิตร โฮม โซลูชั่น</option><option value="3001435">สหมิตรค้าวัสดุ</option><option value="3000996">สหวัฒน์ค้าอุปกรณ์ก่อ</option><option value="3001840">สหวัฒน์ซีเมนต์ขาว</option><option value="3001441">สหวัฒน์นคร</option><option value="3001009">สหสินไทยค้าวัตถุก่อส</option><option value="3001381">สหสินปอแซ</option><option value="3001764">สหอรพรรณ</option><option value="3001051">สหะชัยลำนารายณ์</option><option value="3001090">สหะชัยวัสดุก่อสร้าง</option><option value="3001488">สะเดากานต์พานิช</option><option value="3001386">สันติรัตน์โกสุม</option><option value="3001087">สัมพันธ์วัสดุก่อสร้า</option><option value="3001460">สากลภัณฑ์</option><option value="3001817">สามชัยตะแกรงเหล็ก</option><option value="3001821">สามชัยอุตสาหกรรมก่อส</option><option value="3001112">สามย่านเคหะภัณฑ์</option><option value="3001609">สามัคคีที่ดินและเคหะ</option><option value="3001385">สารคามพัฒนาการก่อสร้</option><option value="3001182">สำราญค้าวัตถุก่อสร้า</option><option value="3001616">สิทธิพงษ์รวมมิตร</option><option value="3015346">สิทธิพันธุ์</option><option value="3001005">สิทธิพันธุ์ค้าไม้</option><option value="3001688">สิทธิศักดิ์ก่อสร้าง</option><option value="3001004">สินไทยซิเมนต์กรุงเทพ</option><option value="3000989">สินไทยธนวัฒน์</option><option value="3001382">สินค้าซิเมนต์ไทย</option><option value="3001242">สินค้าดี</option><option value="3001181">สินชัยค้าวัสดุก่อสร้</option><option value="3001245">สินชัยพาณิชย์แพร่</option><option value="3001719">สินธานีอุตสาหกรรม</option><option value="3001066">สินธุมงคล</option><option value="3001211">สินรัตนาภัณฑ์</option><option value="3000998">สิริชัยค้าวัสดุก่อสร</option><option value="3001228">สี่ก๊กกำแพงเพชร</option><option value="3001605">สี่พระยา แลนด์</option><option value="3001563">สี่พระยาก่อสร้าง</option><option value="3001249">สุเกียววัสดุก่อสร้าง</option><option value="3001224">สุโขทัยแสงอุปกรณ์</option><option value="3001833">สุโขทัยคอนกรีตอัดแรง</option><option value="3001836">สุโขทัยคอนกรีตอัดแรง</option><option value="3001223">สุโขทัยซิเมนต์</option><option value="3001887">สุขกมลรัชดา</option><option value="3001882">สุขภัณฑ์ เซ็นเตอร์ (</option><option value="3001857">สุขภัณฑ์ เซ็นเตอร์ ก</option><option value="3001083">สุทธิพงศ์ค้าวัสดุก่อ</option><option value="3001243">สุทินพาณิชย์</option><option value="3001869">สุนทรประสิทธิ์ค้าวัส</option><option value="3019926">สุบินสตีล</option><option value="3020692">สุบินสตีล</option><option value="3001042">สุภาพัฒน์</option><option value="3004149">สุรสิทธิ์ค้าวัสดุ</option><option value="3001206">สุรสิทธิ์พิจิตร</option><option value="3001640">สุราษฎร์โกลเด็นแลนด์</option><option value="3001418">สุราษฎร์ธัญญาวัฒน์ค้</option><option value="3001894">สุราษฎร์สุขภัณฑ์เทรด</option><option value="3001851">สุรินทร์โฟลทกลาส</option><option value="3001232">สุลาวัลย์การค้า</option><option value="3001408">สุวิทย์วัสดุก่อสร้าง</option><option value="3001174">หงี่ล่งเชียง</option><option value="3006278">หจก.โลหะภัณฑ์</option><option value="3005826">หจก.สหพานิช</option><option value="3000976">หทัยวิศธ์</option><option value="3001345">หนองคายโลหะกิจ</option><option value="3001340">หนองคายวิศิษฎ์</option><option value="3001196">หนองบัวแหลมทองค้าไม้</option><option value="3001404">หมงฮวดบริการ</option><option value="3001768">หล่มสักคอนกรีต</option><option value="3001161">หลักเมือง-ถาวรพาณิชย</option><option value="3001464">หลังสวนวัสดุ</option><option value="3001209">หลีเฮง</option><option value="3001251">หสน.เซ่งเฮงจั่น</option><option value="3001465">หสน.ชุมพรจี้เซ่งฮวด</option><option value="3001273">หสน.ซิ้นเชียงหลี</option><option value="3001276">หสน.ซิ้นเชียงหลี</option><option value="3001271">หสน.ทวีพาณิชย์ลำพูน</option><option value="3001439">หสน.ล้อฮุยเทียม</option><option value="3001045">หสน.อารยะรังสฤษฎ์</option><option value="3001030">หัวเวียง</option><option value="3001438">หัวไทรค้าวัสดุ</option><option value="3001419">หัวถนน(สุราษฎร์ธานี)</option><option value="3001774">หาญเจริญคอนกรีต</option><option value="3001587">หาญกิติชัย</option><option value="3001911">หาดใหญ่ บี.เอ็ม.เทรด</option><option value="3001480">หาดใหญ่ไพรัตน์</option><option value="3001483">หาดใหญ่สหะสินค้าเหล็</option><option value="3001914">หินกองสุขภัณฑ์เซ็นเต</option><option value="3000970">อ.กมลภัทรา</option><option value="3001503">อ.สหกิจไทย</option><option value="3013389">อนันต์ สตีล</option><option value="3001137">อนุสรณ์ค้าวัสดุก่อสร</option><option value="3006848">อภิธน โฮมมาร์ท</option><option value="3001299">อมรภัทรค้าวัสดุก่อสร</option><option value="3001370">อมรศักยะ</option><option value="3001201">อรรถพงษ์วัสดุก่อสร้า</option><option value="3001126">อรัญโพธิ์ทอง</option><option value="3001457">อริยพาณิชย์</option><option value="3000979">ออเซ่งฮวด</option><option value="3001155">อ้อมใหญ่ค้าไม้</option><option value="3006194">อ้อมใหญ่ค้าไม้</option><option value="3001303">อังครงค์รักษ์พาณิชย์</option><option value="3001611">อัศวบุตร การโยธา</option><option value="3001044">อ่างทองไทยพนา</option><option value="3001568">อาร์.เค.ยูเนี่ยน คอน</option><option value="3001934">อาร์ค่อนเอ็นจิเนียริ</option><option value="3001677">อาร์คีเทคเจอรัล</option><option value="3001629">อาร์ตกรีต</option><option value="3001703">อาร์ตติโก้ คร๊าฟส์</option><option value="3001349">อารีพาณิชย์ (เลย)</option><option value="3001104">อ่าวอุดมค้าไม้</option><option value="3001362">อำนาจเจริญคอนกรีต</option><option value="3001069">อินเตอร์เคหะภัณฑ์</option><option value="3001700">อินเตอร์เนชั่นแนลมาซ</option><option value="3001920">อินเตอร์สุขภัณฑ์เซรา</option><option value="3001562">อินซูค่อน และบริการ</option><option value="3001608">อินทรชัยคอนสตรัคชั่น</option><option value="3001058">อินทร์บุรีซีเมนต์</option><option value="3001056">อิมหลีค้าวัสดุก่อสร้</option><option value="3001254">อิวหลีพาณิชย์</option><option value="3001690">อิวาตานิอินเตอร์เนชั</option><option value="3004165">อี.แอนด์ ซี</option><option value="3014700">อี.จี.</option><option value="3001412">อึ้งจิบง้วนพาณิชย์</option><option value="3001142">อื้อเลี่ยงไทย</option><option value="3001948">อุดม</option><option value="3001070">อุดมชัย</option><option value="3001144">อุดมชัยสมุทรสงครามซิ</option><option value="3001388">อุดมภัณฑ์การช่าง</option><option value="3001476">อุดมรักษ์การค้า</option><option value="3001429">อุดมรัตน์นคร</option><option value="3001336">อุดรเจริญทองโลหะกิจ</option><option value="3001338">อุดรกวงเฮง</option><option value="3001330">อุดรนำธงชัย</option><option value="3001540">อุดรสยาม</option><option value="3001335">อุดรสยามโลหะกิจ</option><option value="3001334">อุดรสยามฮาร์ดแวร์</option><option value="3001785">อุตสาหกรรมกระดาษคราฟ</option><option value="3001777">อุบลคอนกรีตอัดแรง</option><option value="3001378">อุบลศิริมหาชัย</option><option value="3001379">อุบลอนันต์พานิช</option><option value="3001901">อุปกรณ์เคหะ</option><option value="3001275">ฮกเชียงล้งพาณิชย์</option><option value="3001022">ฮ้อนำผลวัฒนา</option><option value="3001928">ฮับโหมวเฮง</option><option value="3001655">ฮิตาชิเฟอร์ไรท์</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    
                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>GRADE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <br />
                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control as="select">
                                                            <option value={0}>Create Date</option>
                                                            <option value={1}>Update Date</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 1 ||
                                                    this.state.selectDate === 2 ||
                                                    this.state.selectDate === 3 ||
                                                    this.state.selectDate === 4 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 5 ||
                                                    this.state.selectDate === 6 ||
                                                    this.state.selectDate === 7 ||
                                                    this.state.selectDate === 8 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>From</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 9 ||
                                                    this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 11 ||
                                                    this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>At Month</option>
                                                        <option value={6}>Between Month</option>
                                                        <option value={7}>More than</option>
                                                        <option value={8}>More than or equal</option>
                                                        <option value={9}>At Quater</option>
                                                        <option value={10}>Between Quater</option>
                                                        <option value={11}>At Year</option>
                                                        <option value={12}>Between Year</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>To</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>More than</option>
                                                            <option value={6}>More than or equal</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                        {
                                                this.state.selectValue === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > SEARCH </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="สร้างใบสั่งซื้อ (SYS)">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info"/></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" onClick={e => this.setShowModal(e, "Create")}>สร้างใบสั่งซื้อ</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>วันที่ต้องการรับ</th>
                                        <th>เลขที่ใบจัดรถ</th>
                                        <th>รายละเอียดสินค้า</th>
                                        <th>สั่งซื้อ ท่อน/ตัน</th>
                                        <th>รับ ท่อน/ตัน</th>
                                        <th>สุทธิต่อหน่วย(บาท/ตัน)</th>
                                        <th>จำนวนเงิน(บาท)</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
