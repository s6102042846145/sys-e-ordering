import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else if (type === "Edit") {

            this.setState({ isModal: true });
            this.setState({ setTitleModal: "MORE.." })

        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })

            this.setState({ isModal: true });
        }

    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={10}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">เลือก SAP DP.TEXT FILE</Form.Label>
                                            <Col sm={9}>
                                                <Form.Control type="file" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={2}>
                                        <Form.Group as={Row}>
                                            <Button variant="success" size="sm" >UPLOAD</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>



                                <Form.Group as={Row} className="mt-sm-2">


                                    <Col sm={12} className="mt-sm-1">
                                        <Button variant="success" className="mr-sm-1 wid-200">CAPTURE ORDER ค้าง</Button>
                                        <Button variant="success" className="mr-sm-1 wid-200">CAPTURE DP ค้างรับ</Button>
                                        <Button variant="success" className="mr-sm-1 wid-220">CAPTURE แผนผลิต</Button>
                                        <Button variant="success" className="mr-sm-1 wid-220">CAPTURE วันที่รับ(ลูกค้า)</Button>
                                        <Button variant="success" className="mr-sm-1 wid-200">CAPTURE REBATE</Button>
                                        <Button variant="success" className="mt-sm-1 wid-200">RE-CALUATE AMT.</Button>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={12}>
                                        <Button variant="primary" className="mr-sm-1 wid-200">EXPORT CSV DP.</Button>
                                    </Col>
                                </Form.Group>
                                <Row>
                                    <Col sm={12}>
                                        <Table ref="tbl" striped hover responsive bordered >
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>SALES.ORG.CD</th>
                                                    <th>PLANT</th>
                                                    <th>SEC	</th>
                                                    <th>REC(S)</th>
                                                    <th>TON</th>
                                                    <th>NET.AMT</th>
                                                    <th>TOTAL AMT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colSpan="8" className="text-center">
                                                        <Form.Label >No Data.</Form.Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                <Form.Control as={Row}>
                                    <Form.Label className="text-c-red">A202007</Form.Label>
                                </Form.Control>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="success">SAVE</Button>
                                <Button variant="success">SAVE REV</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SHIP.YEAR</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="years" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SHIP.MONTH</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="month" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GROUP BY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="NONE">NONE</option><option value="SEC">SECTION</option><option value="CUST">CUSTOMER</option><option value="PG">PRODUCT GRP.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <hr />
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Schedule.Cd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER NO</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CUST.CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CUSTOMER</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>TRADER CODE 1</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SALES.ORG.CD</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CHANNEL CD</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PROJECT DESC.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SIZE.STD</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="ASTM2003">ASTM A6/A6M:2003</option><option value="BS">BS EN 10034:1993</option><option value="BS3">BS EN 10279:2000</option><option value="BS1">BSEN 10056-1:1999</option><option value="EURO">EN 53-1962,EN 19-1957</option><option value="GB706">GB/T 706-2008</option><option value="GOST">GOST 380-94</option><option value="JISA5528-2012">JIS A5528 : 2012</option><option value="JISA5528">JIS A5528-2006</option><option value="JIS1990">JIS G3192-1990</option><option value="JIS1994">JIS G3192-1994</option><option value="SNI-C">SNI-C : SNI 07-0052-2006</option><option value="SNI-H">SNI-H : SNI 2610-2011</option><option value="SNI-I">SNI-I : SNI 07-0329-2005</option><option value="SNI-L">SNI-L : SNI 07-2054-2006</option><option value="SNI-WF">SNI-WF : SNI 07-7178-2006</option><option value="TIS">TIS 1227:1996</option><option value="TIS/JIS">TIS 1227:1996/JIS G3192:1990</option><option value="TIS2015">TIS 1227:2558 (2015)</option><option value="TIS1390-2539">TIS1390-2539 (1996)</option><option value="TIS1390">TIS1390-2560 (2017)</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="L">ANGLE</option><option value="B">BLOOM</option><option value="C">CHANNEL</option><option value="T">CUT-BEAM</option><option value="V">HVA</option><option value="I">I-BEAM</option><option value="M">MODULAR</option><option value="R">RAIL</option><option value="S">SHEET-PILE</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE.STD</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="AS/NZS">AS/NZS 3679.1:2016</option><option value="ASTM">ASTM A36/A36M:2005</option><option value="ASTM5">ASTM A36/A36M:2005/JIS G3101-2004</option><option value="OTHER3">ASTM A36/A36M:2005/JIS G3101-2004</option><option value="ASTM3">ASTM A572/A572M:2007</option><option value="ASTM4">ASTM A572/A572M:2007/BS EN 10025:1993</option><option value="OTHER2">ASTM A572/A572M:2007/BS EN 10025:1993</option><option value="ASTM7">ASTM A572/A572M:2013a</option><option value="ASTM2">ASTM A992/A992M:2004a</option><option value="ASTM6">ASTM A992/A992M:2004a/ASTM A572/A572M:2007</option><option value="BS/EN">BS 4360:1986</option><option value="BS/EN5">BS 7191:1989</option><option value="BS/EN6">BS EN 10025-2 : 1993</option><option value="BS/EN2">BS EN 10025-2 : 2004</option><option value="BS/EN3">BS EN 10034:1993 / BS EN 10025-2 : 2004</option><option value="BS/EN4">BS EN 10248-1 : 1996</option><option value="CNS">CNS 13812 G3262 : 2014</option><option value="DIN">DIN 17100 :1980</option><option value="GBT714">GB T714 : 2000</option><option value="GOST">GOST 380-94</option><option value="JISA5528-2012">JIS A5528 : 2012</option><option value="JISA5528-2012-1">JIS A5528 : 2012/EN 10248-1:1996</option><option value="JISA5528">JIS A5528-2006</option><option value="JIS3">JIS A5528-2006 / BS EN 10248-1 : 1996</option><option value="OTHER1">JIS A5528-2006 / BS EN 10248-1 : 1996</option><option value="JIS">JIS G3101-2004</option><option value="JIS4">JIS G3106:2004 / TIS 1227:1996</option><option value="OTHER4">JIS G3106:2004 / TIS 1227:1996</option><option value="JIS1">JIS G3106-2004</option><option value="JIS2">JIS G3136-2005</option><option value="MS1490">MS 1490 : 2001</option><option value="MSEN">MS EN 10025-2 : 2011</option><option value="SNI-C">SNI-C : SNI 07-0052-2006</option><option value="SNI-H">SNI-H : SNI 2610-2011</option><option value="SNI-I">SNI-I : SNI 07-0329-2005</option><option value="SNI-L">SNI-L : SNI 07-2054-2006</option><option value="SNI-WF">SNI-WF : SNI 07-7178-2006</option><option value="TIS">TIS 1227:1996</option><option value="TIS2015">TIS 1227:2558 (2015)</option><option value="TIS1390-2539">TIS1390-2539 (1996)</option><option value="TIS1390">TIS1390-2560 (2017)</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>NOMINAL SIZE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="100 PFC">100 PFC</option><option value="100 UC">100 UC</option><option value="100X10">100X10</option><option value="100X100">100X100</option><option value="100X100X10">100X100X10</option><option value="100X12">100X12</option><option value="100X13">100X13</option><option value="100X150">100X150</option><option value="100X200">100X200</option><option value="100X50">100X50</option><option value="100X50 Drilled">100X50 Drilled</option><option value="100X6">100X6</option><option value="100X7">100X7</option><option value="100X7 Drilled">100X7 Drilled</option><option value="100X8">100X8</option><option value="100X9">100X9</option><option value="102X102">102X102</option><option value="102X6.4">102X6.4</option><option value="102X7.9">102X7.9</option><option value="102X9.5">102X9.5</option><option value="120X10">120X10</option><option value="120X12">120X12</option><option value="120X8">120X8</option><option value="122X175">122X175</option><option value="125CT">125CT</option><option value="125X125">125X125</option><option value="125X175">125X175</option><option value="125X250">125X250</option><option value="125X65">125X65</option><option value="127X127">127X127</option><option value="130X10">130X10</option><option value="130X12">130X12</option><option value="130X13">130X13</option><option value="130X14">130X14</option><option value="130X15">130X15</option><option value="130X16">130X16</option><option value="130X8">130X8</option><option value="130X9">130X9</option><option value="150 PFC">150 PFC</option><option value="150 UB">150 UB</option><option value="150 UC">150 UC</option><option value="150-300">150-300</option><option value="150X10">150X10</option><option value="150X100">150X100</option><option value="150X12">150X12</option><option value="150X13">150X13</option><option value="150X14">150X14</option><option value="150X15">150X15</option><option value="150X150">150X150</option><option value="150X150 SYS2">150X150 SYS2</option><option value="150X150X33.8">150X150X33.8</option><option value="150X16">150X16</option><option value="150X18">150X18</option><option value="150X19">150X19</option><option value="150X200">150X200</option><option value="150X300">150X300</option><option value="150X75">150X75</option><option value="152X102">152X102</option><option value="152X12.7">152X12.7</option><option value="152X152">152X152</option><option value="152X25.4">152X25.4</option><option value="155CT">155CT</option><option value="160X88">160X88</option><option value="168X249">168X249</option><option value="170X250">170X250</option><option value="175X12">175X12</option><option value="175X15">175X15</option><option value="175X175">175X175</option><option value="175X250">175X250</option><option value="175X350">175X350</option><option value="175X90">175X90</option><option value="178X171">178X171</option><option value="180 PFC">180 PFC</option><option value="180 UB">180 UB</option><option value="180X75">180X75</option><option value="180X94">180X94</option><option value="195X300">195X300</option><option value="198X199">198X199</option><option value="2 STOREY HOUSE">2 STOREY HOUSE</option><option value="200 PFC">200 PFC</option><option value="200 UB">200 UB</option><option value="200 UC">200 UC</option><option value="200X100">200X100</option><option value="200X102">200X102</option><option value="200X13">200X13</option><option value="200X15">200X15</option><option value="200X150">200X150</option><option value="200X150(B6)">200X150(B6)</option><option value="200X150(B6-A)">200X150(B6-A)</option><option value="200X150(B6-B)">200X150(B6-B)</option><option value="200X150(B6-C)">200X150(B6-C)</option><option value="200X150(B6-D)">200X150(B6-D)</option><option value="200X150(B6-E)">200X150(B6-E)</option><option value="200X150(B6-EA)">200X150(B6-EA)</option><option value="200X150(B6-F)">200X150(B6-F)</option><option value="200X150(B6-G)">200X150(B6-G)</option><option value="200X150(B6-H)">200X150(B6-H)</option><option value="200X150(B6-I)">200X150(B6-I)</option><option value="200X150(B6-J)">200X150(B6-J)</option><option value="200X150(B6-K)">200X150(B6-K)</option><option value="200X150(B6-L)">200X150(B6-L)</option><option value="200X16">200X16</option><option value="200X18">200X18</option><option value="200X20">200X20</option><option value="200X200">200X200</option><option value="200X200( SC2-17)">200X200( SC2-17)</option><option value="200X200(SC2-1)">200X200(SC2-1)</option><option value="200X200(SC2-10)">200X200(SC2-10)</option><option value="200X200(SC2-11)">200X200(SC2-11)</option><option value="200X200(SC2-12)">200X200(SC2-12)</option><option value="200X200(SC2-13)">200X200(SC2-13)</option><option value="200X200(SC2-14)">200X200(SC2-14)</option><option value="200X200(SC2-15)">200X200(SC2-15)</option><option value="200X200(SC2-16)">200X200(SC2-16)</option><option value="200X200(SC2-17A)">200X200(SC2-17A)</option><option value="200X200(SC2-18)">200X200(SC2-18)</option><option value="200X200(SC2-18A)">200X200(SC2-18A)</option><option value="200X200(SC2-19)">200X200(SC2-19)</option><option value="200X200(SC2-2)">200X200(SC2-2)</option><option value="200X200(SC2-20)">200X200(SC2-20)</option><option value="200X200(SC2-21)">200X200(SC2-21)</option><option value="200X200(SC2-22)">200X200(SC2-22)</option><option value="200X200(SC2-23)">200X200(SC2-23)</option><option value="200X200(SC2-24)">200X200(SC2-24)</option><option value="200X200(SC2-25)">200X200(SC2-25)</option><option value="200X200(SC2-3)">200X200(SC2-3)</option><option value="200X200(SC2-4)">200X200(SC2-4)</option><option value="200X200(SC2-5)">200X200(SC2-5)</option><option value="200X200(SC2-6)">200X200(SC2-6)</option><option value="200X200(SC2-7)">200X200(SC2-7)</option><option value="200X200(SC2-8)">200X200(SC2-8)</option><option value="200X200(SC2-9)">200X200(SC2-9)</option><option value="200X200(SC3-1)">200X200(SC3-1)</option><option value="200X204">200X204</option><option value="200X22">200X22</option><option value="200X24">200X24</option><option value="200X25">200X25</option><option value="200X300">200X300</option><option value="200X350">200X350</option><option value="200X80">200X80</option><option value="200X90">200X90</option><option value="203X102">203X102</option><option value="203X133">203X133</option><option value="203X165">203X165</option><option value="203X19">203X19</option><option value="203X203">203X203</option><option value="203X25.4">203X25.4</option><option value="203X28.6">203X28.6</option><option value="205CT">205CT</option><option value="208X202">208X202</option><option value="220X300">220X300</option><option value="225X200">225X200</option><option value="225X300">225X300</option><option value="228.5X191">228.5X191</option><option value="230 PFC">230 PFC</option><option value="230CT">230CT</option><option value="230X450">230X450</option><option value="244X175">244X175</option><option value="244X252">244X252</option><option value="244X300">244X300</option><option value="250 PFC">250 PFC</option><option value="250 UB">250 UB</option><option value="250 UC">250 UC</option><option value="250X116">250X116</option><option value="250X118">250X118</option><option value="250X125">250X125</option><option value="250X125H">250X125H</option><option value="250X125L">250X125L</option><option value="250X175">250X175</option><option value="250X200">250X200</option><option value="250X22">250X22</option><option value="250X23">250X23</option><option value="250X24">250X24</option><option value="250X25">250X25</option><option value="250X250">250X250</option><option value="250X26">250X26</option><option value="250X27">250X27</option><option value="250X28">250X28</option><option value="250X35">250X35</option><option value="250X90">250X90</option><option value="250X90H">250X90H</option><option value="250X90L">250X90L</option><option value="254X102">254X102</option><option value="254X146">254X146</option><option value="254X203">254X203</option><option value="254X254">254X254</option><option value="265CT">265CT</option><option value="267X210">267X210</option><option value="294X200">294X200</option><option value="294X300">294X300</option><option value="294X302">294X302</option><option value="298X199">298X199</option><option value="300 PFC">300 PFC</option><option value="300-600">300-600</option><option value="300X150">300X150</option><option value="300X150(B2-O)">300X150(B2-O)</option><option value="300X150(B3-A)">300X150(B3-A)</option><option value="300X150(B3-A-)">300X150(B3-A-)</option><option value="300X150(B3-B)">300X150(B3-B)</option><option value="300X150(B3-C)">300X150(B3-C)</option><option value="300X150(B3-D)">300X150(B3-D)</option><option value="300X150(B3-DA)">300X150(B3-DA)</option><option value="300X150(B3-E)">300X150(B3-E)</option><option value="300X150(B3-H)">300X150(B3-H)</option><option value="300X150(B3-I)">300X150(B3-I)</option><option value="300X150(B3-J)">300X150(B3-J)</option><option value="300X150(B3-K)">300X150(B3-K)</option><option value="300X150(B3-L)">300X150(B3-L)</option><option value="300X150(B3-M)">300X150(B3-M)</option><option value="300X150(B3-N)">300X150(B3-N)</option><option value="300X150(B3-O)">300X150(B3-O)</option><option value="300X150(B3-OA)">300X150(B3-OA)</option><option value="300X150(B3-P)">300X150(B3-P)</option><option value="300X150(B3-Q)">300X150(B3-Q)</option><option value="300X150(B3-R)">300X150(B3-R)</option><option value="300X150(B3-S)">300X150(B3-S)</option><option value="300X150(B5)">300X150(B5)</option><option value="300X150(B5-A)">300X150(B5-A)</option><option value="300X150(B5-B)">300X150(B5-B)</option><option value="300X150(B5-C)">300X150(B5-C)</option><option value="300X150(B5-D)">300X150(B5-D)</option><option value="300X150(B5-E)">300X150(B5-E)</option><option value="300X150(B5-F)">300X150(B5-F)</option><option value="300X150(B5-G)">300X150(B5-G)</option><option value="300X150(B5-GA)">300X150(B5-GA)</option><option value="300X150(B5-H)">300X150(B5-H)</option><option value="300X150(B5-I)">300X150(B5-I)</option><option value="300X150(B5-J)">300X150(B5-J)</option><option value="300X150(B5-K)">300X150(B5-K)</option><option value="300X150(B5-KA)">300X150(B5-KA)</option><option value="300X150(B5-L)">300X150(B5-L)</option><option value="300X150(B5-LA)">300X150(B5-LA)</option><option value="300X150(B5-M)">300X150(B5-M)</option><option value="300X150(B5-N)">300X150(B5-N)</option><option value="300X150H">300X150H</option><option value="300X150L">300X150L</option><option value="300X150M">300X150M</option><option value="300X200">300X200</option><option value="300X200(B2-A)">300X200(B2-A)</option><option value="300X200(B2-A-)">300X200(B2-A-)</option><option value="300X200(B2-AA)">300X200(B2-AA)</option><option value="300X200(B2-AB)">300X200(B2-AB)</option><option value="300X200(B2-AC)">300X200(B2-AC)</option><option value="300X200(B2-B)">300X200(B2-B)</option><option value="300X200(B2-C)">300X200(B2-C)</option><option value="300X200(B2-D)">300X200(B2-D)</option><option value="300X200(B2-D-)">300X200(B2-D-)</option><option value="300X200(B2-E)">300X200(B2-E)</option><option value="300X200(B2-F)">300X200(B2-F)</option><option value="300X200(B2-G)">300X200(B2-G)</option><option value="300X200(B2-H)">300X200(B2-H)</option><option value="300X200(B2-HA)">300X200(B2-HA)</option><option value="300X200(B2-I)">300X200(B2-I)</option><option value="300X200(B2-J)">300X200(B2-J)</option><option value="300X200(B2-K)">300X200(B2-K)</option><option value="300X200(B2-L)">300X200(B2-L)</option><option value="300X200(B2-M)">300X200(B2-M)</option><option value="300X200(B2-N)">300X200(B2-N)</option><option value="300X200(B2-O)">300X200(B2-O)</option><option value="300X200(B2-P)">300X200(B2-P)</option><option value="300X200(B2-Q)">300X200(B2-Q)</option><option value="300X200(B2-R)">300X200(B2-R)</option><option value="300X200(B2-RA)">300X200(B2-RA)</option><option value="300X200(B2-S)">300X200(B2-S)</option><option value="300X200(B2-T)">300X200(B2-T)</option><option value="300X200(B2-TA)">300X200(B2-TA)</option><option value="300X200(B2-U)">300X200(B2-U)</option><option value="300X200(B2-V)">300X200(B2-V)</option><option value="300X200(B2-W)">300X200(B2-W)</option><option value="300X200(B4)">300X200(B4)</option><option value="300X200(B4-A)">300X200(B4-A)</option><option value="300X200(B4-B)">300X200(B4-B)</option><option value="300X200(B4-C)">300X200(B4-C)</option><option value="300X200(B4-D)">300X200(B4-D)</option><option value="300X200(B4-E)">300X200(B4-E)</option><option value="300X200(B4-EA)">300X200(B4-EA)</option><option value="300X200(B4-F)">300X200(B4-F)</option><option value="300X200(B4-FA)">300X200(B4-FA)</option><option value="300X300">300X300</option><option value="300X305">300X305</option><option value="300X90">300X90</option><option value="300X90H">300X90H</option><option value="300X90L">300X90L</option><option value="300X90M">300X90M</option><option value="304X301">304X301</option><option value="305CT">305CT</option><option value="305X102">305X102</option><option value="305X165">305X165</option><option value="305X203">305X203</option><option value="305X305">305X305</option><option value="310 UB">310 UB</option><option value="310 UC">310 UC</option><option value="338X351">338X351</option><option value="344X354">344X354</option><option value="346X300">346X300</option><option value="350X150">350X150</option><option value="350X150H">350X150H</option><option value="350X150L">350X150L</option><option value="350X175">350X175</option><option value="350X250">350X250</option><option value="350X250(B1-A)">350X250(B1-A)</option><option value="350X250(B1-B)">350X250(B1-B)</option><option value="350X250(B1-C)">350X250(B1-C)</option><option value="350X250(B1-D)">350X250(B1-D)</option><option value="350X250(B7)">350X250(B7)</option><option value="350X250(B7-A)">350X250(B7-A)</option><option value="350X250(B7-AA)">350X250(B7-AA)</option><option value="350X300">350X300</option><option value="350X350">350X350</option><option value="352X125">352X125</option><option value="356X127">356X127</option><option value="356X171">356X171</option><option value="356X254">356X254</option><option value="356X368">356X368</option><option value="356X406">356X406</option><option value="360 UB">360 UB</option><option value="380 PFC">380 PFC</option><option value="380X100">380X100</option><option value="380X100H">380X100H</option><option value="380X100L">380X100L</option><option value="380X100M">380X100M</option><option value="390X300">390X300</option><option value="394X405">394X405</option><option value="396X199">396X199</option><option value="400X100">400X100</option><option value="400X125">400X125</option><option value="400X150">400X150</option><option value="400X150H">400X150H</option><option value="400X150L">400X150L</option><option value="400X170">400X170</option><option value="400X200">400X200</option><option value="400X200( SC1-3)">400X200( SC1-3)</option><option value="400X200(SC1-1)">400X200(SC1-1)</option><option value="400X200(SC1-2)">400X200(SC1-2)</option><option value="400X300">400X300</option><option value="400X400">400X400</option><option value="400X408">400X408</option><option value="406X140">406X140</option><option value="406X178">406X178</option><option value="410 UB">410 UB</option><option value="450X175H">450X175H</option><option value="450X175L">450X175L</option><option value="450X200">450X200</option><option value="450X300">450X300</option><option value="450X550">450X550</option><option value="457X152">457X152</option><option value="457X191">457X191</option><option value="460 UB">460 UB</option><option value="4XTREME (F) EXTENSION">4XTREME (F) EXTENSION</option><option value="4XTREME (F) STANDARD">4XTREME (F) STANDARD</option><option value="4XTREME 5.2M EXTENSION">4XTREME 5.2M EXTENSION</option><option value="4XTREME 5.2m STANDARD">4XTREME 5.2m STANDARD</option><option value="500X200">500X200</option><option value="500X300">500X300</option><option value="506X201">506X201</option><option value="50X100">50X100</option><option value="530 UB">530 UB</option><option value="533X165">533X165</option><option value="533X210">533X210</option><option value="533X312">533X312</option><option value="600X190">600X190</option><option value="600X190H">600X190H</option><option value="600X190L">600X190L</option><option value="600X200">600X200</option><option value="600X300">600X300</option><option value="610 UB">610 UB</option><option value="610X178">610X178</option><option value="610X229">610X229</option><option value="610X305">610X305</option><option value="610X324">610X324</option><option value="62.5X125">62.5X125</option><option value="686X254">686X254</option><option value="700-900">700-900</option><option value="700X300">700X300</option><option value="75X100">75X100</option><option value="75X150">75X150</option><option value="75X40">75X40</option><option value="75X6">75X6</option><option value="75X75">75X75</option><option value="75X9">75X9</option><option value="762X267">762X267</option><option value="800X300">800X300</option><option value="808X302">808X302</option><option value="838X292">838X292</option><option value="87.5X175">87.5X175</option><option value="900X300">900X300</option><option value="90X10">90X10</option><option value="90X12">90X12</option><option value="90X13">90X13</option><option value="90X6">90X6</option><option value="90X7">90X7</option><option value="90X8">90X8</option><option value="97X150">97X150</option><option value="A1">A1</option><option value="A4">A4</option><option value="ALLOY CHARGING">ALLOY CHARGING</option><option value="ANCHOR FRAME">ANCHOR FRAME</option><option value="ARANYAPRATHET1">ARANYAPRATHET1</option><option value="ARANYAPRATHET2">ARANYAPRATHET2</option><option value="ASTRA">ASTRA</option><option value="B&amp;N">B&amp;N</option><option value="B&amp;NM10">B&amp;NM10</option><option value="BAAN CHOKE">BAAN CHOKE</option><option value="BAAN CHOKE 2">BAAN CHOKE 2</option><option value="BAAN MAERIM 1">BAAN MAERIM 1</option><option value="BAAN MAERIM 2">BAAN MAERIM 2</option><option value="BAC ROOF STRUCTURE">BAC ROOF STRUCTURE</option><option value="BANGCHAK SR">BANGCHAK SR</option><option value="BANGKHAE HOUSE">BANGKHAE HOUSE</option><option value="BANGKRUAI COFFEE SHOP">BANGKRUAI COFFEE SHOP</option><option value="BEAM LAOS">BEAM LAOS</option><option value="BL200X350">BL200X350</option><option value="BL200X400">BL200X400</option><option value="BOILER STRUCTURE">BOILER STRUCTURE</option><option value="BORE PILE">BORE PILE</option><option value="BRACKET AND ACCESSORY">BRACKET AND ACCESSORY</option><option value="BUILDING">BUILDING</option><option value="BUILDING 2">BUILDING 2</option><option value="C150XH175">C150XH175</option><option value="CAR PARK BANG BON">CAR PARK BANG BON</option><option value="CASING DIAMETER">CASING DIAMETER</option><option value="CASING EQUIPMENT">CASING EQUIPMENT</option><option value="CASTELLATED BEAM">CASTELLATED BEAM</option><option value="CB 300X150">CB 300X150</option><option value="CB 350X175">CB 350X175</option><option value="CB PLATFORM 1">CB PLATFORM 1</option><option value="CB PLATFORM 1X2.5 H">CB PLATFORM 1X2.5 H</option><option value="CB PLATFORM 1X2.5 L">CB PLATFORM 1X2.5 L</option><option value="CB PLATFORM 1X3.5 H">CB PLATFORM 1X3.5 H</option><option value="CB PLATFORM 1X3.5 L">CB PLATFORM 1X3.5 L</option><option value="CB PLATFORM 2">CB PLATFORM 2</option><option value="CB PLATFORM 3">CB PLATFORM 3</option><option value="CB PLATFORM 4">CB PLATFORM 4</option><option value="CB PLATFORM 5">CB PLATFORM 5</option><option value="CB PLATFORM 6">CB PLATFORM 6</option><option value="CB PLATFORM MARUKEN">CB PLATFORM MARUKEN</option><option value="CB RESEARCH CU">CB RESEARCH CU</option><option value="CB SIAM THAMMANON">CB SIAM THAMMANON</option><option value="CELLULAR BEAM">CELLULAR BEAM</option><option value="CHAIYAPHUM HOUSE">CHAIYAPHUM HOUSE</option><option value="CHAOWALIT CARPORT">CHAOWALIT CARPORT</option><option value="CHAOWALIT CARPORT2">CHAOWALIT CARPORT2</option><option value="CHIANG MAI HOUSE">CHIANG MAI HOUSE</option><option value="CHIANGMAI">CHIANGMAI</option><option value="COATED SAMUI">COATED SAMUI</option><option value="COFFEE SHOP">COFFEE SHOP</option><option value="CORNER I">CORNER I</option><option value="CORNER II">CORNER II</option><option value="CORNER III">CORNER III</option><option value="CORNER IV">CORNER IV</option><option value="CORNER PLATFORM">CORNER PLATFORM</option><option value="CRANE RAIL">CRANE RAIL</option><option value="CRANE STRUCTURE">CRANE STRUCTURE</option><option value="CU TESTING TABLE">CU TESTING TABLE</option><option value="DINKAO">DINKAO</option><option value="DRILLED 300X90">DRILLED 300X90</option><option value="DRILLED 300X90X12X16">DRILLED 300X90X12X16</option><option value="EV CHARGING">EV CHARGING</option><option value="FAB-HOLIDAY INN">FAB-HOLIDAY INN</option><option value="FABRICATED">FABRICATED</option><option value="FABRICATED 530UB">FABRICATED 530UB</option><option value="FABRICATED 530UB (P)">FABRICATED 530UB (P)</option><option value="FABRICATED H 500X200">FABRICATED H 500X200</option><option value="FEST SHOP">FEST SHOP</option><option value="FLEX 5M@0.5M">FLEX 5M@0.5M</option><option value="FLEX 5M@0.5M-INSTALL">FLEX 5M@0.5M-INSTALL</option><option value="FLEX 5M@1M">FLEX 5M@1M</option><option value="FLEX 5M@1M-INSTALL">FLEX 5M@1M-INSTALL</option><option value="FLEX 6M@0.5M">FLEX 6M@0.5M</option><option value="FLEX 6M@0.5M-INSTALL">FLEX 6M@0.5M-INSTALL</option><option value="FLEX 6M@1M">FLEX 6M@1M</option><option value="FLEX 6M@1M-INSTALL">FLEX 6M@1M-INSTALL</option><option value="FRUIT WAREHOUSE">FRUIT WAREHOUSE</option><option value="GARAGE 1">GARAGE 1</option><option value="GARAGE 1-1">GARAGE 1-1</option><option value="GARAGE 2-2">GARAGE 2-2</option><option value="GARAGE 2-4">GARAGE 2-4</option><option value="GARAGE EX 1">GARAGE EX 1</option><option value="GARAGE EX 2-2">GARAGE EX 2-2</option><option value="GARAGE EX 2-4">GARAGE EX 2-4</option><option value="GARAGE SAWANKALOK">GARAGE SAWANKALOK</option><option value="GETABAC">GETABAC</option><option value="G-PARK">G-PARK</option><option value="GUARD RAIL">GUARD RAIL</option><option value="GUIDE BEAM CURVED">GUIDE BEAM CURVED</option><option value="GUIDE BEAM TANGENT">GUIDE BEAM TANGENT</option><option value="HANGAR">HANGAR</option><option value="HANGAR PHITSANULOK">HANGAR PHITSANULOK</option><option value="HE 100 A">HE 100 A</option><option value="HE 100 B">HE 100 B</option><option value="HE 160 A">HE 160 A</option><option value="HE 160 B">HE 160 B</option><option value="HE 180 A">HE 180 A</option><option value="HE 180 B">HE 180 B</option><option value="HE 200 A">HE 200 A</option><option value="HE 200 B">HE 200 B</option><option value="HE 220 A">HE 220 A</option><option value="HE 220 B">HE 220 B</option><option value="HE 260 A">HE 260 A</option><option value="HE 260 B">HE 260 B</option><option value="HE 300 A">HE 300 A</option><option value="HE 300 B">HE 300 B</option><option value="HE 320 A">HE 320 A</option><option value="HE 320 B">HE 320 B</option><option value="HE 360 A">HE 360 A</option><option value="HE 360 B">HE 360 B</option><option value="HE 400 A">HE 400 A</option><option value="HE 400 B">HE 400 B</option><option value="HE 450 A">HE 450 A</option><option value="HE 450 B">HE 450 B</option><option value="HE 500 A">HE 500 A</option><option value="HE 500 B">HE 500 B</option><option value="HE 600 A">HE 600 A</option><option value="HE 600 B">HE 600 B</option><option value="HEMARAJ CARPORT 1">HEMARAJ CARPORT 1</option><option value="HEMARAJ CARPORT ESIE 2">HEMARAJ CARPORT ESIE 2</option><option value="HEMARAJ CARPORT ESIE 4">HEMARAJ CARPORT ESIE 4</option><option value="HEMARAJ CARPORT PLAZA 1">HEMARAJ CARPORT PLAZA 1</option><option value="HEMARAJ CARPORT PLAZA 2">HEMARAJ CARPORT PLAZA 2</option><option value="HIGHWAY SIGN SAHAMIT">HIGHWAY SIGN SAHAMIT</option><option value="HOME BIRD">HOME BIRD</option><option value="HOME SOL EKKAPHAN 1">HOME SOL EKKAPHAN 1</option><option value="HONGSA">HONGSA</option><option value="HONGSA2">HONGSA2</option><option value="HUAI KHWANG HOUSE">HUAI KHWANG HOUSE</option><option value="INSTALL TOYOTA OFFICE">INSTALL TOYOTA OFFICE</option><option value="INTERCONTINENTAL PHUKET">INTERCONTINENTAL PHUKET</option><option value="IPE 100">IPE 100</option><option value="IPE 180">IPE 180</option><option value="IPE 200">IPE 200</option><option value="IPE 240">IPE 240</option><option value="IPE 270">IPE 270</option><option value="IPE 300">IPE 300</option><option value="IPE 330">IPE 330</option><option value="IPE 360">IPE 360</option><option value="IPE 400">IPE 400</option><option value="IPE 450">IPE 450</option><option value="IPE 500">IPE 500</option><option value="IPE 550">IPE 550</option><option value="IPE 600">IPE 600</option><option value="IPEA 100">IPEA 100</option><option value="IPEA 180">IPEA 180</option><option value="IPEA 300">IPEA 300</option><option value="IPEAA 100">IPEAA 100</option><option value="IPEAA 180">IPEAA 180</option><option value="IPEAA 200">IPEAA 200</option><option value="IPEO 300">IPEO 300</option><option value="IS FACTORY1">IS FACTORY1</option><option value="J-Lock">J-Lock</option><option value="JONGSUK-PILAILAK  HOUSE">JONGSUK-PILAILAK  HOUSE</option><option value="KANCHANABURI HOUSE">KANCHANABURI HOUSE</option><option value="KITCHEN PRASONG">KITCHEN PRASONG</option><option value="KRABI HOUSE">KRABI HOUSE</option><option value="LAKSI HOUSE">LAKSI HOUSE</option><option value="LAT LUM KAEO OFFICE">LAT LUM KAEO OFFICE</option><option value="LIFT STRUCTURE">LIFT STRUCTURE</option><option value="MAHACHAI APARTMENT">MAHACHAI APARTMENT</option><option value="MBK BATMINTON COURT">MBK BATMINTON COURT</option><option value="MIN BURI HOUSE">MIN BURI HOUSE</option><option value="MOMOKO">MOMOKO</option><option value="NAKHORN HOUSE">NAKHORN HOUSE</option><option value="NANTHANEE  HOUSE">NANTHANEE  HOUSE</option><option value="NATHINEE HOUSE">NATHINEE HOUSE</option><option value="NUTTHAKORN RES">NUTTHAKORN RES</option><option value="OFFICE PHAHOL (AMG)">OFFICE PHAHOL (AMG)</option><option value="P1">P1</option><option value="PAGODA AXLE (KHONKHEN)">PAGODA AXLE (KHONKHEN)</option><option value="PAGODA STRUCTURE (KHONKHEN)">PAGODA STRUCTURE (KHONKHEN)</option><option value="PAKNAM MARKET">PAKNAM MARKET</option><option value="PANEL III">PANEL III</option><option value="PATTANAKARN HOUSE">PATTANAKARN HOUSE</option><option value="PAWNSHOP">PAWNSHOP</option><option value="PHANIPHAK RES">PHANIPHAK RES</option><option value="PHETKASEM HOUSE">PHETKASEM HOUSE</option><option value="PHITSANULOK 1">PHITSANULOK 1</option><option value="PHITSANULOK 2">PHITSANULOK 2</option><option value="PHITSANULOK 3FIRE PROOF">PHITSANULOK 3FIRE PROOF</option><option value="PHITSANULOK 4FIRE PROOF">PHITSANULOK 4FIRE PROOF</option><option value="PL 100X100">PL 100X100</option><option value="PL 150X100">PL 150X100</option><option value="PL 150X150">PL 150X150</option><option value="PLANET CONSTRUCTION">PLANET CONSTRUCTION</option><option value="PLANKRIT HOUSE">PLANKRIT HOUSE</option><option value="PLATFORM">PLATFORM</option><option value="PLATFORM2">PLATFORM2</option><option value="PLATFORM2 (ADD PIPE)">PLATFORM2 (ADD PIPE)</option><option value="PLATFORM3">PLATFORM3</option><option value="PLATFORM4">PLATFORM4</option><option value="PRECAST MAIN GATE">PRECAST MAIN GATE</option><option value="PRECAST PANEL">PRECAST PANEL</option><option value="PRECAST RESEARCH CENTER">PRECAST RESEARCH CENTER</option><option value="PREFAB">PREFAB</option><option value="PREFAB CCB">PREFAB CCB</option><option value="PREFAB FL DUST COLLECTOR (DSTL)">PREFAB FL DUST COLLECTOR (DSTL)</option><option value="PREFAB PAGODA">PREFAB PAGODA</option><option value="PREFAB ROOF STR RAMA5">PREFAB ROOF STR RAMA5</option><option value="PREFAB SCAFFOLDING TEMPLE">PREFAB SCAFFOLDING TEMPLE</option><option value="PREFAB SMC">PREFAB SMC</option><option value="PREFAB SP-II">PREFAB SP-II</option><option value="PREFAB STR DUST COLLECTOR (DSTL)">PREFAB STR DUST COLLECTOR (DSTL)</option><option value="PREFAB&amp;INSTALL">PREFAB&amp;INSTALL</option><option value="PREFAB-INSTALL HOLIDAY INN GARAGE">PREFAB-INSTALL HOLIDAY INN GARAGE</option><option value="PREFAB-INSTALL SCG LAMPANG GARAGE">PREFAB-INSTALL SCG LAMPANG GARAGE</option><option value="PTT P1-3000">PTT P1-3000</option><option value="PTT P2-3000">PTT P2-3000</option><option value="PTT P2-3000 P">PTT P2-3000 P</option><option value="PUNCHING SHEAR TABLE">PUNCHING SHEAR TABLE</option><option value="RACK">RACK</option><option value="RACK 15T">RACK 15T</option><option value="RACK 30T">RACK 30T</option><option value="RACK1">RACK1</option><option value="RACK2">RACK2</option><option value="RACK3">RACK3</option><option value="RAMA2 HOUSE">RAMA2 HOUSE</option><option value="READY TO WELD SAIMAI 73">READY TO WELD SAIMAI 73</option><option value="READY TO WELD SAIMAI 73 2">READY TO WELD SAIMAI 73 2</option><option value="READY TO WELD SAIMAI 73 3">READY TO WELD SAIMAI 73 3</option><option value="READY TO WELD TAI KINGSING">READY TO WELD TAI KINGSING</option><option value="RIDE PLATE">RIDE PLATE</option><option value="ROLLER PLATE (GR1)">ROLLER PLATE (GR1)</option><option value="ROLLER PLATE (GR2)">ROLLER PLATE (GR2)</option><option value="ROLLER PLATE (GR3)">ROLLER PLATE (GR3)</option><option value="ROLLER PLATE (GR4)">ROLLER PLATE (GR4)</option><option value="ROOF TRUSS SCG">ROOF TRUSS SCG</option><option value="ROOFING C">ROOFING C</option><option value="RUAMJAI1">RUAMJAI1</option><option value="RUAMJAI2">RUAMJAI2</option><option value="SAKCHAI HOUSE">SAKCHAI HOUSE</option><option value="SAKON NAKHON 1">SAKON NAKHON 1</option><option value="SAKON NAKHON 2">SAKON NAKHON 2</option><option value="SALA">SALA</option><option value="SALA STRUCTURE">SALA STRUCTURE</option><option value="SAWANG RUNGROTE 1">SAWANG RUNGROTE 1</option><option value="SAWANG RUNGROTE 2">SAWANG RUNGROTE 2</option><option value="SB4L">SB4L</option><option value="SCG BUILDING26">SCG BUILDING26</option><option value="SCG EMERGENCY STAIR">SCG EMERGENCY STAIR</option><option value="SCG EMERGENCY STAIR (FIRE PROOF)">SCG EMERGENCY STAIR (FIRE PROOF)</option><option value="SCG EXP1">SCG EXP1</option><option value="SCG EXP2">SCG EXP2</option><option value="SCG FOUNDATION SCHOOL">SCG FOUNDATION SCHOOL</option><option value="SHELL CANOPY I">SHELL CANOPY I</option><option value="SIAM PARK CITY">SIAM PARK CITY</option><option value="SILO SUPPORT STRUCTURE">SILO SUPPORT STRUCTURE</option><option value="SIW WAREHOUSE 1">SIW WAREHOUSE 1</option><option value="SIW WAREHOUSE 2">SIW WAREHOUSE 2</option><option value="SMT MAESOD">SMT MAESOD</option><option value="SOLAR EXTENDSION">SOLAR EXTENDSION</option><option value="SOLAR EXTENDSION-INSTALL">SOLAR EXTENDSION-INSTALL</option><option value="SOLAR STANDARD">SOLAR STANDARD</option><option value="SOLAR STANDARD-INSTALL">SOLAR STANDARD-INSTALL</option><option value="SOMPRASONG HOUSE">SOMPRASONG HOUSE</option><option value="SP1">SP1</option><option value="SPLICE PL">SPLICE PL</option><option value="SPREADER BEAM UNDERPIN">SPREADER BEAM UNDERPIN</option><option value="ST1">ST1</option><option value="STANCHION">STANCHION</option><option value="STEEL BRIDGE">STEEL BRIDGE</option><option value="STEEL PLATE">STEEL PLATE</option><option value="Steel Transfer Beam">Steel Transfer Beam</option><option value="STRUT">STRUT</option><option value="STRUT H350X350">STRUT H350X350</option><option value="SUKHUMVIT 62 HOUSE">SUKHUMVIT 62 HOUSE</option><option value="SUPPORT GUIDE BEAM CURVED">SUPPORT GUIDE BEAM CURVED</option><option value="SUPPORT GUIDE BEAM TANGENT">SUPPORT GUIDE BEAM TANGENT</option><option value="SYC">SYC</option><option value="SYS NEW WAREHOUSE">SYS NEW WAREHOUSE</option><option value="SYS PLATFORM">SYS PLATFORM</option><option value="TAKLI HOUSE">TAKLI HOUSE</option><option value="TALING CHAN HOUSE">TALING CHAN HOUSE</option><option value="TANAO HOSTEL">TANAO HOSTEL</option><option value="TEMPORARY BRACING I">TEMPORARY BRACING I</option><option value="TEMPORARY BRACING II">TEMPORARY BRACING II</option><option value="TEMPORARY BRIDGE">TEMPORARY BRIDGE</option><option value="TEST ROOM">TEST ROOM</option><option value="TEST TABLE1">TEST TABLE1</option><option value="TEST TABLE2">TEST TABLE2</option><option value="TEST TABLE3">TEST TABLE3</option><option value="TEST TABLE4">TEST TABLE4</option><option value="TESTING COLUMN">TESTING COLUMN</option><option value="TESTING TABLE">TESTING TABLE</option><option value="THAI FULLMORE">THAI FULLMORE</option><option value="TOYOTA OFFICE">TOYOTA OFFICE</option><option value="TOYOTA RICH 1">TOYOTA RICH 1</option><option value="TOYOTA RICH 2">TOYOTA RICH 2</option><option value="TRACKER POST">TRACKER POST</option><option value="UDON-THANI HOUSE">UDON-THANI HOUSE</option><option value="VCM-SKK">VCM-SKK</option><option value="VCM-SKK3">VCM-SKK3</option><option value="VIBHAWADI HOUSE">VIBHAWADI HOUSE</option><option value="VORRAPOT CARPORT">VORRAPOT CARPORT</option><option value="WAT PA CHANTUK">WAT PA CHANTUK</option><option value="WELFARE HOUSE">WELFARE HOUSE</option><option value="XTREME EXT">XTREME EXT</option><option value="XTREME EXT-INS">XTREME EXT-INS</option><option value="XTREME STD">XTREME STD</option><option value="XTREME STD-INS">XTREME STD-INS</option><option value="YSB200">YSB200</option><option value="เหล็กเส้นกลม RB25">เหล็กเส้นกลม RB25</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">Test</option><option value="ๅๅ">ๅๅ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>WEIGHT</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SIZE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="MY">=6M,9M,12M</option><option value="MN">&lt;&gt;6M,9M,12M</option><option value="FY">=20F,30F,40F</option><option value="FN">&lt;&gt;20F,30F,40F</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>LENGTH</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>M/FT</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="M">METERS</option><option value="F">FEET</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="EST.SHIPMENT DO.,EST.SHIPMENT DO.">
                            <Row>
                                <Col sm={3} className="email-card">
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col sm={9} className="text-right">
                                    <div className="form-check-inline">
                                        <Form.Label className="col-sm-3">SET FIELD VALUE</Form.Label>
                                        <Form.Control as="select" className="wid-150">
                                            <option value="a.Fx_Rate_USD">FX_RATE_USD</option>
                                            <option value="a.SumOffRate">REBATE</option>
                                            <option value="a.InLand">INLAND_THB</option>
                                            <option value="a.FreightAmt">FREIGHT</option>
                                            <option value="a.InsureAmt">INSURANCE</option>
                                            <option value="a.CommAmt">COMMISSION</option>
                                            <option value="a.VC">VC_THB</option>
                                        </Form.Control> :
                                        <Form.Control type="text" className="wid-100 text-center mr-sm-1" defaultValue="0.00" />
                                        <Button className="btn btn-success" size="sm" > SET </Button>
                                        <Form.Label className="col">STATUS</Form.Label>
                                        <Form.Control as="select" className="wid-150 mr-sm-1">
                                            <option value="STOCK1">STOCK 1</option>
                                            <option value="STOCK2">STOCK 2</option>
                                            <option value="ROLL1">ROLLING 1</option>
                                            <option value="ROLL2">ROLLING 2</option>
                                            <option value="NEXT1">NEXT 1</option>
                                            <option value="NEXT2">NEXT 2</option>
                                        </Form.Control>
                                        <Button className="btn btn-success" size="sm" > SET </Button>
                                        <Button className="btn btn-primary ml-sm-2 w-100 mr-sm-3" size="sm" onClick={e => this.setShowModal(e, "Edit")}>  MORE.. </Button>

                                    </div>
                                </Col>


                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className="ml-sm-4" id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>CUSTOMER</th>
                                        <th>PROJECT DESC.</th>
                                        <th>PRODUCT SIZE</th>
                                        <th>GROUP</th>
                                        <th>ORD.
                                            YR.MTH
                                        </th>
                                        <th>SAP#</th>
                                        <th>ORDER
                                            LOT-REV</th>
                                        <th>PAYMENT
                                            TERM</th>
                                        <th>INCO
                                            TERM</th>
                                        <th>วันที่แจ้งลูกค้า</th>
                                        <th>DP.NO</th>
                                        <th>DP
                                            DATE</th>
                                        <th>ROLL
                                            DATE
                                        </th>
                                        <th>PLANT</th>
                                        <th>SEC</th>
                                        <th>NOMINAL</th>
                                        <th>COMBINED
                                            ROLL</th>
                                        <th>ORDER
                                            ท่อน [ ตัน ]</th>
                                        <th>TRADER
                                            COMM%</th>
                                        <th>TRADER
                                            COMM AMT</th>
                                        <th>CODE1
                                            COMM%
                                        </th>
                                        <th>CODE1
                                            COMM AMT</th>
                                        <th>REBATE
                                            RATE</th>
                                        <th>REBATE
                                            AMT</th>
                                        <th>NET
                                            PRICE</th>
                                        <th>NET
                                            AMT
                                        </th>
                                        <th>NET หน้าตั๋ว
                                            AMT</th>
                                        <th>STATUS</th>
                                        <th>UPDATED DATE</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
