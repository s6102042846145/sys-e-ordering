import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> การรับสินค้า </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/create-approval-request">
                                        สร้างใบขอนุมัติตั๋วตัดจ่ายล่วงหน้า
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/create-car-arrangement" >
                                        สร้างใบจัดรถ
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/create-purchase-order-SYS" >
                                        สร้างใบสั่งซื้อ (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/create-check-the-payment-summary-SYS" >
                                        สร้าง ใบสรุปยอดจ่ายเช็ค (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/arrange-a-car-specify-DP-number" >
                                        ใบจัดรถ (กำหนดเลข DP)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/sales-ORG-0180" >
                                        ใบจัดรถ (กำหนดเลข DP) SALES ORG 0180
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/est.shipment-do" >
                                        EST.SHIPMENT DO.
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>
                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/arrange-transportation-ssj" >
                                        จัดการใบจัดรถขนส่ง สสจ
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/approve-cut-off-ticket" >
                                        ใบขอนุมัติตั๋วตัดฝาก
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/domestic-arrange-transport" >
                                        ใบจัดรถขนส่ง (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/purchase-order" >
                                        ใบสั่งซื้อ (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>


                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/summary-of-payment" >
                                        สรุปยอดจ่ายเช็ค
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/delivery-schedule" >
                                        ตารางจัดรถส่งให้ (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/domestic-arrange-a-car" >
                                        ใบจัดรถ (กำหนดเลข DP) CFR
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>  

                                        <Nav.Link className='list-group-item list-group-item-action' href="/domestic/domestic-order/EST.shipment-do.old" >
                                        EST.SHIPMENT DO.OLD
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Nav.Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;