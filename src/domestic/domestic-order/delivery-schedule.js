import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab,Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interaction from "@fullcalendar/interaction";
import timeGrid from "@fullcalendar/timegrid";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ isModal: true });
            this.setState({ setTitleModal: "CUSTOMER" })
        } else if (type === "Add") {
            this.setState({ isModal: false });
            this.setState({ isModalAdd: true });
            this.setState({ setTitleModalAdd: "เพิ่มข้อมูล" })
        } else if (type === "Edit") {
            this.setState({ isModalAdd: true });
            this.setState({ setTitleModalAdd: "แก้ไขข้อมูล" })
        } else if (type === "Add1") {
            this.setState({ isModalAdd1: true });
            this.setState({ setTitleModalAdd1: "รายการสินค้าพร้อมจ่าย" })
        } else if (type === "Add2") {
            this.setState({ isModalAdd2: true });
            this.setState({ setTitleModalAdd2: "ตั๋วตัดฝาก" })
        } else if (type === "Add3") {
            this.setState({ isModalAdd3: true });
            this.setState({ setTitleModalAdd3: "ตั๋วตัดฝากจากใบแจ้งตัด" })
        } else if (type === "Add4") {
            this.setState({ isModalAdd4: true });
            this.setState({ setTitleModalAdd4: "ORDER DETAIL" })
        } else if (type === "Add5") {
            this.setState({ isModalAdd5: true });
            this.setState({ setTitleModalAdd5: "สินค้าจากใบจัดรถ" })
        } else if (type === "Cancal") {
            this.setState({ isModalAdd1: false });
        } else {

        }

    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {
        const event = [
            {
                title: "ช.การช่าง BDC-เทเลอร์ SYS ส่งให้",
                start: "2021-07-27",
                borderColor: "#04a9f5",
                backgroundColor: "#04a9f5",
                textColor: "#fff"
            },
            {
                title: "จองรถขนส่ง",
                start: "2021-07-30",
                end: "2018-08-10",
                borderColor: "#1de9b6",
                backgroundColor: "#1de9b6",
                textColor: "#fff"
            },
            {
                title: "จองรถขนส่ง",
                start: "2021-07-31",
                end: "2018-08-10",
                borderColor: "#1de9b6",
                backgroundColor: "#1de9b6",
                textColor: "#fff"
            },
            {
                title: "จองรถขนส่ง",
                start: "2021-08-01",
                end: "2018-08-10",
                borderColor: "#1de9b6",
                backgroundColor: "#1de9b6",
                textColor: "#fff"
            }
        ];
        const head = {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay"
        };
        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd5} onHide={() => this.setState({ isModalAdd5: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd5}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}> ประเภทเอกสาร</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="P">ตัดจ่ายล่วงหน้า</option><option value="S">ธรรมดา</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ใบจัดรถ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ PO</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ DP</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อคนขับรถ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ทะเบียนรถ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={8}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยการกรอก</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="T">ตัน</option><option value="P">ท่อน</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Grade</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="MY">=6M,9M,12M</option>
                                                    <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                    <option value="FY">=20F,30F,40F</option>
                                                    <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วย</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="M">เมตร</option>
                                                    <option value="F">ฟุต</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control as="select">
                                                        <option value={0}>Create Date</option>
                                                        <option value={1}>Update Date</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 1 ||
                                                this.state.selectDate === 2 ||
                                                this.state.selectDate === 3 ||
                                                this.state.selectDate === 4 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 5 ||
                                                this.state.selectDate === 6 ||
                                                this.state.selectDate === 7 ||
                                                this.state.selectDate === 8 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>From</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 9 ||
                                                this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 11 ||
                                                this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by numeric</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control
                                                    as="select"
                                                    value={this.state.supportedSelect}
                                                    onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                >
                                                    <option value={0}>None</option>
                                                    <option value={1}>At</option>
                                                    <option value={2}>Between</option>
                                                    <option value={3}>Less than</option>
                                                    <option value={4}>Less than or equal</option>
                                                    <option value={5}>At Month</option>
                                                    <option value={6}>Between Month</option>
                                                    <option value={7}>More than</option>
                                                    <option value={8}>More than or equal</option>
                                                    <option value={9}>At Quater</option>
                                                    <option value={10}>Between Quater</option>
                                                    <option value={11}>At Year</option>
                                                    <option value={12}>Between Year</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>To</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>More than</option>
                                                        <option value={6}>More than or equal</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>



                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ประเภท</th>
                                                <th>วันที่
                                                    ต้องการรับ</th>
                                                <th>เลขที่ใบจัดรถ</th>
                                                <th>ทะเบียนรถ</th>
                                                <th>ที่</th>
                                                <th>รายละเอียดสินค้า</th>
                                                <th>สั่งซื้อ
                                                    ท่อน</th>
                                                <th>สั่งซื้อ
                                                    ตัน</th>
                                                <th>พร้อมจ่าย
                                                    ท่อน</th>
                                                <th>พร้อมจ่าย
                                                    ตัน</th>
                                                <th>ยอดจ่าย
                                                    ท่อน</th>
                                                <th>ยอดจ่าย
                                                    ตัน</th>
                                                <th>เลขที่ PO</th>
                                                <th>เลขที่ DP</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colSpan="15" className="text-center">No Data.</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd5: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd4} onHide={() => this.setState({ isModalAdd4: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd4}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ใบแจ้งตัด</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยการกรอก</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="T">ตัน</option><option value="P">ท่อน</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เรียงลำดับ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="1">SIZE,ADV.YR-MTH</option><option value="2">ADV.YR-MTH,SIZE</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Grade</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="MY">=6M,9M,12M</option>
                                                    <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                    <option value="FY">=20F,30F,40F</option>
                                                    <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วย</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="M">เมตร</option>
                                                    <option value="F">ฟุต</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control as="select">
                                                        <option value={0}>Create Date</option>
                                                        <option value={1}>Update Date</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 1 ||
                                                this.state.selectDate === 2 ||
                                                this.state.selectDate === 3 ||
                                                this.state.selectDate === 4 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 5 ||
                                                this.state.selectDate === 6 ||
                                                this.state.selectDate === 7 ||
                                                this.state.selectDate === 8 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>From</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 9 ||
                                                this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 11 ||
                                                this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by numeric</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control
                                                    as="select"
                                                    value={this.state.supportedSelect}
                                                    onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                >
                                                    <option value={0}>None</option>
                                                    <option value={1}>At</option>
                                                    <option value={2}>Between</option>
                                                    <option value={3}>Less than</option>
                                                    <option value={4}>Less than or equal</option>
                                                    <option value={5}>At Month</option>
                                                    <option value={6}>Between Month</option>
                                                    <option value={7}>More than</option>
                                                    <option value={8}>More than or equal</option>
                                                    <option value={9}>At Quater</option>
                                                    <option value={10}>Between Quater</option>
                                                    <option value={11}>At Year</option>
                                                    <option value={12}>Between Year</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>To</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>More than</option>
                                                        <option value={6}>More than or equal</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>



                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>เลขที่ใบแจ้งตัด</th>
                                                <th>วันที่</th>
                                                <th>รายละเอียดสินค้า</th>
                                                <th>พร้อมจ่าย
                                                    ท่อน</th>
                                                <th>พร้อมจ่าย
                                                    ตัน</th>
                                                <th>ยอดจ่าย
                                                    ท่อน</th>
                                                <th>ยอดจ่าย
                                                    ตัน</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colSpan="8" className="text-center">No Data.</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd4: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd3} onHide={() => this.setState({ isModalAdd3: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd3}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ PO</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ DP</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ใบจัดรถ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={8}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยการกรอก</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="T">ตัน</option><option value="P">ท่อน</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={8}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}></Form.Label>
                                            <Col sm={8} className=" mt-sm-2">
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>ตรงตัวลูกค้า</Form.Label>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Grade</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="MY">=6M,9M,12M</option>
                                                    <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                    <option value="FY">=20F,30F,40F</option>
                                                    <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วย</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="M">เมตร</option>
                                                    <option value="F">ฟุต</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control as="select">
                                                        <option value={0}>Create Date</option>
                                                        <option value={1}>Update Date</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 1 ||
                                                this.state.selectDate === 2 ||
                                                this.state.selectDate === 3 ||
                                                this.state.selectDate === 4 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 5 ||
                                                this.state.selectDate === 6 ||
                                                this.state.selectDate === 7 ||
                                                this.state.selectDate === 8 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>From</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 9 ||
                                                this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 11 ||
                                                this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by numeric</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control
                                                    as="select"
                                                    value={this.state.supportedSelect}
                                                    onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                >
                                                    <option value={0}>None</option>
                                                    <option value={1}>At</option>
                                                    <option value={2}>Between</option>
                                                    <option value={3}>Less than</option>
                                                    <option value={4}>Less than or equal</option>
                                                    <option value={5}>At Month</option>
                                                    <option value={6}>Between Month</option>
                                                    <option value={7}>More than</option>
                                                    <option value={8}>More than or equal</option>
                                                    <option value={9}>At Quater</option>
                                                    <option value={10}>Between Quater</option>
                                                    <option value={11}>At Year</option>
                                                    <option value={12}>Between Year</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>To</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>More than</option>
                                                        <option value={6}>More than or equal</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>



                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>เลขที่ PO</th>
                                                <th>เลขที่ DP</th>
                                                <th>วันที่</th>
                                                <th>รายละเอียดสินค้า</th>
                                                <th>พร้อมจ่าย
                                                    ท่อน</th>
                                                <th>พร้อมจ่าย
                                                    ตัน</th>
                                                <th>ยอดจ่าย
                                                    ท่อน</th>
                                                <th>ยอดจ่าย
                                                    ตัน</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colSpan="9" className="text-center">No Data.</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd3: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd2} onHide={() => this.setState({ isModalAdd2: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd2}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER Year.</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="years" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER Month.</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="month" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รอบที่</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="no" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ PO</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ PD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่ใบจัดรถ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER NO.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER LOT.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER REV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>PO.REF</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยการกรอก</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="T">ตัน</option><option value="P">ท่อน</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เรียงลำดับ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="1">SIZE,ADV.YR-MTH</option><option value="2">ADV.YR-MTH,SIZE</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={8}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}></Form.Label>
                                            <Col sm={8} className=" mt-sm-2">
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>ตรงตัวลูกค้า</Form.Label>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Grade</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="MY">=6M,9M,12M</option>
                                                    <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                    <option value="FY">=20F,30F,40F</option>
                                                    <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วย</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="M">เมตร</option>
                                                    <option value="F">ฟุต</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control as="select">
                                                        <option value={0}>Create Date</option>
                                                        <option value={1}>Update Date</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 1 ||
                                                this.state.selectDate === 2 ||
                                                this.state.selectDate === 3 ||
                                                this.state.selectDate === 4 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 5 ||
                                                this.state.selectDate === 6 ||
                                                this.state.selectDate === 7 ||
                                                this.state.selectDate === 8 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>From</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 9 ||
                                                this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 11 ||
                                                this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by numeric</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control
                                                    as="select"
                                                    value={this.state.supportedSelect}
                                                    onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                >
                                                    <option value={0}>None</option>
                                                    <option value={1}>At</option>
                                                    <option value={2}>Between</option>
                                                    <option value={3}>Less than</option>
                                                    <option value={4}>Less than or equal</option>
                                                    <option value={5}>At Month</option>
                                                    <option value={6}>Between Month</option>
                                                    <option value={7}>More than</option>
                                                    <option value={8}>More than or equal</option>
                                                    <option value={9}>At Quater</option>
                                                    <option value={10}>Between Quater</option>
                                                    <option value={11}>At Year</option>
                                                    <option value={12}>Between Year</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>To</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>More than</option>
                                                        <option value={6}>More than or equal</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>



                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>เลขที่ PO</th>
                                                <th>เลขที่ DP</th>
                                                <th>วันที่</th>
                                                <th>อายุตั๋ว</th>
                                                <th>ADV.YYMM</th>
                                                <th>PLANT</th>
                                                <th>รายละเอียดสินค้า</th>
                                                <th>พร้อมจ่าย
                                                    ท่อน</th>
                                                <th>พร้อมจ่าย
                                                    ตัน</th>
                                                <th>ยอดจ่าย
                                                    ท่อน</th>
                                                <th>ยอดจ่าย
                                                    ตัน</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colSpan="12" className="text-center">No Data.</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd2: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd1} onHide={() => this.setState({ isModalAdd1: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd1}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER Year.</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="years" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER Month.</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="month" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รอบที่</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="no" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER NO.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER LOT.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER REV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ORDER Type</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="ALLA">ALL-Advance</option><option value="A">Adv-Stock</option><option value="AP">Adv-Project</option><option value="N">Project</option><option value="S">SYS-Stock</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE45">BE45 - ตั๋วแลกเงิน 45 วัน (BE)</option><option value="BE60">BE60 - ตั๋วแลกเงิน 60 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="BED0">BED0 - ตั๋วแลกเงิน 150 วัน (BE)</option><option value="BEF0">BEF0 - ตั๋วแลกเงิน 180 วัน (BE)</option><option value="BEH0">BEH0 - ตั๋วแลกเงิน 210 วัน (BE)</option><option value="BEJ0">BEJ0 - ตั๋วแลกเงิน 240 วัน (BE)</option><option value="BEL0">BEL0 - ตั๋วแลกเงิน 270 วัน (BE)</option><option value="BEN0">BEN0 - ตั๋วแลกเงิน 300 วัน (BE)</option><option value="BER0">BER0 - ตั๋วแลกเงิน 360 วัน (BE)</option><option value="BS30">BS30 - ตั๋วแลกเงิน 30 วัน (BE) before shipment.</option><option value="BS60">BS60 - ตั๋วแลกเงิน 60 วัน (BE) before shipment.</option><option value="BS90">BS90 - ตั๋วแลกเงิน 90 วัน (BE) before shipment.</option><option value="BSA0">BSA0 - ตั๋วแลกเงิน 105 วัน (BE) before shipment.</option><option value="BSB0">BSB0 - ตั๋วแลกเงิน 120 วัน (BE) before shipment.</option><option value="BSD0">BSD0 - ตั๋วแลกเงิน 150 วัน (BE) before shipment.</option><option value="BSF0">BSF0 - ตั๋วแลกเงิน 180 วัน (BE) before shipment.</option><option value="CA15">CA15 - DLC 15 days</option><option value="CD00">CD00 - DLC</option><option value="CD07">CD07 - DLC 7 days</option><option value="CD15">CD15 - DLC 15 days</option><option value="CD30">CD30 - DLC 30 days</option><option value="CD60">CD60 - DLC 60 days</option><option value="CD90">CD90 - DLC 90 days</option><option value="CDB0">CDB0 - DLC 120 days</option><option value="CH07">CH07 - DLC</option><option value="CH15">CH15 - DLC</option><option value="CH30">CH30 - DLC</option><option value="CH60">CH60 - DLC</option><option value="CH90">CH90 - DLC</option><option value="CT00">CT00 - DLC</option><option value="CT03">CT03 - DLC</option><option value="CV03">CV03 - DLC</option><option value="DB07">DB07 - D/A 7 days after receiced bill.</option><option value="DB30">DB30 - D/A 30 days after receiced bill.</option><option value="DF00">DF00 - Draft.</option><option value="DL15">DL15 - D/A 15 days after B/L date.</option><option value="DL30">DL30 - D/A 30 days after B/L date.</option><option value="DL45">DL45 - D/A 45 days after B/L date.</option><option value="DL60">DL60 - D/A 60 days after B/L date.</option><option value="DL75">DL75 - D/A 75 days after B/L date.</option><option value="DL90">DL90 - D/A 90 days after B/L date.</option><option value="DLB0">DLB0 - D/A 120 days after B/L date.</option><option value="DLF0">DLF0 - D/A 180 days after B/L date.</option><option value="DND0">DND0 - D/A 150 days from B/L date</option><option value="DP00">DP00 - D/P at sight</option><option value="DP07">DP07 - D/P 7 days after B/L date</option><option value="DP15">DP15 - D/P at sight</option><option value="DP30">DP30 - D/P 30 days after B/L date</option><option value="DP45">DP45 - D/P 45 days after B/L date</option><option value="DV07">DV07 - D/A 7 days after invoice,B/L,AWB date.</option><option value="DV14">DV14 - D/A 14 days after invoice,B/L,AWB date.</option><option value="DV30">DV30 - D/A 30 days after invoice,B/L,AWB date.</option><option value="FI15">FI15 - within 15th of next month</option><option value="FI30">FI30 - within 30th of next month</option><option value="HB03">HB03 - HSBC-Dealer Financing (Invoice Financing)</option><option value="KD07">KD07 - Credit 7 days.</option><option value="LC00">LC00 - L/C at sight.</option><option value="LC15">LC15 - L/C 15 days.</option><option value="N120">N120 - Within 120 days  after received bill</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM02">NM02 - เงินสด 2 วัน</option><option value="NM03">NM03 - เงินสด 3 วัน</option><option value="NM05">NM05 - เงินสด 5 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NM50">NM50 - เงินสด 50 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - BG 1 วัน</option><option value="NT02">NT02 - BG 2 วัน</option><option value="NT04">NT04 - BG 4 วัน</option><option value="NT05">NT05 - BG 5 วัน</option><option value="NT07">NT07 - BG 7 วัน</option><option value="NT10">NT10 - BG 10 วัน</option><option value="NT15">NT15 - BG 15 วัน</option><option value="NT21">NT21 - BG 21 วัน</option><option value="NT30">NT30 - BG 30 วัน</option><option value="NT45">NT45 - BG 45 วัน</option><option value="NT50">NT50 - BG 50 วัน</option><option value="NT60">NT60 - BG 60 วัน</option><option value="NT70">NT70 - BG 70 วัน</option><option value="NT75">NT75 - BG 75 วัน</option><option value="NT90">NT90 - BG 90 วัน</option><option value="NTB0">NTB0 - BG 120 วัน</option><option value="NTH0">NTH0 - BG 210 วัน</option><option value="NTJ0">NTJ0 - BG 240 วัน</option><option value="NTL0">NTL0 - BG 270 วัน</option><option value="NTN0">NTN0 - BG 300 วัน</option><option value="NTP0">NTP0 - BG 330 วัน</option><option value="NTR0">NTR0 - BG 360 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL30">TL30 - T/T 30 days after B/L date.</option><option value="TL35">TL35 - T/T 35 days after B/L date.</option><option value="TL45">TL45 - T/T 45 days after B/L date.</option><option value="TL60">TL60 - T/T 60 days after B/L date.</option><option value="TL75">TL75 - T/T 75 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TLB0">TLB0 - T/T 120 days after B/L date.</option><option value="TLD0">TLD0 - T/T 150 days after B/L date</option><option value="TLH0">TLH0 - T/T 210 days after B/L date</option><option value="TLI0">TLI0 - T/T 180 days after B/L date.</option><option value="TQ30">TQ30 - T/T 30 days after received goods</option><option value="TR60">TR60 - T/T remittance 60 days after shipment</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS07">TS07 - T/T before shipment 7 day.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option><option value="ZT00">ZT00 - L/C at sight 98%,T/T 2% after settlement</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">All</option><option value="CFR">CFR - Costs and freight</option><option value="EXW">EXW - Ex works</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>PO.REF</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยการกรอก</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="T">ตัน</option><option value="P">ท่อน</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เรียงลำดับ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="1">SIZE,ADV.YR-MTH</option><option value="2">ADV.YR-MTH,SIZE</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Grade</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="SM400">SM400</option><option value="SM490">SM490</option><option value="SM520">SM520</option><option value="SM570">SM570</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="MY">=6M,9M,12M</option>
                                                    <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                    <option value="FY">=20F,30F,40F</option>
                                                    <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ความยาว</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วย</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option>
                                                    <option value="M">เมตร</option>
                                                    <option value="F">ฟุต</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control as="select">
                                                        <option value={0}>Create Date</option>
                                                        <option value={1}>Update Date</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 1 ||
                                                this.state.selectDate === 2 ||
                                                this.state.selectDate === 3 ||
                                                this.state.selectDate === 4 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 5 ||
                                                this.state.selectDate === 6 ||
                                                this.state.selectDate === 7 ||
                                                this.state.selectDate === 8 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>From</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 9 ||
                                                this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 11 ||
                                                this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Search by numeric</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>From Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control
                                                    as="select"
                                                    value={this.state.supportedSelect}
                                                    onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                >
                                                    <option value={0}>None</option>
                                                    <option value={1}>At</option>
                                                    <option value={2}>Between</option>
                                                    <option value={3}>Less than</option>
                                                    <option value={4}>Less than or equal</option>
                                                    <option value={5}>At Month</option>
                                                    <option value={6}>Between Month</option>
                                                    <option value={7}>More than</option>
                                                    <option value={8}>More than or equal</option>
                                                    <option value={9}>At Quater</option>
                                                    <option value={10}>Between Quater</option>
                                                    <option value={11}>At Year</option>
                                                    <option value={12}>Between Year</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectDate === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 6 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={3}>To</Form.Label>
                                                    <Col sm={4}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                    <Col sm={5}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 10 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                        {
                                            this.state.selectDate === 12 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                    <Col sm={3}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Criteria</Form.Label>
                                            <Col sm={8}>
                                                <Form.Group>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>More than</option>
                                                        <option value={6}>More than or equal</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={3}>
                                        {
                                            this.state.selectValue === 2 ?

                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>To Value</Form.Label>
                                                    <Col sm={8}>
                                                        <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                    </Col>
                                                </Form.Group>

                                                : ''
                                        }
                                    </Col>
                                </Form.Group>



                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ADV.YR-MTH
                                                    /วันหมดอายุ</th>
                                                <th>เลขที่ใบสั่งซื้อ
                                                    /SAP#</th>
                                                <th>ชื่อโครงการ</th>
                                                <th>วิธีการชำระเงิน
                                                    /เงื่อนไขการขนส่ง</th>
                                                <th>รายละเอียดสินค้า</th>
                                                <th>สั่งซื้อ
                                                    ท่อน</th>
                                                <th>สั่งซื้อ
                                                    ตัน</th>
                                                <th>พร้อมจ่าย
                                                    ท่อน</th>
                                                <th>พร้อมจ่าย
                                                    ตัน</th>
                                                <th>ยอดจ่าย
                                                    ท่อน</th>
                                                <th>ยอดจ่าย
                                                    ตัน</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colSpan="12" className="text-center">No Data.</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd1: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModalAdd} onHide={() => this.setState({ isModalAdd: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAdd}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">เลขที่ใบจัดรถขนส่ง</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                            <Form.Label className="col-sm-3">วันที่ต้องการรับสินค้า</Form.Label>
                                            <Col sm={3}>
                                                <input type="date" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> เงื่อนไขการขนส่ง</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="CFR">CFR - ส่งให้</option><option value="EXW">EXW - รับเอง</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">ลูกค้า</Form.Label>
                                            <Col sm={9}>
                                                <input type="text" className="form-control-edit p-1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ประเภทการจัดรถ</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit p-1" defaultValue="PO ตั๋วตัดฝาก" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">ที่อยู่</Form.Label>
                                            <Col sm={9}>
                                                <input type="text" className="form-control-edit p-1" defaultValue="587 ถนนสุทธิสารวินิจฉัย แขวงดินแดง เขตดินแดง จ.กรุงเทพฯ" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เที่ยวที่</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit p-1"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>
                                            </Col>
                                        </Form.Group>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">ประเภทรถขนส่ง <span className="text-c-red">*</span></Form.Label>
                                            <Col sm={3}>
                                                <select className="form-control-edit"><option value="2">10 ล้อ ( 16.0 - 18.0 ตัน )</option><option value="1">6 ล้อ ( 8.0 - 10.0 ตัน )</option><option value="3">เทเลอร์ ( 28.0 - 30.0 ตัน )</option><option value="4">สาลี่ ( 28.0 - 30.0 ตัน )</option></select>
                                            </Col>
                                            <Form.Label className="col-sm-3">สถานที่รับสินค้า</Form.Label>
                                            <Col sm={3}>
                                                <select className="form-control-edit"><option value="NONE">ไม่ระบุ</option><option value="4951">BDC - บ้านบึง</option><option value="4941">SOL - SYS Solution</option><option value="4921">SR - ศรีราชา</option><option value="4911">SYS1 - ระยอง1</option><option value="4931">SYS2 - ระยอง2</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> อัตรา CFR</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center form-control-edit" defaultValue="1.0" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-2">ชื่อ สถานที่จัดส่งลูกค้า <span className="text-c-red">*</span></Form.Label>
                                            <Col sm={2}>
                                                <input type="text" className="form-control-edit p-1" />
                                            </Col>
                                            <Form.Label className="col-sm-2">ที่อยู่ <span className="text-c-red">*</span></Form.Label>
                                            <Col sm={6}>
                                                <input type="text" className="form-control-edit p-1" defaultValue="ช.การช่าง" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-2">จังหวัด/อำเภอ <span className="text-c-red">*</span></Form.Label>
                                            <Col sm={2}>
                                                <input type="text" className="form-control-edit p-1" />
                                            </Col>
                                            <Form.Label className="col-sm-2">วันที่ เวลานัดหมาย <span className="text-c-red">*</span></Form.Label>
                                            <Col sm={6}>
                                                <div className="form-check-inline">
                                                    <input type="date" className="wid-158 p-1" />
                                                    <Form.Label className="ml-sm-4 mr-sm-4">ตั้งแต่</Form.Label>
                                                    <input type="text" className="p-1 text-center wid-70" defaultValue="0000" />
                                                    <Form.Label className="ml-sm-1 mr-sm-1">ถึง</Form.Label>
                                                    <input type="text" className=" p-1 text-center wid-70" defaultValue="0000" />
                                                </div>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">รายละเอียดรถบรรทุก</Form.Label>
                                            <Col sm={9}>
                                                <input type="text" className="form-control-edit p-1" defaultValue="587 ถนนสุทธิสารวินิจฉัย แขวงดินแดง เขตดินแดง จ.กรุงเทพฯ" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">บริษัทรถขนส่ง</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="w-100 p-1" defaultValue="ไม่ระบุ" disabled />
                                            </Col>
                                            <Form.Label className="col-sm-3">ทะเบียนรถ</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="w-100 p-1" defaultValue="SYS ส่งให้" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ชื่อคนขับรถ</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="w-100 p-1" defaultValue="SYS ส่งให้" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">หมายเลขติดต่อ</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="w-100 p-1" defaultValue="-" disabled />
                                            </Col>
                                            <Form.Label className="col-sm-3">เลขที่บัตรประชาชนคนขับ</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="w-100 p-1" defaultValue="-" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">จำนวน copy</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center w-100 p-1" defaultValue="1" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={3}></Form.Label>
                                            <Col sm={3} className="mt-2">
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>ออกใบ Certification</Form.Label>
                                            </Col>
                                            <Col sm={3} className="mt-2">
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>ใบส่งของ</Form.Label>
                                            </Col>
                                            <Col sm={3} className="mt-2">
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>แผนที่</Form.Label>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={12} className="mt-sm-2">
                                        <Tabs variant="pills" defaultActiveKey="detail3">
                                            <Tab eventKey="detail3" title="รายละเอียดสินค้า">
                                                <Row className="mt-sm-2">
                                                    <Col className="email-card">
                                                        <Button id="btnDel" variant="default" className=" btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                                    </Col>

                                                    <Col className="btn-page text-right col-sm-10">
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Add1")}>เพิ่มสินค้าพร้อมจ่าย</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Add2")}>เพิ่มตั๋วตัดฝาก</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Add3")}>เพิ่มตั๋วตัดฝากจากใบแจ้งตัด</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Add4")}>เพิ่มเหล็กที่นำมาตัด</Button>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Add5")}>โอนสินค้าจากใบจัดรถ</Button>
                                                        <div className="form-check-inline">
                                                            <Form.Label >ประเภท </Form.Label>
                                                            <select className="form-control-edit ml-sm-1 mr-sm-n2 mt--4px"><option value="P">ท่อน</option><option value="T">ตัน</option></select>
                                                        </div>
                                                    </Col>
                                                </Row>
                                                <br />
                                                <Table ref="tbl" striped hover responsive bordered>
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check /></th>
                                                            <th>#</th>
                                                            <th>ORDER NO</th>
                                                            <th>ADV.YYMM</th>
                                                            <th>วิธีการ ชำระเงิน</th>
                                                            <th>เปลี่ยนเป็น</th>
                                                            <th>รายละเอียดสินค้า</th>
                                                            <th>รับ (ท่อน)</th>
                                                            <th>รับ (ตัน)</th>
                                                            <th>เลขที่ PO</th>
                                                            <th>เลขที่ DP</th>
                                                            <th>ตั๋ว ตัดฝาก</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr><td colSpan="12" className="text-center">No Data.</td></tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="detail4" title="แผนที่ & UPLOAD เอกสารอื่นๆ">
                                                <Row className="mt-sm-2">
                                                    <Col sm={12}>
                                                        <hr />
                                                        <h5>แผนที่</h5>
                                                        <br /><br />
                                                        <hr />
                                                        <h5>เอกสารอื่นๆ</h5>
                                                        <Row>
                                                            <Col sm={12}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-2">เลือก</Form.Label>
                                                                    <Col sm={5}>
                                                                        <Form.Control type="file" className="form-control-edit" />
                                                                    </Col>
                                                                    <Form.Label >ประเภท </Form.Label>
                                                                    <Col sm={2}>
                                                                        <select className="form-control-edit"><option value="DLV">ใบส่งของ</option><option value="MAP">แผนที่</option><option value="CS">Customer Master</option></select>
                                                                    </Col>

                                                                    <Button variant="success" size="sm" className="mt--4px" >UPLOAD</Button>

                                                                </Form.Group>
                                                            </Col>

                                                        </Row>

                                                        <hr />
                                                    </Col>
                                                </Row>

                                            </Tab>
                                        </Tabs>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-2">รายละเอียดเพิ่มเติม</Form.Label>
                                            <Col sm={10}>
                                                <textarea cols="3" className="w-100" ></textarea>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">วันที่สร้าง</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="22-ก.ค.-64 14:37" disabled />
                                            </Col>
                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="ISS@sys" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">วันที่แก้ไข</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="22-ก.ค.-64 14:37" disabled />
                                            </Col>
                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="ISS@sys" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-3">วันที่ยืนยันการจองรถ</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="-" disabled />
                                            </Col>
                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                            <Col sm={3}>
                                                <input type="text" className="text-center" defaultValue="-" disabled />
                                            </Col>
                                            <Col sm={1}>
                                                <Button variant="success" size="sm" className="mt--4px wid-100">ยืนยันการจอง</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                </Row>
                                <Row>
                                    <span className="col-sm-12">
                                        หมายเหตุ.- หากท่านออกเอกสารการรับสินค้านอกเวลาทำการปกติ (7.30 น. ถึง 17.00 น. และวันหยุด) สามารถรับสินค้าได้ในวันทำการถัดไป **
                                    </span>
                                </Row>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd: false })}>Close</Button>
                                <Button variant="success" >SAVE</Button>
                                <Button variant="primary" >PRINT ใบจัดรถขนส่ง</Button>
                                <Button variant="primary" >PRINT ALL</Button>
                            </Modal.Footer>
                        </Modal>


                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SALES ORG.CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CHANNEL CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>COUNTRY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CUST.CODE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>

                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GROUP</th>
                                                <th>COUNTRY</th>
                                                <th>CODE</th>
                                                <th>NAME</th>
                                                <th>ABBRV.NAME</th>
                                                <th>NAME (ENG.)</th>
                                                <th>ABBRV.NAME (ENG.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="cursor" onClick={e => this.setShowModal(e, "Add")}>
                                                <td >1</td>
                                                <td >0180_10&nbsp;</td>
                                                <td >TH
                                                    &nbsp;-&nbsp;Thailand
                                                    &nbsp;</td>
                                                <td >3001574&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                    </Form>
                                </Col>
                            </Row>
                            <Form.Group as={Row}>
                                <Col>
                                    <Button className="pull-right" size="sm" > SEARCH </Button>
                                </Col>
                            </Form.Group>
                        </MainCard>
                        <MainCard isOption title="ตารางจัดรถส่งให้ (SYS)">

                            <Col sm={12}>
                                <Form.Group as={Row}>
                                    <Tabs variant="pills" defaultActiveKey="detail1">
                                        <Tab eventKey="detail1" title="ใบจัดรถขนส่ง (SYS)">
                                            <Row>
                                                <Col sm={3} className="email-card"></Col>
                                                <Col sm={9} className="text-right">
                                                    <Button variant="success" size="sm" onClick={e => this.setShowModal(e, "Create")} className="text-right">สร้างใบจัดรถ (CFR)</Button>
                                                </Col>
                                            </Row>
                                            <div className="maincontainer">
                                                <FullCalendar
                                                    plugins={[dayGridPlugin, interaction, timeGrid]}
                                                    defaultView="dayGridMonth"
                                                    header={head}
                                                    editable={true}
                                                    defaultDate="2021-07-01"
                                                    droppable={true}
                                                    events={event}
                                                    /*
                                                    eventClick={
                                                        function (arg) {
                                                            alert(arg.event.title)
                                                            alert(arg.event.start)
                                                        }
                                                    }
                                                    */
                                                    eventClick={e => this.setShowModal(e, "Edit")}
                                                />
                                            </div>
                                        </Tab>
                                        <Tab eventKey="detail2" title="สรุปยอดโควต้าส่งให้">
                                            <br />
                                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>ปี-เดือน</th>
                                                        <th>โควต้าเลขที่</th>
                                                        <th>ชื่อลูกค้า</th>
                                                        <th>สั่งซื้อ
                                                            ตัน</th>
                                                        <th>โควต้า
                                                            ตัน</th>
                                                        <th>ใช้แล้ว
                                                            ตัน</th>
                                                        <th>คงเหลือ
                                                            ตัน</th>
                                                        <th>วัน
                                                            หมดอายุ</th>

                                                    </tr>
                                                </thead>
                                            </Table>

                                        </Tab>
                                    </Tabs>
                                </Form.Group>
                            </Col>



                        </MainCard>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>Full Calendar</Card.Title>
                            </Card.Header>
                            <Card.Body className='calendar'>
                                <FullCalendar
                                    defaultView="dayGridMonth"
                                    header={head}
                                    editable={true}
                                    defaultDate="2018-08-12"
                                    droppable={true}
                                    events={event}
                                    plugins={[dayGridPlugin, interaction, timeGrid]}
                                />
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
