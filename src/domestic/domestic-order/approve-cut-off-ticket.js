import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={12} className="text-center">
                                        <h6 className="text-center">ใบขอนุมัติตั๋วตัดฝาก ลงวันที่ 19-ก.ค.-64</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ลูกค้า</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">3001957-บ.มิตซุยแอนด์คัมปนี (ไทยแลนด์) จก.</span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ที่อยู่</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    175 อ.สาธรซิตี้ทาวเวอร์ ชั้น15-17
                                                    แขวงทุ่งมหาเมฆ เขตสาทร กรุงเทพมหานคร</span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    000006 - <span className="text-c-red">ซี.เอส.เอช.</span>
                                                </span>

                                            </Form.Label>
                                        </Form.Group>

                                    </Col>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เลขที่เอกสาร</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    PS20210157</span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    A - ส.ผจ.</span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>สถานที่จ่ายสินค้า</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    SR - ศรีราชา</span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                    EXW- Ex works</span>

                                            </Form.Label>
                                        </Form.Group>
                                    </Col>

                                </Row>

                                <Row>
                                    <Col className="email-card">
                                        <Button id="btnDel" variant="default" className=" btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple"><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>

                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="success">เพิ่มรายการ สินค้า</Button>
                                    </Col>
                                </Row>
                                <br />


                                <Table ref="tbl" striped hover responsive bordered >
                                    <thead>
                                        <tr>
                                            <th><input type='checkbox' /></th>
                                            <th>#</th>
                                            <th>รายละเอียดสินค้า</th>
                                            <th>DN.SYS</th>
                                            <th>DN.CBM</th>
                                            <th>ปริมาณ (ตัน)</th>
                                            <th>ปริมาณ (ท่อน)</th>
                                            <th>วันที่ต้องการรับ</th>
                                            <th>เลขที่ใบจัดรถ</th>
                                            <th>ORDER</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type='checkbox' /></td>
                                            <td>1</td>
                                            <td>H SS400/SM400 150X150X31.5KG/M 6M</td>
                                            <td>0361302241 </td>
                                            <td></td>
                                            <td>15.498</td>
                                            <td>82</td>
                                            <td>16-พ.ย.-09</td>
                                            <td>PC20090263</td>
                                            <td>0910 </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colSpan="5" className="text-right">
                                                <span>รวมทั้งสิ้น</span>
                                            </td>
                                            <td>15.498</td>
                                            <td>82</td>
                                            <td colSpan="3"></td>
                                        </tr>
                                    </tfoot>
                                </Table>


                                <Row>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>วันที่สร้าง</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">19-ก.ค.-21 16:01 </span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>วันที่แก้ไข</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                19-ก.ค. 21 16:01 </span>

                                            </Form.Label>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>โดย</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                ISS@sys </span>

                                            </Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>โดย</Form.Label>
                                            <Form.Label column sm={8}>
                                                :
                                                <span className="ml-1 font-weight-light">
                                                ISS@sys </span>

                                            </Form.Label>
                                        </Form.Group>
                                    </Col>

                                </Row>









                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">PRINT ใบขอนุมัติตั๋วตัดฝาก</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Col sm={12}>
                                            <Form.Group as={Row}>
                                                <Tabs variant="pills" defaultActiveKey="document" className=" form-control-file">
                                                    <Tab eventKey="document" title="DOCUMENT">
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>เลขที่เอกสาร</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>สถานที่รับสินค้า</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">ทั้งหมด</option><option value="4951">BDC - บ้านบึง</option><option value="4941">SOL - SYS Solution</option><option value="4921">SR - ศรีราชา</option><option value="4911">SYS1 - ระยอง1</option><option value="4931">SYS2 - ระยอง2</option>
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">All</option><option value="CFR">CFR - Costs and freight</option><option value="EXW">EXW - Ex works</option>%&gt;
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="A">ส.ผจ.</option><option value="B">ส.ขบ.</option><option value="C">ส.ตท.</option>
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>DP.SYS</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>DP.CBM</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>เลขที่ใบจัดรถ</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>PO.REF</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>M-KEY</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>

                                                    </Tab>
                                                    <Tab eventKey="customer" title="CUSTOMER" >
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>รหัส ซื้อสำหรับ</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>ชื่อ ซื้อสำหรับ</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Dropdown type="company" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>SCG/SYS Dealer</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">ทั้งหมด</option><option value="Y">SCG Dealer</option><option value="N">SYS Dealer</option>
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>

                                                    </Tab>
                                                    <Tab eventKey="product" title="PRODUCT" >
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Dropdown type="section" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control as="select">
                                                                            <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="SM400">SM400</option><option value="SS400">SS400</option><option value="SS540">SS540</option><option value="SS400/SM400">SS400/SM400</option><option value="43A">43A</option><option value="SM520">SM520</option><option value="SM490">SM490</option><option value="SS490">SS490</option><option value="SM570">SM570</option>
                                                                        </Form.Control>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                                                    <Col sm={8}>
                                                                        <Form.Control type="text" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                    </Tab>
                                                </Tabs>
                                            </Form.Group>
                                        </Col>


                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="ใบขอนุมัติตั๋วตัดฝาก">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className="ml-sm-4" id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>เลขที่ใบขออนุมัติ</th>
                                        <th>วันที่เอกสาร</th>
                                        <th>ลูกค้า/ซื้อสำหรับ</th>
                                        <th>สถานที่จ่ายสินค้า</th>
                                        <th>หน่วยงาน</th>
                                        <th>INCO.TERM</th>
                                        <th>ปริมาณ(ท่อน)</th>
                                        <th>ปริมาณ(ตัน)</th>
                                        <th>จำนวนเงิน(บาท)</th>
                                        <th>ออกโดย</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                        <th colSpan="8">
                                            <span>TOTAL</span>                                        
                                        </th>
                                        <th>130</th>
                                        <th>44.240</th>
                                        <th>943,938.46</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
