import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {


        if (type === "Create") {
            this.setState({ isModal: true });
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else if (type === "Edit") {
            this.setState({ isModalDetail: true });
            this.setState({ setTitleModal: "EST.SHIPMENT DO." })

        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }


    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModalDetail} onHide={() => this.setState({ isModalDetail: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={8}>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                                    <Col sm={8} className="text-center">
                                                        <span> A ส.ผจ.
                                                        </span>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">YR-MTH</Form.Label>
                                                    <Col sm={8} className="text-center">
                                                        <span>2021-01
                                                        </span>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">EST.REV</Form.Label>
                                                    <Col sm={8} className="text-center">
                                                        <span> 0
                                                        </span>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <hr />

                                <Col sm={12}>

                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" defaultActiveKey="summary" className="form-control-file">

                                            <Tab eventKey="summary" title="SUMMARY">
                                                <Form.Group as={Row}>
                                                    <Col sm={6}>
                                                        <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>SECTION</th>
                                                                    <th>TON</th>
                                                                    <th>REC(S)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="4" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                        <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>SALES_ORG</th>
                                                                    <th>TON</th>
                                                                    <th>REC(S)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="4" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                        <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>SALES_ORG</th>
                                                                    <th>SECTION</th>
                                                                    <th>TON</th>
                                                                    <th>REC(S)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="5" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                        <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>SALES_ORG</th>
                                                                    <th>PLANT</th>
                                                                    <th>SECTION</th>
                                                                    <th>TON</th>
                                                                    <th>REC(S)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="6" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Col>
                                                    <Col sm={6}>
                                                        <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>PLANT</th>
                                                                    <th>GROUP VC</th>
                                                                    <th>TON</th>
                                                                    <th>REC(S)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="5" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Col>

                                                </Form.Group>
                                            </Tab>
                                            <Tab eventKey="est-data" title="EST.DATA" >
                                                <Row className="mt-sm-2">
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">RECORD TYPE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="SO">SO (1088)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">STATUS</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="ALL">All</option><option value="NONE">NONE (1088)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">DP#</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control type="text" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">SO#</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control type="text" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">ORDER NO</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control type="text" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">CUSTOMER</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="000006">C.S.H. (79)</option><option value="3001949">CHUAPHAIBUL (109)</option><option value="3001956">LOHAH (58)</option><option value="3033022">NAVASIAM (20)</option><option value="3001954">SINKIT (20)</option><option value="3005525">STEEL LINE (2)</option><option value="3014445">THE STEEL (29)</option><option value="3001947">TMI (13)</option><option value="3007503">TMT (155)</option><option value="3014451">เต็กเฮง (32)</option><option value="000007">โลหะไพศาล (64)</option><option value="3001955">กวงฮั้ว (42)</option><option value="3001943">กิมเฮ่งเซ้ง (70)</option><option value="000004">ชุมเสริม (4)</option><option value="3001952">ต.วรคุณ (21)</option><option value="3001951">ธนาสาร (62)</option><option value="3014442">ยงเจริญชัย07 (75)</option><option value="3001945">ศิริกุล (72)</option><option value="3001948">อุดม (161)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">ORD.YR.MTH</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="202001">2020-01 (3)</option><option value="202003">2020-03 (2)</option><option value="202006">2020-06 (1)</option><option value="202007">2020-07 (1)</option><option value="202008">2020-08 (1)</option><option value="202009">2020-09 (5)</option><option value="202010">2020-10 (15)</option><option value="202011">2020-11 (15)</option><option value="202012">2020-12 (59)</option><option value="202101">2021-01 (374)</option><option value="202102">2021-02 (601)</option><option value="202104">2021-04 (11)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">SIZE.STD</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select"><option value="ALL">All</option><option value="TIS2015">TIS 1227:2558 (2015) (1079)</option><option value="TIS1390">TIS1390-2560 (2017) (9)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">SECTION</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="L">ANGLE (93)</option><option value="C">CHANNEL (31)</option><option value="I">I-BEAM (95)</option><option value="S">SHEET-PILE (9)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">NOMINAL SIZE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="100X100">100X100 (37)</option><option value="100X50">100X50 (5)</option><option value="100X7">100X7 (3)</option><option value="120X8">120X8 (9)</option><option value="125X125">125X125 (34)</option><option value="125X65">125X65 (2)</option><option value="130X12">130X12 (11)</option><option value="130X15">130X15 (1)</option><option value="130X9">130X9 (10)</option><option value="150X100">150X100 (11)</option><option value="150X12">150X12 (17)</option><option value="150X15">150X15 (9)</option><option value="150X150">150X150 (19)</option><option value="150X19">150X19 (2)</option><option value="150X75">150X75 (21)</option><option value="175X12">175X12 (3)</option><option value="175X15">175X15 (6)</option><option value="175X175">175X175 (2)</option><option value="200X100">200X100 (28)</option><option value="200X15">200X15 (8)</option><option value="200X150">200X150 (17)</option><option value="200X20">200X20 (9)</option><option value="200X200">200X200 (33)</option><option value="200X25">200X25 (1)</option><option value="200X80">200X80 (2)</option><option value="200X90">200X90 (4)</option><option value="250X125">250X125 (32)</option><option value="250X175">250X175 (29)</option><option value="250X25">250X25 (3)</option><option value="250X250">250X250 (32)</option><option value="250X35">250X35 (1)</option><option value="250X90H">250X90H (1)</option><option value="300X150">300X150 (37)</option><option value="300X150H">300X150H (13)</option><option value="300X150L">300X150L (24)</option><option value="300X150M">300X150M (15)</option><option value="300X200">300X200 (49)</option><option value="300X300">300X300 (74)</option><option value="300X90H">300X90H (1)</option><option value="300X90L">300X90L (1)</option><option value="300X90M">300X90M (1)</option><option value="350X175">350X175 (30)</option><option value="350X250">350X250 (22)</option><option value="350X350">350X350 (49)</option><option value="380X100L">380X100L (15)</option><option value="400X125">400X125 (5)</option><option value="400X150">400X150 (1)</option><option value="400X150H">400X150H (10)</option><option value="400X150L">400X150L (6)</option><option value="400X170">400X170 (3)</option><option value="400X200">400X200 (26)</option><option value="400X300">400X300 (54)</option><option value="400X400">400X400 (38)</option><option value="450X175H">450X175H (5)</option><option value="450X175L">450X175L (2)</option><option value="450X200">450X200 (40)</option><option value="450X300">450X300 (12)</option><option value="500X200">500X200 (31)</option><option value="500X300">500X300 (20)</option><option value="600X190L">600X190L (6)</option><option value="600X200">600X200 (16)</option><option value="600X300">600X300 (52)</option><option value="700X300">700X300 (7)</option><option value="800X300">800X300 (14)</option><option value="900X300">900X300 (7)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">GRADE.STD</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="TIS2015">TIS 1227:2558 (2015) (1079)</option><option value="TIS1390">TIS1390-2560 (2017) (9)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">GRADE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="SM400">SM400 (3)</option><option value="SM520">SM520 (257)</option><option value="SS400">SS400 (781)</option><option value="SS540">SS540 (38)</option><option value="SY295">SY295 (9)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">M/FT</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="M">METERS</option><option value="F">FEET</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4"> SIZE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control type="text" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">WEIGHT</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control type="text" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">LENGTH</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="044000">4.4M (1)</option><option value="045000">4.5M (1)</option><option value="049000">4.9M (1)</option><option value="050000">5M (1)</option><option value="055000">5.5M (1)</option><option value="056000">5.6M (2)</option><option value="059000">5.9M (1)</option><option value="060000">6M (352)</option><option value="061000">6.1M (3)</option><option value="062000">6.2M (2)</option><option value="064000">6.4M (1)</option><option value="065000">6.5M (1)</option><option value="068000">6.8M (3)</option><option value="069000">6.9M (1)</option><option value="070000">7M (4)</option><option value="071000">7.1M (1)</option><option value="073000">7.3M (1)</option><option value="074000">7.4M (1)</option><option value="075000">7.5M (3)</option><option value="077000">7.7M (2)</option><option value="078000">7.8M (4)</option><option value="079000">7.9M (1)</option><option value="080000">8M (9)</option><option value="082000">8.2M (1)</option><option value="083000">8.3M (1)</option><option value="084000">8.4M (1)</option><option value="085000">8.5M (2)</option><option value="086000">8.6M (1)</option><option value="087000">8.7M (1)</option><option value="089000">8.9M (1)</option><option value="090000">9M (210)</option><option value="094000">9.4M (2)</option><option value="095000">9.5M (2)</option><option value="096000">9.6M (1)</option><option value="097000">9.7M (1)</option><option value="100000">10M (9)</option><option value="101000">10.1M (1)</option><option value="102000">10.2M (1)</option><option value="105000">10.5M (1)</option><option value="107000">10.7M (1)</option><option value="110000">11M (4)</option><option value="111000">11.1M (1)</option><option value="115000">11.5M (1)</option><option value="120000">12M (431)</option><option value="130000">13M (1)</option><option value="135000">13.5M (1)</option><option value="140000">14M (1)</option><option value="150000">15M (4)</option><option value="160000">16M (5)</option><option value="163000">16.3M (1)</option><option value="175000">17.5M (1)</option><option value="190000">19M (1)</option><option value="199000">19.9M (1)</option><option value="200000">20M (1)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4"> ROLL DATE</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="">NONE (1088)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่นัดส่งมอบ</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="">NONE (1088)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่คาดว่าจะจ่าย</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="">NONE (1088)</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={12} className="text-right">
                                                        <Button size="sm" variant="primary">SEARCH</Button>
                                                    </Col>
                                                </Row>
                                                <Form.Group as={Row}>
                                                    <Table ref="tbl" striped hover responsive bordered className="mt-sm-1">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>CUSTOMER</th>
                                                                <th>PROJECT
                                                                    DESC.</th>
                                                                <th>PRODUCT
                                                                    SIZE</th>
                                                                <th><Form.Check id="tb-user" /></th>
                                                                <th>#</th>
                                                                <th>GROUP</th>
                                                                <th>ORD.
                                                                    YR.MTH</th>
                                                                <th>SAP#</th>
                                                                <th>ORDER
                                                                    LOT-REV</th>
                                                                <th>PAYMENT
                                                                    TERM</th>
                                                                <th>INCO
                                                                    TERM</th>
                                                                <th>วันที่แจ้งลูกค้า</th>
                                                                <th>DP.NO</th>
                                                                <th>DP
                                                                    DATE</th>
                                                                <th>ROLL
                                                                    DATE</th>
                                                                <th>ท่อน</th>
                                                                <th>ตัน</th>
                                                                <th>วันที่คาดตัดจ่าย
                                                                </th>
                                                                <th>U-PRICE</th>
                                                                <th>PLANT</th>
                                                                <th>SEC</th>
                                                                <th>NOMINAL</th>
                                                                <th>COMBINED
                                                                    ROLL</th>
                                                                <th>ORDER ท่อน</th>
                                                                <th>ORDER ตัน</th>
                                                                <th>STK SYS1 ท่อน
                                                                </th>
                                                                <th>STK SYS1 ตัน
                                                                </th>
                                                                <th>STK SYS1
                                                                    GROUP</th>
                                                                <th>STK SYS1
                                                                    ROLL DATE</th>
                                                                <th>STK SYS2 ท่อน</th>
                                                                <th>STK SYS2 ตัน</th>
                                                                <th>STK SYS2
                                                                    GROUP</th>
                                                                <th>STK SYS2
                                                                    ROLL DATE</th>
                                                                <th>TRADER
                                                                    COMM%</th>
                                                                <th>TRADER
                                                                    COMM AMT</th>
                                                                <th>CODE1
                                                                    COMM%</th>
                                                                <th>CODE1
                                                                    COMM AMT</th>
                                                                <th>REBATE
                                                                    RATE</th>
                                                                <th>REBATE
                                                                    AMT</th>
                                                                <th>NET
                                                                    PRICE</th>
                                                                <th>NET
                                                                    AMT</th>
                                                                <th>NET หน้าตั๋ว
                                                                    AMT</th>
                                                                <th>REC(S)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colSpan="44" className="text-center">
                                                                    <label > No Data.</label>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </Table>
                                                </Form.Group>
                                            </Tab>
                                            <Tab eventKey="func" title="FUNC.">
                                                <Form.Group as={Row} className="mt-sm-2">
                                                <Col sm={12}>
                                                    <input type="text" className="text-center mr-sm-1 wid-200" defaultValue="26-Jul-21 13:40" disabled/>
                                                    <input type="text" className="text-center mr-sm-1 wid-200" defaultValue="-" disabled/>
                                                    </Col>
                                                    <Col sm={12} className="mt-sm-1">
                                                        <Button variant="success" className="mr-sm-1 wid-200">1.CAPTURE ORDER-DTL.</Button>
                                                        <Button variant="success" className="mr-sm-1 wid-200">2.CAPTURE BILLING</Button>
                                                        <Button variant="success" className="mr-sm-1 wid-220">3.CAPTURE ROLLING PLAN.</Button>
                                                        <Button variant="success" className="mr-sm-1 wid-220">4.CAPTURE แผนการส่งมอบ</Button>
                                                        <Button variant="success" className="mr-sm-1 wid-200">5.RE-CAP.GROUP VC</Button>
                                                        <Button variant="success" className="mt-sm-1 wid-200">6.RE-CALUATE AMT.</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Col sm={12}>
                                                        <Button variant="primary" className="mr-sm-1 wid-200">EXPORT XLS SMY.</Button>
                                                        <Button variant="primary" className="mr-sm-1 wid-200">EXPORT ALL</Button>
                                                        <Button variant="primary" className="mr-sm-1 wid-220">EXPORT SO</Button>
                                                        <Button variant="primary" className="mr-sm-1 wid-220">EXPORT BILLING</Button>
                                                        <Button variant="primary" className="mr-sm-1 wid-200">EXPORT DP.BAL</Button>
                                                    </Col>
                                                </Form.Group>
                                            </Tab>
                                            <Tab eventKey="dp" title="DP">
                                                <Row className="mt-sm-2">
                                                    <Col sm={5}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">DP.UPLOAD DATE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" defaultValue="-" className="text-center" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={5}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">DP.TEXT FILE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" defaultValue="-" className="text-center" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={5}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">SAP DP.TEXT FILE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="file" className="text-center w-100" />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}>
                                                        <Button size="sm" variant="success" className="w-100">UPLOAD</Button>
                                                    </Col>
                                                </Row>
                                                <Row className="mt-sm-1">
                                                    <Col sm={5}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4"></Form.Label>
                                                            <Col sm={8}>
                                                                <input type="checkbox" />
                                                                <Form.Label className="ml-sm-1 text-c-red">CANCELED</Form.Label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}>
                                                        <Button size="sm" variant="primary" className="w-100">EXPORT ALL</Button>
                                                    </Col>
                                                </Row>
                                                <Row className="mt-sm-1">
                                                    <Col sm={5}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">หมายเหตุ.-</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="4" cols="70"></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    
                                                </Row>
                                                <Row>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">CREATED BY</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" defaultValue="ISS@sys" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">CREATED DATE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" defaultValue="27-07-21" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">UP.KEY</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">UPDATED BY</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" defaultValue="ISS@sys" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">UPDATED DATE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" defaultValue="27-07-21 07:43" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">SYSTEM CODE</Form.Label>
                                                            <Col sm={8}>
                                                                <input type="text" className="text-center w-100" defaultValue="27" disabled />
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                            </Tab>
                                        </Tabs>
                                    </Form.Group>
                                </Col>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalDetail: false })}>Close</Button>
                                <Button variant="success">SAVE</Button>
                                <Button variant="success">SAVE REV</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal size="lg" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">YEAR</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="years" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">MONTH</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="month" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="NONE">None</option><option value="B">ส.ขบ.</option><option value="A">ส.ผจ.</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="success">NEW</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}> YEAR</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="years" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>MONTH</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="month" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="B">ส.ขบ.</option><option value="A">ส.ผจ.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>EST.REV</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="EST.SHIPMENT DO.,EST.SHIPMENT DO.">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>OPEN NEW EST.SHIP.DO</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className="ml-sm-1" id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>หน่วยงาน</th>
                                        <th>YR-MTH</th>
                                        <th>EST.REV</th>
                                        <th>TON(S)</th>
                                        <th>REMARK.-</th>
                                        <th>UPDATED DATE</th>
                                        <th>REC(S)</th>
                                        <th>N</th>
                                        <th>วันที่แก้ไข</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux >
        );
    }
}

export default SizeMaster;
