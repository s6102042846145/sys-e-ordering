import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";



import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    // $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        // data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
               
                <Row>
                    <Col>
                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                   ...
                                
                                </Row>

                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                        <Row>
                            <Col sm={12}>
                                <Form>
                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Quota.Year</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Quota.Month</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>สถานะ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="1WK">จะหมดอายุใน1 อาทิตย์</option><option value="ACT">Acitve</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Quota.NO.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Quota.REV.</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE45">BE45 - ตั๋วแลกเงิน 45 วัน (BE)</option><option value="BE60">BE60 - ตั๋วแลกเงิน 60 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="BED0">BED0 - ตั๋วแลกเงิน 150 วัน (BE)</option><option value="BEF0">BEF0 - ตั๋วแลกเงิน 180 วัน (BE)</option><option value="BEH0">BEH0 - ตั๋วแลกเงิน 210 วัน (BE)</option><option value="BEJ0">BEJ0 - ตั๋วแลกเงิน 240 วัน (BE)</option><option value="BEL0">BEL0 - ตั๋วแลกเงิน 270 วัน (BE)</option><option value="BEN0">BEN0 - ตั๋วแลกเงิน 300 วัน (BE)</option><option value="BER0">BER0 - ตั๋วแลกเงิน 360 วัน (BE)</option><option value="BS30">BS30 - ตั๋วแลกเงิน 30 วัน (BE) before shipment.</option><option value="BS60">BS60 - ตั๋วแลกเงิน 60 วัน (BE) before shipment.</option><option value="BS90">BS90 - ตั๋วแลกเงิน 90 วัน (BE) before shipment.</option><option value="BSA0">BSA0 - ตั๋วแลกเงิน 105 วัน (BE) before shipment.</option><option value="BSB0">BSB0 - ตั๋วแลกเงิน 120 วัน (BE) before shipment.</option><option value="BSD0">BSD0 - ตั๋วแลกเงิน 150 วัน (BE) before shipment.</option><option value="BSF0">BSF0 - ตั๋วแลกเงิน 180 วัน (BE) before shipment.</option><option value="CA15">CA15 - DLC 15 days</option><option value="CD00">CD00 - DLC</option><option value="CD07">CD07 - DLC 7 days</option><option value="CD15">CD15 - DLC 15 days</option><option value="CD30">CD30 - DLC 30 days</option><option value="CD60">CD60 - DLC 60 days</option><option value="CD90">CD90 - DLC 90 days</option><option value="CDB0">CDB0 - DLC 120 days</option><option value="CH07">CH07 - DLC</option><option value="CH15">CH15 - DLC</option><option value="CH30">CH30 - DLC</option><option value="CH60">CH60 - DLC</option><option value="CH90">CH90 - DLC</option><option value="CT00">CT00 - DLC</option><option value="CT03">CT03 - DLC</option><option value="CV03">CV03 - DLC</option><option value="DB07">DB07 - D/A 7 days after receiced bill.</option><option value="DB30">DB30 - D/A 30 days after receiced bill.</option><option value="DF00">DF00 - Draft.</option><option value="DL15">DL15 - D/A 15 days after B/L date.</option><option value="DL30">DL30 - D/A 30 days after B/L date.</option><option value="DL45">DL45 - D/A 45 days after B/L date.</option><option value="DL60">DL60 - D/A 60 days after B/L date.</option><option value="DL75">DL75 - D/A 75 days after B/L date.</option><option value="DL90">DL90 - D/A 90 days after B/L date.</option><option value="DLB0">DLB0 - D/A 120 days after B/L date.</option><option value="DLF0">DLF0 - D/A 180 days after B/L date.</option><option value="DND0">DND0 - D/A 150 days from B/L date</option><option value="DP00">DP00 - D/P at sight</option><option value="DP07">DP07 - D/P 7 days after B/L date</option><option value="DP15">DP15 - D/P at sight</option><option value="DP30">DP30 - D/P 30 days after B/L date</option><option value="DP45">DP45 - D/P 45 days after B/L date</option><option value="DV07">DV07 - D/A 7 days after invoice,B/L,AWB date.</option><option value="DV14">DV14 - D/A 14 days after invoice,B/L,AWB date.</option><option value="DV30">DV30 - D/A 30 days after invoice,B/L,AWB date.</option><option value="FI15">FI15 - within 15th of next month</option><option value="FI30">FI30 - within 30th of next month</option><option value="HB03">HB03 - HSBC-Dealer Financing (Invoice Financing)</option><option value="KD07">KD07 - Credit 7 days.</option><option value="LC00">LC00 - L/C at sight.</option><option value="LC15">LC15 - L/C 15 days.</option><option value="N120">N120 - Within 120 days  after received bill</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM02">NM02 - เงินสด 2 วัน</option><option value="NM03">NM03 - เงินสด 3 วัน</option><option value="NM05">NM05 - เงินสด 5 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NM50">NM50 - เงินสด 50 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - BG 1 วัน</option><option value="NT02">NT02 - BG 2 วัน</option><option value="NT04">NT04 - BG 4 วัน</option><option value="NT05">NT05 - BG 5 วัน</option><option value="NT07">NT07 - BG 7 วัน</option><option value="NT10">NT10 - BG 10 วัน</option><option value="NT15">NT15 - BG 15 วัน</option><option value="NT21">NT21 - BG 21 วัน</option><option value="NT30">NT30 - BG 30 วัน</option><option value="NT45">NT45 - BG 45 วัน</option><option value="NT50">NT50 - BG 50 วัน</option><option value="NT60">NT60 - BG 60 วัน</option><option value="NT70">NT70 - BG 70 วัน</option><option value="NT75">NT75 - BG 75 วัน</option><option value="NT90">NT90 - BG 90 วัน</option><option value="NTB0">NTB0 - BG 120 วัน</option><option value="NTH0">NTH0 - BG 210 วัน</option><option value="NTJ0">NTJ0 - BG 240 วัน</option><option value="NTL0">NTL0 - BG 270 วัน</option><option value="NTN0">NTN0 - BG 300 วัน</option><option value="NTP0">NTP0 - BG 330 วัน</option><option value="NTR0">NTR0 - BG 360 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL30">TL30 - T/T 30 days after B/L date.</option><option value="TL35">TL35 - T/T 35 days after B/L date.</option><option value="TL45">TL45 - T/T 45 days after B/L date.</option><option value="TL60">TL60 - T/T 60 days after B/L date.</option><option value="TL75">TL75 - T/T 75 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TLB0">TLB0 - T/T 120 days after B/L date.</option><option value="TLD0">TLD0 - T/T 150 days after B/L date</option><option value="TLH0">TLH0 - T/T 210 days after B/L date</option><option value="TLI0">TLI0 - T/T 180 days after B/L date.</option><option value="TQ30">TQ30 - T/T 30 days after received goods</option><option value="TR60">TR60 - T/T remittance 60 days after shipment</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS07">TS07 - T/T before shipment 7 day.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option><option value="ZT00">ZT00 - L/C at sight 98%,T/T 2% after settlement</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">All</option><option value="CFR">CFR - Costs and freight</option><option value="CIF">CIF - Costs,Insurance &amp; freight</option><option value="CIP">CIP - Carriage &amp; Insurance paid to</option><option value="CPT">CPT - Carriage paid to</option><option value="DAP">DAP - Delivered at Place</option><option value="DDP">DDP - Delivery Duty Paid</option><option value="DDU">DDU - Delivery Duty Unpaid</option><option value="EXW">EXW - Ex works</option><option value="FAS">FAS - Free along ship</option><option value="FCA">FCA - Free carrier</option><option value="FOB">FOB - Free on board</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ChannelCd</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อโครงการ</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Project Type</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>TYPE</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">All</option><option value="N">Project</option><option value="Y">Re-Export</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Status</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="2">ทำเอกสารเสร็จ</option><option value="3">ตรวจสอบครั้งที่ 1 แล้ว</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                    <br />
                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control as="select">
                                                            <option value={0}>Create Date</option>
                                                            <option value={1}>Update Date</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 1 ||
                                                    this.state.selectDate === 2 ||
                                                    this.state.selectDate === 3 ||
                                                    this.state.selectDate === 4 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 5 ||
                                                    this.state.selectDate === 6 ||
                                                    this.state.selectDate === 7 ||
                                                    this.state.selectDate === 8 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>From</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 9 ||
                                                    this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 11 ||
                                                    this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectValue === 1 ||
                                                this.state.selectValue === 2 ||
                                                this.state.selectValue === 3 ||
                                                this.state.selectValue === 4 ||
                                                this.state.selectValue === 5 ||
                                                this.state.selectValue === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>From Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control
                                                        as="select"
                                                        value={this.state.supportedSelect}
                                                        onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                    >
                                                        <option value={0}>None</option>
                                                        <option value={1}>At</option>
                                                        <option value={2}>Between</option>
                                                        <option value={3}>Less than</option>
                                                        <option value={4}>Less than or equal</option>
                                                        <option value={5}>At Month</option>
                                                        <option value={6}>Between Month</option>
                                                        <option value={7}>More than</option>
                                                        <option value={8}>More than or equal</option>
                                                        <option value={9}>At Quater</option>
                                                        <option value={10}>Between Quater</option>
                                                        <option value={11}>At Year</option>
                                                        <option value={12}>Between Year</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                            {
                                                this.state.selectDate === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 6 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={3}>To</Form.Label>
                                                        <Col sm={4}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                        <Col sm={5}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 10 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                            {
                                                this.state.selectDate === 12 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                        <Col sm={3}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>Criteria</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Group>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>More than</option>
                                                            <option value={6}>More than or equal</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={3}>
                                        {
                                                this.state.selectValue === 2 ?

                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>To Value</Form.Label>
                                                        <Col sm={8}>
                                                            <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                        </Col>
                                                    </Form.Group>

                                                    : ''
                                            }
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > SEARCH </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="CHECK 1 DOMESTIC QUOTATION">
                            <Row>
                                <Col className="email-card">
                                   <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>SIZE STD.</th>
                                        <th>SEC</th>
                                        <th>SIZE ID</th>
                                        <th>NOMINAL SIZE</th>
                                        <th>DIM_DESC</th>
                                        <th>WEIGHT</th>
                                        <th>UM</th>
                                        <th>N</th>
                                        <th>วันที่แก้ไข</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
