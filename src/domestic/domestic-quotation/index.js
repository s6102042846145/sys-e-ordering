import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> QUOTATION </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/domestic-domest-quotation">
                                            DOMESTIC QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/check2-domestic-quotation" >
                                            CHECK 2 DOMESTIC QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/cs-approve-domestic-quotation" >
                                            CS APPROVE DOMESTIC QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/renewal-certificate-possibility" >
                                            ใบขอต่ออายุ POSSIBILITY
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/check1-domestic-quotation" >
                                            CHECK 1 DOMESTIC QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/approve-domestic-quotation" >
                                            APPROVE DOMESTIC QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/domestic/domestic-quotation/cs-approve-export-quotation" >
                                            CS APPROVE EXPORT QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;