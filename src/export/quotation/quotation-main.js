import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> QUOTATION </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/export-quotation"}>
                                            EXPORT QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/check-2-export-quotation"}>
                                            CHECK 2 EXPORT QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/inquiry-process"}>
                                            INQUIRY IN PROCESS
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/check-1-export-quotation"}>
                                            CHECK 1 EXPORT QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/approve-export-quotation"}>
                                            APPROVE EXPORT QUOTATION
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/quotation/upload-export"}>
                                            UPLOAD EXPORT ORDER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;