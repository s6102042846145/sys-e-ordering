import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {

    

    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> ORDER CONFIRM </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/order-confirm/export-order"}>
                                        EXPORT ORDER CONFIRMED 
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/order-confirm/approve-order"}>
                                        APPROVE EXPORT ORDER CONFIRM 
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/order-confirm/check-order"}>
                                        CHECK EXPORT ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/order-confirm/export-invoice"}>
                                        EXPORT INVOICE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;