import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> EXPORT PROCESS </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/export-process/check-low"}>
                                            CHECK LOW PRICE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                        <a className="list-group-item list-group-item-action" href={"/export/export-process/approve-low-2"}>
                                            APPROVE LOW PRICE #2
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                    <div className="card m-15" >
                                        <a className="list-group-item list-group-item-action" href={"/export/export-process/approve-low-1"}>
                                            APPROVE LOW PRICE #1
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </a>
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;