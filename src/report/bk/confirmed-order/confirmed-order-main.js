import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> CONFIRMED ORDER </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/report/confirmed-order/summary-order">
                                        SUMMARY ORDER BY CONFIRMED CUSTOMER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/report/confirmed-order/confirmsummaryproduct" >
                                        SUMMARY ORDER BY CONFIRMED PRODUCT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                      
                                        <Link className='list-group-item list-group-item-action' to="/report/confirmed-order/confirmordersap" >
                                        ตรวจสอบ ORDER - SAP 
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                      
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/report/confirmed-order/confirmbycustumer" >
                                        SUMMARY ORDER BY CONFIRMED CUSTOMER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/report/confirmed-order/confirmreportorder" >
                                            รายงานติดตามสถานะ ORDER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;