import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        //data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            // { "data": "N", render: function (data, type, row) { return data; } },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "PREVIEW" })
        } else {
            this.setState({ setTitleModal: "PREVIEW" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>SIZE</th>
                                            <th>GRADE</th>
                                            <th>GROUP</th>
                                            <th>LENGTH</th>
                                            <th>Adv-Stock</th>
                                            <th>Adv-Project</th>
                                            <th>TOTAL</th>
                                            <th>TYPE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colSpan="9" className="text-center">No Data.</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ตั้งแต่ปี</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เดือน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รอบที่</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="SM400">SM400</option><option value="SS400">SS400</option><option value="SS540">SS540</option><option value="SS400/SM400">SS400/SM400</option><option value="43A">43A</option><option value="SM520">SM520</option><option value="SM490">SM490</option><option value="SS490">SS490</option><option value="SM570">SM570</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="MY">=6M,9M,12M</option><option value="MN">&lt;&gt;6M,9M,12M</option><option value="FY">=20F,30F,40F</option><option value="FN">&lt;&gt;20F,30F,40F</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาว</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>หน่วย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="M">เมตร</option><option value="F">ฟุต</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>กลุ่มสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="0001">0001</option><option value="0002">0002</option><option value="0003">0003</option><option value="0004">0004</option><option value="0005">0005</option><option value="0006">0006</option><option value="0007">0007</option><option value="0008">0008</option><option value="0009">0009</option><option value="0010">0010</option><option value="0011">0011</option><option value="0012">0012</option><option value="0013">0013</option><option value="0014">0014</option><option value="0015">0015</option><option value="0016">0016</option><option value="0017">0017</option><option value="0018">0018</option><option value="0019">0019</option><option value="0020">0020</option><option value="0021">0021</option><option value="0022">0022</option><option value="0023">0023</option><option value="0024">0024</option><option value="0025">0025</option><option value="0026">0026</option><option value="0027">0027</option><option value="0028">0028</option><option value="0029">0029</option><option value="0031">0031</option><option value="0032">0032</option><option value="0033">0033</option><option value="0034">0034</option><option value="0035">0035</option><option value="0037">0037</option><option value="0038">0038</option><option value="0039">0039</option><option value="0040">0040</option><option value="0041">0041</option><option value="0042">0042</option><option value="0043">0043</option><option value="0044">0044</option><option value="0045">0045</option><option value="0046">0046</option><option value="0047">0047</option><option value="0048">0048</option><option value="0049">0049</option><option value="0050">0050</option><option value="0051">0051</option><option value="0052">0052</option><option value="0053">0053</option><option value="0054">0054</option><option value="0055">0055</option><option value="0056">0056</option><option value="0061">0061</option><option value="0062">0062</option><option value="0063">0063</option><option value="0064">0064</option><option value="0065">0065</option><option value="0066">0066</option><option value="0067">0067</option><option value="0068">0068</option><option value="0069">0069</option><option value="0070">0070</option><option value="0071">0071</option><option value="0072">0072</option><option value="0073">0073</option><option value="0074">0074</option><option value="0075">0075</option><option value="0076">0076</option><option value="0077">0077</option><option value="0078">0078</option><option value="0079">0079</option><option value="0080">0080</option><option value="0081">0081</option><option value="0082">0082</option><option value="0083">0083</option><option value="0084">0084</option><option value="0085">0085</option><option value="0086">0086</option><option value="0087">0087</option><option value="0088">0088</option><option value="0089">0089</option><option value="0090">0090</option><option value="0091">0091</option><option value="0092">0092</option><option value="0093">0093</option><option value="0094">0094</option><option value="0095">0095</option><option value="0096">0096</option><option value="0097">0097</option><option value="0098">0098</option><option value="0099">0099</option><option value="0100">0100</option><option value="0101">0101</option><option value="0102">0102</option><option value="0103">0103</option><option value="0104">0104</option><option value="0105">0105</option><option value="0106">0106</option><option value="0107">0107</option><option value="0108">0108</option><option value="0109">0109</option><option value="0110">0110</option><option value="0111">0111</option><option value="0112">0112</option><option value="0113">0113</option><option value="0114">0114</option><option value="0115">0115</option><option value="0116">0116</option><option value="0117">0117</option><option value="0118">0118</option><option value="0119">0119</option><option value="0120">0120</option><option value="0121">0121</option><option value="0122">0122</option><option value="0123">0123</option><option value="0124">0124</option><option value="0125">0125</option><option value="0126">0126</option><option value="0127">0127</option><option value="0128">0128</option><option value="0129">0129</option><option value="0130">0130</option><option value="0131">0131</option><option value="0132">0132</option><option value="0133">0133</option><option value="0134">0134</option><option value="0135">0135</option><option value="0136">0136</option><option value="0137">0137</option><option value="0138">0138</option><option value="0139">0139</option><option value="0140">0140</option><option value="0141">0141</option><option value="0142">0142</option><option value="0144">0144</option><option value="0145">0145</option><option value="0146">0146</option><option value="0147">0147</option><option value="0148">0148</option><option value="0149">0149</option><option value="0150">0150</option><option value="0151">0151</option><option value="0152">0152</option><option value="0153">0153</option><option value="0155">0155</option><option value="0156">0156</option><option value="0157">0157</option><option value="0158">0158</option><option value="0159">0159</option><option value="0160">0160</option><option value="0161">0161</option><option value="0162">0162</option><option value="0163">0163</option><option value="0164">0164</option><option value="0165">0165</option><option value="0166">0166</option><option value="0167">0167</option><option value="0168">0168</option><option value="0169">0169</option><option value="0170">0170</option><option value="0171">0171</option><option value="0172">0172</option><option value="0173">0173</option><option value="0174">0174</option><option value="0175">0175</option><option value="0176">0176</option><option value="0177">0177</option><option value="0178">0178</option><option value="0179">0179</option><option value="0180">0180</option><option value="0181">0181</option><option value="0182">0182</option><option value="0183">0183</option><option value="0184">0184</option><option value="0185">0185</option><option value="0187">0187</option><option value="0188">0188</option><option value="0189">0189</option><option value="0190">0190</option><option value="0191">0191</option><option value="0192">0192</option><option value="0193">0193</option><option value="0194">0194</option><option value="0195">0195</option><option value="0196">0196</option><option value="0197">0197</option><option value="0198">0198</option><option value="0199">0199</option><option value="0200">0200</option><option value="0201">0201</option><option value="0202">0202</option><option value="0203">0203</option><option value="0204">0204</option><option value="0205">0205</option><option value="0206">0206</option><option value="0207">0207</option><option value="0208">0208</option><option value="0209">0209</option><option value="0210">0210</option><option value="0211">0211</option><option value="0212">0212</option><option value="0213">0213</option><option value="0214">0214</option><option value="0215">0215</option><option value="0216">0216</option><option value="0217">0217</option><option value="0218">0218</option><option value="0219">0219</option><option value="0220">0220</option><option value="0221">0221</option><option value="0222">0222</option><option value="0223">0223</option><option value="0224">0224</option><option value="0226">0226</option><option value="0227">0227</option><option value="0228">0228</option><option value="0229">0229</option><option value="0230">0230</option><option value="0232">0232</option><option value="0233">0233</option><option value="0234">0234</option><option value="0235">0235</option><option value="0236">0236</option><option value="0237">0237</option><option value="0238">0238</option><option value="0239">0239</option><option value="0240">0240</option><option value="0241">0241</option><option value="0242">0242</option><option value="0243">0243</option><option value="0244">0244</option><option value="0245">0245</option><option value="0246">0246</option><option value="0247">0247</option><option value="0250">0250</option><option value="0251">0251</option><option value="0252">0252</option><option value="0253">0253</option><option value="0254">0254</option><option value="0255">0255</option><option value="0256">0256</option><option value="0257">0257</option><option value="0258">0258</option><option value="0259">0259</option><option value="0260">0260</option><option value="0261">0261</option><option value="0262">0262</option><option value="0263">0263</option><option value="0264">0264</option><option value="0265">0265</option><option value="0266">0266</option><option value="0267">0267</option><option value="0268">0268</option><option value="0269">0269</option><option value="0270">0270</option><option value="0271">0271</option><option value="0272">0272</option><option value="0273">0273</option><option value="0274">0274</option><option value="0275">0275</option><option value="0276">0276</option><option value="0277">0277</option><option value="0278">0278</option><option value="0280">0280</option><option value="0281">0281</option><option value="0282">0282</option><option value="0283">0283</option><option value="0284">0284</option><option value="0285">0285</option><option value="0286">0286</option><option value="0287">0287</option><option value="0288">0288</option><option value="0289">0289</option><option value="0290">0290</option><option value="0291">0291</option><option value="0292">0292</option><option value="0294">0294</option><option value="0295">0295</option><option value="0296">0296</option><option value="0297">0297</option><option value="0298">0298</option><option value="0299">0299</option><option value="0301">0301</option><option value="0302">0302</option><option value="0303">0303</option><option value="0305">0305</option><option value="0306">0306</option><option value="0308">0308</option><option value="0309">0309</option><option value="0310">0310</option>
                                                            <option value="0311">0311</option><option value="0312">0312</option><option value="0313">0313</option><option value="0314">0314</option><option value="0316">0316</option><option value="0317">0317</option><option value="0318">0318</option><option value="0323">0323</option><option value="0324">0324</option><option value="0325">0325</option><option value="0326">0326</option><option value="0327">0327</option><option value="0328">0328</option><option value="0329">0329</option><option value="0330">0330</option><option value="0331">0331</option><option value="0332">0332</option><option value="0333">0333</option><option value="0334">0334</option><option value="0335">0335</option><option value="0336">0336</option><option value="0337">0337</option><option value="0338">0338</option><option value="0339">0339</option><option value="0340">0340</option><option value="0341">0341</option><option value="0342">0342</option><option value="0343">0343</option><option value="0344">0344</option><option value="0345">0345</option><option value="0346">0346</option><option value="0347">0347</option><option value="0348">0348</option><option value="0349">0349</option><option value="0350">0350</option><option value="0351">0351</option><option value="0352">0352</option><option value="0353">0353</option><option value="0354">0354</option><option value="0355">0355</option><option value="0356">0356</option><option value="0357">0357</option><option value="0359">0359</option><option value="0360">0360</option><option value="0361">0361</option><option value="0362">0362</option><option value="0364">0364</option><option value="0365">0365</option><option value="0366">0366</option><option value="0367">0367</option><option value="0368">0368</option><option value="0369">0369</option><option value="0370">0370</option><option value="0372">0372</option><option value="0373">0373</option><option value="0374">0374</option><option value="0375">0375</option><option value="0376">0376</option><option value="0377">0377</option><option value="0378">0378</option><option value="0379">0379</option><option value="0380">0380</option><option value="0382">0382</option><option value="0383">0383</option><option value="0384">0384</option><option value="0385">0385</option><option value="0386">0386</option><option value="0387">0387</option><option value="0388">0388</option><option value="0390">0390</option><option value="0391">0391</option><option value="0392">0392</option><option value="0393">0393</option><option value="0394">0394</option><option value="0395">0395</option><option value="0396">0396</option><option value="0397">0397</option><option value="0398">0398</option><option value="0400">0400</option><option value="0401">0401</option><option value="0402">0402</option><option value="0403">0403</option><option value="0404">0404</option><option value="0405">0405</option><option value="0406">0406</option><option value="0407">0407</option><option value="0408">0408</option><option value="0409">0409</option><option value="0410">0410</option><option value="0412">0412</option><option value="0413">0413</option><option value="0414">0414</option><option value="0415">0415</option><option value="0416">0416</option><option value="0417">0417</option><option value="0418">0418</option><option value="0419">0419</option><option value="0420">0420</option><option value="0421">0421</option><option value="0422">0422</option><option value="0424">0424</option><option value="0425">0425</option><option value="0426">0426</option><option value="0427">0427</option><option value="0428">0428</option><option value="0429">0429</option><option value="0430">0430</option><option value="0431">0431</option><option value="0432">0432</option><option value="0433">0433</option><option value="0434">0434</option><option value="0435">0435</option><option value="0436">0436</option><option value="0437">0437</option><option value="0439">0439</option><option value="0440">0440</option><option value="0442">0442</option><option value="0443">0443</option><option value="0444">0444</option><option value="0445">0445</option><option value="0446">0446</option><option value="0447">0447</option><option value="0448">0448</option><option value="0449">0449</option><option value="0450">0450</option><option value="0451">0451</option><option value="0452">0452</option><option value="0453">0453</option><option value="0454">0454</option><option value="0455">0455</option><option value="0456">0456</option><option value="0458">0458</option><option value="0459">0459</option><option value="0460">0460</option><option value="0461">0461</option><option value="0462">0462</option><option value="0463">0463</option><option value="0464">0464</option><option value="0465">0465</option><option value="0466">0466</option><option value="0467">0467</option><option value="0468">0468</option><option value="0469">0469</option><option value="0470">0470</option><option value="0471">0471</option><option value="0472">0472</option><option value="0473">0473</option><option value="0474">0474</option><option value="0475">0475</option><option value="0476">0476</option><option value="0477">0477</option><option value="0478">0478</option><option value="0479">0479</option><option value="0480">0480</option><option value="0481">0481</option><option value="0482">0482</option><option value="0483">0483</option><option value="0484">0484</option><option value="0485">0485</option><option value="0486">0486</option><option value="0487">0487</option><option value="0488">0488</option><option value="0489">0489</option><option value="0492">0492</option><option value="0494">0494</option><option value="0495">0495</option><option value="0496">0496</option><option value="0497">0497</option><option value="0498">0498</option><option value="0499">0499</option><option value="0500">0500</option><option value="0501">0501</option><option value="0502">0502</option><option value="0503">0503</option><option value="0504">0504</option><option value="0505">0505</option><option value="0506">0506</option><option value="0507">0507</option><option value="0508">0508</option><option value="0509">0509</option><option value="0510">0510</option><option value="0511">0511</option><option value="0512">0512</option><option value="0513">0513</option><option value="0515">0515</option><option value="0516">0516</option><option value="0517">0517</option><option value="0518">0518</option><option value="0519">0519</option><option value="0520">0520</option><option value="0521">0521</option><option value="0522">0522</option><option value="0524">0524</option><option value="0525">0525</option><option value="0527">0527</option><option value="0528">0528</option><option value="0529">0529</option><option value="0530">0530</option><option value="0531">0531</option><option value="0532">0532</option><option value="0533">0533</option><option value="0534">0534</option><option value="0535">0535</option><option value="0536">0536</option><option value="0537">0537</option><option value="0538">0538</option><option value="0539">0539</option><option value="0540">0540</option><option value="0542">0542</option><option value="0544">0544</option><option value="0545">0545</option><option value="0546">0546</option><option value="0548">0548</option><option value="0549">0549</option><option value="0550">0550</option><option value="0551">0551</option><option value="0552">0552</option><option value="0553">0553</option><option value="0554">0554</option><option value="0555">0555</option><option value="0556">0556</option><option value="0557">0557</option><option value="0558">0558</option><option value="0559">0559</option><option value="0560">0560</option><option value="0561">0561</option><option value="0562">0562</option><option value="0563">0563</option><option value="0565">0565</option><option value="0566">0566</option><option value="0567">0567</option><option value="0568">0568</option><option value="0569">0569</option><option value="0570">0570</option><option value="0571">0571</option><option value="0572">0572</option><option value="0573">0573</option><option value="0574">0574</option><option value="0575">0575</option><option value="0576">0576</option><option value="0577">0577</option><option value="0579">0579</option><option value="0580">0580</option><option value="0581">0581</option><option value="0582">0582</option><option value="0583">0583</option><option value="0584">0584</option><option value="0585">0585</option><option value="0586">0586</option><option value="0587">0587</option><option value="0588">0588</option><option value="0589">0589</option><option value="0590">0590</option><option value="0591">0591</option><option value="0592">0592</option><option value="0593">0593</option><option value="0594">0594</option><option value="0595">0595</option><option value="0596">0596</option><option value="0597">0597</option><option value="0598">0598</option><option value="0599">0599</option><option value="0600">0600</option><option value="0601">0601</option><option value="0602">0602</option><option value="0603">0603</option><option value="0604">0604</option><option value="0605">0605</option><option value="0606">0606</option><option value="0607">0607</option><option value="0608">0608</option><option value="0609">0609</option><option value="0610">0610</option><option value="0611">0611</option><option value="0612">0612</option><option value="0613">0613</option><option value="0614">0614</option><option value="0615">0615</option><option value="0616">0616</option><option value="0617">0617</option><option value="0618">0618</option><option value="0619">0619</option><option value="0620">0620</option><option value="0621">0621</option><option value="0622">0622</option><option value="0623">0623</option><option value="0624">0624</option><option value="0625">0625</option><option value="0627">0627</option><option value="0628">0628</option><option value="0629">0629</option><option value="0630">0630</option>
                                                            <option value="0631">0631</option><option value="0632">0632</option><option value="0633">0633</option><option value="0634">0634</option><option value="0635">0635</option><option value="0636">0636</option><option value="0637">0637</option><option value="0638">0638</option><option value="0639">0639</option><option value="0640">0640</option><option value="0642">0642</option><option value="0643">0643</option><option value="0644">0644</option><option value="0645">0645</option><option value="0646">0646</option><option value="0647">0647</option><option value="0648">0648</option><option value="0649">0649</option><option value="0650">0650</option><option value="0651">0651</option><option value="0652">0652</option><option value="0653">0653</option><option value="0654">0654</option><option value="0655">0655</option><option value="0656">0656</option><option value="0657">0657</option><option value="0658">0658</option><option value="0659">0659</option><option value="0660">0660</option><option value="0661">0661</option><option value="0662">0662</option><option value="0663">0663</option><option value="0664">0664</option><option value="0665">0665</option><option value="0666">0666</option><option value="0667">0667</option><option value="0668">0668</option><option value="0669">0669</option><option value="0670">0670</option><option value="0671">0671</option><option value="0672">0672</option><option value="0674">0674</option><option value="0675">0675</option><option value="0676">0676</option><option value="0677">0677</option><option value="0678">0678</option><option value="0679">0679</option><option value="0680">0680</option><option value="0771">0771</option><option value="150300">150300</option><option value="150300">150300</option><option value="150300">150300</option><option value="1504prj">1504prj</option><option value="2014">2014</option><option value="2014">2014</option><option value="20150301">20150301</option><option value="20150301">20150301</option><option value="20150301">20150301</option><option value="20150301">20150301</option><option value="2015SM520">2015SM520</option><option value="2015SM520">2015SM520</option><option value="20170701">20170701</option><option value="2020Q">2020Q</option><option value="2021Q">2021Q</option><option value="2021Q-plus">2021Q-plus</option><option value="2021SM520">2021SM520</option><option value="2021SP">2021SP</option><option value="AD20110701">AD20110701</option><option value="AD20140101">AD20140101</option><option value="AD20140201">AD20140201</option><option value="AD20140201">AD20140201</option><option value="AD20140201">AD20140201</option><option value="AD20140201">AD20140201</option><option value="AD20140301">AD20140301</option><option value="AD20140301">AD20140301</option><option value="AD20140301">AD20140301</option><option value="AD20140301">AD20140301</option><option value="AD20140401">AD20140401</option><option value="AD20140401">AD20140401</option><option value="AD20140501">AD20140501</option><option value="AD20140501">AD20140501</option><option value="AD20140601">AD20140601</option><option value="AD20140601">AD20140601</option><option value="AD20140701">AD20140701</option><option value="AD20140701">AD20140701</option><option value="AD20140701">AD20140701</option><option value="AD20140701">AD20140701</option><option value="AD20140801">AD20140801</option><option value="AD20140801">AD20140801</option><option value="AD20140801">AD20140801</option><option value="AD20140801">AD20140801</option><option value="AD20140901">AD20140901</option><option value="AD20140901">AD20140901</option><option value="AD20140901">AD20140901</option><option value="AD20140901">AD20140901</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141001">AD20141001</option><option value="AD20141011">AD20141011</option><option value="AD20141101">AD20141101</option><option value="AD20141101">AD20141101</option><option value="AD20141101">AD20141101</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20141201">AD20141201</option><option value="AD20150001">AD20150001</option><option value="AD20150001">AD20150001</option><option value="AD20150001">AD20150001</option><option value="AD20150101">AD20150101</option><option value="AD20150101">AD20150101</option><option value="AD20150101">AD20150101</option><option value="AD20150101">AD20150101</option><option value="AD20150202">AD20150202</option><option value="AD20150301">AD20150301</option><option value="AD20150301">AD20150301</option><option value="AD20150301">AD20150301</option><option value="AD20150303">AD20150303</option><option value="AD20150401">AD20150401</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150501">AD20150501</option><option value="AD20150901">AD20150901</option><option value="AD20150901">AD20150901</option><option value="AD20150902">AD20150902</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151001">AD20151001</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option>
                                                            <option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD20151003">AD20151003</option><option value="AD201510101">AD201510101</option><option value="AD201510201">AD201510201</option><option value="AD201510301">AD201510301</option><option value="AD201510301">AD201510301</option><option value="AD201510501">AD201510501</option><option value="AD20190801">AD20190801</option><option value="AD20190801">AD20190801</option><option value="AD20190802">AD20190802</option><option value="AD20190802">AD20190802</option><option value="AD20190901">AD20190901</option><option value="AD20190902">AD20190902</option><option value="ASTM2003">ASTM2003</option><option value="AU20090501">AU20090501</option><option value="BS">BS</option><option value="HVA2015">HVA2015</option><option value="MY20090801">MY20090801</option><option value="NONE2">NONE2</option><option value="PJ1401001">PJ1401001</option><option value="PJ1405CPAC">PJ1405CPAC</option><option value="PJ1405ธนสาร">PJ1405ธนสาร</option><option value="PJ1405ธนสาร">PJ1405ธนสาร</option><option value="PJ1405ธนสาร">PJ1405ธนสาร</option><option value="PJ140701">PJ140701</option><option value="PJ140701">PJ140701</option><option value="PJ140701">PJ140701</option><option value="PJ140801">PJ140801</option><option value="PJ140901">PJ140901</option><option value="PJ140901">PJ140901</option><option value="PJ1410 กวงหลี">PJ1410 กวงหลี</option><option value="PJ141101">PJ141101</option><option value="PJ141101">PJ141101</option><option value="PJ141101">PJ141101</option><option value="PJ150101">PJ150101</option><option value="PJ150301">PJ150301</option><option value="PJ150401">PJ150401</option><option value="PJ150401">PJ150401</option><option value="PJ150401">PJ150401</option><option value="PJ150401">PJ150401</option><option value="PJ150501">PJ150501</option><option value="PJ150601">PJ150601</option><option value="PJ201205SP(C)">PJ201205SP(C)</option><option value="PJ20130403">PJ20130403</option><option value="PJ20131401">PJ20131401</option><option value="PJ20131401">PJ20131401</option><option value="PJ20131402">PJ20131402</option><option value="PJ20140101">PJ20140101</option><option value="PJ20140101">PJ20140101</option><option value="PJ20140101">PJ20140101</option><option value="PJ20140104">PJ20140104</option><option value="PJ20140201">PJ20140201</option><option value="PJ20140201">PJ20140201</option><option value="PJ20140301">PJ20140301</option><option value="PJ20140301">PJ20140301</option><option value="PJ20140401">PJ20140401</option><option value="PJ20140501">PJ20140501</option><option value="PJ201407PT">PJ201407PT</option><option value="PJ20141001">PJ20141001</option><option value="PJ20141101">PJ20141101</option><option value="PJ201501">PJ201501</option><option value="PJ201501">PJ201501</option><option value="PJ20150201">PJ20150201</option><option value="PJ20150401">PJ20150401</option><option value="PJ20150402">PJ20150402</option><option value="PJ201506">PJ201506</option><option value="PJ201509">PJ201509</option><option value="PJSP1509">PJSP1509</option><option value="PK20101301">PK20101301</option><option value="PK20150201">PK20150201</option><option value="PK20150201">PK20150201</option><option value="PK20150201">PK20150201</option><option value="PK20150201">PK20150201</option><option value="Q">Q</option><option value="Q">Q</option><option value="Q">Q</option><option value="Q">Q</option><option value="Q">Q</option><option value="Q-Plus">Q-Plus</option><option value="Q-Plus">Q-Plus</option><option value="SMALL SECTION">SMALL SECTION</option><option value="SMALL SECTION">SMALL SECTION</option><option value="Small Section">Small Section</option><option value="SO202002">SO202002</option><option value="Special">Special</option><option value="STOCK SM520">STOCK SM520</option><option value="STOCK SM520">STOCK SM520</option><option value="TIS2015">TIS2015</option><option value="VALUE SERIES">VALUE SERIES</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไขการส่ง</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>พนักงานขาย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value=".load.">load list(s)..</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <br />

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ค้นหาโดย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value="a.CreatedDt">วันที่สร้าง</option><option value="a.Order_Date">วันที่สั่งซื้อ</option><option value="a.UpdatedDt">วันที่แก้ไข</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Total_Weight">TOTAL TON</option><option value="a.Total_Piece">TOTAL PCS</option><option value="a.NetTotalAmount">TOTAL AMT.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไข</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > ค้นหา </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>

                        <MainCard isOption title="SUMMARY ADV.ORDER BY PRODUCT">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>PREVIEW</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>SIZE</th>
                                        <th>GRADE</th>
                                        <th>GROUP</th>
                                        <th>LENGTH</th>
                                        <th>Adv-Stock</th>
                                        <th>Adv-Project</th>
                                        <th>TOTAL</th>
                                        <th>TYPE</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
