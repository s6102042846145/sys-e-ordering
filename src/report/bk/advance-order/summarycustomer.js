import React from 'react';
import { Row, Col, Card, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        //data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
           // { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ตั้งแต่ ปี</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เดือน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                     </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                            <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รอบที่</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option>
                                                     </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER Type</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option><option value="ALLA">ALL-Advance</option><option value="A">Adv-Stock</option><option value="AP">Adv-Project</option><option value="N">Project</option><option value="S">SYS-Stock</option>
                                                           </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option>
                                                    </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการขนส่ง</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                       <option value="ALL">ทั้งหมด</option>
                                                    </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SELECTION</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                    <option value="ALL">ทั้งหมด</option><option value="B">B-BLOOM</option><option value="C">C-CHANNEL</option><option value="I">I-I-BEAM</option><option value="L">L-ANGLE</option><option value="M">M-MODULAR</option><option value="R">R-RAIL</option><option value="S">S-SHEET-PILE</option><option value="T">T-CUT-BEAM</option><option value="V">V-HVA</option>
                                                    </Form.Control>    
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <Form.Control type="text" />
                                                </Col>
                                            </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="SM400">SM400</option><option value="SS400">SS400</option><option value="SS540">SS540</option><option value="SS400/SM400">SS400/SM400</option><option value="43A">43A</option><option value="SM520">SM520</option><option value="SM490">SM490</option><option value="SS490">SS490</option><option value="SM570">SM570</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>กลุ่มสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                            <Form.Control as="select">
                                                               <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="1037">Test1234567</option><option value="1033">SM400</option><option value="1035">SM 520</option><option value="1036">Group Q- plus 2021</option><option value="1034">Group Q-2021</option><option value="1030">โกดังใช้เอง</option><option value="1032">โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV (TIS1390-2560)</option><option value="0757">2014 HAV TOYOTA RICH</option><option value="0864">2020 สหมิตร</option><option value="1031">sheet pile 400x150</option><option value="1028">2020 โปรแกรมพิเศษ</option><option value="1029">2020 SP-II, SP-III, SP-IIIA, SP IV SCG</option><option value="0740">2020 Small Section</option><option value="1027">2020 สินค้าพิเศษ</option><option value="AD20070180">Q-Plus 2020</option><option value="AD20070181">สินค้าสั่งพิเศษ 2020</option><option value="AD20070183">Q  2020 VALUE SERIES</option><option value="AD20070184">Q-Plus 2020-VALUE SERIES</option><option value="AD20070185">Q  2020 - 1 full</option><option value="AD20070186">Q  2020 - 2</option><option value="AD20070187">Q  2020 - 3</option><option value="AD20070188">Q  2020 - 4</option><option value="0764">2020 สินค้าชุด Group Q</option><option value="1014">1910 Q- plus</option><option value="1025">1909  สินค้าชุด Group Q</option><option value="1026">1909  สินค้าชุด Group Q -Plus (new) sm520</option><option value="1011">1907  สินค้าชุด Group Q</option><option value="1020">1907  สินค้าชุด Group Q -Plus (new)</option><option value="1021">1908  สินค้าชุด Group Q</option><option value="1022">1908  สินค้าชุด Group Q -Plus (new)</option><option value="1023">1908  สินค้าชุด Group Q SM520</option><option value="1024">1908  สินค้าชุด Group Q -Plus (new) SM520</option><option value="1017">1906  สินค้าชุด Group Q</option><option value="1019">1906  สินค้าชุด Group Q sm520</option><option value="1018">1906  สินค้าชุด Group Q -Plus (new)</option><option value="1012">1905  สินค้าชุด Group Q</option><option value="1013">1905  สินค้าชุด Group Q -Plus (new)</option><option value="1015">1905  สินค้าชุด Group Q-plus  Value Series</option><option value="1016">1904 Stock อุดม</option><option value="1004">1904  สินค้าชุด Group Q</option><option value="1005">1904  สินค้าชุด Group Q -Plus (new)</option><option value="1006">1904  สินค้าชุด Group Q Value Series</option><option value="1007">1904  สินค้าชุด Group Q SM520</option><option value="1008">1904  สินค้าชุด Group Q -Plus (new) SM520</option><option value="1009">1904  สินค้าชุด Group Q Value Series SM520</option><option value="1010">1904  สินค้าชุด Group Q-plus  Value Series</option><option value="0990">1901  สินค้าชุด Group Q</option><option value="0991">1901  สินค้าชุด Group Q -Plus (new)</option><option value="0992">1901  สินค้าชุด Group Q SM520</option><option value="0993">1901  สินค้าชุด Group Q -Plus (new) Sm520</option>
                                                            </Form.Control>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>พนักงานขาย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="0914">0914-Phongthon P.</option><option value="1138">1138-Thanaphol P.</option><option value="1155">1155-Benjawan J.</option><option value="1176">1176-Ketthip Thingam</option><option value="1234">1234-Voratuch N.</option><option value="1410">1410-Wutthichai W.</option><option value="1854">1854-vanita e.</option><option value="1968">1968-Kritsada k</option><option value="2007">2007-Rattipun T</option><option value="2104">2104-Natnaphong S.</option><option value="2207">2207-Chinnakrit N</option><option value="2298">2298-Kumpol Jirawantana</option><option value="2321">2321-Witsarud W.</option><option value="2374">2374-Gunpragob Cheawchan</option><option value="2383">2383-Atchanat A.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                                </Col> 
                                        </Form.Group>
 
                                        <br />

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ค้นหาโดย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                            <option value="a.CreatedDt">วันที่สร้าง</option><option value="a.Order_Date">วันที่สั่งซื้อ</option><option value="a.UpdatedDt">วันที่แก้ไข</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="a.Total_Weight">TOTAL TON</option><option value="a.Total_Piece">TOTAL PCS</option><option value="a.NetTotalAmount">TOTAL AMT.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไข</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > ค้นหา </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        
                        <MainCard isOption title="SUMMARY ADV.ORDER BY CUSTOMER">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>PREVIEW</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>SIZE STD.</th>
                                        <th>SEC</th>
                                        <th>SIZE ID</th>
                                        <th>NOMINAL SIZE</th>
                                        <th>DIM_DESC</th>
                                        <th>WEIGHT</th>
                                        <th>UM</th>
                                        <th>N</th>
                                        <th>วันที่แก้ไข</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
