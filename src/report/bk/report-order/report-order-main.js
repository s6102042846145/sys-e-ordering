import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import { Link } from "react-router-dom";
class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> รายงานการรับสินค้า </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' href="/report/report-order/reportordersys">
                                            รายการแจ้งรับสินค้า (SYS)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' href="/report/report-order/reportordersales" >
                                            DAILY SALES 
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                       
                                        <Link className='list-group-item list-group-item-action' href="/report/report-order/reportorderproduct" >
                                            รายงานสรุปการรับสินค้าตามใบจัดรถ SYS 
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                       

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' href="/report/report-order/reportorderoff" >
                                            รายงานใบจัดรถที่ไม่ออกตั๋ว
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' href="/report/report-order/reportordercutsys" >
                                            รายงาน รายการตั๋วตัดฝาก SYS
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;