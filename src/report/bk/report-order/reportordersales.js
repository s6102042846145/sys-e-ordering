import React from 'react';
import { Row, Col, Card, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        //data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
           /* { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }*/
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (            
            <Aux>
               
                <Row>
                    <Col>
                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                        <Row>
                        <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ADV.ตั้งแต่ ปี</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เดือน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รอบที่</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ADV.ถึง ปี</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เดือน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>พนักงานขาย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="NONE">None</option><option value="0914">0914-Phongthon P.</option><option value="1138">1138-Thanaphol P.</option><option value="1155">1155-Benjawan J.</option><option value="1176">1176-Ketthip Thingam</option><option value="1234">1234-Voratuch N.</option><option value="1410">1410-Wutthichai W.</option><option value="1854">1854-vanita e.</option><option value="1968">1968-Kritsada k</option><option value="2007">2007-Rattipun T</option><option value="2104">2104-Natnaphong S.</option><option value="2207">2207-Chinnakrit N</option><option value="2298">2298-Kumpol Jirawantana</option>
                                                        <option value="2321">2321-Witsarud W.</option><option value="2374">2374-Gunpragob Cheawchan</option><option value="2383">2383-Atchanat A.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                <Form.Label column sm={4}>ORDER LOT.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>   
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER REV.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRP.CHANNEL</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">All</option><option value="049020">SYS DEALER</option><option value="018020">SCG DEALER</option><option value="049040">INDIRECT EXPORT</option><option value="049030">EXPORT</option><option value="A">SYS+SCG Dealer</option><option value="C">PROJECT</option><option value="D">Domestic (SYS+SCG Dealer+Project)</option><option value="B">Project+Indirect</option><option value="E">Export+Indirect</option>
                                                    </Form.Control> 
                                                    </Col>
                                                </Form.Group>
                                            </Col>     
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="CA15">CA15 - D-L/C sight 15 วัน</option><option value="CD00">CD00 - D-L/C sight</option><option value="CD07">CD07 - D-L/C 7 วัน</option><option value="CD15">CD15 - D-L/C 15 วัน</option><option value="CD60">CD60 - D-L/C 60 วัน</option><option value="CD90">CD90 - D-L/C 90 วัน</option><option value="CDB0">CDB0 - D-L/C 120 วัน</option><option value="DP00">DP00 - D/P AT SIGHT</option><option value="LC00">LC00 - L/C at sight.</option><option value="LCS0">LCS0 - L/C AT SIGHT.</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - เงินเชื่อ 1 วัน</option><option value="NT07">NT07 - เงินเชื่อ 7 วัน</option><option value="NT15">NT15 - เงินเชื่อ 15 วัน</option><option value="NT30">NT30 - เงินเชื่อ 30 วัน</option><option value="NT45">NT45 - เงินเชื่อ 45 วัน</option><option value="NT60">NT60 - เงินเชื่อ 60 วัน</option><option value="NTB0">NTB0 - เงินเชื่อ 120 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option>
                                                    </Form.Control> 
                                                    </Col>
                                                </Form.Group>
                                            </Col>     
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                    <Col sm={8}>
                                                    <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="B">B-BLOOM</option><option value="C">C-CHANNEL</option><option value="I">I-I-BEAM</option><option value="L">L-ANGLE</option><option value="M">M-MODULAR</option><option value="R">R-RAIL</option><option value="S">S-SHEET-PILE</option><option value="T">T-CUT-BEAM</option><option value="V">V-HVA</option>
                                                    </Form.Control> 
                                                    </Col>
                                                </Form.Group>
                                            </Col>  
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Hierarchy2</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col> 
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="NONE">ไม่ระบุ</option><option value="SM400">SM400</option><option value="SS400">SS400</option><option value="SS540">SS540</option><option value="SS400/SM400">SS400/SM400</option><option value="43A">43A</option><option value="SM520">SM520</option><option value="SM490">SM490</option><option value="SS490">SS490</option><option value="SM570">SM570</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="MY">=6M,9M,12M</option><option value="MN">&lt;&gt;6M,9M,12M</option><option value="FY">=20F,30F,40F</option><option value="FN">&lt;&gt;20F,30F,40F</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาว</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>หน่วย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="M">เมตร</option><option value="F">ฟุต</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ChannelCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PLANT</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="4941">พิษณุโลก</option><option value="4911">ระยอง1</option><option value="4931">ระยอง2</option><option value="4921">ศรีราชา</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <br />
                                        
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ค้นหาโดย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value="a.SODt">วันที่ so</option><option value="a.SystemDt">วันที่ system</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="a.Total_Weight">TOTAL TON</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไข</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col>
                                            <Button className="pull-right" size="sm" > ค้นหา </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        </MainCard>
                        <MainCard isOption title="DAILY SALES>">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info"/></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red"/></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="danger" >EXPORT CSV</Button>
                                    <Button size="sm" variant="success" >EXPORT XLS</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>SALEMAN</th>
                                        <th>SALES</th>
                                        <th>EST</th>
                                        <th>EST-GM</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
