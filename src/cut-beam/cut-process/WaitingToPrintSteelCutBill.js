import React from 'react';
import { Row, Col, Form, Button, Table, Modal, Tabs, Tab, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "col1": "15-02-10",
        "col2": "06-04-10",
        "col3": "3001945",
        "col4": "ศิริกุล",
        "col5": "CV10020002",
        "col6": "0014000029",
        "col7": "09-03-60",
        "col8": "1,947.86",
        "col9": "128.49",
        "col10": "1,908.92",
        "col11": "23-11-53",
        "col12": "Y"
    },
    {
        "id": 2,
        "col1": "15-02-10",
        "col2": "06-04-10",
        "col3": "3001945",
        "col4": "ศิริกุล",
        "col5": "CV10020002",
        "col6": "0014000029",
        "col7": "15-02-53",
        "col8": "679.59",
        "col9": "47.57",
        "col10": "706.77",
        "col11": "23-11-53",
        "col12": "Y"
    },
    {
        "id": 3,
        "col1": "15-02-10",
        "col2": "06-04-10",
        "col3": "3001945",
        "col4": "ศิริกุล",
        "col5": "CV10020002",
        "col6": "0014000029",
        "col7": "15-02-53",
        "col8": "281.12",
        "col9": "19.68",
        "col10": "292.37",
        "col11": "23-11-53",
        "col12": "Y"
    },
];

function atable() {
    let tableZero = '#data-table';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", render: function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "col1", render: function (data, type, row) { return data; } },
            { "data": "col2", render: function (data, type, row) { return data; } },
            { "data": "col3", render: function (data, type, row) { return data; } },
            { "data": "col4", render: function (data, type, row) { return data; } },
            { "data": "col5", render: function (data, type, row) { return data; } },
            { "data": "col6", render: function (data, type, row) { return data; } },
            { "data": "col7", render: function (data, type, row) { return data; } },
            { "data": "col8", render: function (data, type, row) { return data; } },
            { "data": "col9", render: function (data, type, row) { return data; } },
            { "data": "col10", render: function (data, type, row) { return data; } },
            { "data": "col11", render: function (data, type, row) { return data; } },
            {
                sortable: false,
                className: "text-center",
                "data": "col12", render: function (data, type, row) {
                    return "<button id='btnDel' type='button' class='btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple btn btn-default'><span class='feather icon-file text-c-blue'></span></button><button id='btnDel' type='button' class='btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple btn btn-default'><span class='feather icon-file-minus text-c-red'></span></button><button id='btnDel' type='button' class='btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple btn btn-default'><span class='feather icon-file-minus text-c-red'></span></button>"
                }
            },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class Index extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        this.setState({ setTitleModal: "รายละเอียด" })

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#checkbox-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnMail').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnMail').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnMail').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnMail').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnMail').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnMail').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>ลูกค้า</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <Form.Label>3001945-บ.ศิริกุลสตีล จก.</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>issue date</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วิธีการชำระเงิน</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <select className="form-control-edit">
                                                                <option value="NONE">ไม่ระบุ</option><option value="NT60">BG 60 DAYS</option><option value="CDB0">DLC 120 days</option><option value="CA15">DLC 15 days</option><option value="CD90">DLC 90 days</option><option value="NT00">เงินสด</option>
                                                            </select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>ที่อยู่</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <Form.Label>8/88 ม.11</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ออกใบแจ้งหนี้</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}></Col>
                                                        <Col sm={8}>
                                                            <Form.Label>ต.บางปลา อ.บางพลี</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>due date</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}></Col>
                                                        <Col sm={8}>
                                                            <Form.Label>จ.สมุทรปราการ</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={2}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={8}>
                                                            <Form.Check label=" : ระบุ เลขที่ Inv" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={2}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={8}>
                                                            <Form.Check label=" : ระบุ เลขที่ Inv" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}></Col>
                                                <Col sm={4}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={12}>
                                                    <Tabs variant="pills" defaultActiveKey="home" className="mb-3">
                                                        <Tab eventKey="home" title="SIZE">
                                                            <Table ref="tbl" striped hover responsive bordered id="data-table-add">
                                                                <thead>
                                                                    <tr>
                                                                        <th><Form.Check id="checkbox-select-all" /></th>
                                                                        <th>#</th>
                                                                        <th>รายการก่อนตัด</th>
                                                                        <th>ปริมาณตัน</th>
                                                                        <th>ปริมาณท่อน</th>
                                                                        <th>เสร็จตัน</th>
                                                                        <th>เสร็จท่อน</th>
                                                                        <th>ราคาค่าตัด</th>
                                                                        <th>ประเภทการตัด</th>
                                                                        <th>เลขที่DP</th>
                                                                        <th>วันที่ต้องการรับ</th>
                                                                    </tr>
                                                                </thead>
                                                            </Table>
                                                        </Tab>
                                                        <Tab eventKey="profile" title="REMARK">
                                                            <Form.Group as={Row}>
                                                                <Col sm={12}>
                                                                    <Form.Group as={Row}>
                                                                        <Form.Label column sm={1}>Remark</Form.Label>
                                                                        <Col sm={11}>
                                                                            <textarea rows="4" cols="160" />
                                                                        </Col>
                                                                    </Form.Group>
                                                                </Col>
                                                            </Form.Group>
                                                        </Tab>
                                                    </Tabs>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>ปริมาณตันรวม</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>ปริมาณท่อนรวม</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>รวมค่าบริการตัด(บาท)</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>VAT 7.00%</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>รวม(บาท)</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>หัก ณ ที่จ่าย 3.00%</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>รวมทั้งสิ้น(บาท)</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <br />
                                            <Row>
                                                <Col className="btn-page">
                                                    <Button size="sm" variant="primary">PAYMENT</Button>
                                                </Col>
                                                <Col className="btn-page text-right" sm>
                                                    <Button size="sm" variant="success">ตรวจสอบ</Button>
                                                    <Button size="sm" variant="danger">ยกเลิกตรวจสอบ</Button>
                                                    <Button id="btnDel" variant="default" className="mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-plus text-c-green" /></Button>
                                                    <Button id="btnDel" variant="default" className="mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-save text-c-blue" /></Button>
                                                    <Button id="btnDel" variant="default" className="mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                                </Col>
                                            </Row>
                                            <br />
                                            <Table ref="tbl" striped hover responsive bordered id="data-table-add">
                                                <thead>
                                                    <tr>
                                                        <th><Form.Check id="checkbox-select-all" /></th>
                                                        <th>#</th>
                                                        <th>ประเภท</th>
                                                        <th>ธนาคาร</th>
                                                        <th>เลขที่เช็ค</th>
                                                        <th>วันที่ออกเช็ค</th>
                                                        <th>ผู้สร้าง</th>
                                                        <th>วันที่สร้าง</th>
                                                    </tr>
                                                    <tr>
                                                        <th colSpan={5}>ประเทศ/สาขา</th>
                                                        <th>จำนวนเงิน</th>
                                                        <th>ผู้แก้ไข</th>
                                                        <th>วันที่แก้ไข</th>
                                                    </tr>
                                                </thead>
                                            </Table>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>รวมจ่ายแล้ว(บาท)</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={3}></Col>
                                                <Col sm={3}></Col>
                                                <Col sm={2}></Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>คงเหลือ(บาท)</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Row>
                                                <Col className="btn-page">
                                                    <Button size="sm" variant="primary">SEND MAIL</Button>
                                                </Col>
                                                <Col className="btn-page text-right" sm>
                                                    <Button id="btnDel" variant="default" className="mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-mail text-c-green" /></Button>
                                                </Col>
                                            </Row>
                                            <br />
                                            <Table ref="tbl" striped hover responsive bordered id="data-table-add">
                                                <thead>
                                                    <tr>
                                                        <th><Form.Check id="checkbox-select-all" /></th>
                                                        <th>#</th>
                                                        <th>E-MAIL ADDRESS 3 [ Account ]</th>
                                                    </tr>
                                                </thead>
                                            </Table>
                                            <hr />
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ยืนยัน</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ส.สจ. พิมพ์เอกสาร</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ส.สจ. รับเหล็กจากลูกค้า</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ผู้รับเหล็กตัดเสร็จ</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่ใบแจ้งหนี้</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่ Inv</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ระบุเลขที่ Inv</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="Y">ระบุเลขที่ Inv แล้ว</option>
                                                            <option value="N">ไม่ระบุเลขที่ Inv</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อผู้แก้ไข</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่ใบเสร็จ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ระบุเลขที่ใบเสร็จ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="Y">ระบุเลขที่ใบเสร็จแล้ว</option>
                                                            <option value="N">ไม่ระบุเลขที่ใบเสร็จ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE45">BE45 - ตั๋วแลกเงิน 45 วัน (BE)</option><option value="BE60">BE60 - ตั๋วแลกเงิน 60 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="BED0">BED0 - ตั๋วแลกเงิน 150 วัน (BE)</option><option value="BEF0">BEF0 - ตั๋วแลกเงิน 180 วัน (BE)</option><option value="BEH0">BEH0 - ตั๋วแลกเงิน 210 วัน (BE)</option><option value="BEJ0">BEJ0 - ตั๋วแลกเงิน 240 วัน (BE)</option><option value="BEL0">BEL0 - ตั๋วแลกเงิน 270 วัน (BE)</option><option value="BEN0">BEN0 - ตั๋วแลกเงิน 300 วัน (BE)</option><option value="BER0">BER0 - ตั๋วแลกเงิน 360 วัน (BE)</option><option value="BS30">BS30 - ตั๋วแลกเงิน 30 วัน (BE) before shipment.</option><option value="BS60">BS60 - ตั๋วแลกเงิน 60 วัน (BE) before shipment.</option><option value="BS90">BS90 - ตั๋วแลกเงิน 90 วัน (BE) before shipment.</option><option value="BSA0">BSA0 - ตั๋วแลกเงิน 105 วัน (BE) before shipment.</option><option value="BSB0">BSB0 - ตั๋วแลกเงิน 120 วัน (BE) before shipment.</option><option value="BSD0">BSD0 - ตั๋วแลกเงิน 150 วัน (BE) before shipment.</option><option value="BSF0">BSF0 - ตั๋วแลกเงิน 180 วัน (BE) before shipment.</option><option value="CA15">CA15 - DLC 15 days</option><option value="CD00">CD00 - DLC</option><option value="CD07">CD07 - DLC 7 days</option><option value="CD15">CD15 - DLC 15 days</option><option value="CD30">CD30 - DLC 30 days</option><option value="CD60">CD60 - DLC 60 days</option><option value="CD90">CD90 - DLC 90 days</option><option value="CDB0">CDB0 - DLC 120 days</option><option value="CH07">CH07 - DLC</option><option value="CH15">CH15 - DLC</option><option value="CH30">CH30 - DLC</option><option value="CH60">CH60 - DLC</option><option value="CH90">CH90 - DLC</option><option value="CT00">CT00 - DLC</option><option value="CT03">CT03 - DLC</option><option value="CV03">CV03 - DLC</option><option value="DB07">DB07 - D/A 7 days after receiced bill.</option><option value="DB30">DB30 - D/A 30 days after receiced bill.</option><option value="DF00">DF00 - Draft.</option><option value="DL15">DL15 - D/A 15 days after B/L date.</option><option value="DL30">DL30 - D/A 30 days after B/L date.</option><option value="DL45">DL45 - D/A 45 days after B/L date.</option><option value="DL60">DL60 - D/A 60 days after B/L date.</option><option value="DL75">DL75 - D/A 75 days after B/L date.</option><option value="DL90">DL90 - D/A 90 days after B/L date.</option><option value="DLB0">DLB0 - D/A 120 days after B/L date.</option><option value="DLF0">DLF0 - D/A 180 days after B/L date.</option><option value="DND0">DND0 - D/A 150 days from B/L date</option><option value="DP00">DP00 - D/P at sight</option><option value="DP07">DP07 - D/P 7 days after B/L date</option><option value="DP15">DP15 - D/P at sight</option><option value="DP30">DP30 - D/P 30 days after B/L date</option><option value="DP45">DP45 - D/P 45 days after B/L date</option><option value="DV07">DV07 - D/A 7 days after invoice,B/L,AWB date.</option><option value="DV14">DV14 - D/A 14 days after invoice,B/L,AWB date.</option><option value="DV30">DV30 - D/A 30 days after invoice,B/L,AWB date.</option><option value="FI15">FI15 - within 15th of next month</option><option value="FI30">FI30 - within 30th of next month</option><option value="HB03">HB03 - HSBC-Dealer Financing (Invoice Financing)</option><option value="KD07">KD07 - Credit 7 days.</option><option value="LC00">LC00 - L/C at sight.</option><option value="LC15">LC15 - L/C 15 days.</option><option value="N120">N120 - Within 120 days  after received bill</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM02">NM02 - เงินสด 2 วัน</option><option value="NM03">NM03 - เงินสด 3 วัน</option><option value="NM05">NM05 - เงินสด 5 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NM50">NM50 - เงินสด 50 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - BG 1 วัน</option><option value="NT02">NT02 - BG 2 วัน</option><option value="NT04">NT04 - BG 4 วัน</option><option value="NT05">NT05 - BG 5 วัน</option><option value="NT07">NT07 - BG 7 วัน</option><option value="NT10">NT10 - BG 10 วัน</option><option value="NT15">NT15 - BG 15 วัน</option><option value="NT21">NT21 - BG 21 วัน</option><option value="NT30">NT30 - BG 30 วัน</option><option value="NT45">NT45 - BG 45 วัน</option><option value="NT50">NT50 - BG 50 วัน</option><option value="NT60">NT60 - BG 60 วัน</option><option value="NT70">NT70 - BG 70 วัน</option><option value="NT75">NT75 - BG 75 วัน</option><option value="NT90">NT90 - BG 90 วัน</option><option value="NTB0">NTB0 - BG 120 วัน</option><option value="NTH0">NTH0 - BG 210 วัน</option><option value="NTJ0">NTJ0 - BG 240 วัน</option><option value="NTL0">NTL0 - BG 270 วัน</option><option value="NTN0">NTN0 - BG 300 วัน</option><option value="NTP0">NTP0 - BG 330 วัน</option><option value="NTR0">NTR0 - BG 360 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL30">TL30 - T/T 30 days after B/L date.</option><option value="TL35">TL35 - T/T 35 days after B/L date.</option><option value="TL45">TL45 - T/T 45 days after B/L date.</option><option value="TL60">TL60 - T/T 60 days after B/L date.</option><option value="TL75">TL75 - T/T 75 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TLB0">TLB0 - T/T 120 days after B/L date.</option><option value="TLD0">TLD0 - T/T 150 days after B/L date</option><option value="TLH0">TLH0 - T/T 210 days after B/L date</option><option value="TLI0">TLI0 - T/T 180 days after B/L date.</option><option value="TQ30">TQ30 - T/T 30 days after received goods</option><option value="TR60">TR60 - T/T remittance 60 days after shipment</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS07">TS07 - T/T before shipment 7 day.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option><option value="ZT00">ZT00 - L/C at sight 98%,T/T 2% after settlement</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Acc.Status</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="NONE">None</option><option value="BAL">Balance</option><option value="OUT">OutStand</option><option value="ALL">ทั้งหมด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="000005">เสริมสิริ</option><option value="3001948">บริษัท อุดมโลหะกิจ (1975) จำกัด</option><option value="3001945">บริษัท ศิริกุลสตีล จำกัด</option><option value="3001951">บริษัท ธนสารเซ็นทรัลสตีล จำกัด</option><option value="3007314">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="3007503">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="000007">บ. โลหะไพศาลวานิช จก.</option><option value="000006">บ. ซี.เอส.เอช.สตีล จก.</option><option value="000004">ชุมเสริม</option><option value="T000025">ZTCO INTERNATIONAL</option><option value="T000011">VULCAN STEEL</option><option value="3005353">V.S.V. ASIA CO.,LTD.</option><option value="T000032">TRIDENT STEEL</option><option value="T000031">TRANSCAPE STEELS (PTY) LTD</option><option value="3005317">TRADEARBED PTE. LTD.</option><option value="T000035">TOTAL EXPRESS LIMITED</option><option value="T000017">SURDEX STEEL PTY LIMITED</option><option value="3005316">SUMITOMO CORPORATION (SINGAPORE)</option><option value="3005260">SUMITOMO CORPORATION</option><option value="3005242">STEMCOR UK LIMITED</option><option value="T000018">STEMCOR (S.E.A.) PTE. LTD.</option><option value="T000012">STEELFORCE TRADING PTY.LTD.</option><option value="3005320">STARPOLY CORPORATION</option><option value="T000016">SOUTHERN STEEL GROUP PTY LIMITED</option><option value="T000022">SIMS STEEL</option><option value="T000007">SIAM CEMENT MYANMAR TRADING</option><option value="T000008">SIAM CEMENT CAMBODIA TRADING</option><option value="3005336">SHIMIZU - MARUBENI CONSORTIUM</option><option value="T000026">SEMET CO., LTD</option><option value="3005330">SCT-VIETNAM</option><option value="T000021">RAFFRAY BROTHERS CO LTD</option><option value="T000034">PLAN B</option><option value="T000003">PFC ENGINEERING SDN.BHD.</option><option value="T000001">ORIENT MATERIAL</option><option value="T000024">ORERORT (PTY) LTD</option><option value="3005312">NOMURA TRADING CO.,LTD.</option><option value="3005259">NICHIMEN CORPORATION</option><option value="3005311">NEWCO RESOURCES PTE. LTD.</option><option value="T000029">NATIONAL STEEL GROUP</option><option value="3005247">MITSUI AND CO (HONGKONG) LTD.</option><option value="3005310">MITSUI &amp; CO.,LTD.SINGAPORE BRANCH</option><option value="3002017">MITSUI &amp; CO.,LTD.</option><option value="3005309">MITSUI &amp; CO.,LTD.</option><option value="T000037">MITSUI &amp; CO. (ASIA PACIFIC) PTE. LTD.</option><option value="3005248">METAL ONE HONG KONG LIMITED</option><option value="T000027">MELSTEEL (PTY) LTD</option><option value="3005246">MARUBENI-ITOCHU STEEL PTE  LTD.</option><option value="3005307">MARUBENI SINGAPORE PTE. LTD.</option><option value="T000030">MACSTEEL TRADING</option><option value="T000006">LION METAL (THAILAND) CO.,LTD.</option><option value="T000020">LAM PO TANG AND CO LTD</option><option value="3005303">KAWASHO - SINGAPORE</option><option value="T000013">HORAN STEEL LIMITED</option><option value="T000002">HANWA (MALAYSIA) SDN. BHD.</option><option value="T000036">GLOBAL ALLIANZ(UK) LTD</option><option value="3005294">GAYATHRI STEELS</option><option value="T000010">G.A.M. STEEL PTY.LTD.</option><option value="T000033">ENSYS (PVT) LTD</option><option value="3005243">CORUS TRADING LIMITED</option><option value="T000015">COIL STEELS</option><option value="3005232">CMC (AUSTRALIA) PTY. LIMITED</option><option value="T000004">Chumserm</option><option value="T000014">CENTRAL STEEL TRADING PTY LTD</option><option value="3005264">CEMENTHAI TRADING (M) SDN.BHD.</option><option value="T000009">BLUESCOPE DISTRIBUTION</option><option value="T000023">ASIA NAVIGATION PTE LTD</option><option value="T000019">ARABIAN INTERNATIONAL COMPANY</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะอนุมัติ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="Y">อนุมัติแล้ว</option>
                                                            <option value="N">ยังไม่อนุมัติ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะส่ง e-mail</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="Y">ส่ง e-mail แล้ว</option>
                                                            <option value="N">ยังไม่ได้ส่ง e-mail</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.NetTotalAmount">Net Total Amount</option><option value="a.PaidAmount">Paid Amount</option><option value="a.VatAmount">VAT Amount</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="อนุมัติใบแจ้งหนี้ค่าตัดเหล็ก">
                            <Row>
                                <Col className="btn-page">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                </Col>
                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="outline-success">SEND TO SAP</Button>
                                    <Button size="sm" variant="primary">PDF ใบแจ้งหนี้ค่าตัด</Button>
                                    <Button size="sm" variant="primary">PDF ใบส่งของ</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="checkbox-select-all" /></th>
                                        <th>#</th>
                                        <th>วันที่สร้าง</th>
                                        <th>Due Date</th>
                                        <th>CUST.CODE</th>
                                        <th>ชื่อลูกค้า - ซื้อสำหรับ</th>
                                        <th>เลขที่ใบแจ้ง</th>
                                        <th>เลขที่ Inv</th>
                                        <th>Posting Date</th>
                                        <th>Amt.ค่าบริการ</th>
                                        <th>Amt.VAT</th>
                                        <th>Amt. รวม</th>
                                        <th>อนุมัติ</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default Index;
