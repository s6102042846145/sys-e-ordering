import React from 'react';
import { Row, Col, Form, Button, Table, Modal, Tabs, Tab, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const dataList = [
    {
        "id": 1,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001945-ศิริกุล",
        "col7": "SYS ดำเนินการ",
        "col8": "-",
        "col9": ""
    },
    {
        "id": 2,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001949-เชื้อไพบูลย์",
        "col7": "SYS ดำเนินการ",
        "col8": "AOW1000842-0",
        "col9": ""
    },
    {
        "id": 3,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001945-ศิริกุล",
        "col7": "SYS ดำเนินการ",
        "col8": "-",
        "col9": ""
    }
];
const dataList2 = [
    {
        "id": 1,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001945-ศิริกุล",
        "col7": "SYS ดำเนินการ",
        "col8": "-"
    },
    {
        "id": 2,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001949-เชื้อไพบูลย์",
        "col7": "SYS ดำเนินการ",
        "col8": "AOW1000842-0"
    },
    {
        "id": 3,
        "col1": "SR",
        "col2": "นำเหล็กมาตัดเอง",
        "col3": "15-02-10",
        "col4": "CB10020001",
        "col5": "0490-20",
        "col6": "3001945-ศิริกุล",
        "col7": "SYS ดำเนินการ",
        "col8": "-"
    }
];


function atable() {
    let tableZero = '#data-table';
    let tableZeroAdd = '#data-table-add';

    $(tableZero).DataTable({
        data: dataList,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", render: function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "col1", render: function (data, type, row) { return data; } },
            { "data": "col2", render: function (data, type, row) { return data; } },
            { "data": "col3", render: function (data, type, row) { return data; } },
            { "data": "col4", render: function (data, type, row) { return data; } },
            { "data": "col5", render: function (data, type, row) { return data; } },
            { "data": "col6", render: function (data, type, row) { return data; } },
            { "data": "col7", render: function (data, type, row) { return data; } },
            { "data": "col8", render: function (data, type, row) { return data; } },
            {
                sortable: false,
                className: "text-center",
                "data": "id", render: function (data, type, row) {
                    return "<button id='btnDel' type='button' class='btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple btn btn-default'><span class='feather icon-upload-cloud text-c-blue'></span></button>"
                }
            },
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });

    $(tableZeroAdd).DataTable({
        data: dataList2,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", render: function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "col1", render: function (data, type, row) { return data; } },
            { "data": "col2", render: function (data, type, row) { return data; } },
            { "data": "col3", render: function (data, type, row) { return data; } },
            { "data": "col4", render: function (data, type, row) { return data; } },
            { "data": "col5", render: function (data, type, row) { return data; } },
            { "data": "col6", render: function (data, type, row) { return data; } },
            { "data": "col7", render: function (data, type, row) { return data; } },
            { "data": "col8", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class Index extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#checkbox-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>เลขที่ใบแจ้งตัดเหล็ก</Form.Label>
                                                        </Col>
                                                        <Col sm={6}>
                                                            <select className="form-control-edit">
                                                                <option value="ALL">สร้าง ตั๋วตัดฝาก</option><option value="4951">สร้าง ฝากตัด</option><option value="4941">สร้าง ตัดเหล็กสต๊อก</option>
                                                            </select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>วันที่เอกสาร</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>สถานะ</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={8}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={3}>
                                                            <Form.Label>ลูกค้า</Form.Label>
                                                        </Col>
                                                        <Col sm={9}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>วิธีการชำระเงิน</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit">
                                                                <option value="NONE">ไม่มี</option>
                                                            </select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={8}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={3}>
                                                            <Form.Label>ที่อยู่</Form.Label>
                                                        </Col>
                                                        <Col sm={9}>
                                                            <input type="text" className="form-control-file" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>สถานที่รับสินค้า</Form.Label>
                                                        </Col>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit">
                                                                <option value="4941">SYS Solution</option><option value="4951">บ้านบึง</option><option value="4911">ระยอง1</option><option value="4931">ระยอง2</option><option value="4921">ศรีราชา</option>
                                                            </select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={12}>
                                                    <Tabs variant="pills" defaultActiveKey="home" className="mb-3">
                                                        <Tab eventKey="home" title="รายละเอียดสินค้า">
                                                            <Table ref="tbl" striped hover responsive bordered id="data-table-add">
                                                                <thead>
                                                                    <tr>
                                                                        <th><Form.Check id="checkbox-select-all" /></th>
                                                                        <th>#</th>
                                                                        <th>รายการก่อนตัด</th>
                                                                        <th>ปริมาณตัน</th>
                                                                        <th>ปริมาณท่อน</th>
                                                                        <th>เสร็จตัน</th>
                                                                        <th>เสร็จท่อน</th>
                                                                        <th>ราคาค่าตัด</th>
                                                                        <th>ประเภทการตัด</th>
                                                                        <th>เลขที่DP</th>
                                                                        <th>วันที่ต้องการรับ</th>
                                                                    </tr>
                                                                </thead>
                                                            </Table>
                                                        </Tab>
                                                        <Tab eventKey="profile" title="เอกสารแนบ">
                                                            <center>ไม่มีเอกสารแนบ</center>
                                                        </Tab>
                                                    </Tabs>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={12}>
                                                    <Row>
                                                        <Col className="btn-page">
                                                            <Button size="sm" variant="danger" >ยกเลิก ใบแจ้ง</Button>
                                                        </Col>
                                                        <Col className="btn-page text-right" sm>
                                                            <Button size="sm" variant="success"> บันทึก </Button>
                                                            <Button size="sm" variant="primary" >ตัวอย่างพิมพ์</Button>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Form.Group>
                                            <br />
                                            <Form.Group as={Row}>
                                                <Col sm={12}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={2}>
                                                            <Form.Label>รายละเอียดเพิ่มเติม</Form.Label>
                                                        </Col>
                                                        <Col sm={10}>
                                                            <textarea rows="3" cols="100" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>วันที่สร้าง</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>08-Jul-21 00:00 </Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>ISS@sys </Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>วันที่แก้ไข</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>08-Jul-21 00:00 </Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>ISS@sys </Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>วันที่อนุมัติ</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={4}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={8}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={12}>
                                                            <Button size="sm" variant="success"> บันทึก </Button>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <hr />
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ยืนยัน</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ส.สจ. พิมพ์เอกสาร</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ส.สจ. รับเหล็กจากลูกค้า</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={6}>
                                                            <Form.Label>วันที่ SYS ผู้รับเหล็กตัดเสร็จ</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Col sm={1}>
                                                            <Form.Label>โดย</Form.Label>
                                                        </Col>
                                                        <Col className="text-center" sm={6}>
                                                            <Form.Label>-</Form.Label>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <br />
                                            <p>หมายเหตุ.- หากท่านออกเอกสารการรับสินค้านอกเวลาทำการปกติ (7.30 น. ถึง 17.00 น. และวันหยุด) สามารถรับสินค้าได้ในวันทำการถัดไป **</p>
                                        </Form>
                                    </Col>
                                </Row>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" size="sm" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary" size="sm">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" placeholder="ใบขอนุมัติตัดเหล็ก" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานที่รับสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="4951">BDC-บ้านบึง</option><option value="4941">SOL-SYS Solution</option><option value="4921">SR-ศรีราชา</option><option value="4911">SYS1-ระยอง1</option><option value="4931">SYS2-ระยอง2</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}></Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Order No.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Rev</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="000005">เสริมสิริ</option><option value="3001948">บริษัท อุดมโลหะกิจ (1975) จำกัด</option><option value="3001945">บริษัท ศิริกุลสตีล จำกัด</option><option value="3001951">บริษัท ธนสารเซ็นทรัลสตีล จำกัด</option><option value="3007314">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="3007503">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="000007">บ. โลหะไพศาลวานิช จก.</option><option value="000006">บ. ซี.เอส.เอช.สตีล จก.</option><option value="000004">ชุมเสริม</option><option value="T000025">ZTCO INTERNATIONAL</option><option value="T000011">VULCAN STEEL</option><option value="3005353">V.S.V. ASIA CO.,LTD.</option><option value="T000032">TRIDENT STEEL</option><option value="T000031">TRANSCAPE STEELS (PTY) LTD</option><option value="3005317">TRADEARBED PTE. LTD.</option><option value="T000035">TOTAL EXPRESS LIMITED</option><option value="T000017">SURDEX STEEL PTY LIMITED</option><option value="3005316">SUMITOMO CORPORATION (SINGAPORE)</option><option value="3005260">SUMITOMO CORPORATION</option><option value="3005242">STEMCOR UK LIMITED</option><option value="T000018">STEMCOR (S.E.A.) PTE. LTD.</option><option value="T000012">STEELFORCE TRADING PTY.LTD.</option><option value="3005320">STARPOLY CORPORATION</option><option value="T000016">SOUTHERN STEEL GROUP PTY LIMITED</option><option value="T000022">SIMS STEEL</option><option value="T000007">SIAM CEMENT MYANMAR TRADING</option><option value="T000008">SIAM CEMENT CAMBODIA TRADING</option><option value="3005336">SHIMIZU - MARUBENI CONSORTIUM</option><option value="T000026">SEMET CO., LTD</option><option value="3005330">SCT-VIETNAM</option><option value="T000021">RAFFRAY BROTHERS CO LTD</option><option value="T000034">PLAN B</option><option value="T000003">PFC ENGINEERING SDN.BHD.</option><option value="T000001">ORIENT MATERIAL</option><option value="T000024">ORERORT (PTY) LTD</option><option value="3005312">NOMURA TRADING CO.,LTD.</option><option value="3005259">NICHIMEN CORPORATION</option><option value="3005311">NEWCO RESOURCES PTE. LTD.</option><option value="T000029">NATIONAL STEEL GROUP</option><option value="3005247">MITSUI AND CO (HONGKONG) LTD.</option><option value="3005310">MITSUI &amp; CO.,LTD.SINGAPORE BRANCH</option><option value="3002017">MITSUI &amp; CO.,LTD.</option><option value="3005309">MITSUI &amp; CO.,LTD.</option><option value="T000037">MITSUI &amp; CO. (ASIA PACIFIC) PTE. LTD.</option><option value="3005248">METAL ONE HONG KONG LIMITED</option><option value="T000027">MELSTEEL (PTY) LTD</option><option value="3005246">MARUBENI-ITOCHU STEEL PTE  LTD.</option><option value="3005307">MARUBENI SINGAPORE PTE. LTD.</option><option value="T000030">MACSTEEL TRADING</option><option value="T000006">LION METAL (THAILAND) CO.,LTD.</option><option value="T000020">LAM PO TANG AND CO LTD</option><option value="3005303">KAWASHO - SINGAPORE</option><option value="T000013">HORAN STEEL LIMITED</option><option value="T000002">HANWA (MALAYSIA) SDN. BHD.</option><option value="T000036">GLOBAL ALLIANZ(UK) LTD</option><option value="3005294">GAYATHRI STEELS</option><option value="T000010">G.A.M. STEEL PTY.LTD.</option><option value="T000033">ENSYS (PVT) LTD</option><option value="3005243">CORUS TRADING LIMITED</option><option value="T000015">COIL STEELS</option><option value="3005232">CMC (AUSTRALIA) PTY. LIMITED</option><option value="T000004">Chumserm</option><option value="T000014">CENTRAL STEEL TRADING PTY LTD</option><option value="3005264">CEMENTHAI TRADING (M) SDN.BHD.</option><option value="T000009">BLUESCOPE DISTRIBUTION</option><option value="T000023">ASIA NAVIGATION PTE LTD</option><option value="T000019">ARABIAN INTERNATIONAL COMPANY</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR50">A572 GR.50</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SN"></option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY390">SY390</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาวพิเศษ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="MY">=6M,9M,12M</option>
                                                            <option value="MN">&lt;&gt;6M,9M,12M</option>
                                                            <option value="FY">=20F,30F,40F</option>
                                                            <option value="FN">&lt;&gt;20F,30F,40F</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ความยาว</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>หน่วย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="M">เมตร</option>
                                                            <option value="F">ฟุต</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ลูกค้าอนุมัติ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="Y">อมุมัติ</option><option value="N">ไม่อมุมัติ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SYS อนุมัติ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="Y">อมุมัติ</option><option value="N">ไม่อมุมัติ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะรายการ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0">รออนุมัติ New</option><option value="1">รออนุมัติ Revise</option><option value="2">รออนุมัติ Completed</option><option value="0_1_2">รออนุมัติ </option><option value="5">ยกเลิก</option><option value="6">อนุมัติ</option><option value="7">SYS ดำเนินการ</option><option value="8">ปิด</option><option value="Y">Canceled</option><option value="N">Actived</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Col sm={4}></Col>
                                                    <Col sm={8}>
                                                        <Form.Check label=" : ใบแจ้งตัดที่มีกาารับสินค้าจากการตัด" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Col sm={4}></Col>
                                                    <Col sm={8}>
                                                        <Form.Check label=" : ลูกค้าค้างรับ" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="TON">TON</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="ใบแจ้งตัดเหล็ก (SYS)">
                            <Row>
                                <Col className="btn-page">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>
                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="checkbox-select-all" /></th>
                                        <th>#</th>
                                        <th>PLANT</th>
                                        <th>ประเภท</th>
                                        <th>วันที่สร้าง</th>
                                        <th>เลขที่ใบแจ้งตัด</th>
                                        <th>กลุ่ม</th>
                                        <th>ชื่อลูกค้า-ซื้อสำหรับ</th>
                                        <th>สถานะ</th>
                                        <th>เลขที่ใบสั่งซื้อ</th>
                                        <th>Upload</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default Index;
