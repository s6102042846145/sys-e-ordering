import React from 'react';
import { Row, Col, Card, Nav } from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {

    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Memo for CUT </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/cut-beam-report/ReportFromCustomer">
                                            รายงานรับสินค้าจากลูกค้า
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/cut-beam-report/ReportCuttingBill">
                                            รายงานใบแจ้งหนี้ค่าตัด
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/cut-beam-report/ReportReceivedFromCut" >
                                            รายงานรับสินค้าจากการตัด
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/cut-beam-report/ReportCashForCutting">
                                            รายงานรับเงินค่าตัดเหล็ก
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;