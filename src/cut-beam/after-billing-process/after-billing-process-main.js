import React from 'react';
import { Row, Col, Card, Nav } from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {

    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> After Billing Process </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/after-billing-process/ImportDataBillDueDate">
                                            นำเข้าข้อมูล BILL DUE DATE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/after-billing-process/ReceiptInformation" >
                                            ข้อมูลใบรับชำระ OCW
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/after-billing-process/DataBillDueDate" >
                                            ข้อมูล BILL DUE DATE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/cut-beam/after-billing-process/WaybillData" >
                                            ข้อมูลใบนำส่ง
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;