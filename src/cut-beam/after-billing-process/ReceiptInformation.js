import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "col1": "25-เม.ย.-60",
        "col2": "RV17040002",
        "col3": "4-เม.ย.-60",
        "col4": "3001577",
        "col5": "ไทยเฮอร์ริค",
        "col6": "CASH",
        "col7": "718,150.40",
        "col8": "123",
        "col9": "25-เม.ย.-60",
        "col10": "SD21070002",
        "col11": "11-ก.ค.64"
    },
    {
        "id": 2,
        "col1": "25-เม.ย.-60",
        "col2": "RV17040003",
        "col3": "10-เม.ย.-60",
        "col4": "3001947",
        "col5": "ไทยเมทอล",
        "col6": "CASH",
        "col7": "1,417,202.16",
        "col8": "456",
        "col9": "25-เม.ย.-60",
        "col10": "SD21070002",
        "col11": "25-เม.ย.-60"
    },
    {
        "id": 3,
        "col1": "25-เม.ย.-60",
        "col2": "RV17040004",
        "col3": "4-เม.ย.-60",
        "col4": "3001947",
        "col5": "ไทยเมทอล",
        "col6": "TRAN",
        "col7": "3,696,311.66",
        "col8": "111",
        "col9": "25-เม.ย.-60",
        "col10": "SD21070004",
        "col11": "25-เม.ย.-60"
    }
];

function atable() {
    let tableZero = '#data-table';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", render: function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "col1", render: function (data, type, row) { return data; } },
            { "data": "col2", render: function (data, type, row) { return data; } },
            { "data": "col3", render: function (data, type, row) { return data; } },
            { "data": "col4", render: function (data, type, row) { return data; } },
            { "data": "col5", render: function (data, type, row) { return data; } },
            { "data": "col6", render: function (data, type, row) { return data; } },
            { "data": "col7", render: function (data, type, row) { return data; } },
            { "data": "col8", render: function (data, type, row) { return data; } },
            { "data": "col9", render: function (data, type, row) { return data; } },
            { "data": "col10", render: function (data, type, row) { return data; } },
            { "data": "col11", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class Index extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#checkbox-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#checkbox-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>ลูกค้า</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>issue date</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>เลขที่ใบรับชำระ</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>ที่อยู่</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>วันที่ Due</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>วิธีการชำระเงิน</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="NONE">None</option><option value="CASH">CASH</option><option value="CHEQUE">CHEQUE</option><option value="TRAN">TRANSFER</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Col sm={2}>
                                                <Form.Label>BANK</Form.Label>
                                            </Col>
                                            <Col sm={3}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                            <Col sm={7}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Col sm={2}>
                                                <Form.Label>BRANCH</Form.Label>
                                            </Col>
                                            <Col sm={10}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>เลขที่เช็ค</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>วันที่เช็ค</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>เลขที่ใบนำส่ง</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>เลขที่ SAP</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-file" renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Row>
                                    <Col className="email-card">
                                        <Button id="btnDel" variant="default" className="btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple"><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="btn-page text-right" sm>
                                        <Button size="sm" variant="success"> เพิ่มรายการ </Button>
                                        <Button size="sm" variant="success"> บันทึก </Button>
                                    </Col>
                                </Row>
                                <br />
                                <Table ref="tbl" striped hover responsive bordered id="data-table">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="checkbox-select-all" /></th>
                                            <th>#</th>
                                            <th>ประเภท</th>
                                            <th>DOC.DATE</th>
                                            <th>DUE.DATE</th>
                                            <th>ASSIGN</th>
                                            <th>DOC NO</th>
                                            <th>TERM</th>
                                            <th>หมายเหตุ.-</th>
                                            <th>DLC NO</th>
                                            <th>SAP Date</th>
                                            <th>จำนวนเงิน</th>
                                        </tr>
                                    </thead>
                                </Table>
                                <Form.Group as={Row}>
                                    <Col sm={4}> </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={6}>
                                                <Form.Label>จำนวนเงินรวม(บาท)</Form.Label>
                                            </Col>
                                            <Col sm={6}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={8}>
                                        <Form.Group as={Row}>
                                            <Col sm={2}>
                                                <Form.Label>รายละเอียดเพิ่มเติม</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <textarea rows="3" cols="95" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>CREATED DATE</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>BY</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>UPDATED DATE</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Label>BY</Form.Label>
                                            </Col>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่ใบรับชำระ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เลขที่ SAP</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>KEY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="CASH">CASH</option><option value="CHEQUE">CHEQUE</option><option value="TRAN">TRAN</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CHEQUE#</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="000005">เสริมสิริ</option><option value="3001948">บริษัท อุดมโลหะกิจ (1975) จำกัด</option><option value="3001945">บริษัท ศิริกุลสตีล จำกัด</option><option value="3001951">บริษัท ธนสารเซ็นทรัลสตีล จำกัด</option><option value="3007314">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="3007503">บริษัท ค้าเหล็กไทย จำกัด (มหาชน)</option><option value="000007">บ. โลหะไพศาลวานิช จก.</option><option value="000006">บ. ซี.เอส.เอช.สตีล จก.</option><option value="000004">ชุมเสริม</option><option value="T000025">ZTCO INTERNATIONAL</option><option value="T000011">VULCAN STEEL</option><option value="3005353">V.S.V. ASIA CO.,LTD.</option><option value="T000032">TRIDENT STEEL</option><option value="T000031">TRANSCAPE STEELS (PTY) LTD</option><option value="3005317">TRADEARBED PTE. LTD.</option><option value="T000035">TOTAL EXPRESS LIMITED</option><option value="T000017">SURDEX STEEL PTY LIMITED</option><option value="3005316">SUMITOMO CORPORATION (SINGAPORE)</option><option value="3005260">SUMITOMO CORPORATION</option><option value="3005242">STEMCOR UK LIMITED</option><option value="T000018">STEMCOR (S.E.A.) PTE. LTD.</option><option value="T000012">STEELFORCE TRADING PTY.LTD.</option><option value="3005320">STARPOLY CORPORATION</option><option value="T000016">SOUTHERN STEEL GROUP PTY LIMITED</option><option value="T000022">SIMS STEEL</option><option value="T000007">SIAM CEMENT MYANMAR TRADING</option><option value="T000008">SIAM CEMENT CAMBODIA TRADING</option><option value="3005336">SHIMIZU - MARUBENI CONSORTIUM</option><option value="T000026">SEMET CO., LTD</option><option value="3005330">SCT-VIETNAM</option><option value="T000021">RAFFRAY BROTHERS CO LTD</option><option value="T000034">PLAN B</option><option value="T000003">PFC ENGINEERING SDN.BHD.</option><option value="T000001">ORIENT MATERIAL</option><option value="T000024">ORERORT (PTY) LTD</option><option value="3005312">NOMURA TRADING CO.,LTD.</option><option value="3005259">NICHIMEN CORPORATION</option><option value="3005311">NEWCO RESOURCES PTE. LTD.</option><option value="T000029">NATIONAL STEEL GROUP</option><option value="3005247">MITSUI AND CO (HONGKONG) LTD.</option><option value="3005310">MITSUI &amp; CO.,LTD.SINGAPORE BRANCH</option><option value="3002017">MITSUI &amp; CO.,LTD.</option><option value="3005309">MITSUI &amp; CO.,LTD.</option><option value="T000037">MITSUI &amp; CO. (ASIA PACIFIC) PTE. LTD.</option><option value="3005248">METAL ONE HONG KONG LIMITED</option><option value="T000027">MELSTEEL (PTY) LTD</option><option value="3005246">MARUBENI-ITOCHU STEEL PTE  LTD.</option><option value="3005307">MARUBENI SINGAPORE PTE. LTD.</option><option value="T000030">MACSTEEL TRADING</option><option value="T000006">LION METAL (THAILAND) CO.,LTD.</option><option value="T000020">LAM PO TANG AND CO LTD</option><option value="3005303">KAWASHO - SINGAPORE</option><option value="T000013">HORAN STEEL LIMITED</option><option value="T000002">HANWA (MALAYSIA) SDN. BHD.</option><option value="T000036">GLOBAL ALLIANZ(UK) LTD</option><option value="3005294">GAYATHRI STEELS</option><option value="T000010">G.A.M. STEEL PTY.LTD.</option><option value="T000033">ENSYS (PVT) LTD</option><option value="3005243">CORUS TRADING LIMITED</option><option value="T000015">COIL STEELS</option><option value="3005232">CMC (AUSTRALIA) PTY. LIMITED</option><option value="T000004">Chumserm</option><option value="T000014">CENTRAL STEEL TRADING PTY LTD</option><option value="3005264">CEMENTHAI TRADING (M) SDN.BHD.</option><option value="T000009">BLUESCOPE DISTRIBUTION</option><option value="T000023">ASIA NAVIGATION PTE LTD</option><option value="T000019">ARABIAN INTERNATIONAL COMPANY</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="0">รอเลขที่ SAP</option><option value="1">รอออกใบนำส่ง</option><option value="2">ออกใบนำส่งแล้ว</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value="a.CreditDt">วันที่ due</option><option value="a.DocDt">Issue date</option><option value="a.SAPDt">วันที่ส่ง SAP</option><option value="a.CreateDt">วันที่สร้าง</option><option value="a.UpdateDt">วันที่แก้ไข</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="AMOUNT">จำนวนเงิน</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="ข้อมูลใบรับชำระ OCW">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>
                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" onClick={e => this.setShowModal(e, "Create")}>สร้างใบส่ง</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="checkbox-select-all" /></th>
                                        <th>#</th>
                                        <th>วันที่เอกสาร</th>
                                        <th>เลขที่เอกสาร</th>
                                        <th>DUE DATE</th>
                                        <th>CUST.CODE</th>
                                        <th>ชื่อลูกค้า - ซื้อสำหรับ</th>
                                        <th>PAY TYPE</th>
                                        <th>เป็นจำนวนเงิน</th>
                                        <th>SAP#</th>
                                        <th>SAP Date</th>
                                        <th>เลขที่นำส่ง</th>
                                        <th>วันที่นำส่ง</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default Index;
