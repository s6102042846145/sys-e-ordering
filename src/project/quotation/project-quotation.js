import React from 'react';
import {
    Row, Col, Form, Button, Table, Modal, InputGroup
    , FormControl
    , Collapse
    , Tabs
    , Tab
} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        isBasic: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ isModal: true });
            this.setState({ setTitleModal: "CUSTOMER" })
        } else if (type === "Add") {
            this.setState({ isModalAdd: true });
            this.setState({ setTitleModal: "เพิ่มข้อมูล" });
            this.setState({ isModal: false });
        } else if (type === "User") {
            this.setState({ isModalUser: true });
            this.setState({ setTitleModalUser: "User" });
        } else if (type === "SUPPLIER") {
            this.setState({ isModalSupplier: true });
            this.setState({ setTitleModalSupplier: "PROJECT SUPPLIER" });
        } else if (type === "Attention") {
            this.setState({ isModalAttention: true });
            this.setState({ setTitleModalAttention: "ATTENTION" });
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {
        const { isBasic, isMultiTarget, accordionKey } = this.state;
        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal backdrop="static" size="xl" show={this.state.isModalAttention} onHide={() => this.setState({ isModalAttention: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAttention}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ATTENTION</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>CODE</th>
                                            <th>CUSTOMER</th>
                                            <th>ATTENTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className="cursor" onClick={() => this.setState({ isModalAttention: false })}>
                                            <td align="center">1</td>
                                            <td align="center">3001574 </td>
                                            <td align="center">บ. ช.การช่าง จก. (มหาชน) </td>
                                            <td align="left">Sales Representative</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAttention: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal backdrop="static" size="xl" show={this.state.isModalSupplier} onHide={() => this.setState({ isModalSupplier: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalSupplier}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Department</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="department" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SAP ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Manager ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>หน่วยงาน</th>
                                            <th>TYPE</th>
                                            <th>COUNTRY</th>
                                            <th>CODE</th>
                                            <th>PROJECT SUPPLIER'S NAME</th>
                                            <th>SYSTEM.CODE</th>
                                            <th>UPDATE DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colSpan="8"></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalSupplier: false })}>Close</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal id="modelUser" backdrop="static" size="xl" show={this.state.isModalUser} onHide={() => this.setState({ isModalUser: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalUser}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Department</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="department" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SAP ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Manager ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>DEPARTMENT</th>
                                            <th>USER ID</th>
                                            <th>USER'S NAME</th>
                                            <th>MANAGER ID</th>
                                            <th>EMAIL</th>
                                            <th>TEL</th>
                                            <th>FAX</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className="cursor" onClick={() => this.setState({ isModalUser: false })}>
                                            <td >1</td>
                                            <td >Marketing&nbsp;</td>
                                            <td >2007&nbsp;</td>
                                            <td align="left">&nbsp;Rattipun T</td>
                                            <td >0912&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalUser: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal backdrop="static" size="xl" show={this.state.isModalAdd} onHide={() => this.setState({ isModalAdd: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เลขที่</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" placeholder="เลขที่ใบเสนอราคา" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Sales Person</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="2007-Rattipun T" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "User")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Status</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" defaultValue="เปิดเอกสาร" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ลูกค้า</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" defaultValue="3001574 ช.การช่าง TH" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="-">ไม่ระบุ</option>
                                                    <option value="N">Project</option>
                                                    <option value="Y">Re-Export</option>
                                                    <option value="S">STOCK</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่</Form.Label>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ชื่อโครงการ</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ประเภทโครงการ</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="NONE">None</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> วิธีการชำระเงิน</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="NONE">None</option><option value="NT60">เงินเชื่อ 60 วัน</option><option value="NT00">เงินสด</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เจ้าของ</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8}>
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>เสนอราคาพิเศษ</Form.Label>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ผู้รับเหมา</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ปี-เดือน</Form.Label>
                                            <Col sm={3}>
                                                <DropdownEdit type="years" />
                                            </Col>
                                            <Col sm={5}><DropdownEdit type="month" /></Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เงื่อนไขการขนส่ง</Form.Label>
                                            <Col sm={6}>
                                                <select className="form-control-edit"><option value="NONE">None</option><option value="CFR">Costs and freight</option><option value="EXW">Ex works</option></select>
                                            </Col>
                                            <Col sm={2} className="text-right">
                                                <Button className="btn-sm mb-sm-1" onClick={() => this.setState({ isBasic: !isBasic })}>...</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Collapse in={this.state.isBasic}>
                                    <Row>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">attention</Form.Label>
                                                <Col sm={8}>
                                                    <InputGroup>
                                                        <FormControl size="sm" className="form-control-edit" defaultValue="Sales Representative" disabled />
                                                        <InputGroup.Append>
                                                            <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "Attention")}>
                                                                <div className="mt--4px">
                                                                    <i className="feather icon-search"></i>
                                                                </div>
                                                            </Button>
                                                        </InputGroup.Append>
                                                    </InputGroup>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> TEL no.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> FAX no.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ซื้อสำหรับ</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">สถานที่ส่งสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="3000028">กรุงเทพฯ คลองเตย</option><option value="3000000">กรุงเทพฯ คลองสาน</option><option value="3000038">กรุงเทพฯ คลองสามวา</option><option value="3000039">กรุงเทพฯ คันนายาว</option><option value="3000027">กรุงเทพฯ จตุจักร</option><option value="3000032">กรุงเทพฯ จอมทอง</option><option value="3000025">กรุงเทพฯ ดอนเมือง</option><option value="3000037">กรุงเทพฯ ดินแดง</option><option value="3000001">กรุงเทพฯ ดุสิต</option><option value="3000002">กรุงเทพฯ ตลิ่งชัน</option><option value="3000040">กรุงเทพฯ ทวีวัฒนา</option><option value="3000041">กรุงเทพฯ ทุ่งครุ</option><option value="3000003">กรุงเทพฯ ธนบุรี</option><option value="3000008">กรุงเทพฯ บางเขน</option><option value="3000042">กรุงเทพฯ บางแค</option><option value="3000005">กรุงเทพฯ บางกอกใหญ่</option><option value="3000004">กรุงเทพฯ บางกอกน้อย</option><option value="3000006">กรุงเทพฯ บางกะปิ</option><option value="3000007">กรุงเทพฯ บางขุนเทียน</option><option value="3000035">กรุงเทพฯ บางคอแหลม</option><option value="3000024">กรุงเทพฯ บางซื่อ</option><option value="3000043">กรุงเทพฯ บางนา</option><option value="3000044">กรุงเทพฯ บางบอน</option><option value="3000030">กรุงเทพฯ บางพลัด</option><option value="3000009">กรุงเทพฯ บางรัก</option><option value="3000031">กรุงเทพฯ บึงกุ่ม</option><option value="3000010">กรุงเทพฯ ปทุมวัน</option><option value="3000026">กรุงเทพฯ ประเวศ</option><option value="3000011">กรุงเทพฯ ป้อมปราบศัตรูพ่าย</option><option value="3000012">กรุงเทพฯ พญาไท</option><option value="3000013">กรุงเทพฯ พระโขนง</option><option value="3000014">กรุงเทพฯ พระนคร</option><option value="3000015">กรุงเทพฯ ภาษีเจริญ</option><option value="3000016">กรุงเทพฯ มีนบุรี</option><option value="3000017">กรุงเทพฯ ยานนาวา</option><option value="3000033">กรุงเทพฯ ราชเทวี</option><option value="3000018">กรุงเทพฯ ราษฎร์บูรณะ</option><option value="3000019">กรุงเทพฯ ลาดกระบัง</option><option value="3000029">กรุงเทพฯ ลาดพร้าว</option><option value="3000045">กรุงเทพฯ วังทองหลาง</option><option value="3000046">กรุงเทพฯ วัฒนา</option><option value="3000036">กรุงเทพฯ สวนหลวง</option><option value="3000047">กรุงเทพฯ สะพานสูง</option><option value="3000020">กรุงเทพฯ สัมพันธวงศ์</option><option value="3000034">กรุงเทพฯ สาทร</option><option value="3000048">กรุงเทพฯ สายไหม</option><option value="3000021">กรุงเทพฯ หนองแขม</option><option value="3000022">กรุงเทพฯ หนองจอก</option><option value="3000049">กรุงเทพฯ หลักสี่</option><option value="3000023">กรุงเทพฯ ห้วยขวาง</option><option value="3000168">อยุธยา บางปะอิน</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> สถานที่รับสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">ไม่ระบุ</option><option value="4951">BDC - บ้านบึง</option><option value="4941">SOL - SYS Solution</option><option value="4921">SR - ศรีราชา</option><option value="4911">SYS1 - ระยอง1</option><option value="4931">SYS2 - ระยอง2</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">SalesOrg</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Chartering</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NON">None</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Currency <span className="text-c-red">*</span> </Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="AUD">AUD - Australian Dollars</option><option value="EUR">EUR - European Currency Unit</option><option value="MYR">MYR - Malaysian Ringgit</option><option value="THB">THB - Thai Baht</option><option value="USD">USD - US Dollars</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Channel</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit" disabled><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ประเทศปลายทาง</Form.Label>
                                                <Col sm={8}>
                                                    <DropdownEdit type="country" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">F/X Rate</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <input type="text" className="form-control-edit mr-1" placeholder="(THB)" />
                                                        <input type="text" className="form-control-edit" placeholder="(USD)" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Validity From</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <input type="Date" className="form-control-edit mr-1" /> :
                                                        <input type="Date" className="form-control-edit ml-1" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">PO.Ref.</Form.Label>
                                                <Col sm={9}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">วันที่ต้องการรับสินค้า	</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                        <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                        <DropdownEdit type="month" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                <Col sm={4}>
                                                    <input type="Date" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ถึง	</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                        <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                        <DropdownEdit type="month" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                <Col sm={4}>
                                                    <input type="Date" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Reason of Rev.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">Reason of Cancel</Form.Label>
                                                <Col sm={4}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                                <Col sm={5} className="mt--4px text-right">
                                                    <Button size="sm" variant="danger" className="mr-2">SET CANCEL</Button>
                                                    <Button size="sm" variant="danger">SET CLOSE</Button>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                    </Row>


                                </Collapse>
                                <Row className="mt-1">
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">KEY IN UNIT</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">SALES UNIT</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8} className="mt--4px text-right">
                                                <Button size="sm" variant="primary" className="mr-2">1.TO INVOICE</Button>
                                                <Button size="sm" variant="primary">2.TO ORDER</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>



                                <br /><br />
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right col-sm-7" sm>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-100">SAVE</Button>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-150">SHOW SAVE AS</Button>
                                        <Button size="sm" variant="primary" className="mr-sm-1 wid-100">SAVE REV</Button>
                                        <Button size="sm" variant="primary" className="mr-sm-1 wid-100">MORE...</Button>
                                        <Button size="sm" variant="warning">PRINT QUOTA.</Button>
                                    </Col>
                                </Form.Group>

                                <Col sm={12}>
                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" defaultActiveKey="size" className="form-control-file">
                                            <Tab eventKey="size" title="SIZE">
                                                <Row className="mt-2">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">CASH DISC.</Form.Label>
                                                            <Col className="col-sm-8" sm>
                                                                <div className="form-check-inline">
                                                                    <input type="text" className="form-control-edit" />
                                                                    <Button size="sm" variant="primary" className="mr-sm-1 wid-100">
                                                                        <span className="f-12 mt--4px">CAL.</span>
                                                                    </Button>
                                                                    <Button size="sm" variant="primary" className="wid-100 ">
                                                                        <span className="f-12 mt--4px">START</span>
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={6}>

                                                    </Col>
                                                </Row>
                                                <Tabs variant="pills" defaultActiveKey="ORDERVIEW" className="form-group float-sm-right">
                                                    <Tab eventKey="ORDERVIEW" title="ORDER VIEW">
                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>SIZE</th>
                                                                    <th>Min Ton</th>
                                                                    <th>ปริมาณ ท่อน</th>
                                                                    <th>ราคาเชื่อ บาท</th>
                                                                    <th>/หน่วย</th>
                                                                    <th>Rebate บาท</th>
                                                                    <th>ราคา บาท</th>
                                                                    <th>เป็นจำนวนเงิน บาท</th>
                                                                    <th>กำหนดส่ง โดยประมาณ</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="12" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="NOMINALVIEW" title="NOMINAL VIEW">
                                                        <br /><br />
                                                        <Row className="mt-2">
                                                            <Col sm={5}></Col>
                                                            <Col sm={7}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={5}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-5">PRODUCT GRP</Form.Label>
                                                                            <Col sm={7}>
                                                                                <select className="form-control-edit"><option value="NONE">None</option><option value="PJ19080002">B-ASTM A6/A6M:2003</option><option value="PJ19100001">B-BS EN 10034:1993</option><option value="PJ19080001">B-TIS 1227:2558 (2015)</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={5}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-3">GRADE.</Form.Label>
                                                                            <Col sm={9}>
                                                                                <select className="form-control-edit"><option value="NONE">None</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">test</option><option value="ๅๅ">ๅๅ</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={2}>
                                                                        <Button className="f-12 w-100" size="sm" variant="success">เพิ่ม</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Row>

                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>SIZE.STD.</th>
                                                                    <th>PRODUCT GRP.</th>
                                                                    <th>GRADE</th>
                                                                    <th>EX.PRICE</th>
                                                                    <th>PRICE</th>
                                                                    <th>SEQ</th>
                                                                    <th>UPDATED DATE</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="9" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="PLANVIEW" title="PLAN VIEW">
                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>PRODUCT SIZE</th>
                                                                    <th>SIZE.STD/GRADE STD.</th>
                                                                    <th>ปริมาณ ตัน</th>
                                                                    <th>ปริมาณ ท่อน</th>
                                                                    <th>PLAN</th>
                                                                    <th>วันที่ต้องการ</th>
                                                                    <th>กำหนดส่ง โดยประมาณ</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="11" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="PSIVIEW" title="PSI VIEW">
                                                        <br /><br />
                                                        <Row className="mt-2">
                                                            <Col sm={12}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={7}>
                                                                        <div className="form-check-inline">
                                                                            <Button className="f-12 mr-1" size="sm" variant="danger">CLEAR ออกจาก GROUP ทุกรายการ</Button>
                                                                            <Button className="f-12" size="sm" variant="success">ขออนุมัติต่ออายุ POSSIBILITY</Button>

                                                                        </div>

                                                                    </Col>
                                                                    <Col sm={3}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-3">OPTION</Form.Label>
                                                                            <Col sm={9}>
                                                                                <select className="form-control-edit"><option value="SIZE">BY SIZE</option><option value="PN">BY PRODUCT</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={2}>
                                                                        <Button className="f-12 w-100" size="sm" variant="success">เข้า GROUP ทุกรายการ</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Row>

                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>รายละเอียดสินค้า</th>
                                                                    <th>SIZE STD. GRADE STD.</th>
                                                                    <th>ท่อน[ตัน]</th>
                                                                    <th>คงเหลือ ท่อน[ตัน]</th>
                                                                    <th>PSI GROUP</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="6" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                </Tabs>

                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="REBATE" title="REBATE" >
                                                <Row className="mt-1">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-5">ระบุชื่อ REBATE ที่ต้องการสร้างใหม่</Form.Label>
                                                            <Col sm={7}>
                                                                <input type="text" className="form-control-edit" />

                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="mt--4px text-right">
                                                                <Button size="sm" variant="success" className="mr-1">1.NEW หักหน้าตั๋ว</Button>
                                                                <Button size="sm" variant="success">2.NEW หักภายหลัง</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered>
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>CODE</th>
                                                            <th>REBATE'S NAME</th>
                                                            <th>RATE</th>
                                                            <th>AMOUNT</th>
                                                            <th>TYPE</th>
                                                            <th>CHANNEL</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="8" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="QUOTATION" title="QUOTATION" >
                                                <Row className="mt-2">
                                                    <Col sm={3}></Col>
                                                    <Col sm={9}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-8">สร้าง QUOTATION จาก REV</Form.Label>
                                                                    <Col sm={4}>
                                                                        <select className="form-control-edit"><option value="">0</option></select>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-5">ย้อนดู REV เดิม</Form.Label>
                                                                    <Col sm={7}>
                                                                        <select className="form-control-edit"><option value="NONE">None</option></select>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-4">QUOTA.NO</Form.Label>
                                                                    <Col sm={8}>
                                                                        <input type="text" className="form-control-edit" disabled defaultValue="QUD2100014-0" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="text-right">
                                                                <Button className="mr-1 f-12 wid-70" size="sm" variant="primary">OPEN</Button>
                                                                <Button className="mr-1 f-12 wid-70" size="sm" variant="success">SAVE</Button>
                                                                <Button className="f-12 wid-70" size="sm" variant="danger">REMOVE</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={9} >
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <div className="form-check-inline w-100">
                                                                    <select className="form-control-edit mr-1">
                                                                        <option value="C2">2COL</option><option value="C1">CASH</option><option value="CCREDIT">CREDIT</option><option value="TERM">TERM</option><option value="NQ">NO QTY.2COL</option><option value="NQ_C1">NO QTY.CASH</option><option value="NQ_CC">NO QTY.CREDIT</option>
                                                                    </select>
                                                                    <select className="form-control-edit">
                                                                        <option value="QUD_TH">QUD_TH</option>
                                                                        <option value="QUD_TH">QUD_TH_REBATE</option>
                                                                        <option value="QUD_TH">QUD_TH_SOL</option>
                                                                        <option value="QUD_EN">QUD_EN</option>
                                                                        <option value="QUD_EN">QUD_EN_REBATE</option>
                                                                        <option value="QUD_EN">QUD_EN_SOL</option>
                                                                    </select>
                                                                </div>
                                                            </Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Button className="mr-1 f-12 wid-130" size="sm" variant="success">1.DEFAULT QUOTA.</Button>
                                                                    <Button className="mr-1 f-12 wid-145" size="sm" variant="success">2.COPY QUOTATION</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="success">3.เพิ่ม HEADER</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="success">4.เพิ่ม FOOTER</Button>
                                                                </Form.Group>
                                                                <Form.Group as={Row}>
                                                                    <Button className="mr-1 f-12 wid-130" size="sm" variant="primary">1.PRINT QUOTA.</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="primary">2.PRINT QUOTA. ASTM</Button>
                                                                    <Button className="f-12" size="sm" variant="primary">3.PRINT QUOTA. NOMINAL</Button>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={3}></Col>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>HEADER</th>
                                                            <th>SEQ	</th>
                                                            <th>CAPTION</th>
                                                            <th>HIDE</th>
                                                            <th>UPDATE DATE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="7" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="ATTACHFILES" title="ATTACH FILES" >
                                                <hr className="mt-2" />
                                                <center>no attach files</center>
                                                <hr />
                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-upload" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="SPACIALRQ" title="SPACIAL RQ." >
                                                <Row className="mt-2">
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="success" size="sm">SAVE SPACIAL RQ.</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Special Quality/grade</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Dimension/length</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Testing </Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Certificate</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Export document</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Container</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Packing </Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Pre load inspection</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Overstock</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Other</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>

                                                </Row>

                                            </Tab>
                                            <Tab eventKey="ORDERINVOICE" title="ORDER & INVOICE" >
                                                <Row className="mt-2">
                                                    <Table ref="tbl" striped hover responsive bordered >
                                                        <thead>
                                                            <tr>
                                                                <th><Form.Check id="tb-group" /></th>
                                                                <th>#</th>
                                                                <th>COUNTRY</th>
                                                                <th>STATUS</th>
                                                                <th>YR-MTH</th>
                                                                <th>ORDER DATE</th>
                                                                <th>ORDER NO.</th>
                                                                <th>SAP#</th>
                                                                <th>CUSTOMER</th>
                                                                <th>TON</th>
                                                                <th>AMOUNT</th>
                                                                <th>CURR</th>
                                                                <th>CREATE DATE</th>
                                                                <th>REF.QUOTA</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colSpan="14" className="text-center">
                                                                    <label > No Data.</label>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </Table>

                                                </Row>

                                            </Tab>





                                        </Tabs>
                                    </Form.Group>
                                </Col>
                                <Form.Group as={Row}>
                                    <Col sm={12}>
                                        <Row className="mt-1">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณตันรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0.000 ตัน</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Freight</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">จำนวนเงินรวม(THB)</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณท่อนรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0 ท่อน</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Insurance</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">VAT 7.00%</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.00</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">Total BDL</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0 Bdls. 	 </label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Commission</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">จำนวนเงินรวมสุทธิ(THB)</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.00</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">หมายเหตุ.-</Form.Label>
                                                    <Col sm={8}>
                                                        <textarea rows="5" cols="95" name="comment" form="usrform"></textarea>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่สร้าง</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">14-Jul--21 10:25</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">0704</label>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่แก้ไข</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">14-Jul--21 10:25</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">0704</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="success" className="w-100">ทำเอกสารเสร็จ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 1</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 1</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 2</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 2</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ อนุมัติระดับ ส่วน</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดอนุมัติ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ลูกค้ายืนยัน</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>

                                                </Form.Group>
                                            </Col>

                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Tabs variant="pills" defaultActiveKey="USER" className="form-control-file">
                                                        <Tab eventKey="USER" title="SYS USER">

                                                        </Tab>
                                                        <Tab eventKey="CUSTOMER" title="CUSTOMER">
                                                            <Table striped hover responsive bordered id="example2">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>E-MAIL ADDRESS</th>
                                                                        <th>NAME</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colSpan="3" className="text-center">No Data.</td>

                                                                    </tr>
                                                                </tbody>
                                                            </Table>


                                                        </Tab>
                                                    </Tabs>
                                                </Form.Group>
                                            </Col>


                                        </Row>


                                    </Col>
                                </Form.Group>



                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd: false })}>Close</Button>
                                <Button variant="primary" title="send email"><i className="feather icon-navigation"></i></Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SALES ORG.CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CHANNEL CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>COUNTRY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CUST.CODE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>

                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GROUP</th>
                                                <th>COUNTRY</th>
                                                <th>CODE</th>
                                                <th>NAME</th>
                                                <th>ABBRV.NAME</th>
                                                <th>NAME (ENG.)</th>
                                                <th>ABBRV.NAME (ENG.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="cursor" onClick={e => this.setShowModal(e, "Add")}>
                                                <td >1</td>
                                                <td >0180_10&nbsp;</td>
                                                <td >TH
                                                    &nbsp;-&nbsp;Thailand
                                                    &nbsp;</td>
                                                <td >3001574&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Quota.Year</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="years" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Quota.Month</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="month" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>LVL.อนุมัติ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="ALL">ทั้งหมด</option><option value="ผจผ">ผจผ.</option><option value="ผจส">ผจส.</option><option value="ผจฝ">ผจฝ.</option><option value="กจก">กจก.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="1WK">จะหมดอายุใน1 อาทิตย์</option><option value="6">ลูกค้ายืนยัน</option><option value="ACT">Acitve</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Quota.NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Quota.REV.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE45">BE45 - ตั๋วแลกเงิน 45 วัน (BE)</option><option value="BE60">BE60 - ตั๋วแลกเงิน 60 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="BED0">BED0 - ตั๋วแลกเงิน 150 วัน (BE)</option><option value="BEF0">BEF0 - ตั๋วแลกเงิน 180 วัน (BE)</option><option value="BEH0">BEH0 - ตั๋วแลกเงิน 210 วัน (BE)</option><option value="BEJ0">BEJ0 - ตั๋วแลกเงิน 240 วัน (BE)</option><option value="BEL0">BEL0 - ตั๋วแลกเงิน 270 วัน (BE)</option><option value="BEN0">BEN0 - ตั๋วแลกเงิน 300 วัน (BE)</option><option value="BER0">BER0 - ตั๋วแลกเงิน 360 วัน (BE)</option><option value="BS30">BS30 - ตั๋วแลกเงิน 30 วัน (BE) before shipment.</option><option value="BS60">BS60 - ตั๋วแลกเงิน 60 วัน (BE) before shipment.</option><option value="BS90">BS90 - ตั๋วแลกเงิน 90 วัน (BE) before shipment.</option><option value="BSA0">BSA0 - ตั๋วแลกเงิน 105 วัน (BE) before shipment.</option><option value="BSB0">BSB0 - ตั๋วแลกเงิน 120 วัน (BE) before shipment.</option><option value="BSD0">BSD0 - ตั๋วแลกเงิน 150 วัน (BE) before shipment.</option><option value="BSF0">BSF0 - ตั๋วแลกเงิน 180 วัน (BE) before shipment.</option><option value="CA15">CA15 - DLC 15 days</option><option value="CD00">CD00 - DLC</option><option value="CD07">CD07 - DLC 7 days</option><option value="CD15">CD15 - DLC 15 days</option><option value="CD30">CD30 - DLC 30 days</option><option value="CD60">CD60 - DLC 60 days</option><option value="CD90">CD90 - DLC 90 days</option><option value="CDB0">CDB0 - DLC 120 days</option><option value="CH07">CH07 - DLC</option><option value="CH15">CH15 - DLC</option><option value="CH30">CH30 - DLC</option><option value="CH60">CH60 - DLC</option><option value="CH90">CH90 - DLC</option><option value="CT00">CT00 - DLC</option><option value="CT03">CT03 - DLC</option><option value="CV03">CV03 - DLC</option><option value="DB07">DB07 - D/A 7 days after receiced bill.</option><option value="DB30">DB30 - D/A 30 days after receiced bill.</option><option value="DF00">DF00 - Draft.</option><option value="DL15">DL15 - D/A 15 days after B/L date.</option><option value="DL30">DL30 - D/A 30 days after B/L date.</option><option value="DL45">DL45 - D/A 45 days after B/L date.</option><option value="DL60">DL60 - D/A 60 days after B/L date.</option><option value="DL75">DL75 - D/A 75 days after B/L date.</option><option value="DL90">DL90 - D/A 90 days after B/L date.</option><option value="DLB0">DLB0 - D/A 120 days after B/L date.</option><option value="DLF0">DLF0 - D/A 180 days after B/L date.</option><option value="DND0">DND0 - D/A 150 days from B/L date</option><option value="DP00">DP00 - D/P at sight</option><option value="DP07">DP07 - D/P 7 days after B/L date</option><option value="DP15">DP15 - D/P at sight</option><option value="DP30">DP30 - D/P 30 days after B/L date</option><option value="DP45">DP45 - D/P 45 days after B/L date</option><option value="DV07">DV07 - D/A 7 days after invoice,B/L,AWB date.</option><option value="DV14">DV14 - D/A 14 days after invoice,B/L,AWB date.</option><option value="DV30">DV30 - D/A 30 days after invoice,B/L,AWB date.</option><option value="FI15">FI15 - within 15th of next month</option><option value="FI30">FI30 - within 30th of next month</option><option value="HB03">HB03 - HSBC-Dealer Financing (Invoice Financing)</option><option value="KD07">KD07 - Credit 7 days.</option><option value="LC00">LC00 - L/C at sight.</option><option value="LC15">LC15 - L/C 15 days.</option><option value="N120">N120 - Within 120 days  after received bill</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM02">NM02 - เงินสด 2 วัน</option><option value="NM03">NM03 - เงินสด 3 วัน</option><option value="NM05">NM05 - เงินสด 5 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NM50">NM50 - เงินสด 50 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - BG 1 วัน</option><option value="NT02">NT02 - BG 2 วัน</option><option value="NT04">NT04 - BG 4 วัน</option><option value="NT05">NT05 - BG 5 วัน</option><option value="NT07">NT07 - BG 7 วัน</option><option value="NT10">NT10 - BG 10 วัน</option><option value="NT15">NT15 - BG 15 วัน</option><option value="NT21">NT21 - BG 21 วัน</option><option value="NT30">NT30 - BG 30 วัน</option><option value="NT45">NT45 - BG 45 วัน</option><option value="NT50">NT50 - BG 50 วัน</option><option value="NT60">NT60 - BG 60 วัน</option><option value="NT70">NT70 - BG 70 วัน</option><option value="NT75">NT75 - BG 75 วัน</option><option value="NT90">NT90 - BG 90 วัน</option><option value="NTB0">NTB0 - BG 120 วัน</option><option value="NTH0">NTH0 - BG 210 วัน</option><option value="NTJ0">NTJ0 - BG 240 วัน</option><option value="NTL0">NTL0 - BG 270 วัน</option><option value="NTN0">NTN0 - BG 300 วัน</option><option value="NTP0">NTP0 - BG 330 วัน</option><option value="NTR0">NTR0 - BG 360 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL30">TL30 - T/T 30 days after B/L date.</option><option value="TL35">TL35 - T/T 35 days after B/L date.</option><option value="TL45">TL45 - T/T 45 days after B/L date.</option><option value="TL60">TL60 - T/T 60 days after B/L date.</option><option value="TL75">TL75 - T/T 75 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TLB0">TLB0 - T/T 120 days after B/L date.</option><option value="TLD0">TLD0 - T/T 150 days after B/L date</option><option value="TLH0">TLH0 - T/T 210 days after B/L date</option><option value="TLI0">TLI0 - T/T 180 days after B/L date.</option><option value="TQ30">TQ30 - T/T 30 days after received goods</option><option value="TR60">TR60 - T/T remittance 60 days after shipment</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS07">TS07 - T/T before shipment 7 day.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option><option value="ZT00">ZT00 - L/C at sight 98%,T/T 2% after settlement</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="CFR">CFR - Costs and freight</option><option value="CIF">CIF - Costs,Insurance &amp; freight</option><option value="CIP">CIP - Carriage &amp; Insurance paid to</option><option value="CPT">CPT - Carriage paid to</option><option value="DAP">DAP - Delivered at Place</option><option value="DDP">DDP - Delivery Duty Paid</option><option value="DDU">DDU - Delivery Duty Unpaid</option><option value="EXW">EXW - Ex works</option><option value="FAS">FAS - Free along ship</option><option value="FCA">FCA - Free carrier</option><option value="FOB">FOB - Free on board</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ChannelCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อโครงการ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Project Type</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>TYPE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="N">Project</option><option value="Y">Re-Export</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="L">ANGLE</option><option value="B">BLOOM</option><option value="C">CHANNEL</option><option value="T">CUT-BEAM</option><option value="V">HVA</option><option value="I">I-BEAM</option><option value="M">MODULAR</option><option value="R">RAIL</option><option value="S">SHEET-PILE</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">Test</option><option value="ๅๅ">ๅๅ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Status</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALLA">All Active</option><option value="ALL">ทั้งหมด</option><option value="1">เปิดเอกสาร</option><option value="2">รอตรวจสอบ 1</option><option value="3">รอตรวจสอบ 2</option><option value="4">รออนุมัติ</option><option value="5">อนุมัติ</option><option value="6">ลูกค้ายืนยัน</option><option value="7">SYS ดำเนิการ</option><option value="8">ปิดเอกสาร</option><option value="9">ยกเลิกเอกสาร</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="PROJECT QUOTATION">
                            <Form.Group as={Row} className="mb-sm-3">
                                <Col sm={7}></Col>
                                <Col sm={5} className="btn-page text-right">
                                    <Button size="sm" variant="success" onClick={e => this.setShowModal(e, "Create")}>สร้าง QUOTATION ใหม่</Button>
                                    <Button size="sm" variant="success" className="wid-150" onClick={e => this.setShowModal(e, "Create")}>สร้าง INQUIRY</Button>
                                    <Button size="sm" variant="success" className="wid-150" onClick={e => this.setShowModal(e, "Create")}>สร้างลูกค้าทั่วไป</Button>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                <Col sm={3} className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>
                                <Col sm={9} >
                                    <Row>
                                        <Col sm={5} >
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-5"> เหตุผลการ ยกเลิก/ปิด</Form.Label>
                                                <Col sm={7}>
                                                        <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={7} className="btn-page text-right">
                                            <Button size="sm" variant="danger" className="wid-150">ปรับสถานะ CANCEL</Button>
                                            <Button size="sm" variant="danger" className="wid-150">ปรับสถานะ CLOSE</Button>
                                            <Button size="sm" variant="success" className="wid-150">ปรับสถานะ ACTIVE</Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Form.Group>
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className='' id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>QUOTA.NO</th>
                                        <th>ชื่อลูกค้า</th>
                                        <th>REBATE</th>
                                        <th>ผู้รับเหมา</th>
                                        <th>ชื่อโครงการ</th>
                                        <th>PO REF.</th>
                                        <th>ปริมาณ(ตัน)</th>
                                        <th>OC(ตัน)</th>
                                        <th>วันที่สร้าง</th>
                                        <th>LVL.อนุมัติ</th>
                                        <th>สถานะ</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row >
            </Aux >
        );
    }
}

export default SizeMaster;
