import React from 'react';
import {
    Row, Col, Form, Button, Table, Modal, InputGroup
    , FormControl
    , Collapse
    , Tabs
    , Tab
} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ช.การช่าง",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51",
        "status": "รออนุมัติ",
        "orderNo": "OCP2100050-0-0",
        "Number": "CA20210001",
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "status", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "orderNo", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "Number", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        isBasic: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ isModal: true });
            this.setState({ setTitleModal: "CUSTOMER" })
        } else if (type === "Add") {
            this.setState({ isModalAdd: true });
            this.setState({ setTitleModal: "เพิ่มข้อมูล" });
            this.setState({ isModal: false });
        } else if (type === "User") {
            this.setState({ isModalUser: true });
            this.setState({ setTitleModalUser: "User" });
        } else if (type === "SUPPLIER") {
            this.setState({ isModalSupplier: true });
            this.setState({ setTitleModalSupplier: "PROJECT SUPPLIER" });
        } else if (type === "Attention") {
            this.setState({ isModalAttention: true });
            this.setState({ setTitleModalAttention: "ATTENTION" });
        } else {

            this.setState({ isModalEdit: true });
            this.setState({ setTitleModalEdit: "แก้ไขข้อมูล" })
        }
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {
        const { isBasic, isMultiTarget, accordionKey } = this.state;
        $('#example-select-all').click(function (event) {
            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal backdrop="static" size="xl" show={this.state.isModalEdit} onHide={() => this.setState({ isModalEdit: false })}>

                            <Modal.Body className="f-12">
                                <Tabs variant="pills" defaultActiveKey="Order" className="form-control-file">
                                    <Tab eventKey="Order" title="PROJECT ORDER CONFIRM">
                                        <Row className=" mt-4">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">เลขที่</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-edit" placeholder="เลขที่ใบเสนอราคา" disabled />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">Sales Person</Form.Label>
                                                    <Col sm={8}>
                                                        <InputGroup>
                                                            <FormControl size="sm" className="form-control-edit" defaultValue="2007-Rattipun T" disabled />
                                                            <InputGroup.Append>
                                                                <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "User")}>
                                                                    <div className="mt--4px">
                                                                        <i className="feather icon-search"></i>
                                                                    </div>
                                                                </Button>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </Col>
                                                </Form.Group>
                                            </Col>

                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">Status</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-edit" defaultValue="เปิดเอกสาร" disabled />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-edit" defaultValue="3001574 ช.การช่าง TH" disabled />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">Type</Form.Label>
                                                    <Col sm={8}>
                                                        <select className="form-control-edit">
                                                            <option value="-">ไม่ระบุ</option>
                                                            <option value="N">Project</option>
                                                            <option value="Y">Re-Export</option>
                                                            <option value="S">STOCK</option>
                                                        </select>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="Date" className="form-control-edit" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ชื่อโครงการ</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-edit" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ประเภทโครงการ</Form.Label>
                                                    <Col sm={8}>
                                                        <select className="form-control-edit"><option value="NONE">None</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option></select>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4"> วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <select className="form-control-edit">
                                                            <option value="NONE">None</option><option value="NT60">เงินเชื่อ 60 วัน</option><option value="NT00">เงินสด</option>
                                                        </select>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">เจ้าของ</Form.Label>
                                                    <Col sm={8}>
                                                        <InputGroup>
                                                            <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                            <InputGroup.Append>
                                                                <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                                    <div className="mt--4px">
                                                                        <i className="feather icon-search"></i>
                                                                    </div>
                                                                </Button>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="form-control-edit" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4"></Form.Label>
                                                    <Col sm={8}>
                                                        <input type="checkbox" className="mr-1" />
                                                        <Form.Label>เสนอราคาพิเศษ</Form.Label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ผู้รับเหมา</Form.Label>
                                                    <Col sm={8}>
                                                        <InputGroup>
                                                            <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                            <InputGroup.Append>
                                                                <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                                    <div className="mt--4px">
                                                                        <i className="feather icon-search"></i>
                                                                    </div>
                                                                </Button>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปี-เดือน</Form.Label>
                                                    <Col sm={3}>
                                                        <DropdownEdit type="years" />
                                                    </Col>
                                                    <Col sm={5}><DropdownEdit type="month" /></Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">เงื่อนไขการขนส่ง</Form.Label>
                                                    <Col sm={6}>
                                                        <select className="form-control-edit"><option value="NONE">None</option><option value="CFR">Costs and freight</option><option value="EXW">Ex works</option></select>
                                                    </Col>
                                                    <Col sm={2} className="text-right">
                                                        <Button className="btn-sm mb-sm-1" onClick={() => this.setState({ isBasic: !isBasic })}>...</Button>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Collapse in={this.state.isBasic}>
                                            <Row>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">attention</Form.Label>
                                                        <Col sm={8}>
                                                            <InputGroup>
                                                                <FormControl size="sm" className="form-control-edit" defaultValue="Sales Representative" disabled />
                                                                <InputGroup.Append>
                                                                    <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "Attention")}>
                                                                        <div className="mt--4px">
                                                                            <i className="feather icon-search"></i>
                                                                        </div>
                                                                    </Button>
                                                                </InputGroup.Append>
                                                            </InputGroup>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4"> TEL no.</Form.Label>
                                                        <Col sm={8}>
                                                            <input type="text" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4"> FAX no.</Form.Label>
                                                        <Col sm={8}>
                                                            <input type="text" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">ซื้อสำหรับ</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">สถานที่ส่งสินค้า</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"><option value="NONE">None</option><option value="3000028">กรุงเทพฯ คลองเตย</option><option value="3000000">กรุงเทพฯ คลองสาน</option><option value="3000038">กรุงเทพฯ คลองสามวา</option><option value="3000039">กรุงเทพฯ คันนายาว</option><option value="3000027">กรุงเทพฯ จตุจักร</option><option value="3000032">กรุงเทพฯ จอมทอง</option><option value="3000025">กรุงเทพฯ ดอนเมือง</option><option value="3000037">กรุงเทพฯ ดินแดง</option><option value="3000001">กรุงเทพฯ ดุสิต</option><option value="3000002">กรุงเทพฯ ตลิ่งชัน</option><option value="3000040">กรุงเทพฯ ทวีวัฒนา</option><option value="3000041">กรุงเทพฯ ทุ่งครุ</option><option value="3000003">กรุงเทพฯ ธนบุรี</option><option value="3000008">กรุงเทพฯ บางเขน</option><option value="3000042">กรุงเทพฯ บางแค</option><option value="3000005">กรุงเทพฯ บางกอกใหญ่</option><option value="3000004">กรุงเทพฯ บางกอกน้อย</option><option value="3000006">กรุงเทพฯ บางกะปิ</option><option value="3000007">กรุงเทพฯ บางขุนเทียน</option><option value="3000035">กรุงเทพฯ บางคอแหลม</option><option value="3000024">กรุงเทพฯ บางซื่อ</option><option value="3000043">กรุงเทพฯ บางนา</option><option value="3000044">กรุงเทพฯ บางบอน</option><option value="3000030">กรุงเทพฯ บางพลัด</option><option value="3000009">กรุงเทพฯ บางรัก</option><option value="3000031">กรุงเทพฯ บึงกุ่ม</option><option value="3000010">กรุงเทพฯ ปทุมวัน</option><option value="3000026">กรุงเทพฯ ประเวศ</option><option value="3000011">กรุงเทพฯ ป้อมปราบศัตรูพ่าย</option><option value="3000012">กรุงเทพฯ พญาไท</option><option value="3000013">กรุงเทพฯ พระโขนง</option><option value="3000014">กรุงเทพฯ พระนคร</option><option value="3000015">กรุงเทพฯ ภาษีเจริญ</option><option value="3000016">กรุงเทพฯ มีนบุรี</option><option value="3000017">กรุงเทพฯ ยานนาวา</option><option value="3000033">กรุงเทพฯ ราชเทวี</option><option value="3000018">กรุงเทพฯ ราษฎร์บูรณะ</option><option value="3000019">กรุงเทพฯ ลาดกระบัง</option><option value="3000029">กรุงเทพฯ ลาดพร้าว</option><option value="3000045">กรุงเทพฯ วังทองหลาง</option><option value="3000046">กรุงเทพฯ วัฒนา</option><option value="3000036">กรุงเทพฯ สวนหลวง</option><option value="3000047">กรุงเทพฯ สะพานสูง</option><option value="3000020">กรุงเทพฯ สัมพันธวงศ์</option><option value="3000034">กรุงเทพฯ สาทร</option><option value="3000048">กรุงเทพฯ สายไหม</option><option value="3000021">กรุงเทพฯ หนองแขม</option><option value="3000022">กรุงเทพฯ หนองจอก</option><option value="3000049">กรุงเทพฯ หลักสี่</option><option value="3000023">กรุงเทพฯ ห้วยขวาง</option><option value="3000168">อยุธยา บางปะอิน</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4"> สถานที่รับสินค้า</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"><option value="NONE">ไม่ระบุ</option><option value="4951">BDC - บ้านบึง</option><option value="4941">SOL - SYS Solution</option><option value="4921">SR - ศรีราชา</option><option value="4911">SYS1 - ระยอง1</option><option value="4931">SYS2 - ระยอง2</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">SalesOrg</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"><option value="NONE">None</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">Chartering</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"><option value="NON">None</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">Currency <span className="text-c-red">*</span> </Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit"><option value="NONE">None</option><option value="AUD">AUD - Australian Dollars</option><option value="EUR">EUR - European Currency Unit</option><option value="MYR">MYR - Malaysian Ringgit</option><option value="THB">THB - Thai Baht</option><option value="USD">USD - US Dollars</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>

                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">Channel</Form.Label>
                                                        <Col sm={8}>
                                                            <select className="form-control-edit" disabled><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option></select>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">ประเทศปลายทาง</Form.Label>
                                                        <Col sm={8}>
                                                            <DropdownEdit type="country" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">F/X Rate</Form.Label>
                                                        <Col sm={8}>
                                                            <div className="form-check-inline w-100">
                                                                <input type="text" className="form-control-edit mr-1" placeholder="(THB)" />
                                                                <input type="text" className="form-control-edit" placeholder="(USD)" />
                                                            </div>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>

                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">Validity From</Form.Label>
                                                        <Col sm={8}>
                                                            <div className="form-check-inline w-100">
                                                                <input type="Date" className="form-control-edit mr-1" /> :
                                                                <input type="Date" className="form-control-edit ml-1" />
                                                            </div>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-3">PO.Ref.</Form.Label>
                                                        <Col sm={9}>
                                                            <input type="text" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>

                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">วันที่ต้องการรับสินค้า	</Form.Label>
                                                        <Col sm={8}>
                                                            <div className="form-check-inline w-100">
                                                                <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                                <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                                <DropdownEdit type="month" />
                                                            </div>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                        <Col sm={4}>
                                                            <input type="Date" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">ถึง	</Form.Label>
                                                        <Col sm={8}>
                                                            <div className="form-check-inline w-100">
                                                                <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                                <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                                <DropdownEdit type="month" />
                                                            </div>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                        <Col sm={4}>
                                                            <input type="Date" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>

                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-4">Reason of Rev.</Form.Label>
                                                        <Col sm={8}>
                                                            <input type="text" className="form-control-edit" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={6}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label className="col-sm-3">Reason of Cancel</Form.Label>
                                                        <Col sm={4}>
                                                            <input type="text" className="form-control-edit" />
                                                        </Col>
                                                        <Col sm={5} className="mt--4px text-right">
                                                            <Button size="sm" variant="danger" className="mr-2">SET CANCEL</Button>
                                                            <Button size="sm" variant="danger">SET CLOSE</Button>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>

                                            </Row>


                                        </Collapse>
                                        <Row className="mt-1">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">KEY IN UNIT</Form.Label>
                                                    <Col sm={8}>
                                                        <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">SALES UNIT</Form.Label>
                                                    <Col sm={8}>
                                                        <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4"></Form.Label>
                                                    <Col sm={8} className="mt--4px text-right">
                                                        <Button size="sm" variant="primary" className="mr-2">1.TO INVOICE</Button>
                                                        <Button size="sm" variant="primary">2.TO ORDER</Button>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>



                                        <br /><br />
                                        <Form.Group as={Row}>
                                            <Col className="email-card">
                                                <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                            </Col>
                                            <Col className="text-right col-sm-7" sm>
                                                <Button size="sm" variant="success" className="mr-sm-1 wid-100">SAVE</Button>
                                                <Button size="sm" variant="success" className="mr-sm-1 wid-150">SHOW SAVE AS</Button>
                                                <Button size="sm" variant="primary" className="mr-sm-1 wid-100">SAVE REV</Button>
                                                <Button size="sm" variant="primary" className="mr-sm-1 wid-100">MORE...</Button>
                                                <Button size="sm" variant="warning">PRINT QUOTA.</Button>
                                            </Col>
                                        </Form.Group>

                                        <Col sm={12}>
                                            <Form.Group as={Row}>
                                                <Tabs variant="pills" defaultActiveKey="size" className="form-control-file">
                                                    <Tab eventKey="size" title="SIZE">
                                                        <Row className="mt-2">
                                                            <Col sm={6} >
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">CASH DISC.</Form.Label>
                                                                    <Col className="col-sm-8" sm>
                                                                        <div className="form-check-inline">
                                                                            <input type="text" className="form-control-edit" />
                                                                            <Button size="sm" variant="primary" className="mr-sm-1 wid-100">
                                                                                <span className="f-12 mt--4px">CAL.</span>
                                                                            </Button>
                                                                            <Button size="sm" variant="primary" className="wid-100 ">
                                                                                <span className="f-12 mt--4px">START</span>
                                                                            </Button>
                                                                        </div>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={6}>

                                                            </Col>
                                                        </Row>
                                                        <Tabs variant="pills" defaultActiveKey="ORDERVIEW" className="form-group float-sm-right">
                                                            <Tab eventKey="ORDERVIEW" title="ORDER VIEW">
                                                                <Table ref="tbl" striped hover responsive bordered >
                                                                    <thead>
                                                                        <tr>
                                                                            <th><Form.Check id="tb-group" /></th>
                                                                            <th>#</th>
                                                                            <th>SIZE</th>
                                                                            <th>Min Ton</th>
                                                                            <th>ปริมาณ ท่อน</th>
                                                                            <th>ราคาเชื่อ บาท</th>
                                                                            <th>/หน่วย</th>
                                                                            <th>Rebate บาท</th>
                                                                            <th>ราคา บาท</th>
                                                                            <th>เป็นจำนวนเงิน บาท</th>
                                                                            <th>กำหนดส่ง โดยประมาณ</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colSpan="12" className="text-center">
                                                                                <label > No Data.</label>
                                                                            </td>

                                                                        </tr>
                                                                    </tbody>
                                                                </Table>
                                                            </Tab>
                                                            <Tab eventKey="NOMINALVIEW" title="NOMINAL VIEW">
                                                                <br /><br />
                                                                <Row className="mt-2">
                                                                    <Col sm={5}></Col>
                                                                    <Col sm={7}>
                                                                        <Form.Group as={Row}>
                                                                            <Col sm={5}>
                                                                                <Form.Group as={Row}>
                                                                                    <Form.Label className="col-sm-5">PRODUCT GRP</Form.Label>
                                                                                    <Col sm={7}>
                                                                                        <select className="form-control-edit"><option value="NONE">None</option><option value="PJ19080002">B-ASTM A6/A6M:2003</option><option value="PJ19100001">B-BS EN 10034:1993</option><option value="PJ19080001">B-TIS 1227:2558 (2015)</option></select>
                                                                                    </Col>
                                                                                </Form.Group>
                                                                            </Col>
                                                                            <Col sm={5}>
                                                                                <Form.Group as={Row}>
                                                                                    <Form.Label className="col-sm-3">GRADE.</Form.Label>
                                                                                    <Col sm={9}>
                                                                                        <select className="form-control-edit"><option value="NONE">None</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">test</option><option value="ๅๅ">ๅๅ</option></select>
                                                                                    </Col>
                                                                                </Form.Group>
                                                                            </Col>
                                                                            <Col sm={2}>
                                                                                <Button className="f-12 w-100" size="sm" variant="success">เพิ่ม</Button>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Row>

                                                                <Table ref="tbl" striped hover responsive bordered >
                                                                    <thead>
                                                                        <tr>
                                                                            <th><Form.Check id="tb-group" /></th>
                                                                            <th>#</th>
                                                                            <th>SIZE.STD.</th>
                                                                            <th>PRODUCT GRP.</th>
                                                                            <th>GRADE</th>
                                                                            <th>EX.PRICE</th>
                                                                            <th>PRICE</th>
                                                                            <th>SEQ</th>
                                                                            <th>UPDATED DATE</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colSpan="9" className="text-center">
                                                                                <label > No Data.</label>
                                                                            </td>

                                                                        </tr>
                                                                    </tbody>
                                                                </Table>
                                                            </Tab>
                                                            <Tab eventKey="PLANVIEW" title="PLAN VIEW">
                                                                <Table ref="tbl" striped hover responsive bordered >
                                                                    <thead>
                                                                        <tr>
                                                                            <th><Form.Check id="tb-group" /></th>
                                                                            <th>#</th>
                                                                            <th>PRODUCT SIZE</th>
                                                                            <th>SIZE.STD/GRADE STD.</th>
                                                                            <th>ปริมาณ ตัน</th>
                                                                            <th>ปริมาณ ท่อน</th>
                                                                            <th>PLAN</th>
                                                                            <th>วันที่ต้องการ</th>
                                                                            <th>กำหนดส่ง โดยประมาณ</th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colSpan="11" className="text-center">
                                                                                <label > No Data.</label>
                                                                            </td>

                                                                        </tr>
                                                                    </tbody>
                                                                </Table>
                                                            </Tab>
                                                            <Tab eventKey="PSIVIEW" title="PSI VIEW">
                                                                <br /><br />
                                                                <Row className="mt-2">
                                                                    <Col sm={12}>
                                                                        <Form.Group as={Row}>
                                                                            <Col sm={7}>
                                                                                <div className="form-check-inline">
                                                                                    <Button className="f-12 mr-1" size="sm" variant="danger">CLEAR ออกจาก GROUP ทุกรายการ</Button>
                                                                                    <Button className="f-12" size="sm" variant="success">ขออนุมัติต่ออายุ POSSIBILITY</Button>

                                                                                </div>

                                                                            </Col>
                                                                            <Col sm={3}>
                                                                                <Form.Group as={Row}>
                                                                                    <Form.Label className="col-sm-3">OPTION</Form.Label>
                                                                                    <Col sm={9}>
                                                                                        <select className="form-control-edit"><option value="SIZE">BY SIZE</option><option value="PN">BY PRODUCT</option></select>
                                                                                    </Col>
                                                                                </Form.Group>
                                                                            </Col>
                                                                            <Col sm={2}>
                                                                                <Button className="f-12 w-100" size="sm" variant="success">เข้า GROUP ทุกรายการ</Button>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Row>

                                                                <Table ref="tbl" striped hover responsive bordered >
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>รายละเอียดสินค้า</th>
                                                                            <th>SIZE STD. GRADE STD.</th>
                                                                            <th>ท่อน[ตัน]</th>
                                                                            <th>คงเหลือ ท่อน[ตัน]</th>
                                                                            <th>PSI GROUP</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colSpan="6" className="text-center">
                                                                                <label > No Data.</label>
                                                                            </td>

                                                                        </tr>
                                                                    </tbody>
                                                                </Table>
                                                            </Tab>
                                                        </Tabs>

                                                        <div className="text-right">
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                        </div>
                                                    </Tab>
                                                    <Tab eventKey="REBATE" title="REBATE" >
                                                        <Row className="mt-1">
                                                            <Col sm={6} >
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-5">ระบุชื่อ REBATE ที่ต้องการสร้างใหม่</Form.Label>
                                                                    <Col sm={7}>
                                                                        <input type="text" className="form-control-edit" />

                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={6}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={12} className="mt--4px text-right">
                                                                        <Button size="sm" variant="success" className="mr-1">1.NEW หักหน้าตั๋ว</Button>
                                                                        <Button size="sm" variant="success">2.NEW หักภายหลัง</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Row>
                                                        <Table ref="tbl" striped hover responsive bordered>
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>CODE</th>
                                                                    <th>REBATE'S NAME</th>
                                                                    <th>RATE</th>
                                                                    <th>AMOUNT</th>
                                                                    <th>TYPE</th>
                                                                    <th>CHANNEL</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="8" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                        <div className="text-right">
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                        </div>
                                                    </Tab>
                                                    <Tab eventKey="QUOTATION" title="QUOTATION" >
                                                        <Row className="mt-2">
                                                            <Col sm={3}></Col>
                                                            <Col sm={9}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={4}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-8">สร้าง QUOTATION จาก REV</Form.Label>
                                                                            <Col sm={4}>
                                                                                <select className="form-control-edit"><option value="">0</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={4}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-5">ย้อนดู REV เดิม</Form.Label>
                                                                            <Col sm={7}>
                                                                                <select className="form-control-edit"><option value="NONE">None</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={4}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-4">QUOTA.NO</Form.Label>
                                                                            <Col sm={8}>
                                                                                <input type="text" className="form-control-edit" disabled defaultValue="QUD2100014-0" />
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={12}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={12} className="text-right">
                                                                        <Button className="mr-1 f-12 wid-70" size="sm" variant="primary">OPEN</Button>
                                                                        <Button className="mr-1 f-12 wid-70" size="sm" variant="success">SAVE</Button>
                                                                        <Button className="f-12 wid-70" size="sm" variant="danger">REMOVE</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={9} >
                                                                <Form.Group as={Row}>
                                                                    <Col sm={4}>
                                                                        <div className="form-check-inline w-100">
                                                                            <select className="form-control-edit mr-1">
                                                                                <option value="C2">2COL</option><option value="C1">CASH</option><option value="CCREDIT">CREDIT</option><option value="TERM">TERM</option><option value="NQ">NO QTY.2COL</option><option value="NQ_C1">NO QTY.CASH</option><option value="NQ_CC">NO QTY.CREDIT</option>
                                                                            </select>
                                                                            <select className="form-control-edit">
                                                                                <option value="QUD_TH">QUD_TH</option>
                                                                                <option value="QUD_TH">QUD_TH_REBATE</option>
                                                                                <option value="QUD_TH">QUD_TH_SOL</option>
                                                                                <option value="QUD_EN">QUD_EN</option>
                                                                                <option value="QUD_EN">QUD_EN_REBATE</option>
                                                                                <option value="QUD_EN">QUD_EN_SOL</option>
                                                                            </select>
                                                                        </div>
                                                                    </Col>
                                                                    <Col sm={8}>
                                                                        <Form.Group as={Row}>
                                                                            <Button className="mr-1 f-12 wid-130" size="sm" variant="success">1.DEFAULT QUOTA.</Button>
                                                                            <Button className="mr-1 f-12 wid-145" size="sm" variant="success">2.COPY QUOTATION</Button>
                                                                            <Button className="mr-1 f-12" size="sm" variant="success">3.เพิ่ม HEADER</Button>
                                                                            <Button className="mr-1 f-12" size="sm" variant="success">4.เพิ่ม FOOTER</Button>
                                                                        </Form.Group>
                                                                        <Form.Group as={Row}>
                                                                            <Button className="mr-1 f-12 wid-130" size="sm" variant="primary">1.PRINT QUOTA.</Button>
                                                                            <Button className="mr-1 f-12" size="sm" variant="primary">2.PRINT QUOTA. ASTM</Button>
                                                                            <Button className="f-12" size="sm" variant="primary">3.PRINT QUOTA. NOMINAL</Button>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={3}></Col>
                                                        </Row>
                                                        <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>HEADER</th>
                                                                    <th>SEQ	</th>
                                                                    <th>CAPTION</th>
                                                                    <th>HIDE</th>
                                                                    <th>UPDATE DATE</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="7" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="ATTACHFILES" title="ATTACH FILES" >
                                                        <hr className="mt-2" />
                                                        <center>no attach files</center>
                                                        <hr />
                                                        <div className="text-right">
                                                            <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-upload" /></Button>
                                                        </div>
                                                    </Tab>
                                                    <Tab eventKey="SPACIALRQ" title="SPACIAL RQ." >
                                                        <Row className="mt-2">
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3"></Form.Label>
                                                                    <Col sm={9} className="text-right">
                                                                        <Button variant="success" size="sm">SAVE SPACIAL RQ.</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Special Quality/grade</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Dimension/length</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Testing </Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Certificate</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Export document</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Container</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3"></Form.Label>
                                                                    <Col sm={9} className="text-right">
                                                                        <Button variant="primary" size="sm">SET</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Packing </Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3"></Form.Label>
                                                                    <Col sm={9} className="text-right">
                                                                        <Button variant="primary" size="sm">SET</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Pre load inspection</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3"></Form.Label>
                                                                    <Col sm={9} className="text-right">
                                                                        <Button variant="primary" size="sm">SET</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Overstock</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3"></Form.Label>
                                                                    <Col sm={9} className="text-right">
                                                                        <Button variant="primary" size="sm">SET</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={2}></Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-3">Other</Form.Label>
                                                                    <Col sm={9}>
                                                                        <textarea rows="3" className="w-100" ></textarea>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={2}></Col>

                                                        </Row>
                                                    </Tab>
                                                </Tabs>
                                            </Form.Group>
                                        </Col>
                                        <Form.Group as={Row}>
                                            <Col sm={12}>
                                                <Row className="mt-1">
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">ปริมาณตันรวม</Form.Label>
                                                            <Col sm={8}>
                                                                <label> 0.000 ตัน</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-5">Total Freight</Form.Label>
                                                            <Col sm={7}>
                                                                <label> 0.000</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-6">จำนวนเงินรวม(THB)</Form.Label>
                                                            <Col sm={6}>
                                                                <label> 0.000</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">ปริมาณท่อนรวม</Form.Label>
                                                            <Col sm={8}>
                                                                <label> 0 ท่อน</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-5">Total Insurance</Form.Label>
                                                            <Col sm={7}>
                                                                <label> 0.000</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-6">VAT 7.00%</Form.Label>
                                                            <Col sm={6}>
                                                                <label> 0.00</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">Total BDL</Form.Label>
                                                            <Col sm={8}>
                                                                <label> 0 Bdls. 	 </label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-5">Total Commission</Form.Label>
                                                            <Col sm={7}>
                                                                <label> 0.000</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-6">จำนวนเงินรวมสุทธิ(THB)</Form.Label>
                                                            <Col sm={6}>
                                                                <label> 0.00</label>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={4}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">หมายเหตุ.-</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="5" cols="95" name="comment" form="usrform"></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่สร้าง</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">14-Jul--21 10:25</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={1}>
                                                                <label className="text-center">0704</label>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่แก้ไข</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">14-Jul--21 10:25</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={1}>
                                                                <label className="text-center">0704</label>
                                                            </Col>
                                                            <Col sm={3}>
                                                                <Button size="sm" variant="success" className="w-100">ทำเอกสารเสร็จ</Button>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 1</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={1}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Col sm={3}>
                                                                <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 1</Button>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 2</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={1}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Col sm={3}>
                                                                <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 2</Button>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่ อนุมัติระดับ ส่วน</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={1}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Col sm={3}>
                                                                <Button size="sm" variant="primary" className="w-100">เปิดอนุมัติ</Button>
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-4">วันที่ลูกค้ายืนยัน</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">-</label>
                                                            </Col>
                                                            <Form.Label className="col-sm-1">โดย</Form.Label>
                                                            <Col sm={3}>
                                                                <label className="text-center">-</label>
                                                            </Col>

                                                        </Form.Group>
                                                    </Col>

                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Tabs variant="pills" defaultActiveKey="USER" className="form-control-file">
                                                                <Tab eventKey="USER" title="SYS USER">

                                                                </Tab>
                                                                <Tab eventKey="CUSTOMER" title="CUSTOMER">
                                                                    <Table striped hover responsive bordered id="example2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>E-MAIL ADDRESS</th>
                                                                                <th>NAME</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td colSpan="3" className="text-center">No Data.</td>

                                                                            </tr>
                                                                        </tbody>
                                                                    </Table>


                                                                </Tab>
                                                            </Tabs>
                                                        </Form.Group>
                                                    </Col>


                                                </Row>


                                            </Col>
                                        </Form.Group>



                                    </Tab>
                                    <Tab eventKey="OrderConfirmed" title="ชื่อลูกค้า">
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={4} className="text-center">
                                                <Form.Label>Order Confirmation/ใบยืนยันการสั่งซื้อ</Form.Label>

                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-2">No.</Form.Label>
                                                    <Col sm={4}>
                                                        <input type="text" className="text-center w-100" disabled defaultValue="OCP2100050" />
                                                    </Col>
                                                    <Form.Label className="col-sm-2">Lot</Form.Label>
                                                    <Col sm={4}>
                                                        <div className="form-check-inline">
                                                            <input type="text" className="text-center wid-30 mr-1" disabled defaultValue="0" />

                                                            <Form.Label >Rev</Form.Label>
                                                            <input type="text" className="text-center wid-35 ml-1" disabled defaultValue="0" />
                                                        </div>
                                                    </Col>


                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={4}></Col>
                                            <Col sm={4} className="text-center">
                                                <Form.Label >ประจำเดือน กรกฏาคม พ.ศ. 2564</Form.Label>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-2">วันที่</Form.Label>
                                                    <Col sm={4}>
                                                        <input type="text" className="text-center w-100" disabled defaultValue="20-07-21" />
                                                    </Col>
                                                    <Form.Label className="col-sm-2">สถานะ </Form.Label>
                                                    <Col sm={4}>
                                                        <input type="text" className="text-center w-100" disabled defaultValue="รออนุมัติ " />
                                                    </Col>

                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">เลขที่ใบสั่งซื้อล่วงหน้า/สถานะ</Form.Label>
                                                    <Col sm={7}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <input type="text" className="text-center ml-1 mr-1 w-100" disabled />

                                                            <Form.Label >Rev</Form.Label>
                                                            <input type="text" className="text-center wid-30  ml-1 mr-1" disabled defaultValue="0" />

                                                            <Form.Label >/</Form.Label>
                                                            <input type="text" className="text-center ml-1 w-100" disabled />
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ชื่อลูกค้า</Form.Label>
                                                    <Col sm={7}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">บ. ช.การช่าง จก. (มหาชน)</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ที่อยู่</Form.Label>
                                                    <Col sm={7}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">587 ถนนสุทธิสารวินิจฉัย แขวงดินแดง เขตดินแดง จ.กรุงเทพฯ</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ชื่อโครงการ</Form.Label>
                                                    <Col sm={7}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1"></span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                            </Col>

                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1"></span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">เงินสด THB</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">เงื่อนไขการขนส่ง</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">Costs and freight</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">สถานที่รับสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">BDC</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">สถานที่ส่งสินค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1">TH  ชลบุรี ศรีราชา  / ไม่ระบุ</span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ประเภทของส่วนเพิ่มที่ได้</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1"></span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ประเภทของส่วนลดที่ได้</Form.Label>
                                                    <Col sm={8}>
                                                        <div className="form-check-inline">
                                                            :
                                                            <span className="ml-1"></span>
                                                        </div>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>


                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณตันรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="text-right" disabled defaultValue="0.000 ตัน" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณท่อนรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className="text-right" disabled defaultValue="0 ท่อน" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">จำนวนเงินรวม(บาท)</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className="text-right" disabled defaultValue="0.00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ค่าขนส่งรวม (บาท)</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className="text-right" disabled defaultValue="0.00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">VAT 7.00%</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className="text-right" disabled defaultValue="0.00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}></Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">จำนวนเงินสุทธิ(บาท)</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className="text-right" disabled defaultValue="0.00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={12}>
                                                <p>หมายเหตุ.- บริษัทฯขอสงวนสิทธิในการพิจารณาความเสียหายที่เกิดจากการไม่รับสินค้าโดยจะคิดค่าปรับเป็นจำนวน 20% ของมูลค่าสินค้าที่รับไม่ครบ หรือพิจารณาสถานภาพการเป็นผู้แทนจำหน่าย </p>
                                            </Col>
                                        </Form.Group>
                                    </Tab>
                                    <Tab eventKey="report" title="ชื่อโครงการ">
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">OCW.NO</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="OCP2100050_0_0" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">CUST</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="3001574" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">CUST</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="ช.การช่าง" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ORDER STATUS</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className=" w-100" disabled defaultValue="1" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">SAP#</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">TERM</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="NT00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">CASH DISC</Form.Label>
                                                    <Col sm={8}>
                                                        <input type="text" className=" w-100" disabled defaultValue="0.00" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">ADV.SET</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className=" w-100" disabled defaultValue="NONE" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={9}></Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">REF.</Form.Label>
                                                    <Col sm={7}>
                                                        <input type="text" className=" w-100" disabled defaultValue="NONE" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-3">NEW ADV.SET</Form.Label>
                                                    <Col sm={9}>
                                                        <select className="form-control-edit">
                                                            <option value="AV19110001">AV19110001-2019-11 โปรแกรมพิเศษC200X80,C200X90 SYS</option>
                                                            <option value="AV19110002">AV19110002-2019-08 โปรแกรมพิเศษC200X80, SCG</option>
                                                            <option value="AV19110003">AV19110003-2019-11 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.1)</option>
                                                            <option value="AV19110004">AV19110004-2019-11 SMALL SECTION  C 100X50  TMT (16.7)</option>
                                                            <option value="AV19110005">AV19110005-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV19110006">AV19110006-2019-11 โปรแกรมพิเศษC200X90,C200X80 SCG</option>
                                                            <option value="AV19110007">AV19110007-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV19110008">AV19110008-2019-11 โปรแกรมพิเศษC200X80 , C200X90  MITSUI</option>
                                                            <option value="AV19110009">AV19110009-2019-11 โปรแกรมพิเศษC200X80 , C200X90  SUMI</option>
                                                            <option value="AV19110010">AV19110010-2019-11 SMALL SECTION C100X50, C150X75 อุดม</option>
                                                            <option value="AV19110011">AV19110011-2019-11 SMALL SECTION L 100X100 อุดม</option>
                                                            <option value="AV19110012">AV19110012-2019-11 SMALL SECTION   C 125X65,L100X100 .เบสท์สตีล</option>
                                                            <option value="AV19110013">AV19110013-2019-11 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV อุดมโลหะกิจ</option>
                                                            <option value="AV19110014">AV19110014-2019-11 GROUP Q เชื้อไพบูลย์</option>
                                                            <option value="AV19110015">AV19110015-2019-11 GROUP Q PLUS SM520 - เต็กเฮง</option>
                                                            <option value="AV19110016">AV19110016-2019-11 SMALL SECTION C100X50, C150X75 อุดม</option>
                                                            <option value="AV19110017">AV19110017-2019-11 SMALL SECTION C100X50, C150X75 อุดม</option>
                                                            <option value="AV19110018">AV19110018-2019-11 SMALL SECTION  C 150X75, C 125X65.  อุดมโลหะกิจ</option>
                                                            <option value="AV19110019">AV19110019-2019-11 SMALL SECTION  C 150X75, C 125X65.  ยงเจริญชัย 2007</option>
                                                            <option value="AV19110020">AV19110020-2019-11 SMALL SECTION   C 125X65,L100X100 .เบสท์สตีล</option>
                                                            <option value="AV19110021">AV19110021-2019-11 GROUP Q SUMI SM520 (SP,LENGTH 200)</option>
                                                            <option value="AV19110022">AV19110022-2019-11 GROUP Q SUMI SM520 (GRADE SM520  1000)</option>
                                                            <option value="AV19110023">AV19110023-2019-11 SMALL SECTION  C 150X75, C 125X65.  เชื้อไพบูลย์</option>
                                                            <option value="AV19110024">AV19110024-2019-11 SMALL SECTION  C 150X75, C 125X65.SS540 โลหะเจริญ</option>
                                                            <option value="AV19110025">AV19110025-2019-11 DL1911-ซื้อชดเชย งาน ONE BANGKOK - ศิริกุล (REBATE 2900)</option>
                                                            <option value="AV19110026">AV19110026-2019-12 GROUP Q</option>
                                                            <option value="AV19110027">AV19110027-2019-12 GROUP Q PLUS</option>
                                                            <option value="AV19110028">AV19110028-2019-11 SMALL SECTION L 100X100X13 ซี เอส เอช</option>
                                                            <option value="AV19110029">AV19110029-2019-11 GROUP Q  MITSUI</option>
                                                            <option value="AV19110030">AV19110030-2019-12 GROUP Q  MITSUI</option>
                                                            <option value="AV19110031">AV19110031-2019-12 GROUP Q  SUMI</option>
                                                            <option value="AV19110032">AV19110032-2019-12 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV19110033">AV19110033-2019-12 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV19110034">AV19110034-2019-12 GROUP Q VALUE SERIES</option>
                                                            <option value="AV19110035">AV19110035-2019-12 GROUP Q SCG</option>
                                                            <option value="AV19110036">AV19110036-2019-12 GROUP Q PLUS SCG</option>
                                                            <option value="AV19110037">AV19110037-2019-12 GROUP Q SM520</option>
                                                            <option value="AV19110038">AV19110038-2019-12 GROUP Q PLUS SM520</option>
                                                            <option value="AV19110039">AV19110039-2019-12 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV19110040">AV19110040-2019-12 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV19110041">AV19110041-2019-12 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV19110042">AV19110042-2019-12 GROUP Q  SUMI SM520</option>
                                                            <option value="AV19110043">AV19110043-2019-12 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV19110044">AV19110044-2019-12 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV19110045">AV19110045-2019-12 GROUP Q SM520 SCG</option>
                                                            <option value="AV19110046">AV19110046-2019-12 GROUP Q SCG VALUE SERIES</option>
                                                            <option value="AV19110047">AV19110047-2019-12 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV19110048">AV19110048-2019-12 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV19110049">AV19110049-2019-12 GROUP Q - PLUS SCG VALUE SERIES</option>
                                                            <option value="AV19110050">AV19110050-2019-11 SMALL SECTION  C 150X75, C 125X65.  เดอะสตีล</option>
                                                            <option value="AV19110051">AV19110051-2019-11 SMALL SECTION  C 150X75, C 125X65.  อุดมโลหะกิจ</option>
                                                            <option value="AV19110052">AV19110052-2019-11 SMALL SECTION C 100X50, C 150X75, C 125X65.  ยงเจริญชัย 2007</option>
                                                            <option value="AV19110053">AV19110053-2019-11 SMALL SECTION C100X50 ซี เอส เอช</option>
                                                            <option value="AV19110054">AV19110054-2019-11 SMALL SECTION C 100X50. เตียฮงฮะ</option>
                                                            <option value="AV19110055">AV19110055-2019-12 GROUP Q  VALUE SERIES SUMI</option>
                                                            <option value="AV19110056">AV19110056-2019-11 SMALL SECTION  C 100X50  TMT (16.9)</option>
                                                            <option value="AV19110057">AV19110057-2019-11 SMALL SECTION C 100X50, C 150X75, C 125X65.  ยงเจริญชัย 2007</option>
                                                            <option value="AV19110058">AV19110058-2019-12 GROUP Q VALUE SERIES MITSUI SM520</option>
                                                            <option value="AV19110059">AV19110059-2019-12 DB1912 SIRIKIT(DISC)</option>
                                                            <option value="AV19110060">AV19110060-2019-12 DB1912 RE1700 SIRIKIT(RE)</option>
                                                            <option value="AV19110061">AV19110061-2019-12 DB1912 RE1400 SIRIKIT(RE)</option>
                                                            <option value="AV19110062">AV19110062-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV  เดอะ สตีล</option>
                                                            <option value="AV19110063">AV19110063-2019-11 SMALL SECTION  C 150X75, C 125X65  โลหะเจริญ</option>
                                                            <option value="AV19110064">AV19110064-2019-12 DL1912-ONE BANGKOK (R3200) - ซื้อชดเชย โลหะเจริญ</option>
                                                            <option value="AV19110065">AV19110065-2019-12 DL1912-ONE BANGKOK (R3200) - ซื้อชดเชย ศิริกุล</option>
                                                            <option value="AV19110066">AV19110066-2019-11 SMALL SECTION C100X50, C150X75 C125X65 อุดม</option>
                                                            <option value="AV19110067">AV19110067-2019-12 GROUP Q (ความยาวพิเศษบวก200)</option>
                                                            <option value="AV19110068">AV19110068-2019-11 SMALL SECTION   C 125X65,150X75 อนันต์สตีล</option>
                                                            <option value="AV19110069">AV19110069-2019-11 SMALL SECTION   C 100X50 อนันต์สตีล</option>
                                                            <option value="AV19110070">AV19110070-2019-11 SMALL SECTION L100X100 ซี เอส เอช</option>
                                                            <option value="AV19110071">AV19110071-2019-11 SMALL SECTION C 100X50. เตียฮงฮะ</option>
                                                            <option value="AV19110072">AV19110072-2019-11 SMALL SECTION  C 150X75, C 125X65 TCS (17.0)</option>
                                                            <option value="AV19110073">AV19110073-2019-12 DL1912-WHIZDOM- S101 ศิริกุล</option>
                                                            <option value="AV19110074">AV19110074-2019-12 DL1912-WHIZDOM- S101 โลหะเจรืญ  (REBATE 1400)</option>
                                                            <option value="AV19110075">AV19110075-2019-11 DL1911-ซื้อชดเชย งาน ONE BANGKOK - ศิริกุล (REBATE 3200)</option>
                                                            <option value="AV19120001">AV19120001-2019-12 SMALL SECTION  C 100X50  TMT (16.9)</option>
                                                            <option value="AV19120002">AV19120002-2019-12 DL1912-STOCK  MITSUI CSH</option>
                                                            <option value="AV19120003">AV19120003-2019-12 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย2007</option>
                                                            <option value="AV19120004">AV19120004-2019-12 SMALL SECTION  C 150X75, C 125X65  เชื้อไพบูล</option>
                                                            <option value="AV19120005">AV19120005-2019-12 SMALL SECTION C100X50, C150X75 C125X65 CSH</option>
                                                            <option value="AV19120006">AV19120006-2019-12 GROUP Q SCG- 2 เอสเมทัล - CFR</option>
                                                            <option value="AV19120007">AV19120007-2019-12 GROUP Q PLUS -2 เอส ส่งให้</option>
                                                            <option value="AV19120008">AV19120008-2019-12 SMALL SECTION  L 100X100  โลหะเจริญ</option>
                                                            <option value="AV19120009">AV19120009-2020-01 DB1912 RE1700 SIRIKIT(RE)</option>
                                                            <option value="AV19120010">AV19120010-2020-01 DB1912 RE1400 SIRIKIT(RE)</option>
                                                            <option value="AV19120011">AV19120011-2020-01 DB1912 SIRIKIT(DISC)</option>
                                                            <option value="AV19120012">AV19120012-2019-12 SMALL SECTION  C 100X50  เป็นเอก</option>
                                                            <option value="AV19120013">AV19120013-2019-12 SMALL SECTION  C 125X65  เป็นเอก</option>
                                                            <option value="AV19120014">AV19120014-2020-01 GROUP Q</option>
                                                            <option value="AV19120015">AV19120015-2020-01 GROUP Q PLUS</option>
                                                            <option value="AV19120016">AV19120016-2019-12 SMALL SECTION  C 150X75, C 125X65 TMT (16.70)</option>
                                                            <option value="AV19120017">AV19120017-2019-12 SMALL SECTION  C 150X75, C 125X65 TCS (17.0)</option>
                                                            <option value="AV19120018">AV19120018-2019-12 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย2007</option>
                                                            <option value="AV19120019">AV19120019-2019-12 SMALL SECTION  C 150X75, C 125X65.  อุดมโลหะกิจ</option>
                                                            <option value="AV19120020">AV19120020-2019-12 SMALL SECTION C100X50, C150X75 อุดม</option>
                                                            <option value="AV19120021">AV19120021-2019-12 SMALL SECTION  C 150X75, C 125X65 TCS (16.6)</option>
                                                            <option value="AV19120022">AV19120022-2019-12 SMALL SECTION  C 150X75, C 125X65  เชื้อไพบูล</option>
                                                            <option value="AV19120023">AV19120023-2019-12 SMALL SECTION C125X65 ,150X75, L100X100 โลหะเจริญ</option>
                                                            <option value="AV19120024">AV19120024-2019-12 SMALL SECTION , L100X100 โลหะเจริญ</option>
                                                            <option value="AV19120025">AV19120025-2019-12 SMALL SECTION   C 125X65,C150X75,L100X100 .เบสท์สตีล</option>
                                                            <option value="AV19120026">AV19120026-2019-12 SMALL SECTION L 100X100 CSH</option>
                                                            <option value="AV19120027">AV19120027-2020-01 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV - รับเอง</option>
                                                            <option value="AV19120028">AV19120028-2020-01 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV อุดมโลหะกิจ</option>
                                                            <option value="AV19120029">AV19120029-2019-12 SMALL SECTION L 100X100,  C 150X75, C 125X65.  เดอะสตีล</option>
                                                            <option value="AV19120030">AV19120030-2019-12 SMALL SECTION   C 125X65,C150X75,L100X100 .เบสท์สตีล</option>
                                                            <option value="AV19120031">AV19120031-2020-01 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV19120032">AV19120032-2019-12 SMALL SECTION C100X50, C150X75 C125X65 โลหะไพศาล</option>
                                                            <option value="AV19120033">AV19120033-2019-12 SMALL SECTION  C 100X50  TMT (16.7)</option>
                                                            <option value="AV19120034">AV19120034-2019-12 SMALL SECTION  C 150X75, C 125X65 TMT (16.60)</option>
                                                            <option value="AV19120035">AV19120035-2020-01 GROUP Q  MITSUI</option>
                                                            <option value="AV19120036">AV19120036-2020-01 GROUP Q  SUMI</option>
                                                            <option value="AV19120037">AV19120037-2020-01 GROUP Q SCG</option>
                                                            <option value="AV19120039">AV19120039-2020-01 GROUP Q PLUS SCG</option>
                                                            <option value="AV19120040">AV19120040-2020-01 GROUP Q SM520</option>
                                                            <option value="AV19120041">AV19120041-2020-01 GROUP Q SM520 SCG</option>
                                                            <option value="AV19120042">AV19120042-2020-01 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV19120043">AV19120043-2020-01 GROUP Q PLUS SM520</option>
                                                            <option value="AV19120044">AV19120044-2020-01 GROUP Q VALUE SERIES</option>
                                                            <option value="AV19120045">AV19120045-2020-01 GROUP Q SCG VALUE SERIES</option>
                                                            <option value="AV19120046">AV19120046-2020-01 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV19120047">AV19120047-2020-01 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV19120048">AV19120048-2020-01 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV19120049">AV19120049-2020-01 GROUP Q  VALUE SERIES SUMI</option>
                                                            <option value="AV19120050">AV19120050-2019-12 โปรแกรมพิเศษ C180X75</option>
                                                            <option value="AV19120051">AV19120051-2019-12 GROUP Q - เป็นเอก H 100X100,H150X150,200X200</option>
                                                            <option value="AV19120052">AV19120052-2019-12 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย2007</option>
                                                            <option value="AV19120053">AV19120053-2020-01 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV19120054">AV19120054-2019-12 GROUP Q VALUE SERIES SM520</option>
                                                            <option value="AV19120055">AV19120055-2020-01 DL2001-ทางพิเศษพระราม 3 - ดาวคะนอง</option>
                                                            <option value="AV19120056">AV19120056-2020-01 DL2001-ทางพิเศษพระราม 3 - ดาวคะนอง</option>
                                                            <option value="AV19120057">AV19120057-2020-01 GROUP Q VALUE SERIES</option>
                                                            <option value="AV19120058">AV19120058-2020-01 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV19120059">AV19120059-2019-12 SMALL SECTION L 100X100 CSH</option>
                                                            <option value="AV19120060">AV19120060-2020-01 GROUP Q  SUMI SM520</option>
                                                            <option value="AV19120061">AV19120061-2020-01 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV19120062">AV19120062-2020-01 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV19120063">AV19120063-2020-01 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV19120064">AV19120064-2020-01 GROUP Q-PLUS  VALUE SERIES SUMI</option>
                                                            <option value="AV19120065">AV19120065-2019-12 SMALL SECTION  C 150X75, C 125X65.  อุดมโลหะกิจ</option>
                                                            <option value="AV19120066">AV19120066-2019-12 SMALL SECTION , L100X100 เดอะสตีล</option>
                                                            <option value="AV19120067">AV19120067-2019-12 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.5)</option>
                                                            <option value="AV19120068">AV19120068-2019-12 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.0)</option>
                                                            <option value="AV19120069">AV19120069-2019-12 SMALL SECTION C100X50 CSH</option>
                                                            <option value="AV19120070">AV19120070-2020-01 โปรแกรมพิเศษ SP-III,SP IV</option>
                                                            <option value="AV20010001">AV20010001-2020-01 SMALL SECTION C100X50 CSH</option>
                                                            <option value="AV20010002">AV20010002-2020-01 DB2001 RE1700 SIRIKIT(RE)</option>
                                                            <option value="AV20010003">AV20010003-2020-01 DB2001 RE1400 SIRIKIT(RE)</option>
                                                            <option value="AV20010004">AV20010004-2020-01 </option>
                                                            <option value="AV20010005">AV20010005-2020-01 SMALL SECTION  C 150X75, C 125X65 TMT (16.70)</option>
                                                            <option value="AV20010006">AV20010006-2020-01 SMALL SECTION L C150X75  เดอะสตีล</option>
                                                            <option value="AV20010007">AV20010007-2020-01 GROUP Q  VALUE SERIES SUMI ( 1000)</option>
                                                            <option value="AV20010008">AV20010008-2020-01 SMALL SECTION  C 150X75, C 125X65  เชื้อไพบูล</option>
                                                            <option value="AV20010009">AV20010009-2019-12 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.6)</option>
                                                            <option value="AV20010010">AV20010010-2020-01 SMALL SECTION  C 100X50  TMT (16.9)</option>
                                                            <option value="AV20010011">AV20010011-2020-01 GROUP Q SCG- 2 เอสเมทัล PJ2001-โกดังใช้เอง</option>
                                                            <option value="AV20010012">AV20010012-2020-01 GROUP Q SCG VALUE SERIES 2 เอสเมทัล PJ2001-โกดังใช้เอง</option>
                                                            <option value="AV20010013">AV20010013-2020-01 GROUP Q PLUS SCG 2 เอสเมทัล PJ2001-โกดังใช้เอง</option>
                                                            <option value="AV20010014">AV20010014-2020-01 SMALL SECTION  C 150X75 โลหะเจริญ</option>
                                                            <option value="AV20010015">AV20010015-2020-01 SMALL SECTION C100X50 MITSUI-อุดม</option>
                                                            <option value="AV20010016">AV20010016-2020-01 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย</option>
                                                            <option value="AV20010017">AV20010017-2020-01 SMALL SECTION C150X75 MITSUI-CSH</option>
                                                            <option value="AV20010018">AV20010018-2020-01 SMALL SECTION  C 100X50  TMT (17.1)</option>
                                                            <option value="AV20010019">AV20010019-2020-01 GROUP Q SM520 - เตียฮงฮะ</option>
                                                            <option value="AV20010020">AV20010020-2020-01 SMALL SECTION L C150X75  เดอะสตีล</option>
                                                            <option value="AV20010021">AV20010021-2020-01 GROUP Q VALUE SERIES ( 1000)</option>
                                                            <option value="AV20010022">AV20010022-2020-01 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย</option>
                                                            <option value="AV20010023">AV20010023-2020-01 GROUP Q PLUS SM520 - เตียฮงฮะ</option>
                                                            <option value="AV20010024">AV20010024-2020-02 GROUP Q</option>
                                                            <option value="AV20010025">AV20010025-2020-02 GROUP Q PLUS</option>
                                                            <option value="AV20010026">AV20010026-2020-01 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย</option>
                                                            <option value="AV20010027">AV20010027-2020-02 GROUP Q SCG</option>
                                                            <option value="AV20010028">AV20010028-2020-02 GROUP Q PLUS SCG</option>
                                                            <option value="AV20010029">AV20010029-2020-02 GROUP Q SM520</option>
                                                            <option value="AV20010030">AV20010030-2020-01 GROUP Q PLUS SM520</option>
                                                            <option value="AV20010031">AV20010031-2020-02 GROUP Q PLUS SM520</option>
                                                            <option value="AV20010032">AV20010032-2020-02 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20010033">AV20010033-2020-02 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20010034">AV20010034-2020-02 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20010035">AV20010035-2020-02 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20010036">AV20010036-2020-02 GROUP Q SCG VALUE SERIES</option>
                                                            <option value="AV20010037">AV20010037-2020-02 GROUP Q - PLUS SCG VALUE SERIES</option>
                                                            <option value="AV20010038">AV20010038-2020-02 GROUP Q  MITSUI</option>
                                                            <option value="AV20010039">AV20010039-2020-02 GROUP Q  SUMI</option>
                                                            <option value="AV20010040">AV20010040-2020-02 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20010041">AV20010041-2020-02 GROUP Q  PLUS  SUMI</option>
                                                            <option value="AV20010042">AV20010042-2020-02 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV20010043">AV20010043-2020-02 GROUP Q  VALUE SERIES SUMI</option>
                                                            <option value="AV20010044">AV20010044-2020-02 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20010045">AV20010045-2020-02 GROUP Q-PLUS  VALUE SERIES SUMI</option>
                                                            <option value="AV20010046">AV20010046-2020-02 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20010047">AV20010047-2020-01 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20010048">AV20010048-2020-01 SMALL SECTION   C 125X65,150X75 ชัยสุภณ</option>
                                                            <option value="AV20010049">AV20010049-2020-01 SMALL SECTION C100X50 อุดม</option>
                                                            <option value="AV20010050">AV20010050-2020-01 SMALL SECTION L 100X100 MITSUI-อุดม</option>
                                                            <option value="AV20010051">AV20010051-2020-01 SMALL SECTION L 100X100 อุดม</option>
                                                            <option value="AV20010052">AV20010052-2020-01 SMALL SECTION  L 100X100 อุดมโลหะกิจ</option>
                                                            <option value="AV20010053">AV20010053-2020-02 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20010054">AV20010054-2020-02 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20010055">AV20010055-2020-02 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20010056">AV20010056-2020-01 SMALL SECTION C 100X50 อนันต์สตีล</option>
                                                            <option value="AV20010057">AV20010057-2020-01 SMALL SECTION C100X50 MITSUI-อุดม</option>
                                                            <option value="AV20010058">AV20010058-2020-01 SMALL SECTION C100X50 MITSUI-อุดม</option>
                                                            <option value="AV20010059">AV20010059-2020-01 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV เดอะสตีล - รับเอง</option>
                                                            <option value="AV20010060">AV20010060-2020-01 โปรแกรมพิเศษ C  250X90L</option>
                                                            <option value="AV20010061">AV20010061-2020-01 SMALL SECTION  C 100X50  TCS (17.0)</option>
                                                            <option value="AV20010062">AV20010062-2020-01 โปรแกรมพิเศษ C250X90L SCG</option>
                                                            <option value="AV20010063">AV20010063-2020-01 โปรแกรมพิเศษ C250X90L,MITSUI</option>
                                                            <option value="AV20010064">AV20010064-2020-01 โปรแกรมพิเศษ C250X90L,SUMI</option>
                                                            <option value="AV20010065">AV20010065-2020-01 SMALL SECTION C150X75 MITSUI-CSH</option>
                                                            <option value="AV20010066">AV20010066-2020-01 SMALL SECTION C150X75 MITSUI-CSH</option>
                                                            <option value="AV20010067">AV20010067-2020-01 SMALL SECTION L100X100 เดอะสตีล</option>
                                                            <option value="AV20010068">AV20010068-2020-01 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.1)</option>
                                                            <option value="AV20010069">AV20010069-2020-02 GROUP Q  VALUE SERIES MITSUI SM520</option>
                                                            <option value="AV20010070">AV20010070-2020-01 SMALL SECTION  C 100X50 ยงเจริญชัย</option>
                                                            <option value="AV20020001">AV20020001-2020-02 SMALL SECTION  C 150X75, C 125X65 TMT (17.00)</option>
                                                            <option value="AV20020002">AV20020002-2020-02 SMALL SECTION  C 150X75, C 125X65 TCS (17.0)</option>
                                                            <option value="AV20020003">AV20020003-2020-02 SMALL SECTION C150X75 MITSUI-CSH</option>
                                                            <option value="AV20020004">AV20020004-2020-02 SMALL SECTION L 100X100  CSH</option>
                                                            <option value="AV20020005">AV20020005-2020-02 SMALL SECTION  L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20020006">AV20020006-2020-02 SMALL SECTION  C 150X75, C 125X65  ยงเจริญชัย</option>
                                                            <option value="AV20020007">AV20020007-2020-02 SMALL SECTION C100X50 MITSUI-อุดม</option>
                                                            <option value="AV20020008">AV20020008-2020-02 SMALL SECTION  C 100X50  TCS (17.0)</option>
                                                            <option value="AV20020009">AV20020009-2020-02 SMALL SECTION  C 150X75, C 125X65  เดอะสตีล</option>
                                                            <option value="AV20020010">AV20020010-2020-02 SMALL SECTION  C 100X50  TMT (16.9)</option>
                                                            <option value="AV20020011">AV20020011-2020-02 DB 2002 CENTRARA DUSIT</option>
                                                            <option value="AV20020012">AV20020012-2020-02 SMALL SECTION  L SS400 100X100 อุดมโลหะเจริญ</option>
                                                            <option value="AV20020013">AV20020013-2020-02 SMALL SECTION  L SS540 100X100 อุดมโลหะเจริญ</option>
                                                            <option value="AV20020014">AV20020014-2020-02 DB 2002 CENTRARA DUSIT (22.6)</option>
                                                            <option value="AV20020015">AV20020015-2020-04 DB 2004 CENTRARA DUSIT</option>
                                                            <option value="AV20020016">AV20020016-2020-02 SMALL SECTION  L SS400 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20020017">AV20020017-2020-02 SMALL SECTION C150X75,125X65  MITSUI-อุดม</option>
                                                            <option value="AV20020018">AV20020018-2020-02 DB SIRIKIT CONVENTION HALL</option>
                                                            <option value="AV20020019">AV20020019-2020-02 โปรแกรมพิเศษC200X80,C200X90 SYS</option>
                                                            <option value="AV20020020">AV20020020-2020-02 โปรแกรมพิเศษC200X90,C200X80 SCG</option>
                                                            <option value="AV20020021">AV20020021-2020-02 โปรแกรมพิเศษC200X80 , C200X90  MITSUI</option>
                                                            <option value="AV20020022">AV20020022-2020-02 โปรแกรมพิเศษC200X80 , C200X90  SUMI</option>
                                                            <option value="AV20020023">AV20020023-2020-02 TEST</option>
                                                            <option value="AV20020024">AV20020024-2020-02 GROUP Q</option>
                                                            <option value="AV20020025">AV20020025-2020-03 GROUP Q</option>
                                                            <option value="AV20020026">AV20020026-2020-03 GROUP Q PLUS</option>
                                                            <option value="AV20020027">AV20020027-2020-02 SMALL SECTION C 125X65  เป็นเอก</option>
                                                            <option value="AV20020028">AV20020028-2020-02 GROUP Q SCG บ้านโป่ง</option>
                                                            <option value="AV20020029">AV20020029-2020-02 GROUP Q  MITSUI - โลหะไพศาลวานิช</option>
                                                            <option value="AV20020030">AV20020030-2020-02 SMALL SECTION  C 150X75, C 125X65  (16.9) ยงเจริญชัย</option>
                                                            <option value="AV20020031">AV20020031-2020-03 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20020032">AV20020032-2020-03 GROUP Q SCG</option>
                                                            <option value="AV20020033">AV20020033-2020-03 GROUP Q PLUS SCG</option>
                                                            <option value="AV20020034">AV20020034-2020-03 SYS สินค้าสั่งพิเศษ SCG</option>
                                                            <option value="AV20020035">AV20020035-2020-03 GROUP Q SM520</option>
                                                            <option value="AV20020036">AV20020036-2020-03 GROUP Q PLUS SM520</option>
                                                            <option value="AV20020037">AV20020037-2020-03 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20020038">AV20020038-2020-03 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20020039">AV20020039-2020-03 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20020040">AV20020040-2020-03 GROUP Q SCG VALUE SERIES</option>
                                                            <option value="AV20020041">AV20020041-2020-03 GROUP Q  MITSUI</option>
                                                            <option value="AV20020042">AV20020042-2020-03 GROUP Q  SUMI</option>
                                                            <option value="AV20020043">AV20020043-2020-03 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20020044">AV20020044-2020-03 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20020045">AV20020045-2020-03 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20020046">AV20020046-2020-03 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20020047">AV20020047-2020-03 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20020048">AV20020048-2020-03 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20020049">AV20020049-2020-03 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV20020050">AV20020050-2020-03 GROUP Q  VALUE SERIES SUMI</option>
                                                            <option value="AV20020051">AV20020051-2020-02 SMALL SECTION  C 150X75, C 125X65 TMT (16.80)</option>
                                                            <option value="AV20020052">AV20020052-2020-03 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV20020053">AV20020053-2020-02 SMALL SECTION  C 150X75, C 125X65 เชื้อไพบูลย์</option>
                                                            <option value="AV20020054">AV20020054-2020-02 SMALL SECTION  C125X65,C100X50 อุดม</option>
                                                            <option value="AV20020055">AV20020055-2020-02 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.1)</option>
                                                            <option value="AV20020056">AV20020056-2020-03 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20020057">AV20020057-2020-03 SYS สินค้าสั่งพิเศษ SCG SM520</option>
                                                            <option value="AV20020058">AV20020058-2020-02 SMALL SECTION L 100X100  อุดม</option>
                                                            <option value="AV20020059">AV20020059-2020-02 SMALL SECTION  L SS400 100X100 อุดมโลหะเจริญ</option>
                                                            <option value="AV20020060">AV20020060-2020-02 SMALL SECTION  C125X65,C100X50 อุดม</option>
                                                            <option value="AV20020061">AV20020061-2020-02 SMALL SECTION L 100X100  CSH</option>
                                                            <option value="AV20020062">AV20020062-2020-02 SMALL SECTION C100X50 , C 150X75 เป็นเอก</option>
                                                            <option value="AV20020063">AV20020063-2020-03 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20020064">AV20020064-2020-03 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20020065">AV20020065-2020-03 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20020066">AV20020066-2020-03 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20020067">AV20020067-2020-02 SMALL SECTION  L SS400 100X100 อุดม</option>
                                                            <option value="AV20020068">AV20020068-2020-02 SMALL SECTION  L SS540 100X100 อุดม</option>
                                                            <option value="AV20020069">AV20020069-2020-02 SMALL SECTION  C 150X75, C 125X65 TCS (16.8)</option>
                                                            <option value="AV20020070">AV20020070-2020-02 SMALL SECTION C150X75,125X65  MITSUI-CSH</option>
                                                            <option value="AV20020071">AV20020071-2020-03 โปรแกรมพิเศษ SP-III,SP IV</option>
                                                            <option value="AV20020072">AV20020072-2020-03 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV -โลหะเจริญ</option>
                                                            <option value="AV20020073">AV20020073-2020-02 SMALL SECTION L 100X100  CSH</option>
                                                            <option value="AV20020074">AV20020074-2020-03 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV อุดมโลหะกิจ</option>
                                                            <option value="AV20020075">AV20020075-2020-02 GROUP Q  SUMI  CD 1.3</option>
                                                            <option value="AV20020076">AV20020076-2020-02 GROUP Q (AV20010024 CD 1.3)</option>
                                                            <option value="AV20020077">AV20020077-2020-02 GROUP Q PLUS (AV20010025 CD 1.3)</option>
                                                            <option value="AV20020078">AV20020078-2020-02 GROUP Q SCG (AV20010027 CD 1.3)</option>
                                                            <option value="AV20020079">AV20020079-2020-02 GROUP Q PLUS SCG (AV20010028 CD 1.3)</option>
                                                            <option value="AV20020080">AV20020080-2020-02 GROUP Q SM520  (AV20010029 CD 1.3)</option>
                                                            <option value="AV20020081">AV20020081-2020-02 GROUP Q PLUS SM520 (AV20010031 CD 1.3)</option>
                                                            <option value="AV20020082">AV20020082-2020-02 GROUP Q  SM520 SCG (AV20010032 CD 1.3)</option>
                                                            <option value="AV20020083">AV20020083-2020-02 GROUP Q PLUS SM520 SCG (AV20010033 CD 1.3)</option>
                                                            <option value="AV20030001">AV20030001-2020-02 GROUP Q VALUE SERIES (AV20010034 CD 1.3)</option>
                                                            <option value="AV20030002">AV20030002-2020-02 GROUP Q PLUS VALUE SERIES (AV20010035 CD 1.3)</option>
                                                            <option value="AV20030003">AV20030003-2020-02 GROUP Q SCG VALUE SERIES (AV20010036 CD 1.3)</option>
                                                            <option value="AV20030004">AV20030004-2020-02 GROUP Q - PLUS SCG VALUE SERIES (AV20010037 CD 1.3)</option>
                                                            <option value="AV20030005">AV20030005-2020-02 GROUP Q  MITSUI  (AV20010038 CD 1.3)</option>
                                                            <option value="AV20030006">AV20030006-2020-02 GROUP Q  SUMI (AV20010039 CD 1.3)</option>
                                                            <option value="AV20030007">AV20030007-2020-02 GROUP Q  PLUS MITSUI  (AV20010040 CD 1.3)</option>
                                                            <option value="AV20030008">AV20030008-2020-02 GROUP Q  PLUS  SUMI  (AV20010041 CD 1.3)</option>
                                                            <option value="AV20030009">AV20030009-2020-02 GROUP Q  VALUE SERIES MITSUI  (AV20010042 CD 1.3)</option>
                                                            <option value="AV20030010">AV20030010-2020-02 GROUP Q  VALUE SERIES SUMI  (AV20010043 CD 1.3)</option>
                                                            <option value="AV20030011">AV20030011-2020-02 GROUP Q-PLUS  VALUE SERIES MITSUI (AV20010044 CD 1.3)</option>
                                                            <option value="AV20030012">AV20030012-2020-02 GROUP Q-PLUS  VALUE SERIES SUMI (AV20010045 CD 1.3)</option>
                                                            <option value="AV20030013">AV20030013-2020-02 GROUP Q  MITSUI SM520 (AV20010046 CD 1.3)</option>
                                                            <option value="AV20030014">AV20030014-2020-02 GROUP Q  SUMI SM520 (AV20010053 CD 1.3)</option>
                                                            <option value="AV20030015">AV20030015-2020-02 GROUP Q  PLUS MITSUI SM520 (AV20010054 CD 1.3)</option>
                                                            <option value="AV20030016">AV20030016-2020-02 GROUP Q  PLUS SUMI SM520 (AV20010055 CD 1.3)</option>
                                                            <option value="AV20030017">AV20030017-2020-02 GROUP Q  VALUE SERIES MITSUI SM520 (AV20010069 CD 1.3)</option>
                                                            <option value="AV20030018">AV20030018-2020-01 GROUP Q SCG (AV19120037 CD 1.3)</option>
                                                            <option value="AV20030019">AV20030019-2020-01 GROUP Q PLUS SCG (AV19120039 CD 1.3)</option>
                                                            <option value="AV20030020">AV20030020-2020-01 GROUP Q SM520 SCG (AV19120041 CD 1.3)</option>
                                                            <option value="AV20030021">AV20030021-2020-01 GROUP Q PLUS SM520 SCG (AV19120042 CD 1.3)</option>
                                                            <option value="AV20030022">AV20030022-2020-01 GROUP Q SCG VALUE SERIES (AV19120045 CD 1.3)</option>
                                                            <option value="AV20030023">AV20030023-2020-01 GROUP Q (AV19120014 CD 1.3)</option>
                                                            <option value="AV20030024">AV20030024-2020-01 GROUP Q PLUS (AV19120015 CD 1.3)</option>
                                                            <option value="AV20030025">AV20030025-2020-01 GROUP Q SM520 (AV19120040 CD 1.3)</option>
                                                            <option value="AV20030026">AV20030026-2020-01 GROUP Q PLUS SM520 (AV19120043 CD 1.3)</option>
                                                            <option value="AV20030027">AV20030027-2020-01 GROUP Q VALUE SERIES (AV19120044 CD 1.3)</option>
                                                            <option value="AV20030028">AV20030028-2020-01 GROUP Q VALUE SERIES (AV19120047 CD 1.3)</option>
                                                            <option value="AV20030029">AV20030029-2020-01 GROUP Q PLUS VALUE SERIES (AV19120058 CD 1.3)</option>
                                                            <option value="AV20030030">AV20030030-2020-01 GROUP Q SM520 - เตียฮงฮะ (AV20010019 CD 1.3)</option>
                                                            <option value="AV20030031">AV20030031-2020-01 GROUP Q  MITSUI (AV19120035 CD 1.3)</option>
                                                            <option value="AV20030032">AV20030032-2020-01 GROUP Q  SUMI (AV19120036 CD 1.3)</option>
                                                            <option value="AV20030033">AV20030033-2020-01 GROUP Q  PLUS MITSUI (AV19120046 CD 1.3)</option>
                                                            <option value="AV20030034">AV20030034-2020-01 GROUP Q  PLUS SUMI (AV19120047 CD 1.3)</option>
                                                            <option value="AV20030035">AV20030035-2020-01 GROUP Q  VALUE SERIES MITSUI (AV19120048 CD 1.3)</option>
                                                            <option value="AV20030036">AV20030036-2020-01 GROUP Q  VALUE SERIES SUMI (AV19120049 CD 1.3)</option>
                                                            <option value="AV20030037">AV20030037-2020-01 GROUP Q  MITSUI SM520 (AV19120053 CD 1.3)</option>
                                                            <option value="AV20030038">AV20030038-2020-01 GROUP Q  SUMI SM520 (AV19120060  CD 1.3)</option>
                                                            <option value="AV20030039">AV20030039-2020-01 GROUP Q  PLUS MITSUI SM520 (AV19120061 CD 1.3)</option>
                                                            <option value="AV20030040">AV20030040-2020-01 GROUP Q  PLUS SUMI SM520  (AV19120062 CD 1.3)</option>
                                                            <option value="AV20030041">AV20030041-2020-01 GROUP Q-PLUS  VALUE SERIES MITSUI (AV19120063 CD 1.3)</option>
                                                            <option value="AV20030042">AV20030042-2020-01 GROUP Q-PLUS  VALUE SERIES SUMI (AV19120064 CD 1.3)</option>
                                                            <option value="AV20030043">AV20030043-2020-01 GROUP Q  MITSUI SM520 (AV20010047 CD 1.3)</option>
                                                            <option value="AV20030044">AV20030044-2020-02 โปรแกรมพิเศษC200X90,C200X80 SCG (AV20020020 CD 1.3)</option>
                                                            <option value="AV20030045">AV20030045-2020-02 โปรแกรมพิเศษC200X80,C200X90 SYS (AV20020019 CD 1.3)</option>
                                                            <option value="AV20030046">AV20030046-2020-02 SMALL SECTION  C125X65,C100X50 อุดม (AV20020054 CD 1.3)</option>
                                                            <option value="AV20030047">AV20030047-2020-02 GROUP Q SCG บ้านโป่ง (AV20020028 CD 1.3)</option>
                                                            <option value="AV20030048">AV20030048-2020-01 โปรแกรมพิเศษ C  250X90L (AV20010060 CD 1.3)</option>
                                                            <option value="AV20030049">AV20030049-2020-01 โปรแกรมพิเศษ C250X90L,MITSUI  (AV20010063 CD 1.3)</option>
                                                            <option value="AV20030050">AV20030050-2020-01 โปรแกรมพิเศษ SP-III,SP IV (AV19120070 CD 1.3)</option>
                                                            <option value="AV20030051">AV20030051-2019-12 SMALL SECTION C100X50, C150X75 อุดม (AV19120020 CD 1.3)</option>
                                                            <option value="AV20030052">AV20030052-2019-12 GROUP Q (AV19110026 CD 1.3)</option>
                                                            <option value="AV20030053">AV20030053-2019-12 GROUP Q PLUS (AV19110027 CD 1.3)</option>
                                                            <option value="AV20030054">AV20030054-2019-12 GROUP Q  MITSUI  (AV19110030 CD 1.3)</option>
                                                            <option value="AV20030055">AV20030055-2019-11 GROUP Q  PLUS MITSUI (AV19100036 CD 1.3)</option>
                                                            <option value="AV20030056">AV20030056-2019-11 GROUP Q  PLUS SUMI (AV19100037 CD 1.3)</option>
                                                            <option value="AV20030057">AV20030057-2020-01 โปรแกรมพิเศษ C250X90L SCG (AV20010062 CD 1.3)</option>
                                                            <option value="AV20030058">AV20030058-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV (AV19110005 CD 1.3)</option>
                                                            <option value="AV20030059">AV20030059-2019-09 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV - รับเอง (AV19080062 CD 1.3)</option>
                                                            <option value="AV20030060">AV20030060-2019-12 GROUP Q SCG  (AV19110035 CD 1.3)</option>
                                                            <option value="AV20030061">AV20030061-2019-12 GROUP Q PLUS SCG  (AV19110036 CD 1.3)</option>
                                                            <option value="AV20030062">AV20030062-2019-12 GROUP Q PLUS (AV19110027 CD 1.3)</option>
                                                            <option value="AV20030063">AV20030063-2019-11 GROUP Q SCG (AV19100027 CD 1.3)</option>
                                                            <option value="AV20030064">AV20030064-2019-10 GROUP Q SCG (AV19090034 CD 1.3)</option>
                                                            <option value="AV20030065">AV20030065-2019-10 GROUP Q PLUS (AV19090025 CD 1.3)</option>
                                                            <option value="AV20030066">AV20030066-2019-09 GROUP Q PLUS (AV19080026 CD 1.3)</option>
                                                            <option value="AV20030067">AV20030067-2020-02 SMALL SECTION C150X75 MITSUI-CSH  (AV20020003 CD 1.3)</option>
                                                            <option value="AV20030068">AV20030068-2020-02 SMALL SECTION C150X75,125X65  MITSUI-CSH (AV20020070 CD 1.3)</option>
                                                            <option value="AV20030069">AV20030069-2020-02 SMALL SECTION L 100X100  CSH  (AV20020061 CD 1.3)</option>
                                                            <option value="AV20030070">AV20030070-2020-02 SMALL SECTION L 100X100  CSH (AV20020073 CD 1.3)</option>
                                                            <option value="AV20030071">AV20030071-2020-02 SMALL SECTION L 100X100  CSH  (AV20020004 CD 1.3)</option>
                                                            <option value="AV20030072">AV20030072-2020-02 SMALL SECTION C100X50 MITSUI-อุดม  (AV20020007 CD 1.3)</option>
                                                            <option value="AV20030073">AV20030073-2020-02 SMALL SECTION C150X75,125X65  MITSUI-อุดม (AV20020017 CD 1.3)</option>
                                                            <option value="AV20030074">AV20030074-2020-02 SMALL SECTION L 100X100  อุดม (AV20020058 CD 1.3)</option>
                                                            <option value="AV20030075">AV20030075-2019-09 GROUP Q SM520  (AV19080031 CD 1.3)</option>
                                                            <option value="AV20030076">AV20030076-2020-03 SMALL SECTION C100X50 อุดม DEMCO</option>
                                                            <option value="AV20030077">AV20030077-2020-03 SMALL SECTION  C 100X50  TMT (16.5)</option>
                                                            <option value="AV20030078">AV20030078-2020-03 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.1)</option>
                                                            <option value="AV20030079">AV20030079-2020-03 GROUP Q  SUMI -ธนาสาร 7 M</option>
                                                            <option value="AV20030080">AV20030080-2020-03 SMALL SECTION C150X75,125X65  MITSUI-CSH</option>
                                                            <option value="AV20030081">AV20030081-2020-03 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030082">AV20030082-2020-03 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030083">AV20030083-2020-03 GROUP Q PLUS- โลหะเจริญ ความยาว 10เมตร 8เมตร</option>
                                                            <option value="AV20030085">AV20030085-2020-01 SMALL SECTION  C 150X75 โลหะเจริญ (AV20010014 CD 1.3)</option>
                                                            <option value="AV20030086">AV20030086-2020-03 SMALL SECTION C 125X65</option>
                                                            <option value="AV20030087">AV20030087-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV เชื้อไพบูลย์</option>
                                                            <option value="AV20030088">AV20030088-2020-03 SMALL SECTION  C 100X50  TMT (16.7)</option>
                                                            <option value="AV20030089">AV20030089-2020-03 SMALL SECTION  C 150X75, C 125X65 TMT (16.70)</option>
                                                            <option value="AV20030090">AV20030090-2019-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV (AV19110005 CD 1.3) - เชื้อไพบูลย์</option>
                                                            <option value="AV20030091">AV20030091-2020-02 SMALL SECTION  C 150X75, C 125X65  (17.2) ยงเจริญชัย</option>
                                                            <option value="AV20030092">AV20030092-2020-03 DB 2003 CENTRARA DUSIT</option>
                                                            <option value="AV20030093">AV20030093-2020-04 GROUP Q  MITSUI ไม่ใช้</option>
                                                            <option value="AV20030094">AV20030094-2020-03 DB2003 โลหะไพศาล</option>
                                                            <option value="AV20030095">AV20030095-2020-02 SMALL SECTION  C 150X75, C 125X65 เชื้อไพบูลย์</option>
                                                            <option value="AV20030096">AV20030096-2020-03 SMALL SECTION  C 100X50  TCS (16.5)</option>
                                                            <option value="AV20030097">AV20030097-2020-03 SMALL SECTION  C 150X75, C 125X65 TCS (16.7)</option>
                                                            <option value="AV20030098">AV20030098-2020-03 SMALL SECTION  C 100X50  TCS (16.7)</option>
                                                            <option value="AV20030099">AV20030099-2020-03 SMALL SECTION  C 150X75, C 125X65 TCS (16.8)</option>
                                                            <option value="AV20030100">AV20030100-2020-04 GROUP Q</option>
                                                            <option value="AV20030101">AV20030101-2020-03 สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20030102">AV20030102-2020-03 สินค้าพิเศษสต็อก</option>
                                                            <option value="AV20030103">AV20030103-2020-03 MITSUI  สินค้าพิเศษสต็อก</option>
                                                            <option value="AV20030104">AV20030104-2020-03 MITSUI  สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20030105">AV20030105-2020-03 SUMI  สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20030106">AV20030106-2020-03 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20030107">AV20030107-2020-03 SMALL SECTION   C 150X75 ชัยสุภณ</option>
                                                            <option value="AV20030108">AV20030108-2020-03 SMALL SECTION  L SS400 100X100 อุดม</option>
                                                            <option value="AV20030109">AV20030109-2020-03 SMALL SECTION  C SM520 150X75  อุดม</option>
                                                            <option value="AV20030110">AV20030110-2020-04 GROUP Q PLUS</option>
                                                            <option value="AV20030111">AV20030111-2020-03 FORESTIAS บางนา (R700) - ศิริกุล</option>
                                                            <option value="AV20030112">AV20030112-2020-03 GROUP Q VALUE SERIES ( 1000)</option>
                                                            <option value="AV20030113">AV20030113-2020-03 SMALL SECTION   C 150X75 เป็นเอก</option>
                                                            <option value="AV20030114">AV20030114-2020-03 SMALL SECTION C 150 X75 ยงเจริญชัย</option>
                                                            <option value="AV20030115">AV20030115-2020-03 SMALL SECTION C100X50  MITSUI-อุดม</option>
                                                            <option value="AV20030116">AV20030116-2020-03 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030117">AV20030117-2020-04 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030118">AV20030118-2020-03 SMALL SECTION  C 150X75, C 125X65 TMT (16.50)</option>
                                                            <option value="AV20030119">AV20030119-2020-03 SMALL SECTION C 125X65 SIRIKUL</option>
                                                            <option value="AV20030120">AV20030120-2020-04 GROUP Q SCG</option>
                                                            <option value="AV20030121">AV20030121-2020-04 GROUP Q PLUS SCG</option>
                                                            <option value="AV20030122">AV20030122-2020-04 GROUP Q SM520</option>
                                                            <option value="AV20030123">AV20030123-2020-04 สินค้าสั่งพิเศษ SCG</option>
                                                            <option value="AV20030124">AV20030124-2020-04 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20030125">AV20030125-2020-03 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV -เชื้อไพบูลย์ -รับเอง</option>
                                                            <option value="AV20030126">AV20030126-2020-04 GROUP Q PLUS SM520</option>
                                                            <option value="AV20030127">AV20030127-2020-04 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20030128">AV20030128-2020-04 GROUP Q  MITSUI</option>
                                                            <option value="AV20030129">AV20030129-2020-04 GROUP Q  SUMI</option>
                                                            <option value="AV20030130">AV20030130-2020-03 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20030131">AV20030131-2020-05 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20030132">AV20030132-2020-04 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20030133">AV20030133-2020-04 GROUP Q  PLUS  SUMI</option>
                                                            <option value="AV20030134">AV20030134-2020-03 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.8)</option>
                                                            <option value="AV20030135">AV20030135-2020-04 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030136">AV20030136-2020-04 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20030137">AV20030137-2020-04 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20030138">AV20030138-2020-04 GROUP Q SCG VALUE SERIES</option>
                                                            <option value="AV20030139">AV20030139-2020-04 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV20030140">AV20030140-2020-04 GROUP Q  VALUE SERIES SUMI</option>
                                                            <option value="AV20030141">AV20030141-2020-04 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20030142">AV20030142-2020-04 GROUP Q SUMI SM520</option>
                                                            <option value="AV20030143">AV20030143-2020-04 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20030144">AV20030144-2020-04 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20030145">AV20030145-2020-04 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20030146">AV20030146-2020-03 SYS สินค้าสั่งพิเศษ SCG SM520</option>
                                                            <option value="AV20030147">AV20030147-2020-04 MITSUI  สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20030148">AV20030148-2020-04 MITSUI  สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20030149">AV20030149-2020-04 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20030150">AV20030150-2020-04 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20030151">AV20030151-2020-03 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TCS (17.0)</option>
                                                            <option value="AV20030152">AV20030152-2020-03 SMALL SECTIONL 100X100 MITSUI-อุดม</option>
                                                            <option value="AV20030153">AV20030153-2020-03 DB 2003 CENTRARA DUSITR R2200</option>
                                                            <option value="AV20030154">AV20030154-2020-04 DB 2004 CENTRARA DUSITR R2200</option>
                                                            <option value="AV20030155">AV20030155-2020-05 DB 2005 CENTRARA DUSITR R2200</option>
                                                            <option value="AV20030156">AV20030156-2020-03 SMALL SECTION LSS540 100X100</option>
                                                            <option value="AV20030157">AV20030157-2020-02 GROUP Q  MITSUI CSH</option>
                                                            <option value="AV20030158">AV20030158-2020-03 SMALL SECTION C 125X65,C150X75 ยงเจริญชัย</option>
                                                            <option value="AV20030159">AV20030159-2020-03 SMALL SECTION LSS540 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20030160">AV20030160-2020-02 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20030161">AV20030161-2020-04 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20030162">AV20030162-2020-04 GROUP Q-PLUS  VALUE SERIES SM520 MITSUI</option>
                                                            <option value="AV20030163">AV20030163-2019-04 DB 2003- โลหะเจริญ -ซื้อคืนศูนย์ศิริกิตต์ (REBATE 1700)</option>
                                                            <option value="AV20030164">AV20030164-2020-03 DB2001 RE1700 SIRIKIT(RE1700) 6,9</option>
                                                            <option value="AV20030165">AV20030165-2020-03 DB2003 ซื้อคืนงานสิริกิต (RE1400 ) 12 เมตร</option>
                                                            <option value="AV20030166">AV20030166-2020-03 DB2003   ซื้อคืนงานสิริกิต (RE1700 ) 6,9 เมตร</option>
                                                            <option value="AV20030167">AV20030167-2020-03 SMALL SECTION L 100X7 อุดม</option>
                                                            <option value="AV20030168">AV20030168-2020-03 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.6)</option>
                                                            <option value="AV20030169">AV20030169-2020-03 SMALL SECTION L 100X10 โลหะเจริญ</option>
                                                            <option value="AV20030170">AV20030170-2020-03 SMALL SECTION L 100X10 ,7 โลหะเจริญ</option>
                                                            <option value="AV20030172">AV20030172-2020-04 สินค้าพิเศษสต๊อค</option>
                                                            <option value="AV20030173">AV20030173-2020-03 DB 2003 ยงเจริญชัย</option>
                                                            <option value="AV20030174">AV20030174-2020-04 SYS สินค้าสั่งพิเศษ ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20030175">AV20030175-2020-04 สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20030176">AV20030176-2020-03 DB 2003 SM520 ยงเจริญชัย</option>
                                                            <option value="AV20030177">AV20030177-2020-03 GROUP Q PLUS ไทเมทอล</option>
                                                            <option value="AV20030178">AV20030178-2020-03 SMALL SECTION  C 150X75, C 125X65 TWK</option>
                                                            <option value="AV20040001">AV20040001-2020-04 SMALL SECTION C 125X65,C150X75 ยงเจริญชัย</option>
                                                            <option value="AV20040002">AV20040002-2020-04 SMALL SECTION  C 150X75, C 125X65 TMT (16.50)</option>
                                                            <option value="AV20040003">AV20040003-2020-04 GROUP Q</option>
                                                            <option value="AV20040004">AV20040004-2020-04 GROUP Q SM400  SUMI</option>
                                                            <option value="AV20040005">AV20040005-2020-04 SMALL SECTION  C 100X50  TMT (16.2)</option>
                                                            <option value="AV20040006">AV20040006-2020-04 TMT  SM400</option>
                                                            <option value="AV20040007">AV20040007-2020-04 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.6)</option>
                                                            <option value="AV20040008">AV20040008-2020-04 GROUP Q SM400</option>
                                                            <option value="AV20040009">AV20040009-2020-04 GROUP Q - PLUS SM400</option>
                                                            <option value="AV20040010">AV20040010-2020-04 สินค้าพิเศษสต็อค SM400</option>
                                                            <option value="AV20040011">AV20040011-2020-04 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20040012">AV20040012-2020-04 SMALL SECTION C100X50  MITSUI-อุดม</option>
                                                            <option value="AV20040013">AV20040013-2020-04 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20040014">AV20040014-2020-04 SUMII สินค้าพิเศษสต็อก</option>
                                                            <option value="AV20040015">AV20040015-2020-04 DB2004 ซื้อคืนงานสิริกิต (RE1400 ) 12 เมตร</option>
                                                            <option value="AV20040016">AV20040016-2020-03 GROUP Q SCG -สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20040017">AV20040017-2020-03 GROUP Q PLUS SCG  สินค้าซิเมนต์ ส่งให้</option>
                                                            <option value="AV20040018">AV20040018-2020-04 SMALL SECTION  C 150X75, C 125X65 TMT (16.40)</option>
                                                            <option value="AV20040019">AV20040019-2020-04 SMALL SECTION C125X65,150X75 ยงเจริญชัย</option>
                                                            <option value="AV20040020">AV20040020-2020-04 SMALL SECTION L100X100 โลหะเจริญ</option>
                                                            <option value="AV20040021">AV20040021-2020-04 SMALL SECTION C125X65,150X75 ยงเจริญชัย</option>
                                                            <option value="AV20040022">AV20040022-2020-05 GROUP Q</option>
                                                            <option value="AV20040023">AV20040023-2020-05 GROUP Q PLUS</option>
                                                            <option value="AV20040024">AV20040024-2020-05 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20040025">AV20040025-2020-04 SMALL SECTION  C 150X75, C 125X65 TWK</option>
                                                            <option value="AV20040026">AV20040026-2020-04 SMALL SECTION  C 100X50  TCS (16.2)</option>
                                                            <option value="AV20040027">AV20040027-2020-04 SMALL SECTION  C 150X75, C 125X65 TMT (16.2)</option>
                                                            <option value="AV20040028">AV20040028-2020-04 </option>
                                                            <option value="AV20040029">AV20040029-2020-04 SMALL SECTION  C 150X75, C 125X65 TMT (16.30)</option>
                                                            <option value="AV20040030">AV20040030-2020-05 GROUP Q SCG</option>
                                                            <option value="AV20040031">AV20040031-2020-05 GROUP Q PLUS SCG</option>
                                                            <option value="AV20040032">AV20040032-2020-04 SMALL SECTION  C100X50</option>
                                                            <option value="AV20040033">AV20040033-2020-05 สินค้าสั่งพิเศษ SCG</option>
                                                            <option value="AV20040034">AV20040034-2020-05 GROUP Q SM520</option>
                                                            <option value="AV20040035">AV20040035-2020-05 GROUP Q PLUS SM520</option>
                                                            <option value="AV20040036">AV20040036-2020-05 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20040037">AV20040037-2020-05 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20040038">AV20040038-2020-05 GROUP Q  MITSUI</option>
                                                            <option value="AV20040039">AV20040039-2020-05 GROUP Q  SUMI</option>
                                                            <option value="AV20040040">AV20040040-2020-05 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20040041">AV20040041-2020-04 SMALL SECTION C100X50 อุดม</option>
                                                            <option value="AV20040042">AV20040042-2020-05 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20040043">AV20040043-2020-05 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20040044">AV20040044-2020-05 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20040045">AV20040045-2020-05 GROUP Q SUMI SM520</option>
                                                            <option value="AV20040046">AV20040046-2020-05 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20040047">AV20040047-2020-05 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20040048">AV20040048-2020-04 SMALL SECTION C125X65,150X75 ยงเจริญชัย</option>
                                                            <option value="AV20040049">AV20040049-2020-04 SMALL SECTION C100X50  ยงเจริญชัย</option>
                                                            <option value="AV20040050">AV20040050-2020-05 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20040051">AV20040051-2020-04 SMALL SECTION C125X65,150X75 เชื้อไพบูลย์</option>
                                                            <option value="AV20040052">AV20040052-2020-05 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20040053">AV20040053-2020-05 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20040054">AV20040054-2020-04 โปรแกรมพิเศษ C  250X90L</option>
                                                            <option value="AV20040055">AV20040055-2020-01 โปรแกรมพิเศษ C250X90L,MITSUI</option>
                                                            <option value="AV20040056">AV20040056-2020-04 โปรแกรมพิเศษ C250X90L,MITSUI</option>
                                                            <option value="AV20040057">AV20040057-2020-04 SMALL SECTION C150X75 อุดม</option>
                                                            <option value="AV20040058">AV20040058-2019-11 GROUP Q (AV19110022 CD 1.3)</option>
                                                            <option value="AV20040059">AV20040059-2020-04 GROUP Q SCG -สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20040060">AV20040060-2020-04 โปรแกรมพิเศษ C250X90L SCG</option>
                                                            <option value="AV20040061">AV20040061-2020-04 SMALL SECTION C150X75,125X65  MITSUI-CSH</option>
                                                            <option value="AV20040062">AV20040062-2020-05 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20040063">AV20040063-2020-05 SCG สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20040064">AV20040064-2020-05 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20040065">AV20040065-2020-05 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20040066">AV20040066-2020-05 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20040067">AV20040067-2020-05 GROUP Q  SUMI ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20040068">AV20040068-2020-04 SMALL SECTION L  100X100  ยงเจริญชัย</option>
                                                            <option value="AV20040069">AV20040069-2020-04 SMALL SECTION C100X50 อุดม</option>
                                                            <option value="AV20040070">AV20040070-2020-04 SMALL SECTION C100X50  MITSUI-อุดม</option>
                                                            <option value="AV20040071">AV20040071-2019-09 GROUP Q เดอะสตีล</option>
                                                            <option value="AV20040072">AV20040072-2019-10 GROUP Q  เดอะสตีล</option>
                                                            <option value="AV20040073">AV20040073-2020-04 SMALL SECTION C125X65,150X75 ยงเจริญชัย</option>
                                                            <option value="AV20040074">AV20040074-2020-04 SMALL SECTION C125X65 อุดม</option>
                                                            <option value="AV20040075">AV20040075-2020-05 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV20040076">AV20040076-2020-05 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV20040077">AV20040077-2020-04 SMALL SECTION  C 100X50  TMT (16.3)</option>
                                                            <option value="AV20040078">AV20040078-2020-05 DB2005   ฝายพานทอง บางปะกง เซ็นเตอร์รุ่งเรือง , ร้อยเอ็ดรุ่งเรือง</option>
                                                            <option value="AV20040079">AV20040079-2020-05 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV อุดมโลหะกิจ</option>
                                                            <option value="AV20040080">AV20040080-2020-05 GROUP Q SM400</option>
                                                            <option value="AV20040081">AV20040081-2020-04 GROUP Q - PLUS SM400</option>
                                                            <option value="AV20040082">AV20040082-2020-05 GROUP Q - PLUS SM400</option>
                                                            <option value="AV20040083">AV20040083-2020-05 SYS สินค้าสั่งพิเศษ SM400</option>
                                                            <option value="AV20050001">AV20050001-2020-05 SMALL SECTION C100X50 ยงเจริญชัย</option>
                                                            <option value="AV20050002">AV20050002-2020-05 SMALL SECTION C125X65  MITSUI-อุดม</option>
                                                            <option value="AV20050003">AV20050003-2020-05 SMALL SECTION  C 150X75, C 125X65 TCS (16.3)</option>
                                                            <option value="AV20050004">AV20050004-2020-05 SMALL SECTION  C 150X75, C 125X65 TMT (16.30)</option>
                                                            <option value="AV20050005">AV20050005-2020-05 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.6)</option>
                                                            <option value="AV20050006">AV20050006-2020-05 SMALL SECTION C125X65,150X75 เชื้อไพบูลย์</option>
                                                            <option value="AV20050007">AV20050007-2020-05 GROUP Q SCG -สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20050008">AV20050008-2020-05 SMALL SECTION L100X100ยงเจริญชัย</option>
                                                            <option value="AV20050009">AV20050009-2020-05 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20050010">AV20050010-2020-06 DB 2006  CENTRARA DUSIT (R500)</option>
                                                            <option value="AV20050011">AV20050011-2020-07 DB 2007  CENTRARA DUSIT (R500)</option>
                                                            <option value="AV20050012">AV20050012-2020-05 SYS สินค้าสั่งพิเศษ (ความยาวพิเศษ 200)</option>
                                                            <option value="AV20050013">AV20050013-2020-05 GROUP Q (ความยาวพิเศษ  200)</option>
                                                            <option value="AV20050014">AV20050014-2020-05 GROUP Q PLUS (ความยาวพิเศษ  200)</option>
                                                            <option value="AV20050015">AV20050015-2020-05 SYS สินค้าสั่งพิเศษ SM520 (ความยาวพิเศษ 200)</option>
                                                            <option value="AV20050016">AV20050016-2020-05 GROUP Q SM520 (ความยาวพิเศษ 200)</option>
                                                            <option value="AV20050017">AV20050017-2020-05 SMALL SECTION  C 100X50  TMT (16.2)</option>
                                                            <option value="AV20050018">AV20050018-2020-05 SMALL SECTION C100X50  MITSUI-อุดม</option>
                                                            <option value="AV20050019">AV20050019-2020-05 SMALL SECTION C150X75 MITSUI-อุดม</option>
                                                            <option value="AV20050020">AV20050020-2020-05 SMALL SECTION C125X65,150X75 อุดมโลหะกิจ</option>
                                                            <option value="AV20050021">AV20050021-2020-05 SMALL SECTION L100X100,C100X50  ยงเจริญชัย</option>
                                                            <option value="AV20050022">AV20050022-2020-05 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20050023">AV20050023-2020-05 GROUP Q PLUS - ไทเมทอล</option>
                                                            <option value="AV20050024">AV20050024-2020-05 SMALL SECTION  C 150X75, C 125X65 TWK (16.4)</option>
                                                            <option value="AV20050025">AV20050025-2020-05 SMALL SECTION  C125X65 , C150X75 ส.วิไล</option>
                                                            <option value="AV20050026">AV20050026-2020-05 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20050027">AV20050027-2020-05 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20050028">AV20050028-2019-12 โปรแกรมพิเศษ C180X75 ( CD 1.3 AV19120050 )</option>
                                                            <option value="AV20050029">AV20050029-2020-05 SMALL SECTION C150X75 MITSUI- CSH</option>
                                                            <option value="AV20050030">AV20050030-2020-06 GROUP Q</option>
                                                            <option value="AV20050031">AV20050031-2020-06 GROUP Q PLUS</option>
                                                            <option value="AV20050032">AV20050032-2020-05 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20050033">AV20050033-2020-06 GROUP Q PLUS TEST</option>
                                                            <option value="AV20050034">AV20050034-2020-06 GROUP Q SM520</option>
                                                            <option value="AV20050035">AV20050035-2020-03 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV -เชื้อไพบูลย์</option>
                                                            <option value="AV20050036">AV20050036-2020-05 SMALL SECTION  C 100X50  TCS (16.2)</option>
                                                            <option value="AV20050037">AV20050037-2020-05 SMALL SECTION C125X65,150X75 อุดมโลหะกิจ</option>
                                                            <option value="AV20050038">AV20050038-2020-06 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20050039">AV20050039-2020-06 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20050040">AV20050040-2020-05 SMALL SECTION C125X65,150X75 เชื้อไพบูลย์</option>
                                                            <option value="AV20050041">AV20050041-2020-06 GROUP Q PLUS SM520</option>
                                                            <option value="AV20050042">AV20050042-2020-06 GROUP Q SCG</option>
                                                            <option value="AV20050043">AV20050043-2020-06 GROUP Q PLUS SCG</option>
                                                            <option value="AV20050044">AV20050044-2020-05 SMALL SECTION L100X100  MITSUI- CSH</option>
                                                            <option value="AV20050045">AV20050045-2020-06 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20050046">AV20050046-2020-06 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20050047">AV20050047-2020-06 สินค้าสั่งพิเศษ SCG</option>
                                                            <option value="AV20050048">AV20050048-2020-06 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20050049">AV20050049-2020-06 GROUP Q - PLUS SCG VALUE SERIES</option>
                                                            <option value="AV20050050">AV20050050-2020-06 GROUP Q  MITSUI</option>
                                                            <option value="AV20050051">AV20050051-2020-06 GROUP Q  SUMI</option>
                                                            <option value="AV20050052">AV20050052-2020-06 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20050053">AV20050053-2020-06 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20050054">AV20050054-2020-06 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20050055">AV20050055-2020-06 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20050056">AV20050056-2020-06 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20050057">AV20050057-2020-06 GROUP Q SUMI SM520</option>
                                                            <option value="AV20050058">AV20050058-2020-06 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20050059">AV20050059-2020-06 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20050060">AV20050060-2020-06 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20050061">AV20050061-2020-06 GROUP Q-PLUS  VALUE SERIES SUMI</option>
                                                            <option value="AV20050062">AV20050062-2020-05 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20050063">AV20050063-2020-05 SMALL SECTION  C 150X75, C 125X65 KHC (16.4)</option>
                                                            <option value="AV20050064">AV20050064-2020-05 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20050065">AV20050065-2020-05 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20050066">AV20050066-2020-05 SMALL SECTION C150X75 MITSUI-อุดม</option>
                                                            <option value="AV20050067">AV20050067-2020-05 SMALL SECTION L TIS-SS400 100X100 อุดมโลหะกิจ</option>
                                                            <option value="AV20050068">AV20050068-2020-05 SMALL SECTION L TIS-SS400 150X75 อุดมโลหะกิจ</option>
                                                            <option value="AV20050069">AV20050069-2020-05 SMALL SECTION C150X75,C125X65  เดอะสตีล</option>
                                                            <option value="AV20050070">AV20050070-2020-10 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20050071">AV20050071-2020-06 SCG สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20050073">AV20050073-2020-06 GROUP Q - PLUS  H TIS-SM400 588X300</option>
                                                            <option value="AV20050074">AV20050074-2020-06 สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20050075">AV20050075-2020-05 สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV20050076">AV20050076-2020-06 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20050077">AV20050077-2020-06 SCG สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20050078">AV20050078-2020-05 สินค้าสั่งพิเศษ SCG -สินค้าซีเมนต์ ส่งให้</option>
                                                            <option value="AV20050079">AV20050079-2020-05 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20050080">AV20050080-2020-06 GROUP Q - PLUS  SM520 ความยาวพิศษ (ไม่ถึง 10 ตัน)</option>
                                                            <option value="AV20060001">AV20060001-2020-06 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20060002">AV20060002-2020-06 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20060003">AV20060003-2020-06 DB 2006  CENTRARA DUSIT 13M (500)</option>
                                                            <option value="AV20060004">AV20060004-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060005">AV20060005-2020-06 SMALL SECTION  C 150X75, C 125X65 TMT (16.30)</option>
                                                            <option value="AV20060006">AV20060006-2020-06 SMALL SECTION  C 100X50  TMT (16.2)</option>
                                                            <option value="AV20060007">AV20060007-2020-06 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (16.6)</option>
                                                            <option value="AV20060008">AV20060008-2020-06 SMALL SECTION C150X75,C125X65  เดอะสตีล</option>
                                                            <option value="AV20060009">AV20060009-2020-06 SMALL SECTIONC L100X100  เดอะสตีล</option>
                                                            <option value="AV20060010">AV20060010-2020-06 GROUP Q PLUS SCG - 2S</option>
                                                            <option value="AV20060011">AV20060011-2020-06 DB 2006  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20060012">AV20060012-2020-07 DB 2007  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20060013">AV20060013-2020-06 DB 2006  BANGKOK MALL (R1500-1700)</option>
                                                            <option value="AV20060014">AV20060014-2020-07 DB 2007  BANGKOK MALL (R1500-1700)</option>
                                                            <option value="AV20060015">AV20060015-2020-06 DB 2006  BANGKOK MALL-ศิริกุล (R1500)</option>
                                                            <option value="AV20060016">AV20060016-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- CSH</option>
                                                            <option value="AV20060017">AV20060017-2020-06 SMALL SECTION  C150X75 , 125X65 ส.วิไล</option>
                                                            <option value="AV20060018">AV20060018-2020-04 สินค้าสั่งพิเศษ SCG -สินค้าซีเมนต์ ส่งให้</option>
                                                            <option value="AV20060019">AV20060019-2020-07 DB 2007  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20060020">AV20060020-2020-06 SMALL SECTION  C 150X75, C 125X65 TMT (17.00)</option>
                                                            <option value="AV20060021">AV20060021-2020-06 SMALL SECTION C150X75,C125X65  โลหะเจริญ</option>
                                                            <option value="AV20060022">AV20060022-2020-06 SMALL SECTION  C 150X75, C 125X65 TCS (16.5)</option>
                                                            <option value="AV20060023">AV20060023-2020-06 SMALL SECTION  C 100X50  TCS (16.2)</option>
                                                            <option value="AV20060024">AV20060024-2020-06 โปรแกรมพิเศษC200X80,C200X90 SYS</option>
                                                            <option value="AV20060025">AV20060025-2020-06 โปรแกรมพิเศษC200X90,C200X80 SCG</option>
                                                            <option value="AV20060026">AV20060026-2020-06 DB 2006  STOCK</option>
                                                            <option value="AV20060027">AV20060027-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060028">AV20060028-2020-06 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20060029">AV20060029-2020-06 โปรแกรมพิเศษC200X80 , C200X90  MITSUI</option>
                                                            <option value="AV20060030">AV20060030-2020-06 โปรแกรมพิเศษC200X80 , C200X90  SUMI</option>
                                                            <option value="AV20060031">AV20060031-2020-06 SMALL SECTION  C 150X75, C 125X65 TCS (17.0)</option>
                                                            <option value="AV20060032">AV20060032-2020-06 โปรแกรมพิเศษC200X80,C200X90 SYS (SM400)</option>
                                                            <option value="AV20060033">AV20060033-2020-07 DB 2007  BANGKOK MALL (R1500 1400)</option>
                                                            <option value="AV20060034">AV20060034-2020-06 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20060035">AV20060035-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060036">AV20060036-2020-06 SMALL SECTION  C125X65 , C150X75 ส.วิไล</option>
                                                            <option value="AV20060037">AV20060037-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- CSH,UMT (16.70)</option>
                                                            <option value="AV20060038">AV20060038-2020-06 SMALL SECTION  C 150X75, C 125X65 TMT (16.80)</option>
                                                            <option value="AV20060039">AV20060039-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060040">AV20060040-2020-06 DB 2006  งานศูนย์ราชการ</option>
                                                            <option value="AV20060041">AV20060041-2020-06 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20060042">AV20060042-2020-06 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.3)</option>
                                                            <option value="AV20060043">AV20060043-2020-07 GROUP Q</option>
                                                            <option value="AV20060044">AV20060044-2020-07 GROUP Q PLUS</option>
                                                            <option value="AV20060045">AV20060045-2020-07 DB 2007  งานศูนย์ราชการ</option>
                                                            <option value="AV20060046">AV20060046-2020-06 โปรแกรมพิเศษ C200X90,C200X80 เชียงใหม่วิทยา</option>
                                                            <option value="AV20060047">AV20060047-2020-05 GROUP Q PLUS SCG - เชียงใหม่วิทยา</option>
                                                            <option value="AV20060048">AV20060048-2020-06 DB 2006  BANGKOK MALL-โลหะเจริญ (R1500)</option>
                                                            <option value="AV20060049">AV20060049-2020-06 DB 2006  BANGKOK MALL (R1500 1400)</option>
                                                            <option value="AV20060050">AV20060050-2020-06 DB 2006  งานศูนย์ราชการ</option>
                                                            <option value="AV20060051">AV20060051-2020-06 SMALL SECTION  C 150X75, C 125X65 KHC (17.0)</option>
                                                            <option value="AV20060052">AV20060052-2020-07 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20060053">AV20060053-2020-07 GROUP Q SM520</option>
                                                            <option value="AV20060054">AV20060054-2020-07 GROUP Q PLUS SM520</option>
                                                            <option value="AV20060055">AV20060055-2020-07 GROUP Q SCG</option>
                                                            <option value="AV20060056">AV20060056-2020-07 GROUP Q PLUS SCG</option>
                                                            <option value="AV20060057">AV20060057-2020-07 GROUP Q  SM520 SCG</option>
                                                            <option value="AV20060058">AV20060058-2020-07 GROUP Q PLUS SM520 SCG</option>
                                                            <option value="AV20060059">AV20060059-2020-06 SMALL SECTION  C150X75 เตียฮงฮะ</option>
                                                            <option value="AV20060060">AV20060060-2020-07 ...</option>
                                                            <option value="AV20060061">AV20060061-2020-07 GROUP Q  MITSUI</option>
                                                            <option value="AV20060062">AV20060062-2020-07 GROUP Q  SUMI</option>
                                                            <option value="AV20060063">AV20060063-2020-07 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20060064">AV20060064-2020-07 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20060065">AV20060065-2020-07 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20060066">AV20060066-2020-07 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20060067">AV20060067-2020-07 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20060068">AV20060068-2020-07 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20060069">AV20060069-2020-07 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20060070">AV20060070-2020-07 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20060071">AV20060071-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060072">AV20060072-2020-06 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20060073">AV20060073-2020-06 SMALL SECTION C150X75,C125X65  อุดม</option>
                                                            <option value="AV20060074">AV20060074-2020-06 สินค้าสั่งพิเศษ SCG -สินค้าซีเมนต์ ส่งให้</option>
                                                            <option value="AV20060075">AV20060075-2020-06 GROUP Q PLUS SCG - สินค้าซีเมนต์ ส่งให้</option>
                                                            <option value="AV20060076">AV20060076-2020-07 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20060077">AV20060077-2020-06 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20060078">AV20060078-2020-06 SMALL SECTION L100X100  ยงเจริญชัย</option>
                                                            <option value="AV20060079">AV20060079-2020-06 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.5)</option>
                                                            <option value="AV20060080">AV20060080-2020-07 GROUP Q PLUS VALUE SERIES SM520</option>
                                                            <option value="AV20060081">AV20060081-2020-07 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20060082">AV20060082-2020-07 SCG สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20060083">AV20060083-2020-07 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20060084">AV20060084-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- CSH</option>
                                                            <option value="AV20060085">AV20060085-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060086">AV20060086-2020-06 SMALL SECTION  C 100X50  TMT (16.8)</option>
                                                            <option value="AV20060087">AV20060087-2020-07 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20060088">AV20060088-2020-07 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20060089">AV20060089-2020-06 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20060090">AV20060090-2020-07 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20060091">AV20060091-2020-07 GROUP Q PLUS  SM400 VALUE SERIES</option>
                                                            <option value="AV20060092">AV20060092-2020-06 SMALL SECTION C150X75,C125X65  โลหะเจริญ (17.10)</option>
                                                            <option value="AV20060093">AV20060093-2020-06 DB 2006  BANGKOK MALL (R1500 1700)</option>
                                                            <option value="AV20060094">AV20060094-2020-07 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20060095">AV20060095-2020-06 SMALL SECTION  C 100X50  TMT (17.0)</option>
                                                            <option value="AV20060096">AV20060096-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- CSH</option>
                                                            <option value="AV20060097">AV20060097-2020-07 SYS สินค้าสั่งพิเศษ ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20060098">AV20060098-2020-07 SYS สินค้าสั่งพิเศษ SM520 ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20060099">AV20060099-2020-07 DB2007   โกดังใช้เอง ผู้แทนจำหน่าย บจก.สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV20060100">AV20060100-2020-06 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20060101">AV20060101-2020-06 SMALL SECTION C150X75,C125X65  เชื้อไพบูลย์  (17.10)</option>
                                                            <option value="AV20060102">AV20060102-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- UMT</option>
                                                            <option value="AV20060103">AV20060103-2020-06 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- UMT</option>
                                                            <option value="AV20060104">AV20060104-2020-08 DB 2008  BANGKOK MALL (R1500-1700)</option>
                                                            <option value="AV20070001">AV20070001-2020-08 DB 2008  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20070002">AV20070002-2020-08 DB 2008  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20070003">AV20070003-2020-10 DB 2010  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20070004">AV20070004-2020-09 DB 2009  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20070005">AV20070005-2020-10 DB 2010  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20070006">AV20070006-2020-02 Q</option>
                                                            <option value="AV20070007">AV20070007-2020-02 Q - Plus</option>
                                                            <option value="AV20070008">AV20070008-2020-07 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20070009">AV20070009-2020-07 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20070010">AV20070010-2020-07 GROUP Q-PLUS   SM520 VALUE SERIES MITSUI</option>
                                                            <option value="AV20070011">AV20070011-2020-07 GROUP Q-PLUS  VALUE SERIES SM520 MITSUI</option>
                                                            <option value="AV20070012">AV20070012-2020-07 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV20070013">AV20070013-2020-03 Q</option>
                                                            <option value="AV20070014">AV20070014-2020-03 Q - Plus</option>
                                                            <option value="AV20070015">AV20070015-2020-07 SMALL SECTION C150x75</option>
                                                            <option value="AV20070016">AV20070016-2020-04 Q</option>
                                                            <option value="AV20070017">AV20070017-2020-04 Q - Plus</option>
                                                            <option value="AV20070018">AV20070018-2020-07 Q</option>
                                                            <option value="AV20070019">AV20070019-2020-07 Q - Plus</option>
                                                            <option value="AV20070020">AV20070020-2020-05 Q</option>
                                                            <option value="AV20070021">AV20070021-2020-07 SO SCG</option>
                                                            <option value="AV20070022">AV20070022-2020-07 Q - Plus รอ</option>
                                                            <option value="AV20070023">AV20070023-2020-05 Q - Plus</option>
                                                            <option value="AV20070024">AV20070024-2020-05 สินค้าสั่งพิเศษ SCG</option>
                                                            <option value="AV20070025">AV20070025-2020-05 สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20070026">AV20070026-2020-06 Q</option>
                                                            <option value="AV20070027">AV20070027-2020-06 Q - Plus</option>
                                                            <option value="AV20070028">AV20070028-2020-07 ST-SCG</option>
                                                            <option value="AV20070029">AV20070029-2020-07 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.3)</option>
                                                            <option value="AV20070030">AV20070030-2020-06 SO SCG</option>
                                                            <option value="AV20070031">AV20070031-2020-06 ST-SCG</option>
                                                            <option value="AV20070032">AV20070032-2020-06 SC  C200X90,C200x90 SCG</option>
                                                            <option value="AV20070033">AV20070033-2020-07 SMALL SECTION C150X75,C125X65  ยงเจริญชัย</option>
                                                            <option value="AV20070034">AV20070034-2020-06 SMALL SECTION  C125X65 , C150X75 ส.วิไล</option>
                                                            <option value="AV20070035">AV20070035-2020-06 SS 150x75 ส.วิไล</option>
                                                            <option value="AV20070036">AV20070036-2020-05 SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV20070037">AV20070037-2020-04 SO SCG</option>
                                                            <option value="AV20070038">AV20070038-2020-04 ST SCG</option>
                                                            <option value="AV20070039">AV20070039-2020-05 SO SCG</option>
                                                            <option value="AV20070040">AV20070040-2020-05 ST SCG</option>
                                                            <option value="AV20070041">AV20070041-2020-06 GROUP Q - PLUS  SM520 ความยาวพิศษ (ไม่ถึง 10 ตัน)</option>
                                                            <option value="AV20070042">AV20070042-2020-07 GROUP Q PLUS ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20070043">AV20070043-2020-07 GROUP Q SM520 ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20070044">AV20070044-2020-07 SMALL SECTION C150X75,C125X65  เดอะสตีล</option>
                                                            <option value="AV20070045">AV20070045-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070046">AV20070046-2020-07 THE RICE/DUSIT CENTRAL (R1000) - ศิริกุล</option>
                                                            <option value="AV20070047">AV20070047-2020-07 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV อุดมโลหะกิจ</option>
                                                            <option value="AV20070048">AV20070048-2020-06 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20070049">AV20070049-2019-11 SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV20070050">AV20070050-2020-06 SMALL SECTION  C 100X50  TMT (16.8)</option>
                                                            <option value="AV20070051">AV20070051-2020-07 SMALL SECTION  C 100X50  TMT (16.8)</option>
                                                            <option value="AV20070052">AV20070052-2020-07 SMALL SECTION  C 150X75, C 125X65 TMT (17.10)</option>
                                                            <option value="AV20070053">AV20070053-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070054">AV20070054-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070055">AV20070055-2020-07 SMALL SECTION  C 150X75, C 125X65 TCS (17.0)</option>
                                                            <option value="AV20070056">AV20070056-2020-07 Q - Plus - เชียงใหม่</option>
                                                            <option value="AV20070057">AV20070057-2020-07 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.5)</option>
                                                            <option value="AV20070058">AV20070058-2020-07 SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV20070059">AV20070059-2020-08 GROUP Q</option>
                                                            <option value="AV20070060">AV20070060-2020-08 GROUP Q-PLUS</option>
                                                            <option value="AV20070061">AV20070061-2020-07 SMALL SECTION  C 100X50  TMT (16.9)</option>
                                                            <option value="AV20070062">AV20070062-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070063">AV20070063-2020-07 DB 2007  งานศูนย์ราชการ</option>
                                                            <option value="AV20070064">AV20070064-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 โลหะเจริญ</option>
                                                            <option value="AV20070065">AV20070065-2020-06 SO SCG - สินค้าซิเมนต์ ส่งให้</option>
                                                            <option value="AV20070066">AV20070066-2020-07 SS - เป็นเอก  C125x65</option>
                                                            <option value="AV20070067">AV20070067-2020-06 Q - สินค้าซิเมนต์ไทย - ส่งให้</option>
                                                            <option value="AV20070068">AV20070068-2020-06 Q - Plus - สินค้าซิเมนต์ไทย-ส่งให้</option>
                                                            <option value="AV20070069">AV20070069-2020-07 SMALL SECTION C100X50 ยงเจริญชัย</option>
                                                            <option value="AV20070070">AV20070070-2020-08 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20070071">AV20070071-2020-08 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20070072">AV20070072-2020-08 GROUP Q SM520</option>
                                                            <option value="AV20070073">AV20070073-2020-08 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20070074">AV20070074-2020-08 GROUP Q  MITSUI</option>
                                                            <option value="AV20070075">AV20070075-2020-08 GROUP Q SUMI</option>
                                                            <option value="AV20070076">AV20070076-2020-08 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20070077">AV20070077-2020-08 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20070078">AV20070078-2020-07 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- UMT</option>
                                                            <option value="AV20070079">AV20070079-2020-08 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20070080">AV20070080-2020-08 GROUP Q  SUMII SM520</option>
                                                            <option value="AV20070081">AV20070081-2020-08 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20070082">AV20070082-2020-08 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20070083">AV20070083-2020-08 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20070084">AV20070084-2020-08 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20070085">AV20070085-2020-08 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20070086">AV20070086-2020-08 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20070087">AV20070087-2020-08 Q</option>
                                                            <option value="AV20070088">AV20070088-2020-08 Q-Plus</option>
                                                            <option value="AV20070089">AV20070089-2020-08 SO SCG</option>
                                                            <option value="AV20070090">AV20070090-2020-07 SS -ส.วิไล C150x75</option>
                                                            <option value="AV20070091">AV20070091-2020-07 SMALL SECTION C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20070092">AV20070092-2020-07 DB 2007</option>
                                                            <option value="AV20070093">AV20070093-2020-07 SMALL SECTION  C 100X50  TMT (17.0)</option>
                                                            <option value="AV20070094">AV20070094-2020-07 SMALL SECTION  C 150X75, C 125X65 TMT (17.10)</option>
                                                            <option value="AV20070095">AV20070095-2020-07 SMALL SECTION C150X75 อุดม</option>
                                                            <option value="AV20070096">AV20070096-2020-08 ST-SCG</option>
                                                            <option value="AV20070097">AV20070097-2020-07 SMALL SECTION  C 150X75, C 125X65 TMT (17.00)</option>
                                                            <option value="AV20070098">AV20070098-2020-08 DB2008  โกดังใช้เอง ผู้แทนจำหน่าย 2เอส เมทัล</option>
                                                            <option value="AV20070099">AV20070099-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070100">AV20070100-2020-07 SMALL SECTION C150X75,C125X65 ,L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20070101">AV20070101-2020-07 SMALL SECTION  C 150X75, C 125X65 TMT (17.10)</option>
                                                            <option value="AV20070102">AV20070102-2020-07 SMALL SECTION  C 150X75, C 125X65 TMT (17.10)</option>
                                                            <option value="AV20080001">AV20080001-2020-08 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20080002">AV20080002-2020-07 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20080003">AV20080003-2020-08 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20080004">AV20080004-2020-08 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20080005">AV20080005-2020-08 SMALL SECTION  C 150X75, C 125X65 KHC (17.0)</option>
                                                            <option value="AV20080006">AV20080006-2020-08 SMALL SECTION  C 150X75, C 125X65 KHC (16.9)</option>
                                                            <option value="AV20080007">AV20080007-2020-09 DB 2007  BANGKOK MALL (R1500)</option>
                                                            <option value="AV20080008">AV20080008-2020-08 SMALL SECTION  C 150X75, C 125X65 TMT (16.80)</option>
                                                            <option value="AV20080009">AV20080009-2020-08 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.2)</option>
                                                            <option value="AV20080010">AV20080010-2020-08 SMALL SECTION  C 150X75, C 125X65 TMT (17.30)</option>
                                                            <option value="AV20080011">AV20080011-2020-09 DB2009  ป้องกันน้ำท่วมหน้าเจดีย์พระศรีสุริโยทัย จ.พระนครศรีอยุธยา ระยะที่4</option>
                                                            <option value="AV20080012">AV20080012-2020-09 DB2009 ป้องกันน้ำท่ามหน้าเจดีย์พระศรีสุริโยทัย -เป็นเอก</option>
                                                            <option value="AV20080013">AV20080013-2020-08 SMALL SECTION C125X65 อุดมโลหะกิจ</option>
                                                            <option value="AV20080014">AV20080014-2020-08 SMALL SECTION C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20080015">AV20080015-2020-08 SYS สินค้าสั่งพิเศษ SM520- เชื้อไพบูลย์</option>
                                                            <option value="AV20080016">AV20080016-2020-08 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- UMT</option>
                                                            <option value="AV20080017">AV20080017-2020-08 SS -ส.วิไล C125x65</option>
                                                            <option value="AV20080018">AV20080018-2020-08 DB2008 RE1500 SIRIKIT(RE)</option>
                                                            <option value="AV20080019">AV20080019-2020-08 DB2008 SIRIKIT(ความยาวพิเศษ)</option>
                                                            <option value="AV20080020">AV20080020-2020-08 DB 2008  CENTRARA DUSIT</option>
                                                            <option value="AV20080021">AV20080021-2020-09 GROUP Q</option>
                                                            <option value="AV20080022">AV20080022-2020-09 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20080023">AV20080023-2020-09 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20080024">AV20080024-2020-09 GROUP Q-PLUS</option>
                                                            <option value="AV20080025">AV20080025-2020-08 SS -เป็นเอก  C150x75,C100x50</option>
                                                            <option value="AV20080026">AV20080026-2020-08 SMALL SECTION  C 150X75, C 125X65 TCS (16.80)</option>
                                                            <option value="AV20080027">AV20080027-2020-08 SMALL SECTION  C 150X75, C 125X65 TCS (16.80)</option>
                                                            <option value="AV20080028">AV20080028-2020-08 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV20080029">AV20080029-2020-08 SMALL SECTION C150X75 อุดมโลหะกิจ</option>
                                                            <option value="AV20080030">AV20080030-2020-08 SMALL SECTION C125X65 ,C150X75 ยงเจริญชัย</option>
                                                            <option value="AV20080031">AV20080031-2020-08 SYS สินค้าพิเศษสต็อค ความยาวพิเศษ ไม่ถึง 10 ตัน</option>
                                                            <option value="AV20080032">AV20080032-2020-08 DB 2008  งานศูนย์ราชการ</option>
                                                            <option value="AV20080033">AV20080033-2020-08 โปรแกรมพิเศษC200X80,C200X90 SYS</option>
                                                            <option value="AV20080034">AV20080034-2020-09 Q</option>
                                                            <option value="AV20080035">AV20080035-2020-09 SO SCG</option>
                                                            <option value="AV20080036">AV20080036-2020-08 โปรแกรมพิเศษC200X80 , C200X90  MITSUI</option>
                                                            <option value="AV20080037">AV20080037-2020-06 โปรแกรมพิเศษC200X80 , C200X90  SUMI</option>
                                                            <option value="AV20080038">AV20080038-2020-08 SMALL SECTION L100X100ยงเจริญชัย</option>
                                                            <option value="AV20080039">AV20080039-2020-09 DB 2009 BANGKOK MALL (R1400   1500)</option>
                                                            <option value="AV20080040">AV20080040-2020-08 DB 2008  BANGKOK MALL (R1700 1500)</option>
                                                            <option value="AV20080041">AV20080041-2020-08 SMALL SECTION  C 100X50  TMT (16.8)</option>
                                                            <option value="AV20080042">AV20080042-2020-08 SC  C200X90,C200x90 SCG</option>
                                                            <option value="AV20080043">AV20080043-2020-07 GROUP Q  MITSUI</option>
                                                            <option value="AV20080044">AV20080044-2020-08 GROUP Q  MITSUI</option>
                                                            <option value="AV20080045">AV20080045-2020-09 GROUP Q  MITSUI</option>
                                                            <option value="AV20080046">AV20080046-2020-09 GROUP Q  SUMI</option>
                                                            <option value="AV20080047">AV20080047-2020-09 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20080048">AV20080048-2020-09 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20080049">AV20080049-2020-09 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20080050">AV20080050-2020-09 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20080051">AV20080051-2020-09 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20080052">AV20080052-2020-09 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20080053">AV20080053-2020-09 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20080054">AV20080054-2020-09 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20080055">AV20080055-2020-09 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20080056">AV20080056-2020-09 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20080057">AV20080057-2020-09 GROUP Q SM520</option>
                                                            <option value="AV20080058">AV20080058-2020-09 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20080059">AV20080059-2020-09 Q-Plus</option>
                                                            <option value="AV20080060">AV20080060-2020-09 SO SCG</option>
                                                            <option value="AV20080061">AV20080061-2020-09 ST-SCG</option>
                                                            <option value="AV20080062">AV20080062-2020-08 SMALL SECTION C150X75 เชื้อเไพบูลย์</option>
                                                            <option value="AV20080063">AV20080063-2020-08 SYS สินค้าพิเศษ ความยาวพิเศษ ไม่ถึง 10 ตัน</option>
                                                            <option value="AV20080064">AV20080064-2020-08 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.5)</option>
                                                            <option value="AV20080065">AV20080065-2020-08 GROUP Q ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20080066">AV20080066-2020-09 DB2009 SIRIKIT CONVENTION CENTER</option>
                                                            <option value="AV20080067">AV20080067-2020-09 DB2009 SIRIKIT CONVENTION CENTER (R1500)</option>
                                                            <option value="AV20080068">AV20080068-2020-08 DB 2008  BANGKOK MALL (R1700 1500)</option>
                                                            <option value="AV20080069">AV20080069-2020-08 </option>
                                                            <option value="AV20080070">AV20080070-2020-08 SC  C180X75 - ธนาสาร</option>
                                                            <option value="AV20080071">AV20080071-2020-08 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV20080072">AV20080072-2020-08 SS -เตียฮงฮะ  C100x50</option>
                                                            <option value="AV20080073">AV20080073-2020-09 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20080074">AV20080074-2020-08 SS -เบสท์สตีล C 100x50</option>
                                                            <option value="AV20080075">AV20080075-2020-08 โปรแกรมพิเศษ C  250X90L</option>
                                                            <option value="AV20080076">AV20080076-2020-08 โปรแกรมพิเศษ C250X90L,MITSUI</option>
                                                            <option value="AV20080077">AV20080077-2020-08 SC  C200X90,C200x90 SCG</option>
                                                            <option value="AV20080078">AV20080078-2020-08 SC  C250x90L SCG</option>
                                                            <option value="AV20080079">AV20080079-2020-08 โปรแกรมพิเศษ C  250X90L (SM400)</option>
                                                            <option value="AV20080080">AV20080080-2020-09 DB2009 RE1500 SIRIKIT(RE)</option>
                                                            <option value="AV20080081">AV20080081-2020-09 DB2009 SIRIKIT(ความยาวพิเศษ)</option>
                                                            <option value="AV20080082">AV20080082-2020-09 SMALL SECTION,L100X100 เดอะสตีล</option>
                                                            <option value="AV20080083">AV20080083-2020-09 SMALL SECTION C150X75,C125X65 เดอะสตีล</option>
                                                            <option value="AV20080084">AV20080084-2020-08 สินค้าพิเศษสต็อค  SM520</option>
                                                            <option value="AV20080085">AV20080085-2020-08 SMALL SECTION C125X65 ,C150X75 ยงเจริญชัย 16.8</option>
                                                            <option value="AV20080086">AV20080086-2020-08 SS -ส.วิไล C150x75</option>
                                                            <option value="AV20080087">AV20080087-2020-08 SMALL SECTION C125X65 , C150X75 ยงเจริญชัย</option>
                                                            <option value="AV20080088">AV20080088-2020-08 SMALL SECTION C125X65 ,C150X75 L100X100  MITSUI- UMT</option>
                                                            <option value="AV20080089">AV20080089-2020-08 SMALL SECTION C125X65,150X75 เชื้อไพบุลย์</option>
                                                            <option value="AV20080090">AV20080090-2020-08 SMALL SECTION C125X65 ,C150X75 (16.8)  MITSUI- CSH</option>
                                                            <option value="AV20080091">AV20080091-2020-08 SMALL SECTION L100X100  MITSUI- CSH (17.2)</option>
                                                            <option value="AV20080092">AV20080092-2020-08 SYS สินค้าพิเศษสต็อค SUB-SERIES ราคาบวก 1000 บาท</option>
                                                            <option value="AV20090001">AV20090001-2020-09 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20090002">AV20090002-2020-09 สินค้าพิเศษสต็อค  SM520</option>
                                                            <option value="AV20090003">AV20090003-2020-09 SMALL SECTION C150X75,C125X65 อุดม</option>
                                                            <option value="AV20090004">AV20090004-2020-09 SMALL SECTION C125X65 ,MITSUI- UMT</option>
                                                            <option value="AV20090005">AV20090005-2020-08 SS -เบสท์สตีล C 100x50</option>
                                                            <option value="AV20090006">AV20090006-2020-09 SS -เป็นเอก  C100x50</option>
                                                            <option value="AV20090007">AV20090007-2020-09 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20090008">AV20090008-2020-08 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20090009">AV20090009-2020-09 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20090010">AV20090010-2020-09 SS -เป็นเอก C125x65,C150x75</option>
                                                            <option value="AV20090011">AV20090011-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20090012">AV20090012-2020-09 SMALL SECTION  C 150X75, C 125X65 TMT (16.90)</option>
                                                            <option value="AV20090013">AV20090013-2020-09 SMALL SECTION C125X65 ,MITSUI- UMT</option>
                                                            <option value="AV20090014">AV20090014-2020-09 SC  C180X75 - ยงเจริญชัย</option>
                                                            <option value="AV20090015">AV20090015-2020-09 SMALL SECTION C150X75,C125X65 กวงฮั้ว</option>
                                                            <option value="AV20090016">AV20090016-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20090017">AV20090017-2020-09 GROUP Q  PLUS SUMI SM400</option>
                                                            <option value="AV20090018">AV20090018-2020-09 SUMII สินค้าสั่งพิเศษ SM400</option>
                                                            <option value="AV20090019">AV20090019-2020-09 GROUP Q  SUMI SM400</option>
                                                            <option value="AV20090020">AV20090020-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20090021">AV20090021-2020-09 SMALL SECTION,L100X100 โลหะเจริญ</option>
                                                            <option value="AV20090022">AV20090022-2020-09 SYS สินค้าพิเศษสต็อค (ความยาวพิเศษไม่ถึง10ตัน)</option>
                                                            <option value="AV20090023">AV20090023-2020-09 SMALL SECTION C125X65 ,MITSUI- CSH</option>
                                                            <option value="AV20090024">AV20090024-2020-09 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.7)</option>
                                                            <option value="AV20090025">AV20090025-2020-09 SMALL SECTION  C 150X75, C 125X65 TMT (17.50)</option>
                                                            <option value="AV20090026">AV20090026-2020-10 GROUP Q</option>
                                                            <option value="AV20090027">AV20090027-2020-10 GROUP Q-PLUS</option>
                                                            <option value="AV20090028">AV20090028-2020-09 SMALL SECTION L100X100  MITSUI-UMT,CSH (17.7)</option>
                                                            <option value="AV20090029">AV20090029-2020-09 ST-SCG NEW</option>
                                                            <option value="AV20090030">AV20090030-2020-09 SYS สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20090031">AV20090031-2020-09 MITSUI  สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20090032">AV20090032-2020-09 SUMII สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20090033">AV20090033-2020-09 SMALL SECTION C150X75,C125X65 เดอะสตีล</option>
                                                            <option value="AV20090034">AV20090034-2020-09 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV20090035">AV20090035-2020-09 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV20090036">AV20090036-2020-09 SMALL SECTION C150X75,C125X65 เชื้อไพบูลย์</option>
                                                            <option value="AV20090037">AV20090037-2020-09 SS-เป็นเอก  C100x50</option>
                                                            <option value="AV20090038">AV20090038-2020-09 SS -เป็นเอก C125x65,C150x75</option>
                                                            <option value="AV20090039">AV20090039-2020-09 SMALL SECTION L 100X7 เดอะสตีล (17.7)</option>
                                                            <option value="AV20090040">AV20090040-2020-09 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (17.8)</option>
                                                            <option value="AV20090041">AV20090041-2020-10 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20090042">AV20090042-2020-10 GROUP Q SM520</option>
                                                            <option value="AV20090043">AV20090043-2020-10 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20090044">AV20090044-2020-10 Q</option>
                                                            <option value="AV20090045">AV20090045-2020-10 Q-Plus</option>
                                                            <option value="AV20090046">AV20090046-2020-10 SO SCG</option>
                                                            <option value="AV20090047">AV20090047-2020-10 GROUP Q  MITSUI</option>
                                                            <option value="AV20090048">AV20090048-2020-10 GROUP Q  SUMI</option>
                                                            <option value="AV20090049">AV20090049-2020-10 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20090050">AV20090050-2020-09 SS - ส.วิไล L100x100</option>
                                                            <option value="AV20090051">AV20090051-2020-10 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20090052">AV20090052-2020-10 SUMII สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20090053">AV20090053-2020-10 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20090054">AV20090054-2020-10 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20090055">AV20090055-2020-10 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20090056">AV20090056-2020-10 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20090057">AV20090057-2020-10 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20090058">AV20090058-2020-10 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20090059">AV20090059-2020-10 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20090060">AV20090060-2020-07 GROUP Q PLUS SM520 (ลดค่าสนิม TCS)</option>
                                                            <option value="AV20090061">AV20090061-2020-09 SMALL SECTION  C 150X75, C 125X65 TMT (17.90)</option>
                                                            <option value="AV20090062">AV20090062-2020-09 SMALL SECTION  C 150X75, C 125X65 TCS (17.80)</option>
                                                            <option value="AV20090063">AV20090063-2020-09 SMALL SECTION  C 100X50  TCS (17.4)</option>
                                                            <option value="AV20090066">AV20090066-2020-10 GROUP Q  VALUE SERIES MITSUI</option>
                                                            <option value="AV20090067">AV20090067-2020-09 DB2009 SIRIKIT(ความยาวพิเศษ)</option>
                                                            <option value="AV20090068">AV20090068-2020-10 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20090069">AV20090069-2020-10 Q - Valueseries</option>
                                                            <option value="AV20090070">AV20090070-2020-09 SMALL SECTION  C 100X50  TMT (17.4)</option>
                                                            <option value="AV20090071">AV20090071-2020-10 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20090072">AV20090072-2020-10 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20090073">AV20090073-2020-09 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV20090074">AV20090074-2020-10 GROUP Q-PLUS  VALUE SERIES SM520 MITSUI</option>
                                                            <option value="AV20090075">AV20090075-2020-10 GROUP Q-PLUS  SM400 VALUE SERIES SUMI</option>
                                                            <option value="AV20090076">AV20090076-2020-09 SS -ส.วิไล C125x65</option>
                                                            <option value="AV20090077">AV20090077-2020-09 SMALL SECTION C150X75,C125X65 โลหะเจริญ</option>
                                                            <option value="AV20090078">AV20090078-2020-09 SMALL SECTION C100X50 MITSUI- CSH (17.2)</option>
                                                            <option value="AV20090079">AV20090079-2020-09 SMALL SECTION C125X65, C150X75 MITSUI- CSH (17.8)</option>
                                                            <option value="AV20090080">AV20090080-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย (17.8)</option>
                                                            <option value="AV20090081">AV20090081-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย (17.4)</option>
                                                            <option value="AV20090082">AV20090082-2020-09 SMALL SECTION C150X75,C125X65 เชื้อไพบูลย์</option>
                                                            <option value="AV20090083">AV20090083-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย (17.4)</option>
                                                            <option value="AV20090084">AV20090084-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20090085">AV20090085-2020-09 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20090086">AV20090086-2020-09 SS -ช.ม.วิทยา C125x65</option>
                                                            <option value="AV20090087">AV20090087-2020-09 SMALL SECTION C150X75, ยงเจริญชัย</option>
                                                            <option value="AV20090088">AV20090088-2020-09 SMALL SECTION C100X50, ยงเจริญชัย</option>
                                                            <option value="AV20090089">AV20090089-2020-09 SMALL SECTION L100X100, ยงเจริญชัย</option>
                                                            <option value="AV20090090">AV20090090-2020-09 SMALL SECTION C150X75,C125X65 กวงฮั้ว (17.8)</option>
                                                            <option value="AV20090091">AV20090091-2020-10 GROUP Q-PLUS  VALUE SERIES MITSUI</option>
                                                            <option value="AV20090092">AV20090092-2020-09 GROUP Q - ต.วรคุณ</option>
                                                            <option value="AV20090093">AV20090093-2020-09 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.1)</option>
                                                            <option value="AV20100001">AV20100001-2020-10 SYS สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20100002">AV20100002-2020-10 MITSUI  สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20100003">AV20100003-2020-10 SUMII สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20100004">AV20100004-2020-09 SUMII สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20100005">AV20100005-2020-10 ST-SCG</option>
                                                            <option value="AV20100006">AV20100006-2020-10 SMALL SECTION  C 150X75, C 125X65 TMT (17.70)</option>
                                                            <option value="AV20100007">AV20100007-2020-10 SMALL SECTION  C 150X75, C 125X65 TMT (17.70)</option>
                                                            <option value="AV20100008">AV20100008-2020-10 SMALL SECTION C150X75,C125X65 TMT (17.8)</option>
                                                            <option value="AV20100009">AV20100009-2020-10 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20100010">AV20100010-2020-10 GROUP Q-PLUS ความพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20100011">AV20100011-2020-08 SS - ส.วิไล C150x75</option>
                                                            <option value="AV20100012">AV20100012-2020-09 MITSUI  สินค้าสั่งพิเศษ SM520 (NEW)</option>
                                                            <option value="AV20100013">AV20100013-2020-09 Q-Plus - 2 เอส</option>
                                                            <option value="AV20100014">AV20100014-2020-10 SS - เตียฮงฮะ C100x50</option>
                                                            <option value="AV20100015">AV20100015-2020-10 SS - เบสท์สตีล C150x75</option>
                                                            <option value="AV20100017">AV20100017-2020-10 SMALL SECTION C150X75,C125X65 อุดม</option>
                                                            <option value="AV20100018">AV20100018-2020-10 งานโครงการทางด่วนพระราม 3</option>
                                                            <option value="AV20100019">AV20100019-2020-10 SS - ส.วิไล C150x75</option>
                                                            <option value="AV20100020">AV20100020-2020-11 GROUP Q</option>
                                                            <option value="AV20100021">AV20100021-2020-11 GROUP Q-PLUS</option>
                                                            <option value="AV20100022">AV20100022-2020-10 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.1)</option>
                                                            <option value="AV20100023">AV20100023-2020-10 DB 2010  ศูนย์ราชการ</option>
                                                            <option value="AV20100024">AV20100024-2020-10 GROUP Q PLUS VALUE SERIES  SM520</option>
                                                            <option value="AV20100025">AV20100025-2020-11 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20100026">AV20100026-2020-11 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20100027">AV20100027-2020-11 GROUP Q SM520</option>
                                                            <option value="AV20100028">AV20100028-2020-10 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20100029">AV20100029-2020-11 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20100030">AV20100030-2020-11 GROUP Q  MITSUI</option>
                                                            <option value="AV20100031">AV20100031-2020-11 GROUP Q  SUMI</option>
                                                            <option value="AV20100032">AV20100032-2020-11 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20100033">AV20100033-2020-11 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20100034">AV20100034-2020-11 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20100035">AV20100035-2020-11 GROUP Q  ----</option>
                                                            <option value="AV20100036">AV20100036-2020-11 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20100037">AV20100037-2020-11 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20100038">AV20100038-2020-11 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20100039">AV20100039-2020-11 SUMI สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20100040">AV20100040-2020-11 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20100041">AV20100041-2020-11 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20100042">AV20100042-2020-11 Q</option>
                                                            <option value="AV20100043">AV20100043-2020-11 Q-Plus</option>
                                                            <option value="AV20100044">AV20100044-2020-11 SO SCG</option>
                                                            <option value="AV20100045">AV20100045-2020-08 โปรแกรมพิเศษC200X80,C200X90 SYS - อุดม</option>
                                                            <option value="AV20100046">AV20100046-2018-05 ชุดสุดคุ้ม1805 - เต็กเฮง</option>
                                                            <option value="AV20100047">AV20100047-2020-10 PK ชุดสุดคุุ้ม 10/2020</option>
                                                            <option value="AV20100048">AV20100048-2020-10 SMALL SECTION C150X75,C125X65 ศิริกุล</option>
                                                            <option value="AV20100049">AV20100049-2020-08 PK ชุดสุดคุุ้ม 8/2020</option>
                                                            <option value="AV20100050">AV20100050-2020-10 SMALL SECTION  C 100X50  TMT (17.4)</option>
                                                            <option value="AV20100051">AV20100051-2020-11 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20100052">AV20100052-2020-10 SMALL SECTION  C 100X50  SRK (17.5)</option>
                                                            <option value="AV20100053">AV20100053-2020-11 ----</option>
                                                            <option value="AV20100054">AV20100054-2020-11 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20100055">AV20100055-2020-10 SMALL SECTION L 100X100 อุดม</option>
                                                            <option value="AV20100056">AV20100056-2020-10 SMALL SECTION L 100X100 อุดม</option>
                                                            <option value="AV20100057">AV20100057-2020-10 SMALL SECTION C 125X65 ยงเจริญชัย</option>
                                                            <option value="AV20100058">AV20100058-2020-10 SMALL SECTION L 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20100059">AV20100059-2020-10 ST-สินค้าซีเมนต์ ส่งให้</option>
                                                            <option value="AV20100060">AV20100060-2020-11 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20100061">AV20100061-2020-10 SMALL SECTION C150X75,C125X65 กวงฮั้ว (17.8)</option>
                                                            <option value="AV20100062">AV20100062-2020-11 DB 2011  ศูนย์ราชการ</option>
                                                            <option value="AV20100063">AV20100063-2020-10 SS - ส.วิไล C150x75</option>
                                                            <option value="AV20100064">AV20100064-2020-10 SS - เบสท์สตีล LSS400 100x100</option>
                                                            <option value="AV20100065">AV20100065-2020-10 SS - เบสท์สตีล LSS540 100x100</option>
                                                            <option value="AV20100066">AV20100066-2020-09 GROUP Q  PLUS SUMI - TMT</option>
                                                            <option value="AV20100067">AV20100067-2020-11 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20100068">AV20100068-2020-10 GROUP Q  PLUS SUMI - TMT</option>
                                                            <option value="AV20100069">AV20100069-2020-11 ST-SCG</option>
                                                            <option value="AV20100070">AV20100070-2020-11 SYS สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20100071">AV20100071-2020-10 SMALL SECTION C125X65,C150X75 ยงเจริญชัย</option>
                                                            <option value="AV20100072">AV20100072-2020-10 SMALL SECTION C125X65 ,MITSUI- CSH (17000)</option>
                                                            <option value="AV20100073">AV20100073-2020-10 SMALL SECTION C125X65 ,MITSUI- CSH (17500)</option>
                                                            <option value="AV20110001">AV20110001-2020-11 MITSUI  สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20110002">AV20110002-2020-11 SUMII สินค้าพิเศษสต็อค (NEW)</option>
                                                            <option value="AV20110003">AV20110003-2020-11 ST-SCG</option>
                                                            <option value="AV20110004">AV20110004-2020-11 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20110005">AV20110005-2020-11 SMALL SECTION C150X75 ,MITSUI- CSH</option>
                                                            <option value="AV20110006">AV20110006-2020-11 SMALL SECTION C150X75,C125X65 TMT (17.8)</option>
                                                            <option value="AV20110007">AV20110007-2020-11 SMALL SECTION C150X75 ,MITSUI- อุดม</option>
                                                            <option value="AV20110008">AV20110008-2020-11 SMALL SECTION L 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20110009">AV20110009-2020-11 SMALL SECTION C150X75 ,MITSUI- CSH</option>
                                                            <option value="AV20110010">AV20110010-2020-11 SMALL SECTION C125X65 ,MITSUI- อุดม</option>
                                                            <option value="AV20110011">AV20110011-2020-11 SMALL SECTION  C 150X75, C 125X65 TCS (17.80)</option>
                                                            <option value="AV20110012">AV20110012-2020-11 SMALL SECTION C100X50ยงเจริญชัย</option>
                                                            <option value="AV20110013">AV20110013-2020-11 SC  C180X75 - ยงเจริญชัย</option>
                                                            <option value="AV20110014">AV20110014-2020-11 SS - เบสท์สตีล LSS400 100x100</option>
                                                            <option value="AV20110015">AV20110015-2020-11 SMALL SECTION C150X75 เดอะสตีล</option>
                                                            <option value="AV20110016">AV20110016-2020-11 SMALL SECTION L100X100 ,MITSUI- CSH</option>
                                                            <option value="AV20110017">AV20110017-2020-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV20110018">AV20110018-2020-11 SMALL SECTION C100X50ยงเจริญชัย</option>
                                                            <option value="AV20110019">AV20110019-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20110020">AV20110020-2020-11 DB 2011   LEVEL 2-3</option>
                                                            <option value="AV20110021">AV20110021-2020-11 DB 2011 CENTRARA DUSIT (R500)</option>
                                                            <option value="AV20110022">AV20110022-2020-12 DB 2012 CENTRARA DUSIT (R500)</option>
                                                            <option value="AV20110023">AV20110023-2020-11 SS - เตียฮงฮะ C100x50</option>
                                                            <option value="AV20110025">AV20110025-2020-11 SS - เบสท์สตีล LSS540 100x100</option>
                                                            <option value="AV20110026">AV20110026-2020-11 SS - เบสท์สตีล LSS400 100x100</option>
                                                            <option value="AV20110027">AV20110027-2020-11 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.3)</option>
                                                            <option value="AV20110028">AV20110028-2020-11 SMALL SECTION  C 150X75, C 125X65 TCS (17.90)</option>
                                                            <option value="AV20110029">AV20110029-2020-12 DB 2012 ป้องกันน้ำท่ามหน้าเจดีย์พระศรีสุริโยทัย -เบสท์ สตีล</option>
                                                            <option value="AV20110030">AV20110030-2020-11 SMALL SECTION C125X65 ,MITSUI- อุดม</option>
                                                            <option value="AV20110031">AV20110031-2020-11 SMALL SECTION L100X100</option>
                                                            <option value="AV20110032">AV20110032-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 โลหะเจริญ</option>
                                                            <option value="AV20110033">AV20110033-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 เชื้อไพบูลย์</option>
                                                            <option value="AV20110034">AV20110034-2020-11 SYS สินค้าสั่งพิเศษ กรณีสั่งความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20110036">AV20110036-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 อุดม</option>
                                                            <option value="AV20110037">AV20110037-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 อุดม</option>
                                                            <option value="AV20110038">AV20110038-2020-11 SMALL SECTION L 90X90 อุดม</option>
                                                            <option value="AV20110039">AV20110039-2020-12 GROUP Q</option>
                                                            <option value="AV20110040">AV20110040-2020-12 GROUP Q-PLUS</option>
                                                            <option value="AV20110041">AV20110041-2020-10 Q - สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20110042">AV20110042-2020-10 SO SCG - สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20110043">AV20110043-2020-11 GROUP Q-PLUS</option>
                                                            <option value="AV20110044">AV20110044-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 ยงเจริญชัย</option>
                                                            <option value="AV20110045">AV20110045-2020-12 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20110046">AV20110046-2020-12 GROUP Q SM520</option>
                                                            <option value="AV20110047">AV20110047-2020-12 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20110048">AV20110048-2020-12 GROUP Q  MITSUI</option>
                                                            <option value="AV20110049">AV20110049-2020-12 GROUP Q  SUMI</option>
                                                            <option value="AV20110050">AV20110050-2020-12 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20110051">AV20110051-2020-12 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20110052">AV20110052-2020-11 SMALL SECTION C150X75,C125X65 ,L 100X100 อุดม</option>
                                                            <option value="AV20110053">AV20110053-2020-12 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20110054">AV20110054-2020-12 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20110055">AV20110055-2020-12 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20110056">AV20110056-2020-12 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20110057">AV20110057-2020-12 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20110058">AV20110058-2020-12 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20110059">AV20110059-2020-12 Q</option>
                                                            <option value="AV20110060">AV20110060-2020-12 Q-Plus</option>
                                                            <option value="AV20110061">AV20110061-2020-11 SMALL SECTION C150X75,C125X65 ศิริกุล</option>
                                                            <option value="AV20110062">AV20110062-2020-11 SMALL SECTION C100X50 ศิริกุล</option>
                                                            <option value="AV20110063">AV20110063-2020-11 SS - เบสท์สตีล LSS400 100x100</option>
                                                            <option value="AV20110064">AV20110064-2020-11 SS - ส.วิไล C125x65,C150x75</option>
                                                            <option value="AV20110065">AV20110065-2020-11 SMALL SECTION  C 100X50  TMT (17.6)</option>
                                                            <option value="AV20110066">AV20110066-2020-11 SMALL SECTION C150X75,C125X65 TMT (18.0)</option>
                                                            <option value="AV20110067">AV20110067-2020-11 SMALL SECTION C150X75 ,MITSUI- CSH (17.90)</option>
                                                            <option value="AV20110068">AV20110068-2020-11 SMALL SECTION C150X75,C125X65 เดอะสตีล</option>
                                                            <option value="AV20110069">AV20110069-2020-11 SMALL SECTION L 100X100 เดอะสตีล</option>
                                                            <option value="AV20110070">AV20110070-2020-11 SS - เบสท์สตีล C100x50</option>
                                                            <option value="AV20110071">AV20110071-2020-12 GROUP Q-PLUS VALUE SERIES SUMI</option>
                                                            <option value="AV20110072">AV20110072-2020-12 SUMI สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20110073">AV20110073-2020-12 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20110074">AV20110074-2020-11 SMALL SECTION C150X75,C125X65 TMT (18.6)</option>
                                                            <option value="AV20110075">AV20110075-2020-11 DB I 400X150- ยงเจริญชัย</option>
                                                            <option value="AV20110076">AV20110076-2020-12 SO SCG</option>
                                                            <option value="AV20110077">AV20110077-2020-11 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.2)</option>
                                                            <option value="AV20110078">AV20110078-2020-11 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20110079">AV20110079-2020-11 โปรแกรมพิเศษC250X90L SYS</option>
                                                            <option value="AV20110080">AV20110080-2020-12 MITSUI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20110081">AV20110081-2020-11 โปรแกรมพิเศษ C250X90L,MITSUI</option>
                                                            <option value="AV20110082">AV20110082-2020-11 โปรแกรมพิเศษ C250X90L,SUMI</option>
                                                            <option value="AV20110083">AV20110083-2020-11 C180X75,อุดม</option>
                                                            <option value="AV20110084">AV20110084-2020-12 GROUP Q-PLUS VALUE SERIES MITSUI</option>
                                                            <option value="AV20110085">AV20110085-2020-11 SC  C250x90L SCG</option>
                                                            <option value="AV20110086">AV20110086-2020-12 </option>
                                                            <option value="AV20110087">AV20110087-2020-12 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20110088">AV20110088-2020-11 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.3)</option>
                                                            <option value="AV20110089">AV20110089-2020-11 SMALL SECTION L100X100 ยงเจริญชัย</option>
                                                            <option value="AV20110090">AV20110090-2020-10 SMALL SECTION C150X75,C125X65 กวงฮั้ว (17.8)</option>
                                                            <option value="AV20110091">AV20110091-2020-11 SMALL SECTION C150X75,C125X65 กวงฮั้ว (18.0)</option>
                                                            <option value="AV20110092">AV20110092-2020-12 SUMI สินค้าสั่งพิเศษ ความยาวพิเศษไม่ถึง 10 ตัน</option>
                                                            <option value="AV20110094">AV20110094-2020-08 DB 2008   LEVEL 2-3 -บ้านโป่ง</option>
                                                            <option value="AV20110095">AV20110095-2020-11 SMALL SECTION C150X75 ,MITSUI- อุดม</option>
                                                            <option value="AV20120001">AV20120001-2020-12 SYS สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20120002">AV20120002-2020-11 MITSUI  สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20120003">AV20120003-2020-12 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20120004">AV20120004-2020-12 SUMII สินค้าพิเศษสต็อค</option>
                                                            <option value="AV20120005">AV20120005-2020-12 ST-SCG</option>
                                                            <option value="AV20120006">AV20120006-2020-12 SMALL SECTION C150X75,C125X65 ,L 100X100 เชื้อไพบูลย์</option>
                                                            <option value="AV20120007">AV20120007-2020-12 SMALL SECTION C150X75,C125X65 ยงเจริญชัย</option>
                                                            <option value="AV20120008">AV20120008-2020-12 SMALL SECTION C100X50 ยงเจริญชัย</option>
                                                            <option value="AV20120009">AV20120009-2020-12 SMALL SECTION C150X75,C125X65 TMT (18.0)</option>
                                                            <option value="AV20120010">AV20120010-2020-12 SMALL SECTION C150X75 ,MITSUI- CSH (18.00)</option>
                                                            <option value="AV20120011">AV20120011-2020-12 SS - เป็นเอก C125x65</option>
                                                            <option value="AV20120012">AV20120012-2020-12 Q-Plus - 2 เอส</option>
                                                            <option value="AV20120013">AV20120013-2020-12 GROUP Q  PLUS SUMI- TMT SM400</option>
                                                            <option value="AV20120014">AV20120014-2020-12 SMALL SECTION C150X75 ,MITSUI- อุดม (18.00)</option>
                                                            <option value="AV20120015">AV20120015-2020-12 SS - เตียฮงฮะ C100x50</option>
                                                            <option value="AV20120016">AV20120016-2020-12 SMALL SECTION C150X75  ยงเจริญชัย (18.00)</option>
                                                            <option value="AV20120017">AV20120017-2020-12 SMALL SECTION L100X100 ยงเจริญชัย (18.20)</option>
                                                            <option value="AV20120018">AV20120018-2020-12 SS - เบสท์สตีล  C125x65,C150x75</option>
                                                            <option value="AV20120019">AV20120019-2020-12 SS - เบสท์สตีล L100x100</option>
                                                            <option value="AV20120020">AV20120020-2020-12 SMALL SECTION C150X75,C125X65 นวสยาม</option>
                                                            <option value="AV20120021">AV20120021-2020-12 SMALL SECTION  C 100X50  TMT (17.6)</option>
                                                            <option value="AV20120022">AV20120022-2020-12 SS - เบสท์สตีล  C125x65,C150x75</option>
                                                            <option value="AV20120023">AV20120023-2020-12 SMALL SECTION L100X100 MITSUI- CSH</option>
                                                            <option value="AV20120024">AV20120024-2020-12 SS - เบสท์สตีล  L100x100</option>
                                                            <option value="AV20120025">AV20120025-2020-12 SMALL SECTION C150X75 ,MITSUI- CSH</option>
                                                            <option value="AV20120026">AV20120026-2020-12 DB 2020 ASPEN THE FORESTIAS บางนาตราด (REBATE 500)</option>
                                                            <option value="AV20120027">AV20120027-2020-12 SMALL SECTION C150X75,C125X65 TMT (18.3)</option>
                                                            <option value="AV20120028">AV20120028-2020-12 SMALL SECTION L100X7, 100X10, 100X12, 100X13  TMT (18.8)</option>
                                                            <option value="AV20120029">AV20120029-2020-03 GROUP Q  SUMI -ธนาสาร 7 M</option>
                                                            <option value="AV20120030">AV20120030-2020-12 SMALL SECTION C150X75,C125X65 เดอะสตีล</option>
                                                            <option value="AV20120031">AV20120031-2020-12 SYS สินค้าพิเศษสต็อค NEW</option>
                                                            <option value="AV20120032">AV20120032-2020-12 ST-SCG (new)</option>
                                                            <option value="AV20120033">AV20120033-2020-12 SUMII สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20120034">AV20120034-2020-12 SMALL SECTION L 100X7 เดอะสตีล (18.50)</option>
                                                            <option value="AV20120035">AV20120035-2020-12 SMALL SECTION C150X75,C125X65 เดอะสตีล (18.30)</option>
                                                            <option value="AV20120036">AV20120036-2020-12 MITSUI  สินค้าพิเศษสต็อค (NEW )</option>
                                                            <option value="AV20120038">AV20120038-2021-01 GROUP Q</option>
                                                            <option value="AV20120039">AV20120039-2021-01 GROUP Q-PLUS</option>
                                                            <option value="AV20120040">AV20120040-2021-01 GROUP Q SM520</option>
                                                            <option value="AV20120041">AV20120041-2021-01 GROUP Q-PLUS SM520</option>
                                                            <option value="AV20120042">AV20120042-2021-01 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20120043">AV20120043-2021-01 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20120044">AV20120044-2021-01 GROUP Q  MITSUI</option>
                                                            <option value="AV20120045">AV20120045-2021-01 GROUP Q  SUMI</option>
                                                            <option value="AV20120046">AV20120046-2021-01 GROUP Q  MITSUI SM520</option>
                                                            <option value="AV20120047">AV20120047-2021-01 GROUP Q  SUMI SM520</option>
                                                            <option value="AV20120048">AV20120048-2021-01 GROUP Q  PLUS MITSUI</option>
                                                            <option value="AV20120049">AV20120049-2021-01 GROUP Q  PLUS SUMI</option>
                                                            <option value="AV20120050">AV20120050-2021-01 GROUP Q  PLUS MITSUI SM520</option>
                                                            <option value="AV20120051">AV20120051-2021-01 GROUP Q  PLUS SUMI SM520</option>
                                                            <option value="AV20120052">AV20120052-2021-01 Q</option>
                                                            <option value="AV20120053">AV20120053-2021-01 Q-Plus</option>
                                                            <option value="AV20120054">AV20120054-2020-12 SMALL SECTION C 150X75 MITSUI- CSH</option>
                                                            <option value="AV20120055">AV20120055-2020-12 SMALL SECTION C150X75  ยงเจริญชัย (18.50)</option>
                                                            <option value="AV20120056">AV20120056-2020-12 SMALL SECTION C100X50 ยงเจริญชัย (18.10)</option>
                                                            <option value="AV20120057">AV20120057-2021-01 Q-Plus</option>
                                                            <option value="AV20120058">AV20120058-2021-01 SO SCG</option>
                                                            <option value="AV20120059">AV20120059-2020-12 SMALL SECTION C150X75,C125X65 ศิริกุล (18.60)</option>
                                                            <option value="AV20120060">AV20120060-2020-12 SMALL SECTION C150X75,C125X65 นวสยาม</option>
                                                            <option value="AV20120061">AV20120061-2020-12 SO SCG - สินค้าซิเมนต์ไทย ส่งให้</option>
                                                            <option value="AV20120062">AV20120062-2020-12 SMALL SECTION L 100X7 เดอะสตีล (18.80)</option>
                                                            <option value="AV20120063">AV20120063-2020-12 SMALL SECTION C150X75  เดอะสตีล (18.60)</option>
                                                            <option value="AV20120064">AV20120064-2021-01 GROUP Q-PLUS VALUE SERIES MITSUI</option>
                                                            <option value="AV20120065">AV20120065-2021-01 GROUP Q-PLUS VALUE SERIES MITSUI SM520</option>
                                                            <option value="AV20120066">AV20120066-2021-01 GROUP Q-PLUS VALUE SERIES SUMI</option>
                                                            <option value="AV20120067">AV20120067-2021-01 GROUP Q-PLUS VALUE SERIES SUMISM520</option>
                                                            <option value="AV20120068">AV20120068-2021-01 GROUP Q VALUE SERIES MITSUI  SM520</option>
                                                            <option value="AV20120069">AV20120069-2021-01 GROUP Q VALUE SERIES SUMI</option>
                                                            <option value="AV20120070">AV20120070-2021-01 GROUP Q VALUE SERIES SUMI  SM520</option>
                                                            <option value="AV20120071">AV20120071-2021-01 GROUP Q VALUE SERIES MITSUI  SM520</option>
                                                            <option value="AV20120072">AV20120072-2021-01 GROUP Q VALUE SERIES</option>
                                                            <option value="AV20120073">AV20120073-2020-11 GROUP Q VALUE SERIES SM520</option>
                                                            <option value="AV20120074">AV20120074-2021-01 GROUP Q VALUE SERIES SM520</option>
                                                            <option value="AV20120075">AV20120075-2021-01 GROUP Q PLUS VALUE SERIES  SM520</option>
                                                            <option value="AV20120076">AV20120076-2020-12 SC  C180X75 - TMT</option>
                                                            <option value="AV20120077">AV20120077-2020-12 SS - เป็นเอก C125x65 (18.6)</option>
                                                            <option value="AV20120078">AV20120078-2021-01 MITSUI  สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20120079">AV20120079-2021-01 SUMI สินค้าสั่งพิเศษ</option>
                                                            <option value="AV20120080">AV20120080-2021-01 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20120081">AV20120081-2020-12 SMALL SECTION C150X75 ,MITSUI- CSH</option>
                                                            <option value="AV20120082">AV20120082-2020-12 SMALL SECTION C100X50 ,MITSUI- CSH (18.1)</option>
                                                            <option value="AV20120083">AV20120083-2020-12 SMALL SECTION C150X75 ,MITSUI- CSH (18.5)</option>
                                                            <option value="AV20120084">AV20120084-2021-01 MITSUI สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV20120085">AV20120085-2021-01 DB 2101 ASPEN BANGNA - BKK MALL (R1500)</option>
                                                            <option value="AV20120086">AV20120086-2021-02 DB 2101 ASPEN BANGNA - BKK MALL (R1500)</option>
                                                            <option value="AV20120087">AV20120087-2020-12 โปรแกรมพิเศษC200X80,C200X90 SYS</option>
                                                            <option value="AV20120088">AV20120088-2020-12 SMALL SECTION C100X50 ,MITSUI- CSH (18.1)</option>
                                                            <option value="AV20120089">AV20120089-2020-12 SMALL SECTION L100X100 ,MITSUI- CSH (18.7)</option>
                                                            <option value="AV20120090">AV20120090-2020-12 SC  C200X90,C200x90 SCG</option>
                                                            <option value="AV20120091">AV20120091-2020-12 โปรแกรมพิเศษC200X80 , C200X90  MITSUI</option>
                                                            <option value="AV20120092">AV20120092-2020-12 SMALL SECTION C150X75  ยงเจริญชัย (18.70)</option>
                                                            <option value="AV20120093">AV20120093-2020-12 SS - ส.วิไล L100x100</option>
                                                            <option value="AV20120094">AV20120094-2021-01 GROUP Q PLUS VALUE SERIES</option>
                                                            <option value="AV20120095">AV20120095-2020-12 SMALL SECTION C150X75,C125X65 TMT (18.8)</option>
                                                            <option value="AV20120096">AV20120096-2020-12 โปรแกรมพิเศษC200X80 , C200X90</option>
                                                            <option value="AV20120097">AV20120097-2020-12 ST-SCG (new 25/12)</option>
                                                            <option value="AV20120098">AV20120098-2020-12 SYS สินค้าพิเศษสต็อค NEW  (25/12)</option>
                                                            <option value="AV20120099">AV20120099-2020-12 SMALL SECTION C100X50  ยงเจริญชัย (18.70)</option>
                                                            <option value="AV20120100">AV20120100-2020-12 MITSUI  สินค้าพิเศษสต็อค (NEW 25/2 )</option>
                                                            <option value="AV20120101">AV20120101-2020-12 SUMII สินค้าพิเศษสต็อค (NEW ) 25/12</option>
                                                            <option value="AV21010001">AV21010001-2021-01 SYS สินค้าพิเศษสต็อค NEW  (25/12)</option>
                                                            <option value="AV21010002">AV21010002-2021-01 MITSUI  สินค้าพิเศษสต็อค (NEW 25/12)</option>
                                                            <option value="AV21010003">AV21010003-2021-01 SUMI สินค้าพิเศษสต็อค (NEW ) 25/12</option>
                                                            <option value="AV21010004">AV21010004-2021-01 ST-SCG (new 25/12)</option>
                                                            <option value="AV21010005">AV21010005-2021-01 SMALL SECTION C125X65 ยงเจริญชัย (17.90)</option>
                                                            <option value="AV21010006">AV21010006-2020-12 DB2012  โกดังใช้เอง ผู้แทนจำหน่าย บจก.สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV21010007">AV21010007-2020-12 โกดังใช้เอง สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV21010008">AV21010008-2021-01 SMALL SECTION C125X65 อุดม (18.50)</option>
                                                            <option value="AV21010009">AV21010009-2021-01 SMALL SECTION C150X75 ,MITSUI- อุดม (18.50)</option>
                                                            <option value="AV21010010">AV21010010-2021-01 SMALL SECTION L100X100 ,MITSUI- อุดม (19.10)</option>
                                                            <option value="AV21010011">AV21010011-2021-01 SMALL SECTION C150X75,C125X65 กวงฮั้ว (19.0)</option>
                                                            <option value="AV21010012">AV21010012-2021-01 SS - บ้านโป่ง C100x50</option>
                                                            <option value="AV21010013">AV21010013-2021-01 SS - บ้านโป่ง C125x65</option>
                                                            <option value="AV21010014">AV21010014-2021-01 SMALL SECTION C100X50 ,MITSUI- อุดม (18.50)</option>
                                                            <option value="AV21010015">AV21010015-2021-01 SMALL SECTION C100X50  นวสยาม  (18.60)</option>
                                                            <option value="AV21010016">AV21010016-2021-01 SMALL SECTION C125X65 เดอะสตีล  (19.20)</option>
                                                            <option value="AV21010017">AV21010017-2021-01 SS -เบสท์สตีล C100x50</option>
                                                            <option value="AV21010018">AV21010018-2021-01 SMALL SECTION C125X65  โลหะเจริญ  (19.30)</option>
                                                            <option value="AV21010019">AV21010019-2021-01 SMALL SECTION C150X75,C125X65 TMT (20.0)</option>
                                                            <option value="AV21010020">AV21010020-2021-01 SMALL SECTION C125X65 ,MITSUI- CSH (19.30)</option>
                                                            <option value="AV21010021">AV21010021-2021-01 SMALL SECTION C125X65 ,MITSUI- CSH (19.60)</option>
                                                            <option value="AV21010022">AV21010022-2020-12 SYS สินค้าพิเศษสต็อค (ความยาวพิเศษไม่ถึง 10 ตัน 24.4)</option>
                                                            <option value="AV21010023">AV21010023-2021-01 GROUP Q  PLUS SUMI SM520 (ความยาวพิเศษน้อยกว่า 10 ตัน)</option>
                                                            <option value="AV21010024">AV21010024-2021-01 GROUP Q  SUMI SM520 (สั่งหลังปิดจอง ADV)</option>
                                                            <option value="AV21010025">AV21010025-2021-01 GROUP Q  SUMI SM520 (สั่งหลังปิดจอง ADV)(ความยาวพิเศษน้อยกว่า 10 ตัน)</option>
                                                            <option value="AV21010026">AV21010026-2021-01 SUMI สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV21010027">AV21010027-2021-01 SUMI สินค้าพิเศษสต็อค SM520 (ความยาวพิเศษน้อยกว่า 10 ตัน)</option>
                                                            <option value="AV21010028">AV21010028-2021-01 SS -ส.วิไล C125x65</option>
                                                            <option value="AV21010029">AV21010029-2021-01 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV</option>
                                                            <option value="AV21010030">AV21010030-2021-01 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV (1390 - 2560) อุดมโลหะกิจ</option>
                                                            <option value="AV21010031">AV21010031-2021-01 SMALL SECTION C125X65  เชื้อไพบูลย์  (19.80)</option>
                                                            <option value="AV21010032">AV21010032-2021-01 โปรแกรมพิเศษ SP-III,SP-IIIA,SP IV (1390 - 2560)  -  สตีลไลน์</option>
                                                            <option value="AV21010033">AV21010033-2021-01 SYS สินค้าพิเศษสต็อค NEW  (12/1)</option>
                                                            <option value="AV21010034">AV21010034-2021-01 ST-SCG (new 12/1)</option>
                                                            <option value="AV21010035">AV21010035-2021-01 MITSUI  สินค้าพิเศษสต็อค (NEW 12/1)</option>
                                                            <option value="AV21010036">AV21010036-2021-01 SUMI สินค้าพิเศษสต็อค (NEW ) 12/1</option>
                                                            <option value="AV21010037">AV21010037-2021-01 MITSUI  สินค้าพิเศษสต็อค</option>
                                                            <option value="AV21010038">AV21010038-2021-01 SMALL SECTION  C 150X75, C 125X65 TCS (20.60)</option>
                                                            <option value="AV21010039">AV21010039-2021-01 SS -ส.วิไล C150x75</option>
                                                            <option value="AV21010040">AV21010040-2021-02 GROUP Q รอบที่ 1</option>
                                                            <option value="AV21010041">AV21010041-2021-02 GROUP Q-PLUS รอบที่ 1</option>
                                                            <option value="AV21010042">AV21010042-2021-01 SMALL SECTION C125X65  นวสยาม  (20.01)</option>
                                                            <option value="AV21010043">AV21010043-2021-01 SMALL SECTION C125X65,C150X75 ยงเจริญชัย (20.00)</option>
                                                            <option value="AV21010044">AV21010044-2021-01 SMALL SECTION L100X100 ยงเจริญชัย (20.20)</option>
                                                            <option value="AV21010045">AV21010045-2021-01 SS -เบสท์สตีล C100x50</option>
                                                            <option value="AV21010046">AV21010046-2021-01 SC  C180X75 - เบสท์สตีล</option>
                                                            <option value="AV21010047">AV21010047-2021-01 SMALL SECTION L100X100 SUMI- โลหะเจริญ</option>
                                                            <option value="AV21010048">AV21010048-2021-01 SC -เบสท์สตีล C180x75</option>
                                                            <option value="AV21010049">AV21010049-2021-01 SMALL SECTION L100X100 ยงเจริญชัย (20.20)</option>
                                                            <option value="AV21010050">AV21010050-2021-01 SMALL SECTION C125X65  โลหะเจริญ  (20.10)</option>
                                                            <option value="AV21010051">AV21010051-2021-01 GROUP Q  SUMI SM520 (ความยาวพิเศษน้อยกว่า 10 ตัน)</option>
                                                            <option value="AV21010052">AV21010052-2021-01 DB2101  SIRIKIT CONVENTION HALL โลหะไพศาล</option>
                                                            <option value="AV21010053">AV21010053-2021-01 SMALL SECTION C150X75 อุดม (20.60)</option>
                                                            <option value="AV21010054">AV21010054-2021-01 SYS สินค้าพิเศษสต็อค NEW  (18/1)</option>
                                                            <option value="AV21010055">AV21010055-2021-02 GROUP Q รอบที่ 2</option>
                                                            <option value="AV21010057">AV21010057-2021-01 SMALL SECTION C150X75 ,MITSUI- อุดม (20.60)</option>
                                                            <option value="AV21010058">AV21010058-2021-02 SYS สินค้าสั่งพิเศษ</option>
                                                            <option value="AV21010059">AV21010059-2021-02 SYS สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV21010060">AV21010060-2021-02 GROUP Q  MITSUI รอบที่ 1</option>
                                                            <option value="AV21010061">AV21010061-2021-02 GROUP Q  SUMI รอบที่ 1</option>
                                                            <option value="AV21010062">AV21010062-2021-01 ST-SCG (new 18/1)</option>
                                                            <option value="AV21010063">AV21010063-2020-12 SO SCG</option>
                                                            <option value="AV21010064">AV21010064-2021-02 SO SCG</option>
                                                            <option value="AV21010065">AV21010065-2021-02 Q รอบที่ 1</option>
                                                            <option value="AV21010066">AV21010066-2021-02 Q-Plus รอบที่ 1</option>
                                                            <option value="AV21010067">AV21010067-2021-01 SMALL SECTION C125X65 ,C150X75 นวสยาม  (20.60)</option>
                                                            <option value="AV21010068">AV21010068-2021-02 GROUP Q  PLUS MITSUI รอบที่ 1</option>
                                                            <option value="AV21010069">AV21010069-2021-01 DB2101 SIRIKIT CONVENTION CENTER โลหะไพศาล</option>
                                                            <option value="AV21010070">AV21010070-2021-01 MITSUI  สินค้าพิเศษสต็อค (NEW 18/1)</option>
                                                            <option value="AV21010071">AV21010071-2021-02 MITSUI  สินค้าสั่งพิเศษ รอบที่ 1</option>
                                                            <option value="AV21010072">AV21010072-2021-02 SUMI สินค้าสั่งพิเศษ รอบที่ 1</option>
                                                            <option value="AV21010073">AV21010073-2021-01 SUMI สินค้าพิเศษสต็อค (NEW ) 18/1</option>
                                                            <option value="AV21010074">AV21010074-2021-02 GROUP Q  PLUS SUMI รอบที่ 1</option>
                                                            <option value="AV21010075">AV21010075-2021-01 DB2101  SIRIKIT CONVENTION HALL (SIDE TRUSS ) (โซคอน) โลหะไพศาล</option>
                                                            <option value="AV21010076">AV21010076-2020-12 Q - สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV21010077">AV21010077-2020-12 Q-Plus - สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV21010078">AV21010078-2021-01 SMALL SECTION C125X65 ,C150X75 โลหะเจริญ  (20.60)</option>
                                                            <option value="AV21010079">AV21010079-2021-01 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV21010080">AV21010080-2021-01 SMALL SECTION L 100X100 กวงฮั้ว (20.80)</option>
                                                            <option value="AV21010081">AV21010081-2021-01 SMALL SECTION C150X75,C125X65 TMT (21.10)</option>
                                                            <option value="AV21010082">AV21010082-2021-01 SC  C180X75 - TMT (26.10)</option>
                                                            <option value="AV21010083">AV21010083-2020-11 โปรแกรมพิเศษ SP-II, SP-III, SP-IIIA, SP IV - เชื้อไพบูลย์</option>
                                                            <option value="AV21010084">AV21010084-2021-01 SMALL SECTION L 100X100 TMT (20.80)</option>
                                                            <option value="AV21010085">AV21010085-2021-01 SMALL SECTION C 125X65 กวงฮั้ว (20.60)</option>
                                                            <option value="AV21010086">AV21010086-2021-01 SMALL SECTION C150X75,C125X65 TMT (20.60)</option>
                                                            <option value="AV21010087">AV21010087-2021-02 GROUP Q-PLUS  SM520 รอบที่ 1</option>
                                                            <option value="AV21010088">AV21010088-2021-02 GROUP Q  PLUS  SM520 SUMI รอบที่ 1</option>
                                                            <option value="AV21010089">AV21010089-2021-02 GROUP Q  PLUS  SM520 MITSUII รอบที่ 1</option>
                                                            <option value="AV21010090">AV21010090-2020-12 DB2012  โกดังใช้เอง ผู้แทนจำหน่าย บจก.สินค้าซิเมนต์ไทย</option>
                                                            <option value="AV21010091">AV21010091-2021-02 DB2102  โกดังใช้เอง ผู้แทนจำหน่าย บจก ไทยพิพัฒน์ฮาร์ดแวร์</option>
                                                            <option value="AV21010092">AV21010092-2021-02 GROUP Q  PLUS  SM400 MITSUII -CSH</option>
                                                            <option value="AV21010093">AV21010093-2021-02 DB2102  SIRIKIT</option>
                                                            <option value="AV21010094">AV21010094-2021-04 DB2104  SIRIKIT</option>
                                                            <option value="AV21010095">AV21010095-2021-02 GROUP Q-PLUS VALUE SERIES MITSUI</option>
                                                            <option value="AV21010096">AV21010096-2021-02 GROUP Q-PLUS VALUE SERIES SUMI</option>
                                                            <option value="AV21010097">AV21010097-2021-02 MITSUI สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV21010098">AV21010098-2021-02 SUMI  สินค้าสั่งพิเศษ SM520</option>
                                                            <option value="AV21010099">AV21010099-2021-04 DB2104  SIRIKIT</option>
                                                            <option value="AV21010100">AV21010100-2021-02 GROUP Q  MITSUI SM520 รอบที่ 1</option>
                                                            <option value="AV21010101">AV21010101-2021-02 GROUP Q  SUMI SM520 รอบที่ 1</option>
                                                            <option value="AV21010102">AV21010102-2021-01 SMALL SECTION L100X100 เดอะสตีล  (20.70)</option>
                                                            <option value="AV21010103">AV21010103-2021-01 SMALL SECTION L100X100 ยงเจริญชัย (20.70)</option>
                                                            <option value="AV21010104">AV21010104-2021-01 SMALL SECTION C100X50  ยงเจริญชัย (20.30)</option>
                                                            <option value="AV21010105">AV21010105-2021-01 SMALL SECTION C150X75  ยงเจริญชัย (20.50)</option>
                                                            <option value="AV21010106">AV21010106-2021-01 </option>
                                                            <option value="AV21010107">AV21010107-2021-01 SS -เป็นเอก  C150x75,C125x65 +ค่าขนส่ง</option>
                                                            <option value="AV21010108">AV21010108-2021-12 ## TEST SET REBATE BY CUSTOMER ##</option>
                                                            <option value="AV21010109">AV21010109-2021-12 ## TEST SET REBATE BY CUSTOMER ##</option>
                                                            <option value="AV21010110">AV21010110-2021-01 ST-SCG (new 18/1) เป็นเอก +ค่าขนส่ง</option>
                                                            <option value="AV21010111">AV21010111-2021-02 GROUP Q  PLUS  SM520 SUMI รอบที่ 1 (ความยาวพิเศษไม่ถึง 10 ตัน)</option>
                                                            <option value="AV21020001">AV21020001-2021-02 SYS สินค้าพิเศษสต็อค NEW  (18/1)</option>
                                                            <option value="AV21020002">AV21020002-2021-02 ST-SCG (new 18/1)</option>
                                                            <option value="AV21020003">AV21020003-2021-02 MITSUI  สินค้าพิเศษสต็อค (NEW 18/1)</option>
                                                            <option value="AV21020004">AV21020004-2021-02 SUMI สินค้าพิเศษสต็อค (NEW ) 18/1</option>
                                                            <option value="AV21020005">AV21020005-2021-01 SP-II, SP-III, SP-IIIA, SP IV SCG</option>
                                                            <option value="AV21020006">AV21020006-2021-02 SMALL SECTION L 100X100 TMT (20.80)</option>
                                                            <option value="AV21020007">AV21020007-2021-02 SC -เบสท์สตีล C180x75</option>
                                                            <option value="AV21020008">AV21020008-2021-02 DB2102 ศูนย์ประชุมแห่งชาติสิริกิติ์ - เป็นเอก</option>
                                                            <option value="AV21020009">AV21020009-2021-02 DB 2102  INDORAMA สินกิจ</option>
                                                            <option value="AV21020010">AV21020010-2021-02 DB2102 C180X75 - TMT (26.10)</option>
                                                            <option value="AV21020011">AV21020011-2021-02 GROUP Q-PLUS รอบที่ 2</option>
                                                            <option value="AV21020012">AV21020012-2021-04 DB2104  SIRIKIT</option>
                                                            <option value="AV21020013">AV21020013-2021-02 GROUP Q  MITSUI รอบที่ 2</option>
                                                            <option value="AV21020014">AV21020014-2021-02 GROUP Q  SUMI รอบที่ 2</option>
                                                            <option value="AV21020015">AV21020015-2021-02 GROUP Q  PLUS MITSUI รอบที่ 2</option>
                                                            <option value="AV21020016">AV21020016-2021-02 GROUP Q  PLUS SUMI รอบที่ 2</option>
                                                            <option value="AV21020017">AV21020017-2021-02 SYS สินค้าสั่งพิเศษ รอบ2</option>
                                                            <option value="AV21020018">AV21020018-2021-02 MITSUI  สินค้าสั่งพิเศษ รอบ 2</option>
                                                            <option value="AV21020019">AV21020019-2021-02 SUMI  สินค้าสั่งพิเศษ รอบ 2</option>
                                                            <option value="AV21020020">AV21020020-2021-02 SUMI  สินค้าสั่งพิเศษ SM520 รอบที่ 2</option>
                                                            <option value="AV21020021">AV21020021-2021-02 MITSUI  สินค้าสั่งพิเศษ SM520 รอบที่ 2</option>
                                                            <option value="AV21020022">AV21020022-2021-02 Q รอบที่ 2</option>
                                                            <option value="AV21020023">AV21020023-2021-02 Q-Plus รอบที่ 2</option>
                                                            <option value="AV21020024">AV21020024-2021-02 SO SCG รอบที่ 2</option>
                                                            <option value="AV21020025">AV21020025-2021-02 GROUP Q  PLUS  SM520 MITSUI รอบที่ 2</option>
                                                            <option value="AV21020026">AV21020026-2021-02 GROUP Q  PLUS  SM520 SUMII รอบที่ 2</option>
                                                            <option value="AV21020027">AV21020027-2021-02 GROUP Q  MITSUI SM520 รอบที่ 2</option>
                                                            <option value="AV21020028">AV21020028-2021-02 GROUP Q  SUMII SM520 รอบที่ 2</option>
                                                            <option value="AV21020029">AV21020029-2021-02 SMALL SECTION C150X75  ยงเจริญชัย (20.50)</option>
                                                            <option value="AV21020030">AV21020030-2021-02 GROUP Q SM520 รอบที่ 2</option>
                                                            <option value="AV21020031">AV21020031-2021-02 GROUP Q-PLUS SM520 รอบที่ 2</option>
                                                            <option value="AV21020032">AV21020032-2021-02 GROUP Q VALUE SERIES</option>
                                                            <option value="AV21020033">AV21020033-2021-02 GROUP Q VALUE SERIES SUMI</option>
                                                            <option value="AV21020034">AV21020034-2021-02 GROUP Q VALUE SERIES MITSUI</option>
                                                            <option value="AV21020035">AV21020035-2021-02 SYS สินค้าสั่งพิเศษ SM520 รอบ 2</option>
                                                            <option value="AV21020036">AV21020036-2021-02 SS -เบสท์สตีล L100x100</option>
                                                            <option value="AV21020037">AV21020037-2021-02 SMALL SECTION C150X75  เชื้อไพบูลย์  (20.50)</option>
                                                            <option value="AV21020038">AV21020038-2021-02 GROUP Q VALUE SERIES SM520</option>
                                                            <option value="AV21020039">AV21020039-2021-02 GROUP Q รอบที่ 2 TCS</option>
                                                            <option value="AV21020040">AV21020040-2021-02 GROUP Q-PLUS รอบที่ 2 TCS</option>
                                                            <option value="AV21020041">AV21020041-2021-02 GROUP Q SM520 รอบที่ 2 TCS</option>
                                                            <option value="AV21020042">AV21020042-2021-02 GROUP Q-PLUS SM520 รอบที่ 2 TCS</option>
                                                            <option value="AV21020043">AV21020043-2021-02 SMALL SECTION C150X75,C125X65 TMT (20.60)</option>
                                                            <option value="AV21020044">AV21020044-2021-02 MITSUI สินค้าพิเศษสต็อค SM520</option>
                                                            <option value="AV21020045">AV21020045-2021-02 SMALL SECTION C125X65  อุดม  (20.60)</option>
                                                            <option value="AV21030001">AV21030001-2021-03 GROUP Q</option>
                                                            <option value="AV21030002">AV21030002-2021-03 GROUP Q-PLUS</option>
                                                            <option value="AV21040001">AV21040001-2021-04 </option>
                                                            <option value="AV21040002">AV21040002-2021-04 ทดสอบ ADV QQQ</option>
                                                            <option value="AV21040003">AV21040003-2021-04 TRADER ทดสอบบบบบบ</option>
                                                            <option value="AV21040004">AV21040004-2021-04 </option>
                                                            <option value="AV21070001">AV21070001-2021-01 GROUP Q</option>
                                                            <option value="AV21070002">AV21070002-2021-01 GROUP Q</option>
                                                            <option value="AV21070003">AV21070003-2021-01 GROUP Q</option>
                                                        </select>
                                                    <p className="text-c-red">	 กรณี ชุดสุดคุ้มไม่ต้องเลือก New ADV.Set</p>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-3">NEW CASH DISC</Form.Label>
                                                    <Col sm={9}>
                                                        <div className="form-check-inline w-100">
                                                        <input type="text" disabled defaultValue="1.3" className="mr-1 w-100" />
                                                        <Button size="sm" className="mr-1 w-100" variant="success">CASH 1.5</Button>                                                        
                                                    <Button size="sm" className="w-100" variant="success">SET OCW</Button>
                                                    </div>
                                                    </Col>
                                                   
                                                </Form.Group>
                                            </Col>

                                        </Form.Group>
                                    </Tab>
                                </Tabs>


                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalEdit: false })}>Close</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal backdrop="static" size="xl" show={this.state.isModalAttention} onHide={() => this.setState({ isModalAttention: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalAttention}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ATTENTION</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>CODE</th>
                                            <th>CUSTOMER</th>
                                            <th>ATTENTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className="cursor" onClick={() => this.setState({ isModalAttention: false })}>
                                            <td align="center">1</td>
                                            <td align="center">3001574 </td>
                                            <td align="center">บ. ช.การช่าง จก. (มหาชน) </td>
                                            <td align="left">Sales Representative</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAttention: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal backdrop="static" size="xl" show={this.state.isModalSupplier} onHide={() => this.setState({ isModalSupplier: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalSupplier}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Department</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="department" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SAP ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Manager ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>หน่วยงาน</th>
                                            <th>TYPE</th>
                                            <th>COUNTRY</th>
                                            <th>CODE</th>
                                            <th>PROJECT SUPPLIER'S NAME</th>
                                            <th>SYSTEM.CODE</th>
                                            <th>UPDATE DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colSpan="8"></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalSupplier: false })}>Close</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal id="modelUser" backdrop="static" size="xl" show={this.state.isModalUser} onHide={() => this.setState({ isModalUser: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalUser}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Department</Form.Label>
                                            <Col sm={8}>
                                                <Dropdown type="department" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SAP ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>USER's Name</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Manager ID</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right" sm>
                                        <Button size="sm" variant="primary">ค้นหา</Button>
                                    </Col>
                                </Form.Group>
                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>DEPARTMENT</th>
                                            <th>USER ID</th>
                                            <th>USER'S NAME</th>
                                            <th>MANAGER ID</th>
                                            <th>EMAIL</th>
                                            <th>TEL</th>
                                            <th>FAX</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className="cursor" onClick={() => this.setState({ isModalUser: false })}>
                                            <td >1</td>
                                            <td >Marketing&nbsp;</td>
                                            <td >2007&nbsp;</td>
                                            <td align="left">&nbsp;Rattipun T</td>
                                            <td >0912&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalUser: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal backdrop="static" size="xl" show={this.state.isModalAdd} onHide={() => this.setState({ isModalAdd: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เลขที่</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" placeholder="เลขที่ใบเสนอราคา" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Sales Person</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="2007-Rattipun T" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "User")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Status</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" defaultValue="เปิดเอกสาร" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ลูกค้า</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" defaultValue="3001574 ช.การช่าง TH" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">Type</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="-">ไม่ระบุ</option>
                                                    <option value="N">Project</option>
                                                    <option value="Y">Re-Export</option>
                                                    <option value="S">STOCK</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่</Form.Label>
                                            <Col sm={8}>
                                                <input type="Date" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ชื่อโครงการ</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ประเภทโครงการ</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="NONE">None</option><option value="0005">-NAME-</option><option value="0003">PROJECT</option><option value="0002">RE-EXPORT</option><option value="0001">SOLUTION</option><option value="0004">STOCK</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"> วิธีการชำระเงิน</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit">
                                                    <option value="NONE">None</option><option value="NT60">เงินเชื่อ 60 วัน</option><option value="NT00">เงินสด</option>
                                                </select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เจ้าของ</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">หน่วยงาน</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-edit" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8}>
                                                <input type="checkbox" className="mr-1" />
                                                <Form.Label>เสนอราคาพิเศษ</Form.Label>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ผู้รับเหมา</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="None" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "SUPPLIER")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">ปี-เดือน</Form.Label>
                                            <Col sm={3}>
                                                <DropdownEdit type="years" />
                                            </Col>
                                            <Col sm={5}><DropdownEdit type="month" /></Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">เงื่อนไขการขนส่ง</Form.Label>
                                            <Col sm={6}>
                                                <select className="form-control-edit"><option value="NONE">None</option><option value="CFR">Costs and freight</option><option value="EXW">Ex works</option></select>
                                            </Col>
                                            <Col sm={2} className="text-right">
                                                <Button className="btn-sm mb-sm-1" onClick={() => this.setState({ isBasic: !isBasic })}>...</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Collapse in={this.state.isBasic}>
                                    <Row>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">attention</Form.Label>
                                                <Col sm={8}>
                                                    <InputGroup>
                                                        <FormControl size="sm" className="form-control-edit" defaultValue="Sales Representative" disabled />
                                                        <InputGroup.Append>
                                                            <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "Attention")}>
                                                                <div className="mt--4px">
                                                                    <i className="feather icon-search"></i>
                                                                </div>
                                                            </Button>
                                                        </InputGroup.Append>
                                                    </InputGroup>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> TEL no.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> FAX no.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ซื้อสำหรับ</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">สถานที่ส่งสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="3000028">กรุงเทพฯ คลองเตย</option><option value="3000000">กรุงเทพฯ คลองสาน</option><option value="3000038">กรุงเทพฯ คลองสามวา</option><option value="3000039">กรุงเทพฯ คันนายาว</option><option value="3000027">กรุงเทพฯ จตุจักร</option><option value="3000032">กรุงเทพฯ จอมทอง</option><option value="3000025">กรุงเทพฯ ดอนเมือง</option><option value="3000037">กรุงเทพฯ ดินแดง</option><option value="3000001">กรุงเทพฯ ดุสิต</option><option value="3000002">กรุงเทพฯ ตลิ่งชัน</option><option value="3000040">กรุงเทพฯ ทวีวัฒนา</option><option value="3000041">กรุงเทพฯ ทุ่งครุ</option><option value="3000003">กรุงเทพฯ ธนบุรี</option><option value="3000008">กรุงเทพฯ บางเขน</option><option value="3000042">กรุงเทพฯ บางแค</option><option value="3000005">กรุงเทพฯ บางกอกใหญ่</option><option value="3000004">กรุงเทพฯ บางกอกน้อย</option><option value="3000006">กรุงเทพฯ บางกะปิ</option><option value="3000007">กรุงเทพฯ บางขุนเทียน</option><option value="3000035">กรุงเทพฯ บางคอแหลม</option><option value="3000024">กรุงเทพฯ บางซื่อ</option><option value="3000043">กรุงเทพฯ บางนา</option><option value="3000044">กรุงเทพฯ บางบอน</option><option value="3000030">กรุงเทพฯ บางพลัด</option><option value="3000009">กรุงเทพฯ บางรัก</option><option value="3000031">กรุงเทพฯ บึงกุ่ม</option><option value="3000010">กรุงเทพฯ ปทุมวัน</option><option value="3000026">กรุงเทพฯ ประเวศ</option><option value="3000011">กรุงเทพฯ ป้อมปราบศัตรูพ่าย</option><option value="3000012">กรุงเทพฯ พญาไท</option><option value="3000013">กรุงเทพฯ พระโขนง</option><option value="3000014">กรุงเทพฯ พระนคร</option><option value="3000015">กรุงเทพฯ ภาษีเจริญ</option><option value="3000016">กรุงเทพฯ มีนบุรี</option><option value="3000017">กรุงเทพฯ ยานนาวา</option><option value="3000033">กรุงเทพฯ ราชเทวี</option><option value="3000018">กรุงเทพฯ ราษฎร์บูรณะ</option><option value="3000019">กรุงเทพฯ ลาดกระบัง</option><option value="3000029">กรุงเทพฯ ลาดพร้าว</option><option value="3000045">กรุงเทพฯ วังทองหลาง</option><option value="3000046">กรุงเทพฯ วัฒนา</option><option value="3000036">กรุงเทพฯ สวนหลวง</option><option value="3000047">กรุงเทพฯ สะพานสูง</option><option value="3000020">กรุงเทพฯ สัมพันธวงศ์</option><option value="3000034">กรุงเทพฯ สาทร</option><option value="3000048">กรุงเทพฯ สายไหม</option><option value="3000021">กรุงเทพฯ หนองแขม</option><option value="3000022">กรุงเทพฯ หนองจอก</option><option value="3000049">กรุงเทพฯ หลักสี่</option><option value="3000023">กรุงเทพฯ ห้วยขวาง</option><option value="3000168">อยุธยา บางปะอิน</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4"> สถานที่รับสินค้า</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">ไม่ระบุ</option><option value="4951">BDC - บ้านบึง</option><option value="4941">SOL - SYS Solution</option><option value="4921">SR - ศรีราชา</option><option value="4911">SYS1 - ระยอง1</option><option value="4931">SYS2 - ระยอง2</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">SalesOrg</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Chartering</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NON">None</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Currency <span className="text-c-red">*</span> </Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit"><option value="NONE">None</option><option value="AUD">AUD - Australian Dollars</option><option value="EUR">EUR - European Currency Unit</option><option value="MYR">MYR - Malaysian Ringgit</option><option value="THB">THB - Thai Baht</option><option value="USD">USD - US Dollars</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Channel</Form.Label>
                                                <Col sm={8}>
                                                    <select className="form-control-edit" disabled><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option></select>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ประเทศปลายทาง</Form.Label>
                                                <Col sm={8}>
                                                    <DropdownEdit type="country" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">F/X Rate</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <input type="text" className="form-control-edit mr-1" placeholder="(THB)" />
                                                        <input type="text" className="form-control-edit" placeholder="(USD)" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Validity From</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <input type="Date" className="form-control-edit mr-1" /> :
                                                        <input type="Date" className="form-control-edit ml-1" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">PO.Ref.</Form.Label>
                                                <Col sm={9}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">วันที่ต้องการรับสินค้า	</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                        <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                        <DropdownEdit type="month" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                <Col sm={4}>
                                                    <input type="Date" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">ถึง	</Form.Label>
                                                <Col sm={8}>
                                                    <div className="form-check-inline w-100">
                                                        <select className="form-control-edit mr-1"><option value="1">1-Early</option><option value="2">2-Mid</option><option value="3">3-End</option><option value="4">4-User date</option></select>
                                                        <DropdownEdit type="years" /> <span className="mr-1"></span>
                                                        <DropdownEdit type="month" />
                                                    </div>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">User Date	</Form.Label>
                                                <Col sm={4}>
                                                    <input type="Date" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">Reason of Rev.</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-3">Reason of Cancel</Form.Label>
                                                <Col sm={4}>
                                                    <input type="text" className="form-control-edit" />
                                                </Col>
                                                <Col sm={5} className="mt--4px text-right">
                                                    <Button size="sm" variant="danger" className="mr-2">SET CANCEL</Button>
                                                    <Button size="sm" variant="danger">SET CLOSE</Button>
                                                </Col>
                                            </Form.Group>
                                        </Col>

                                    </Row>


                                </Collapse>
                                <Row className="mt-1">
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">KEY IN UNIT</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">SALES UNIT</Form.Label>
                                            <Col sm={8}>
                                                <select className="form-control-edit"><option value="TON">1-MT</option><option value="PC">2-PC</option><option value="ST">3-SET</option><option value="KG">4-KG</option><option value="JOB">5-JOB</option><option value="PK">6-PACKAGE</option></select>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8} className="mt--4px text-right">
                                                <Button size="sm" variant="primary" className="mr-2">1.TO INVOICE</Button>
                                                <Button size="sm" variant="primary">2.TO ORDER</Button>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>



                                <br /><br />
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                    <Col className="text-right col-sm-7" sm>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-100">SAVE</Button>
                                        <Button size="sm" variant="success" className="mr-sm-1 wid-150">SHOW SAVE AS</Button>
                                        <Button size="sm" variant="primary" className="mr-sm-1 wid-100">SAVE REV</Button>
                                        <Button size="sm" variant="primary" className="mr-sm-1 wid-100">MORE...</Button>
                                        <Button size="sm" variant="warning">PRINT QUOTA.</Button>
                                    </Col>
                                </Form.Group>

                                <Col sm={12}>
                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" defaultActiveKey="size" className="form-control-file">
                                            <Tab eventKey="size" title="SIZE">
                                                <Row className="mt-2">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">CASH DISC.</Form.Label>
                                                            <Col className="col-sm-8" sm>
                                                                <div className="form-check-inline">
                                                                    <input type="text" className="form-control-edit" />
                                                                    <Button size="sm" variant="primary" className="mr-sm-1 wid-100">
                                                                        <span className="f-12 mt--4px">CAL.</span>
                                                                    </Button>
                                                                    <Button size="sm" variant="primary" className="wid-100 ">
                                                                        <span className="f-12 mt--4px">START</span>
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={6}>

                                                    </Col>
                                                </Row>
                                                <Tabs variant="pills" defaultActiveKey="ORDERVIEW" className="form-group float-sm-right">
                                                    <Tab eventKey="ORDERVIEW" title="ORDER VIEW">
                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>SIZE</th>
                                                                    <th>Min Ton</th>
                                                                    <th>ปริมาณ ท่อน</th>
                                                                    <th>ราคาเชื่อ บาท</th>
                                                                    <th>/หน่วย</th>
                                                                    <th>Rebate บาท</th>
                                                                    <th>ราคา บาท</th>
                                                                    <th>เป็นจำนวนเงิน บาท</th>
                                                                    <th>กำหนดส่ง โดยประมาณ</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="12" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="NOMINALVIEW" title="NOMINAL VIEW">
                                                        <br /><br />
                                                        <Row className="mt-2">
                                                            <Col sm={5}></Col>
                                                            <Col sm={7}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={5}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-5">PRODUCT GRP</Form.Label>
                                                                            <Col sm={7}>
                                                                                <select className="form-control-edit"><option value="NONE">None</option><option value="PJ19080002">B-ASTM A6/A6M:2003</option><option value="PJ19100001">B-BS EN 10034:1993</option><option value="PJ19080001">B-TIS 1227:2558 (2015)</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={5}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-3">GRADE.</Form.Label>
                                                                            <Col sm={9}>
                                                                                <select className="form-control-edit"><option value="NONE">None</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">test</option><option value="ๅๅ">ๅๅ</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={2}>
                                                                        <Button className="f-12 w-100" size="sm" variant="success">เพิ่ม</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Row>

                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>SIZE.STD.</th>
                                                                    <th>PRODUCT GRP.</th>
                                                                    <th>GRADE</th>
                                                                    <th>EX.PRICE</th>
                                                                    <th>PRICE</th>
                                                                    <th>SEQ</th>
                                                                    <th>UPDATED DATE</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="9" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="PLANVIEW" title="PLAN VIEW">
                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th><Form.Check id="tb-group" /></th>
                                                                    <th>#</th>
                                                                    <th>PRODUCT SIZE</th>
                                                                    <th>SIZE.STD/GRADE STD.</th>
                                                                    <th>ปริมาณ ตัน</th>
                                                                    <th>ปริมาณ ท่อน</th>
                                                                    <th>PLAN</th>
                                                                    <th>วันที่ต้องการ</th>
                                                                    <th>กำหนดส่ง โดยประมาณ</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="11" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                    <Tab eventKey="PSIVIEW" title="PSI VIEW">
                                                        <br /><br />
                                                        <Row className="mt-2">
                                                            <Col sm={12}>
                                                                <Form.Group as={Row}>
                                                                    <Col sm={7}>
                                                                        <div className="form-check-inline">
                                                                            <Button className="f-12 mr-1" size="sm" variant="danger">CLEAR ออกจาก GROUP ทุกรายการ</Button>
                                                                            <Button className="f-12" size="sm" variant="success">ขออนุมัติต่ออายุ POSSIBILITY</Button>

                                                                        </div>

                                                                    </Col>
                                                                    <Col sm={3}>
                                                                        <Form.Group as={Row}>
                                                                            <Form.Label className="col-sm-3">OPTION</Form.Label>
                                                                            <Col sm={9}>
                                                                                <select className="form-control-edit"><option value="SIZE">BY SIZE</option><option value="PN">BY PRODUCT</option></select>
                                                                            </Col>
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col sm={2}>
                                                                        <Button className="f-12 w-100" size="sm" variant="success">เข้า GROUP ทุกรายการ</Button>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Row>

                                                        <Table ref="tbl" striped hover responsive bordered >
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>รายละเอียดสินค้า</th>
                                                                    <th>SIZE STD. GRADE STD.</th>
                                                                    <th>ท่อน[ตัน]</th>
                                                                    <th>คงเหลือ ท่อน[ตัน]</th>
                                                                    <th>PSI GROUP</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colSpan="6" className="text-center">
                                                                        <label > No Data.</label>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </Tab>
                                                </Tabs>

                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="REBATE" title="REBATE" >
                                                <Row className="mt-1">
                                                    <Col sm={6} >
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-5">ระบุชื่อ REBATE ที่ต้องการสร้างใหม่</Form.Label>
                                                            <Col sm={7}>
                                                                <input type="text" className="form-control-edit" />

                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={6}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="mt--4px text-right">
                                                                <Button size="sm" variant="success" className="mr-1">1.NEW หักหน้าตั๋ว</Button>
                                                                <Button size="sm" variant="success">2.NEW หักภายหลัง</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered>
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>CODE</th>
                                                            <th>REBATE'S NAME</th>
                                                            <th>RATE</th>
                                                            <th>AMOUNT</th>
                                                            <th>TYPE</th>
                                                            <th>CHANNEL</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="8" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-plus-circle" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-success" ><span className="feather icon-save" /></Button>
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-danger" ><span className="feather icon-trash-2" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="QUOTATION" title="QUOTATION" >
                                                <Row className="mt-2">
                                                    <Col sm={3}></Col>
                                                    <Col sm={9}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-8">สร้าง QUOTATION จาก REV</Form.Label>
                                                                    <Col sm={4}>
                                                                        <select className="form-control-edit"><option value="">0</option></select>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-5">ย้อนดู REV เดิม</Form.Label>
                                                                    <Col sm={7}>
                                                                        <select className="form-control-edit"><option value="NONE">None</option></select>
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                            <Col sm={4}>
                                                                <Form.Group as={Row}>
                                                                    <Form.Label className="col-sm-4">QUOTA.NO</Form.Label>
                                                                    <Col sm={8}>
                                                                        <input type="text" className="form-control-edit" disabled defaultValue="QUD2100014-0" />
                                                                    </Col>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Col sm={12} className="text-right">
                                                                <Button className="mr-1 f-12 wid-70" size="sm" variant="primary">OPEN</Button>
                                                                <Button className="mr-1 f-12 wid-70" size="sm" variant="success">SAVE</Button>
                                                                <Button className="f-12 wid-70" size="sm" variant="danger">REMOVE</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={9} >
                                                        <Form.Group as={Row}>
                                                            <Col sm={4}>
                                                                <div className="form-check-inline w-100">
                                                                    <select className="form-control-edit mr-1">
                                                                        <option value="C2">2COL</option><option value="C1">CASH</option><option value="CCREDIT">CREDIT</option><option value="TERM">TERM</option><option value="NQ">NO QTY.2COL</option><option value="NQ_C1">NO QTY.CASH</option><option value="NQ_CC">NO QTY.CREDIT</option>
                                                                    </select>
                                                                    <select className="form-control-edit">
                                                                        <option value="QUD_TH">QUD_TH</option>
                                                                        <option value="QUD_TH">QUD_TH_REBATE</option>
                                                                        <option value="QUD_TH">QUD_TH_SOL</option>
                                                                        <option value="QUD_EN">QUD_EN</option>
                                                                        <option value="QUD_EN">QUD_EN_REBATE</option>
                                                                        <option value="QUD_EN">QUD_EN_SOL</option>
                                                                    </select>
                                                                </div>
                                                            </Col>
                                                            <Col sm={8}>
                                                                <Form.Group as={Row}>
                                                                    <Button className="mr-1 f-12 wid-130" size="sm" variant="success">1.DEFAULT QUOTA.</Button>
                                                                    <Button className="mr-1 f-12 wid-145" size="sm" variant="success">2.COPY QUOTATION</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="success">3.เพิ่ม HEADER</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="success">4.เพิ่ม FOOTER</Button>
                                                                </Form.Group>
                                                                <Form.Group as={Row}>
                                                                    <Button className="mr-1 f-12 wid-130" size="sm" variant="primary">1.PRINT QUOTA.</Button>
                                                                    <Button className="mr-1 f-12" size="sm" variant="primary">2.PRINT QUOTA. ASTM</Button>
                                                                    <Button className="f-12" size="sm" variant="primary">3.PRINT QUOTA. NOMINAL</Button>
                                                                </Form.Group>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={3}></Col>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered id="user-group">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="tb-group" /></th>
                                                            <th>#</th>
                                                            <th>HEADER</th>
                                                            <th>SEQ	</th>
                                                            <th>CAPTION</th>
                                                            <th>HIDE</th>
                                                            <th>UPDATE DATE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="7" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="ATTACHFILES" title="ATTACH FILES" >
                                                <hr className="mt-2" />
                                                <center>no attach files</center>
                                                <hr />
                                                <div className="text-right">
                                                    <Button variant="warring" className="btnEdit mr-2 btn waves-effect waves-light btn-icon btn-rounded btn-outline-primary" ><span className="feather icon-upload" /></Button>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="SPACIALRQ" title="SPACIAL RQ." >
                                                <Row className="mt-2">
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="success" size="sm">SAVE SPACIAL RQ.</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Special Quality/grade</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Dimension/length</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Testing </Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Certificate</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Export document</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Container</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Packing </Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Pre load inspection</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Overstock</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3"></Form.Label>
                                                            <Col sm={9} className="text-right">
                                                                <Button variant="primary" size="sm">SET</Button>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={2}></Col>
                                                    <Col sm={8}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-3">Other</Form.Label>
                                                            <Col sm={9}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={2}></Col>

                                                </Row>
                                            </Tab>
                                        </Tabs>
                                    </Form.Group>
                                </Col>
                                <Form.Group as={Row}>
                                    <Col sm={12}>
                                        <Row className="mt-1">
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณตันรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0.000 ตัน</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Freight</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">จำนวนเงินรวม(THB)</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">ปริมาณท่อนรวม</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0 ท่อน</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Insurance</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">VAT 7.00%</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.00</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">Total BDL</Form.Label>
                                                    <Col sm={8}>
                                                        <label> 0 Bdls. 	 </label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-5">Total Commission</Form.Label>
                                                    <Col sm={7}>
                                                        <label> 0.000</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-6">จำนวนเงินรวมสุทธิ(THB)</Form.Label>
                                                    <Col sm={6}>
                                                        <label> 0.00</label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">หมายเหตุ.-</Form.Label>
                                                    <Col sm={8}>
                                                        <textarea rows="5" cols="95" name="comment" form="usrform"></textarea>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่สร้าง</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">14-Jul--21 10:25</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">0704</label>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่แก้ไข</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">14-Jul--21 10:25</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">0704</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="success" className="w-100">ทำเอกสารเสร็จ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 1</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 1</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ ตรวจสอบครั้งที่ 2</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดตรวจสอบ 2</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ อนุมัติระดับ ส่วน</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={1}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Col sm={3}>
                                                        <Button size="sm" variant="primary" className="w-100">เปิดอนุมัติ</Button>
                                                    </Col>
                                                </Form.Group>
                                                <Form.Group as={Row}>
                                                    <Form.Label className="col-sm-4">วันที่ลูกค้ายืนยัน</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>
                                                    <Form.Label className="col-sm-1">โดย</Form.Label>
                                                    <Col sm={3}>
                                                        <label className="text-center">-</label>
                                                    </Col>

                                                </Form.Group>
                                            </Col>

                                            <Col sm={6}>
                                                <Form.Group as={Row}>
                                                    <Tabs variant="pills" defaultActiveKey="USER" className="form-control-file">
                                                        <Tab eventKey="USER" title="SYS USER">

                                                        </Tab>
                                                        <Tab eventKey="CUSTOMER" title="CUSTOMER">
                                                            <Table striped hover responsive bordered id="example2">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>E-MAIL ADDRESS</th>
                                                                        <th>NAME</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colSpan="3" className="text-center">No Data.</td>

                                                                    </tr>
                                                                </tbody>
                                                            </Table>


                                                        </Tab>
                                                    </Tabs>
                                                </Form.Group>
                                            </Col>


                                        </Row>


                                    </Col>
                                </Form.Group>



                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalAdd: false })}>Close</Button>
                                <Button variant="primary" title="send email"><i className="feather icon-navigation"></i></Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>SALES ORG.CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CHANNEL CD</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select" >
                                                    <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>COUNTRY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CUST.CODE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col sm={4}>

                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>NAME.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ABBRV.(ENG.)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                                <br />
                                <Row>
                                    <Col sm={12}>
                                        <Form.Group className="float-sm-right">
                                            <Button size="sm" variant="primary" >ค้นหา</Button>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <br />
                                <Form.Group as={Row}>
                                    <Table ref="tbl" striped hover responsive bordered id="example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>GROUP</th>
                                                <th>COUNTRY</th>
                                                <th>CODE</th>
                                                <th>NAME</th>
                                                <th>ABBRV.NAME</th>
                                                <th>NAME (ENG.)</th>
                                                <th>ABBRV.NAME (ENG.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="cursor" onClick={e => this.setShowModal(e, "Add")}>
                                                <td >1</td>
                                                <td >0180_10&nbsp;</td>
                                                <td >TH
                                                    &nbsp;-&nbsp;Thailand
                                                    &nbsp;</td>
                                                <td >3001574&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>

                            </Modal.Footer>
                        </Modal>
                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER Year</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="years" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER Month</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="month" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รอบที่</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>พนักงานขาย</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option>
                                                            <option value="NONE">None</option>
                                                            <option value="0667">0667-Sayompoo H.</option>
                                                            <option value="0977">0977-Pichai</option>
                                                            <option value="1234">1234-Voratuch N.</option>
                                                            <option value="2168">2168-Sorawit J</option>
                                                            <option value="2298">2298-Kumpol Jirawantana</option>
                                                            <option value="2374">2374-Gunpragob Cheawchan</option>
                                                            <option value="977">977-Pichai</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}> ORDER NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER LOT.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER REV.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ORDER Type</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="ALLA">ALL-Advance</option>
                                                            <option value="A">Adv-Stock</option>
                                                            <option value="AP">Adv-Project</option>
                                                            <option value="N">Project</option>
                                                            <option value="S">SYS-Stock</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>


                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ชื่อลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SalesOrgCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="0001">0001 - new</option><option value="0180">0180 - บริษัทซิเมนต์ไทยการตลาด จำกัด</option><option value="0490">0490 - บริษัทเหล็กสยามยามาโตะ จำกัด</option><option value="0560">0560 - บริษัทสยามมอเตอร์ จำกัด</option><option value="0900">0900 - บริษัทบางซื่อขนส่ง จำกัด</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ChannelCd</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="10">10 - Direct</option><option value="20">20 - Agent</option><option value="30">30 - Export</option><option value="40">40 - Indirect Export</option><option value="50">50 - Retail</option><option value="60">60 - Inter-Company</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option>
                                                            <option value="NONE">None</option>
                                                            <option value="B">ส.ขบ.</option>
                                                            <option value="C">ส.ตท.</option>
                                                            <option value="A">ส.ผจ.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>

                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ADV.Type</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="NONE">ไม่ระบุ</option>
                                                            <option value="0001">ADV SCG Dealer</option>
                                                            <option value="0002">ADV SYS Dealer</option>
                                                            <option value="0003">SYS Project</option>
                                                            <option value="0004">ADV SYS Project</option>
                                                            <option value="0005">SYS Export</option>
                                                            <option value="0006">ADV SCG Project</option>
                                                            <option value="0007">ADV Small Section</option>
                                                            <option value="0008">Quick ADV</option>
                                                            <option value="0009">ADV Sheet Pile</option>
                                                            <option value="0010">Package สุดคุ้ม</option>
                                                            <option value="0011">DL Make Stock</option>
                                                            <option value="0012">DL Customer USE</option>
                                                            <option value="0013">SYS สินค้าพิเศษ</option>
                                                            <option value="0014">SCG สินค้าพิเศษ</option>
                                                            <option value="0015">สินค้าพิเศษสต๊อก</option>
                                                            <option value="0016">-NAME-</option>
                                                            <option value="0001_0002">ADV SYS+SCG Dealer</option>
                                                            <option value="0004_0006">ADV SYS+SCG Project</option>
                                                            <option value="0013_0014">SYS+SCG สินค้าพิเศษ</option>
                                                            <option value="0001_0002_0013_0014">ADV SYS+SCG Dealer ,SYS+SCG สินค้าพิเศษ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="AP00">AP00 - Advance payment before productio</option>
                                                            <option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option>
                                                            <option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option>
                                                            <option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option>
                                                            <option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option>
                                                            <option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option>
                                                            <option value="CA15">CA15 - D-L/C sight 15 วัน</option>
                                                            <option value="CD00">CD00 - D-L/C sight</option>
                                                            <option value="CD07">CD07 - D-L/C 7 วัน</option>
                                                            <option value="CD15">CD15 - D-L/C 15 วัน</option>
                                                            <option value="CD60">CD60 - D-L/C 60 วัน</option>
                                                            <option value="CD90">CD90 - D-L/C 90 วัน</option>
                                                            <option value="CDB0">CDB0 - D-L/C 120 วัน</option>
                                                            <option value="DP00">DP00 - D/P AT SIGHT</option>
                                                            <option value="LC00">LC00 - L/C at sight.</option>
                                                            <option value="LCS0">LCS0 - L/C AT SIGHT.</option>
                                                            <option value="NM01">NM01 - เงินสด 1 วัน</option>
                                                            <option value="NM07">NM07 - เงินสด 7 วัน</option>
                                                            <option value="NM15">NM15 - เงินสด 15 วัน</option>
                                                            <option value="NM30">NM30 - เงินสด 30 วัน</option>
                                                            <option value="NT00">NT00 - เงินสด</option>
                                                            <option value="NT01">NT01 - เงินเชื่อ 1 วัน</option>
                                                            <option value="NT07">NT07 - เงินเชื่อ 7 วัน</option>
                                                            <option value="NT15">NT15 - เงินเชื่อ 15 วัน</option>
                                                            <option value="NT30">NT30 - เงินเชื่อ 30 วัน</option>
                                                            <option value="NT45">NT45 - เงินเชื่อ 45 วัน</option>
                                                            <option value="NT60">NT60 - เงินเชื่อ 60 วัน</option>
                                                            <option value="NTB0">NTB0 - เงินเชื่อ 120 วัน</option>
                                                            <option value="TL15">TL15 - T/T 07 days after B/L date.</option>
                                                            <option value="TL21">TL21 - T/T 15 days after B/L date.</option>
                                                            <option value="TL90">TL90 - T/T 90 days after B/L date.</option>
                                                            <option value="TS00">TS00 - T/T before shipment.</option>
                                                            <option value="TS15">TS15 - T/T before shipment 7 day.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>เงื่อนไขการขนส่ง</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="CFR">CFR-Costs and freight</option>
                                                            <option value="CIF">CIF-Costs,Insurance &amp; freight</option>
                                                            <option value="EXW">EXW-Ex works</option>
                                                            <option value="FAS">FAS-Free along ship</option>
                                                            <option value="FOB">FOB-Free on board</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>


                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>REF.NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>REF.REV.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>REF.TYPE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option>
                                                            <option value="AO">Adv.Order</option>
                                                            <option value="CO">Chg.Adv.Order</option>
                                                            <option value="CP">Chg.Pmt.Adv.Order</option>
                                                            <option value="CW">Chg.Plant.Adv.Order</option>
                                                            <option value="QUP">PROJECT QUOTA.</option>
                                                            <option value="QUD">PROJECT DOMESTIC</option>
                                                            <option value="QUE">PROJECT EXPORT</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ซื้อสำหรับ</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="company" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SECTION</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="section" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">Test</option><option value="ๅๅ">ๅๅ</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row} className="mt-2">
                                                    <Form.Label column sm={4}></Form.Label>
                                                    <Col sm={8}>
                                                        <input type="checkbox" className="mr-1" />
                                                        <Form.Label>Only lot 0 rev 0</Form.Label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>STATUS</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALLA">All Active</option><option value="ALL">ทั้งหมด</option><option value="1">เปิดเอกสาร</option><option value="2">รอตรวจสอบ 1</option><option value="3">รอตรวจสอบ 2</option><option value="4">รออนุมัติ</option><option value="5">อนุมัติ</option><option value="6">ลูกค้ายืนยัน</option><option value="7">SYS ดำเนิการ</option><option value="8">ปิดเอกสาร</option><option value="9">ยกเลิกเอกสาร</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>



                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>JOB NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PO.REF</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row} className="mt-2">
                                                    <Form.Label column sm={4}></Form.Label>
                                                    <Col sm={8}>
                                                        <input type="checkbox" className="mr-1" />
                                                        <Form.Label>Order ค้างรับ</Form.Label>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="PROJECT ORDER CONFIRM">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info " /></Button>
                                </Col>
                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" className="wid-150" onClick={e => this.setShowModal(e, "Create")}>สร้าง ORDER</Button>
                                    <Button size="sm" variant="success" className="wid-150" onClick={e => this.setShowModal(e, "Create")}>สร้าง ORDER กันสินค้า</Button>
                                    <Button size="sm" variant="success" className="wid-150" onClick={e => this.setShowModal(e, "Create")}>สร้างลูกค้าทั่วไป</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check className='ml-sm-4' id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>ชื่อลูกค้า</th>
                                        <th>ชื่อโครงการ</th>
                                        <th>JOB NO</th>
                                        <th>ปี-เดือน-รอบที่</th>
                                        <th>สถานะ</th>
                                        <th>แสดง</th>
                                        <th>เลขที่ใบสั่งซื้อ</th>
                                        <th>วันที่สั่ง</th>
                                        <th>วิธีการชำระเงิน</th>
                                        <th>เงื่อนไขการขนส่ง</th>
                                        <th>ปริมาณ ท่อน</th>
                                        <th>ปริมาณ ตัน</th>
                                        <th>จำนวนเงิน (บาท)</th>
                                        <th>เลขที่ใบสรุป ยอดจ่ายเช็ค</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row >
            </Aux >
        );
    }
}

export default SizeMaster;
