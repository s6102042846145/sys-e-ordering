import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> ORDER CONFIRM </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/project/project-order-confirm/project-order-confirm">
                                        PROJECT ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/project/project-order-confirm/show-order-confirm" >
                                        แสดง PROJECT ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/project/project-order-confirm/check-project-order" >
                                        ตรวจสอบ PROJECT ORDER CONFIRM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/project/project-order-confirm/project-invoice" >
                                        ใบแจ้งหนี้ (INVOICE)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;