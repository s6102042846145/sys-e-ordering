import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> SCHEDULE SHIPMENT </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/schedule-shipment-page">
                                        SCHEDULE SHIPMENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/vessel-agent" >
                                        VESSEL AGENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/shipment-period" >
                                        SHIPMENT PERIOD
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/svessel" >
                                        VESSEL
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/vessel-type" >
                                        VESSEL TYPE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                        <Link className='list-group-item list-group-item-action' to="/export-doc/schedule-shipment/list-rebate-byorder" >
                                        LIST REBATE BY ORDER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        


                                        

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;