import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else if (type === "User") {
            this.setState({ isModalUser: true });
            this.setState({ setTitleModalUser: "User" });
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">COUNTRY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="NONE">None</option><option value="AE">AE-Unit.Arab Emir.</option><option value="AL">AL-Albania</option><option value="AR">AR-Argentina</option><option value="AS">AS-Samoa American</option><option value="AT">AT-Austria</option><option value="AU">AU-Australia</option><option value="BA">BA-Bosnia and Herzegovina</option><option value="BD">BD-Bangladesh</option><option value="BE">BE-Belgium</option><option value="BG">BG-Bulgaria</option><option value="BH">BH-Bahrain</option><option value="BN">BN-Brunei</option><option value="BR">BR-Brazil</option><option value="BY">BY-Byelorussia</option><option value="CA">CA-Canada</option><option value="CD">CD-Congo</option><option value="CI">CI-Cote d' lvoire</option><option value="CL">CL-Chile</option><option value="CN">CN-China</option><option value="CO">CO-Colombia</option><option value="CR">CR-Costa Rica</option><option value="CU">CU-Cuba</option><option value="CZ">CZ-CZECH REPUBLIC</option><option value="DE">DE-Germany</option><option value="DK">DK-DENMARK</option><option value="DM">DM-Dominican Repubilic</option><option value="DZ">DZ-Algeria</option><option value="EC">EC-Ecuador</option><option value="EE">EE-ESTONIA</option><option value="EG">EG-EGYPT</option><option value="ER">ER-Eritrea</option><option value="ES">ES-SPAIN</option><option value="ET">ET-ETHlopia</option><option value="FI">FI-FINLAND</option><option value="FJ">FJ-Fiji</option><option value="FR">FR-France</option><option value="GB">GB-Grate Britain</option><option value="GE">GE-Georgia</option><option value="GH">GH-Ghana</option><option value="GR">GR-GREECE</option><option value="GT">GT-Guatemala</option><option value="HK">HK-Hong Kong</option><option value="HN">HN-honduras</option><option value="HR">HR-Croatia</option><option value="HU">HU-HUNGARY</option><option value="ID">ID-Indonesia</option><option value="IL">IL-ISRAEL</option><option value="IN">IN-India</option><option value="IQ">IQ-IRAQ</option><option value="IR">IR-IRAN</option><option value="IT">IT-Italy</option><option value="JM">JM-Jamaica</option><option value="JO">JO-JORDAN</option><option value="JP">JP-Japan</option><option value="KE">KE-Kenya</option><option value="KH">KH-Cambodia</option><option value="KP">KP-North Korea</option><option value="KR">KR-South Korea</option><option value="KW">KW-KUWAIT</option><option value="LA">LA-Laos</option><option value="LB">LB-LEBANON</option><option value="LK">LK-Sri Lanka</option><option value="LU">LU-LUXEMBOURG</option><option value="LV">LV-LATVIA</option><option value="LY">LY-LIBYA</option><option value="MA">MA-MOROCCO</option><option value="MG">MG-Madagascar</option><option value="MK">MK-FYR Macedonia</option><option value="MM">MM-Myanmar</option><option value="MU">MU-Mauritius</option><option value="MV">MV-Maldives</option><option value="MX">MX-Mexico</option><option value="MY">MY-Malaysia</option><option value="MZ">MZ-Mozambique</option><option value="NC">NC-New Caledonia</option><option value="NG">NG-Nigeria</option><option value="NI">NI-Nicaragua</option><option value="NL">NL-NETHERLANDS</option><option value="NM">NM-Noumea</option><option value="NO">NO-Norway</option><option value="NZ">NZ-New Zealand</option><option value="OM">OM-OMAN</option><option value="PE">PE-Peru</option><option value="PG">PG-Papua Nw Guinea</option><option value="PH">PH-Philippines</option><option value="PK">PK-Pakistan</option><option value="PL">PL-POLAND</option><option value="PR">PR-Puerto Rico</option><option value="PT">PT-PORTUGAL</option><option value="PY">PY-Paraguay</option><option value="QA">QA-Qatar</option><option value="RM">RM-Sebia and Montenego</option><option value="RO">RO-ROMANIA</option><option value="RU">RU-Russia</option><option value="SA">SA-Saudi Arabia</option><option value="SB">SB-Solomon Islands</option><option value="SE">SE-SWEDEN</option><option value="SF">SF-South Africa</option><option value="SG">SG-Singapore</option><option value="SI">SI-SLOVENIA</option><option value="SK">SK-SLOVAKIA</option><option value="SL">SL-Sierra leone</option><option value="SN">SN-Senegal</option><option value="SU">SU-Samoa</option><option value="SV">SV-El salvalor</option><option value="SY">SY-SYRIA</option><option value="SZ">SZ-Switzerland</option><option value="TH">TH-Thailand</option><option value="TN">TN-TUNISIA</option><option value="TR">TR-Turkey</option><option value="TT">TT-Trinidad and tobago</option><option value="TW">TW-Taiwan</option><option value="TZ">TZ-Tanzania</option><option value="UA">UA-Ukraine</option><option value="UK">UK-United Kingdom</option><option value="US">US-United state</option><option value="UY">UY-Uruguay</option><option value="UZ">UZ-Uzbekistan</option><option value="VE">VE-Venezuela</option><option value="VN">VN-Vietnam</option><option value="VU">VU-Vanuatu</option><option value="WS">WS-Western Samoa</option><option value="YE">YE-Yemen</option><option value="ZA">ZA-South Africa</option><option value="ZW">ZW-Zimbabwe</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">YEAR</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">MONTH</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">VESSEL TYPE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="S">SHIP</option><option value="T">TRAIN</option><option value="C">TRUCK</option><option value="B">V-BULK</option><option value="R">V-CONTAINER</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="L">V-LOLO</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">VESSEL CODE</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">VESSEL'S NAME</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">SHIPPING AGENT</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">VESSEL AGENT</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">VESSEL AGENT (LOCAL)</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <br />
                                    <Form.Group as={Row}>
                                        <Table ref="tbl" striped hover responsive bordered id="example">
                                            <thead>
                                                <tr>
                                                    <th><Form.Check id="example-select-all" /></th>
                                                    <th>#</th>
                                                    <th>FROM PORT</th>
                                                    <th>TO PORT</th>
                                                    <th>LAYCAN PERIOD</th>
                                                    <th>FREIGHT</th>
                                                    <th>CAPACITY TONS</th>
                                                    <th>DISTANCE KMS</th>
                                                    <th>DURATION DAYS</th>
                                                    <th>UPDATED DATE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr className="cursor" onClick={e => this.setShowModal(e, "Add")}>
                                                    <td >1</td>
                                                    <td >0180_10&nbsp;</td>
                                                    <td >TH
                                                        &nbsp;-&nbsp;Thailand
                                                        &nbsp;</td>
                                                    <td >3001574&nbsp;</td>
                                                    <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                    <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                    <td align="left">&nbsp;บ. ช.การช่าง จก. (มหาชน)&nbsp;</td>
                                                    <td align="left">&nbsp;ช.การช่าง&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">CREATED BY</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="text-center" defaultValue="0704" disabled />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">CREATED DATE</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="text-center" defaultValue="27-Jul-21" disabled />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}></Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">UPDATED BY</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="text-center" defaultValue="0704" disabled />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">UPDATED DATE</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="text-center" defaultValue="27-Jul-21 18:35" disabled />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label className="col-sm-4">SYSTEM CODE</Form.Label>
                                                <Col sm={8}>
                                                    <input type="text" className="text-center" disabled defaultValue="1" />
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>

                                </Row>

                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="success">Save</Button>
                                <Button variant="success">Save As</Button>
                            </Modal.Footer>
                        </Modal>


                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>COUNTRY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="AE">AE-Unit.Arab Emir.</option><option value="AL">AL-Albania</option><option value="AR">AR-Argentina</option><option value="AS">AS-Samoa American</option><option value="AT">AT-Austria</option><option value="AU">AU-Australia</option><option value="BA">BA-Bosnia and Herzegovina</option><option value="BD">BD-Bangladesh</option><option value="BE">BE-Belgium</option><option value="BG">BG-Bulgaria</option><option value="BH">BH-Bahrain</option><option value="BN">BN-Brunei</option><option value="BR">BR-Brazil</option><option value="BY">BY-Byelorussia</option><option value="CA">CA-Canada</option><option value="CD">CD-Congo</option><option value="CI">CI-Cote d' lvoire</option><option value="CL">CL-Chile</option><option value="CN">CN-China</option><option value="CO">CO-Colombia</option><option value="CR">CR-Costa Rica</option><option value="CU">CU-Cuba</option><option value="CZ">CZ-CZECH REPUBLIC</option><option value="DE">DE-Germany</option><option value="DK">DK-DENMARK</option><option value="DM">DM-Dominican Repubilic</option><option value="DZ">DZ-Algeria</option><option value="EC">EC-Ecuador</option><option value="EE">EE-ESTONIA</option><option value="EG">EG-EGYPT</option><option value="ER">ER-Eritrea</option><option value="ES">ES-SPAIN</option><option value="ET">ET-ETHlopia</option><option value="FI">FI-FINLAND</option><option value="FJ">FJ-Fiji</option><option value="FR">FR-France</option><option value="GB">GB-Grate Britain</option><option value="GE">GE-Georgia</option><option value="GH">GH-Ghana</option><option value="GR">GR-GREECE</option><option value="GT">GT-Guatemala</option><option value="HK">HK-Hong Kong</option><option value="HN">HN-honduras</option><option value="HR">HR-Croatia</option><option value="HU">HU-HUNGARY</option><option value="ID">ID-Indonesia</option><option value="IL">IL-ISRAEL</option><option value="IN">IN-India</option><option value="IQ">IQ-IRAQ</option><option value="IR">IR-IRAN</option><option value="IT">IT-Italy</option><option value="JM">JM-Jamaica</option><option value="JO">JO-JORDAN</option><option value="JP">JP-Japan</option><option value="KE">KE-Kenya</option><option value="KH">KH-Cambodia</option><option value="KP">KP-North Korea</option><option value="KR">KR-South Korea</option><option value="KW">KW-KUWAIT</option><option value="LA">LA-Laos</option><option value="LB">LB-LEBANON</option><option value="LK">LK-Sri Lanka</option><option value="LU">LU-LUXEMBOURG</option><option value="LV">LV-LATVIA</option><option value="LY">LY-LIBYA</option><option value="MA">MA-MOROCCO</option><option value="MG">MG-Madagascar</option><option value="MK">MK-FYR Macedonia</option><option value="MM">MM-Myanmar</option><option value="MU">MU-Mauritius</option><option value="MV">MV-Maldives</option><option value="MX">MX-Mexico</option><option value="MY">MY-Malaysia</option><option value="MZ">MZ-Mozambique</option><option value="NC">NC-New Caledonia</option><option value="NG">NG-Nigeria</option><option value="NI">NI-Nicaragua</option><option value="NL">NL-NETHERLANDS</option><option value="NM">NM-Noumea</option><option value="NO">NO-Norway</option><option value="NZ">NZ-New Zealand</option><option value="OM">OM-OMAN</option><option value="PE">PE-Peru</option><option value="PG">PG-Papua Nw Guinea</option><option value="PH">PH-Philippines</option><option value="PK">PK-Pakistan</option><option value="PL">PL-POLAND</option><option value="PR">PR-Puerto Rico</option><option value="PT">PT-PORTUGAL</option><option value="PY">PY-Paraguay</option><option value="QA">QA-Qatar</option><option value="RM">RM-Sebia and Montenego</option><option value="RO">RO-ROMANIA</option><option value="RU">RU-Russia</option><option value="SA">SA-Saudi Arabia</option><option value="SB">SB-Solomon Islands</option><option value="SE">SE-SWEDEN</option><option value="SF">SF-South Africa</option><option value="SG">SG-Singapore</option><option value="SI">SI-SLOVENIA</option><option value="SK">SK-SLOVAKIA</option><option value="SL">SL-Sierra leone</option><option value="SN">SN-Senegal</option><option value="SU">SU-Samoa</option><option value="SV">SV-El salvalor</option><option value="SY">SY-SYRIA</option><option value="SZ">SZ-Switzerland</option><option value="TH">TH-Thailand</option><option value="TN">TN-TUNISIA</option><option value="TR">TR-Turkey</option><option value="TT">TT-Trinidad and tobago</option><option value="TW">TW-Taiwan</option><option value="TZ">TZ-Tanzania</option><option value="UA">UA-Ukraine</option><option value="UK">UK-United Kingdom</option><option value="US">US-United state</option><option value="UY">UY-Uruguay</option><option value="UZ">UZ-Uzbekistan</option><option value="VE">VE-Venezuela</option><option value="VN">VN-Vietnam</option><option value="VU">VU-Vanuatu</option><option value="WS">WS-Western Samoa</option><option value="YE">YE-Yemen</option><option value="ZA">ZA-South Africa</option><option value="ZW">ZW-Zimbabwe</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>YEAR</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}> MONTH</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option><option value="13">13 - Back Order</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>	 VESSEL TYPE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="S">SHIP</option><option value="T">TRAIN</option><option value="C">TRUCK</option><option value="B">V-BULK</option><option value="R">V-CONTAINER</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="0">-VESSEL TYPE NAME-</option><option value="L">V-LOLO</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>	 VESSEL CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>VESSEL'S NAME</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SYSTEM CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>AGENT</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="V0054">-agent-</option><option value="V0055">-agent-</option><option value="V0057">-agent-</option><option value="V0035">ANCHOR MARINE CO., LTD</option><option value="16100001">APC Logistics (THAI) Co.,LTD. Tel :  66 2 714 1491?3, Fax :  66 2 714 1782</option><option value="49">ASC LINER SERVICES Tel : 038-915-037, 038-915-048</option><option value="57">ASL LOGISTICS CO.,LTD Tel : (02) 683-5477</option><option value="17">Atlantic</option><option value="V0005">BALIWAG SHIPPING AGENCY  TEL:632-522-3056 FAX: 632-521-9023   EMAIL:BSAI@BNI.COM.PH        PIC: MR.D</option><option value="03">Benline</option><option value="27">Berthlink</option><option value="S03">BETTER SYSTEM TEL: (02) 671-5858  FAX: (02) 672-5503-5</option><option value="V0001">BILLION LOGISTICS CO.,LTD.Tel :  66 (0) 2672 3817-21, (0) 2672 3833-4 Fax :  66 (0) 2672 3822</option><option value="16110001">C.H. Robinson Freight Services (Thailand) Limited Tel :   6626341313 Ext.322</option><option value="52">CARGO-PARTNER Tel : 02-649-9813 FAX : 02-683-0070</option><option value="V0020">CENTRAL OCEANS</option><option value="V0031">Central Oceans</option><option value="V0049">CNK LINE &amp; LOGISTICS CO., LTD.</option><option value="V0032">Company Registered</option><option value="09">Cont bulk</option><option value="17030001">CONTERM CONSOLIDATION SERVICES (THAILAND) CO., LTD Tel : (662) 338-7488 Fax : (662) 745 0029</option><option value="V0025">CONTERM CONSOLIDATION SERVICES (THAILAND) CO., LTD.</option><option value="V0033">CONTERM CONSOLIDATION SERVICES (THAILAND) CO., LTD.</option><option value="40">CONTRANS SINGAPORE - MR.PATRICK</option><option value="V0039">COSCO SHIPPING LINES CO.,LTD.</option><option value="V0023">Customer service sea export East-West Logistics Co.,Ltd.   – Laem Chabang Branch</option><option value="48">DAMCO CO.,LTD Tel : 66-2752-9436 Fax: 66-2657-9400-02</option><option value="45">DEXTRANS WORLDWIDE (THAILAND) CO.,LTD. TEL: (02) 294-2250-1  FAX: (02) 681-0556-8</option><option value="V0015">DSV Air and Sea LTD</option><option value="12">DYNAMIC MARINE SERVICES SDN.BHD  TEL NO. 603-33221190 E-mail : dynamic.marine09@gmail.com</option><option value="S01">EASTERN KAMALAPHORN CO.,LTD TEL : (02) 980-8644 FAX : (02) 574-2699</option><option value="V0019">East-West Logistics Co., Ltd.</option><option value="44">EXCEL TRANSPORT INTERNATIONAL CO.,LTD  TEL : (02) 643-8008-10 FAX : (02) 643-9414-5</option><option value="19">Fearnleys</option><option value="S02">FIRST UNI-TRADE K.CHERRY FAX (02)236-5791, 233-7977  TEL (02) 233-3945,233-7997,266-5811,236-5707</option><option value="54">FUJITRANS (THAILAND) CO.,LTD Tel : 662-632-7711 FAX : 662-632-7719</option><option value="20">Gold Ship</option><option value="35">Gulf Agency Company (Thailand)Ltd.</option><option value="46">HELLMANN LOGISTICS (THAILAND) CO.,LTD. TEL: (02) 661-9638  FAX: (02) 661-9630</option><option value="V0043">HONOUR LANE LOGISTICS CO., LTD. – THAILAND</option><option value="V0022">HTNS (THAILAND) CO., LTD.</option><option value="V0034">I T L SERVICES AND LOGISTICS CO.,LTD.</option><option value="36">IFW LOGISTICS CO.,LTD  TEL : (02) 287-1104 FAX : (02) 287-1105-6</option><option value="50">INTERGROUP AGENCIES Tel : 02-656-0099</option><option value="V0045">INTERUNION (THAILAND) CO., LTD.</option><option value="34">JARDINE PACIFIC K.JERMSAK TEL : (02) 253-7890</option><option value="47">JUNO MARINE CO.,LTD. Tel: 662-6902391, 662-6911454 Fax: 662-691-1683</option><option value="V0002">KLine (Thailand) Ltd. Tel:  662-625-0461</option><option value="V0016">KUEHNE  NAGEL     Tel:   66 (0) 2 018 8800 Ext 8926 Fax:   66 (0) 2207 0672</option><option value="V0029">Lbh (Thailand) Co.,Ltd.</option><option value="39">LIGHT HOUSE - K.NOPPOL (02) 654-3100</option><option value="V0051">LION TRANS CO.,LTD</option><option value="V0050">LION TRANS CO.,LTD.</option><option value="41">MARITIME ALLIANCE CO.,LTD TEL : (02) 632-9898#50</option><option value="29">Max Logistics</option><option value="24">Meea Logistics</option><option value="V0030">MMP LOGISTICS CO., LTD.</option><option value="V0008">MOL(HK) AGENCY LD. TEL: 852-2823-6800 EMAIL : SuiWah.Li@mol-liner.com PIC : MR.S.W.LI</option><option value="04">Moon Star</option><option value="21">Nam Yoen Yong</option><option value="V0026">Ngow Hock Agency Co., Ltd.</option><option value="V0010">NORTRANS SHIPPINCONTACT : MS.JACQUELINE NG. E.MAIL : jacqueline@nortrans.com DIRECT : (65) 6319 8776</option><option value="V0009">NORTRANS SHIPPING AGENCIES PTE LTD  CONTACT : MS.JACQUELINE NG. E.MAIL : jacqueline@nortrans.com DIR</option><option value="V0011">NORTRANS SHIPPING AGENCIES PTE LTD  CONTACT : MS.JACQUELINE NG. E.MAIL : jacqueline@nortrans.com DIR</option><option value="V0007">OCEAN BASE CO.,LTD. TEL: 0852-2815-8989   FAX: 0852-2815-8800 PIC: CAPT C.M. TSANG/MS. S.J. XU</option><option value="05">Orient Star</option><option value="V0048">ORIENTAL LOGISTICS GROUP (THAILAND) CO.,LTD.</option><option value="V0040">PAN CONTINENTAL SHIPPING CO.,LTD. C/O  BEN LINE AGENCIES ( THAILAND ) LTD</option><option value="V0006">PHILHUA SHIPPING INC TEL:632-8338518, 8338618 8338868, 8332451 FAX:632-8338898 TLX:(023) 479480</option><option value="08">Phulsawat</option><option value="53">PROGISTICS INTERNATIONAL CO.,LTD Tel : 662-663-5099 FAX : 662-663-5100,261-2130</option><option value="32">RAINBOW MARITIME</option><option value="S04">RASOTS GLOBAL SERVICES CO., LTD TEL: (02) 639-8200-4</option><option value="V0003">Rohlig (Thailand ) Ltd. T  66 2 3673131- 40 F  66.2.3673141- 42</option><option value="37">ROHLING THAILAND K.WASANA TEL : (02) 712-1267-68</option><option value="15">Sang Thai</option><option value="V0021">SCG LOGISTICS MANAGEMENT CO., LTD.</option><option value="V0046">SCG LOGISTICS MANAGEMENT CO., LTD.</option><option value="V0036">SCHENKER (THAI) LTD.</option><option value="23">Sea Born</option><option value="33">SEA CONCERT K.WIWAT TEL : (02) 260-8150</option><option value="V0041">Sea Horse Maritime co.,Ltd</option><option value="28">Sea Link</option><option value="18">Sealite</option><option value="V0028">SEASTAR LOGISTICS CO.,LTD.</option><option value="22">Shipmate</option><option value="38">SIAM ECL K.AREEYA TEL : (02) 677-4401-8 FAX : (02) 677-4409-10</option><option value="55">SIAM THARA AGENCY CO.,LTD Tel : 662-655-8755  FAX: (02) 662-655-8756</option><option value="26">Sin Kit</option><option value="V0012">SITC CONTAINER LINES (SHANGHAI)CO.,LTD.</option><option value="V0042">SONIC INTERFREIGHT PUBLIC COMPANY LIMITED</option><option value="V0044">SONIC INTERFREIGHT PUBLIC COMPANY LIMITED</option><option value="51">STARLINE LOGISTIC Tel : 02-725-5111</option><option value="V0024">Sunrise Logix (Thailand) Co., Ltd.</option><option value="V0027">Sunrise Logix (Thailand) Co., Ltd.</option><option value="V0004">SUNSHIP AGENCIES (MALAYSIA) SDN. BHD. TEL : 603-31688623/5571 E-mail : ops.sunship@gmail.com PIC MR.</option><option value="31">SUPPLY STREAM (TRAIN)</option><option value="11">T.S Transrail</option><option value="42">THAI COPEX - K.SURASIT : (02) 5590664</option><option value="01">Thai Rainbow</option><option value="02">Thoresen</option><option value="25">Toyo Fuji</option><option value="43">TOYOFUJI LOGISTICS - K.JARIYA : (02) 6328779</option><option value="16070001">Trading as Rasots Global Services Company Limited Tel   02 639 8200-4</option><option value="30">Truck</option><option value="V0017">True Marine Co., Ltd. (Head Office)</option><option value="17020001">TSL NAXCO (THAILAND) COMPANY LIMITED Tel:  66 (0) 2022-0500 Fax :  66 (0) 2022-0598-99</option><option value="10">UIT</option><option value="V0052">UNI SHINE INTERNATIONAL CO.,LTD.</option><option value="06">Unithai</option><option value="13">V.SHIP AGENCY PTE LTD TEL 65-6885-0510 E-MAIL: agency.singapore@vships.com PIC: Mr. Dennis Kow</option><option value="V0018">V.SHIPS AGENCY PTE LTD TEL  65-6885-0510 E-MAIL: AGENCY.SINGAPORE@XSHIPS.COM PIC MR.DENNIS KOW</option><option value="07">Wallem</option><option value="56">WEISS-ROHLIG (THAILAND) LTD Tel : 662-381-2940  FAX: (02) 381-2937-38</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>

                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="VESSEL">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>COUNTRY</th>
                                        <th>YR-MTH</th>
                                        <th>CODE</th>
                                        <th>VESSEL NAME</th>
                                        <th>SHIP.AGENT</th>
                                        <th>VESSEL AGENT</th>
                                        <th>VESSEL TYPE</th>
                                        <th>UPDATED DATE</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;
