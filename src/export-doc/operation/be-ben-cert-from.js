import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";


import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10303",
        "NominalSize": "102X102",
        "DimDesc": "H 102X102X19.3KG/M",
        "Weighr": "19.30",
        "UM": "KG/M",
        "N": "33",
        "UpdateDate": "29-Aug-19 07:51"
    },
    {
        "id": 2,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10602",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X23.8KG/M",
        "Weighr": "23.80",
        "UM": "KG/M",
        "N": "32",
        "UpdateDate": "19-Sep-12 09:00"
    },
    {
        "id": 3,
        "sizeSTD": "ASTM2003",
        "sec": "H",
        "sizeID": "10603",
        "NominalSize": "127X127",
        "DimDesc": "H 127X127X28.1KG/M",
        "Weighr": "28.10",
        "UM": "KG/M",
        "N": "16",
        "UpdateDate": "19-Sep-12 09:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        //data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "sizeSTD", render: function (data, type, row) { return data; } },
            { "data": "sec", render: function (data, type, row) { return data; } },
            { "data": "sizeID", render: function (data, type, row) { return data; } },
            { "data": "NominalSize", render: function (data, type, row) { return data; } },
            { "data": "DimDesc", render: function (data, type, row) { return data; } },
            { "data": "Weighr", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "UM", render: function (data, type, row) { return data; } },
            { "data": "N", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class SizeMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else if (type === "User") {
            this.setState({ isModalUser: true });
            this.setState({ setTitleModalUser: "User" });
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>

                <Row>
                    <Col>
                        <Modal id="modelUser" backdrop="static" size="xl" show={this.state.isModalUser} onHide={() => this.setState({ isModalUser: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModalUser}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Form.Group as={Row}>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Contract No</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Contract Date</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Latest Delivery DT From</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>To</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>สถานะ</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="O">Operate</option><option value="M">Completed</option><option value="K">Checked</option><option value="A">Approved</option><option value="N">Actived</option><option value="C">Closed</option><option value="Y">Canceled</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>ลูกค้า</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Expiry DT From</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>To</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>หน่วยงาน</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="C">ส.ตท.</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Payment Term</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="NONE">None</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>CURRENCY</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control as="select">
                                                    <option value="NONE">NONE</option><option value="AUD">Australian Dollars</option><option value="EUR">European Currency Unit</option><option value="MYR">Malaysian Ringgit</option><option value="THB">Thai Baht</option><option value="USD">US Dollars</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Ton(s)</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" disabled defaultValue="0.000" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Amount</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" disabled defaultValue="0.000" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Bank DLC No.</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Bank</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}>Nego.Bank</Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label column sm={4}></Form.Label>
                                            <Col sm={8}>
                                                <Form.Control type="text" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <br /><br />
                                <Form.Group as={Row}>
                                    <Col className="email-card">
                                        <Button id="btnDelUser" variant="default" className="d-none btn btn-default" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                    </Col>
                                </Form.Group>

                                <Col sm={12}>
                                    <Form.Group as={Row}>
                                        <Tabs variant="pills" defaultActiveKey="size" className="form-control-file">
                                            <Tab eventKey="CREDIT DOC." title="CREDIT DOC.">
                                                <Row>
                                                </Row>
                                                <Table ref="tbl" striped hover responsive bordered id="user-group" className="mt-4">
                                                    <thead>
                                                        <tr>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>CREDIT#</th>
                                                                <th>ETD.DATE</th>
                                                                <th>ORDER TON</th>
                                                                <th>ORDER AMT.</th>
                                                                <th>CURR ENCY</th>
                                                                <th>PAY.TERM.</th>
                                                                <th>REF.DOC#</th>
                                                                <th>BANK</th>
                                                                <th>NEGO.BANK</th>
                                                                <th>ORDER#</th>
                                                            </tr>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="9" className="text-center">
                                                                <label > No Data.</label>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="LC-TEXT" title="LC-TEXT" >
                                                <Table ref="tbl" striped hover responsive bordered className="mt-2">
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">PSCC CODE (philippine).</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">DRAWEE</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">DRAWN UNDER</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col><Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">TEXT 1</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col><Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">TEXT 2</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col><Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">TEXT 3</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col><Col sm={12}>
                                                        <Form.Group as={Row}>
                                                            <Form.Label className="col-sm-2">TEXT 4</Form.Label>
                                                            <Col sm={8}>
                                                                <textarea rows="3" className="w-100" ></textarea>
                                                            </Col>
                                                        </Form.Group>
                                                    </Col>
                                                    

                                                </Table>

                                            </Tab>





                                        </Tabs>
                                    </Form.Group>
                                </Col>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModalUser: false })}>Close</Button>
                                <Button variant="primary">Add</Button>
                            </Modal.Footer>
                        </Modal>

                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={6}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">CUSTOMER</Form.Label>
                                            <Col sm={8}>
                                                <InputGroup>
                                                    <FormControl size="sm" className="form-control-edit" defaultValue="2007-Rattipun T" disabled />
                                                    <InputGroup.Append>
                                                        <Button size="sm" className="h-25px" onClick={e => this.setShowModal(e, "User")}>
                                                            <div className="mt--4px">
                                                                <i className="feather icon-search"></i>
                                                            </div>
                                                        </Button>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <hr />
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-2">หมายเหตุ.-</Form.Label>
                                            <Col sm={8}>
                                                <textarea rows="3" className="w-100" ></textarea>
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่สร้าง</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="23-Jul-21" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">โดย</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="ISS@sys" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" disabled defaultValue="_" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่แก้ไข</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="23-Jul-21 14:45" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">โดย</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="ISS@sys" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4"></Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" disabled defaultValue="_" />
                                            </Col>
                                        </Form.Group>
                                    </Col>

                                    <Col sm={12}>
                                        <Form.Group as={Row}>
                                            <hr />
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่สั่งพิมพ์</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="-" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">โดย</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="-" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">วันที่อัพเดตข้อมูล</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" disabled defaultValue="-" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">โดย</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="text-center" defaultValue="-" disabled />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Modal.Footer>
                                        <Button variant="success">REFRESH CREDIT.DOC.</Button>
                                    </Modal.Footer>
                                </Row>

                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="success">Save</Button>
                                <Button variant="success">Save As</Button>
                            </Modal.Footer>
                        </Modal>



                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>	 YEAR SHIP</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>MONTH SHIP</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>วิธีการชำระเงิน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="AP00">AP00 - Advance payment before productio</option><option value="B120">B120 - ตั๋วแลกเงิน 120 วัน (BE)</option><option value="BE30">BE30 - ตั๋วแลกเงิน 30 วัน (BE)</option><option value="BE45">BE45 - ตั๋วแลกเงิน 45 วัน (BE)</option><option value="BE60">BE60 - ตั๋วแลกเงิน 60 วัน (BE)</option><option value="BE75">BE75 - ตั๋วแลกเงิน 75 วัน (BE)</option><option value="BE90">BE90 - ตั๋วแลกเงิน 90 วัน (BE)</option><option value="BEA0">BEA0 - ตั๋วแลกเงิน 105 วัน (BE)</option><option value="BED0">BED0 - ตั๋วแลกเงิน 150 วัน (BE)</option><option value="BEF0">BEF0 - ตั๋วแลกเงิน 180 วัน (BE)</option><option value="BEH0">BEH0 - ตั๋วแลกเงิน 210 วัน (BE)</option><option value="BEJ0">BEJ0 - ตั๋วแลกเงิน 240 วัน (BE)</option><option value="BEL0">BEL0 - ตั๋วแลกเงิน 270 วัน (BE)</option><option value="BEN0">BEN0 - ตั๋วแลกเงิน 300 วัน (BE)</option><option value="BER0">BER0 - ตั๋วแลกเงิน 360 วัน (BE)</option><option value="BS30">BS30 - ตั๋วแลกเงิน 30 วัน (BE) before shipment.</option><option value="BS60">BS60 - ตั๋วแลกเงิน 60 วัน (BE) before shipment.</option><option value="BS90">BS90 - ตั๋วแลกเงิน 90 วัน (BE) before shipment.</option><option value="BSA0">BSA0 - ตั๋วแลกเงิน 105 วัน (BE) before shipment.</option><option value="BSB0">BSB0 - ตั๋วแลกเงิน 120 วัน (BE) before shipment.</option><option value="BSD0">BSD0 - ตั๋วแลกเงิน 150 วัน (BE) before shipment.</option><option value="BSF0">BSF0 - ตั๋วแลกเงิน 180 วัน (BE) before shipment.</option><option value="CA15">CA15 - DLC 15 days</option><option value="CD00">CD00 - DLC</option><option value="CD07">CD07 - DLC 7 days</option><option value="CD15">CD15 - DLC 15 days</option><option value="CD30">CD30 - DLC 30 days</option><option value="CD60">CD60 - DLC 60 days</option><option value="CD90">CD90 - DLC 90 days</option><option value="CDB0">CDB0 - DLC 120 days</option><option value="CH07">CH07 - DLC</option><option value="CH15">CH15 - DLC</option><option value="CH30">CH30 - DLC</option><option value="CH60">CH60 - DLC</option><option value="CH90">CH90 - DLC</option><option value="CT00">CT00 - DLC</option><option value="CT03">CT03 - DLC</option><option value="CV03">CV03 - DLC</option><option value="DB07">DB07 - D/A 7 days after receiced bill.</option><option value="DB30">DB30 - D/A 30 days after receiced bill.</option><option value="DF00">DF00 - Draft.</option><option value="DL15">DL15 - D/A 15 days after B/L date.</option><option value="DL30">DL30 - D/A 30 days after B/L date.</option><option value="DL45">DL45 - D/A 45 days after B/L date.</option><option value="DL60">DL60 - D/A 60 days after B/L date.</option><option value="DL75">DL75 - D/A 75 days after B/L date.</option><option value="DL90">DL90 - D/A 90 days after B/L date.</option><option value="DLB0">DLB0 - D/A 120 days after B/L date.</option><option value="DLF0">DLF0 - D/A 180 days after B/L date.</option><option value="DND0">DND0 - D/A 150 days from B/L date</option><option value="DP00">DP00 - D/P at sight</option><option value="DP07">DP07 - D/P 7 days after B/L date</option><option value="DP15">DP15 - D/P at sight</option><option value="DP30">DP30 - D/P 30 days after B/L date</option><option value="DP45">DP45 - D/P 45 days after B/L date</option><option value="DV07">DV07 - D/A 7 days after invoice,B/L,AWB date.</option><option value="DV14">DV14 - D/A 14 days after invoice,B/L,AWB date.</option><option value="DV30">DV30 - D/A 30 days after invoice,B/L,AWB date.</option><option value="FI15">FI15 - within 15th of next month</option><option value="FI30">FI30 - within 30th of next month</option><option value="HB03">HB03 - HSBC-Dealer Financing (Invoice Financing)</option><option value="KD07">KD07 - Credit 7 days.</option><option value="LC00">LC00 - L/C at sight.</option><option value="LC15">LC15 - L/C 15 days.</option><option value="N120">N120 - Within 120 days  after received bill</option><option value="NM01">NM01 - เงินสด 1 วัน</option><option value="NM02">NM02 - เงินสด 2 วัน</option><option value="NM03">NM03 - เงินสด 3 วัน</option><option value="NM05">NM05 - เงินสด 5 วัน</option><option value="NM07">NM07 - เงินสด 7 วัน</option><option value="NM15">NM15 - เงินสด 15 วัน</option><option value="NM30">NM30 - เงินสด 30 วัน</option><option value="NM50">NM50 - เงินสด 50 วัน</option><option value="NT00">NT00 - เงินสด</option><option value="NT01">NT01 - BG 1 วัน</option><option value="NT02">NT02 - BG 2 วัน</option><option value="NT04">NT04 - BG 4 วัน</option><option value="NT05">NT05 - BG 5 วัน</option><option value="NT07">NT07 - BG 7 วัน</option><option value="NT10">NT10 - BG 10 วัน</option><option value="NT15">NT15 - BG 15 วัน</option><option value="NT21">NT21 - BG 21 วัน</option><option value="NT30">NT30 - BG 30 วัน</option><option value="NT45">NT45 - BG 45 วัน</option><option value="NT50">NT50 - BG 50 วัน</option><option value="NT60">NT60 - BG 60 วัน</option><option value="NT70">NT70 - BG 70 วัน</option><option value="NT75">NT75 - BG 75 วัน</option><option value="NT90">NT90 - BG 90 วัน</option><option value="NTB0">NTB0 - BG 120 วัน</option><option value="NTH0">NTH0 - BG 210 วัน</option><option value="NTJ0">NTJ0 - BG 240 วัน</option><option value="NTL0">NTL0 - BG 270 วัน</option><option value="NTN0">NTN0 - BG 300 วัน</option><option value="NTP0">NTP0 - BG 330 วัน</option><option value="NTR0">NTR0 - BG 360 วัน</option><option value="TL15">TL15 - T/T 07 days after B/L date.</option><option value="TL21">TL21 - T/T 15 days after B/L date.</option><option value="TL30">TL30 - T/T 30 days after B/L date.</option><option value="TL35">TL35 - T/T 35 days after B/L date.</option><option value="TL45">TL45 - T/T 45 days after B/L date.</option><option value="TL60">TL60 - T/T 60 days after B/L date.</option><option value="TL75">TL75 - T/T 75 days after B/L date.</option><option value="TL90">TL90 - T/T 90 days after B/L date.</option><option value="TLB0">TLB0 - T/T 120 days after B/L date.</option><option value="TLD0">TLD0 - T/T 150 days after B/L date</option><option value="TLH0">TLH0 - T/T 210 days after B/L date</option><option value="TLI0">TLI0 - T/T 180 days after B/L date.</option><option value="TQ30">TQ30 - T/T 30 days after received goods</option><option value="TR60">TR60 - T/T remittance 60 days after shipment</option><option value="TS00">TS00 - T/T before shipment.</option><option value="TS07">TS07 - T/T before shipment 7 day.</option><option value="TS15">TS15 - T/T before shipment 7 day.</option><option value="ZT00">ZT00 - L/C at sight 98%,T/T 2% after settlement</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}> Contract.NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>รหัสลูกค้า</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>COUNTRY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="AL">AL-Albania</option><option value="DZ">DZ-Algeria</option><option value="AR">AR-Argentina</option><option value="AU">AU-Australia</option><option value="AT">AT-Austria</option><option value="BH">BH-Bahrain</option><option value="BD">BD-Bangladesh</option><option value="BE">BE-Belgium</option><option value="BA">BA-Bosnia and Herzegovina</option><option value="BR">BR-Brazil</option><option value="BN">BN-Brunei</option><option value="BG">BG-Bulgaria</option><option value="BY">BY-Byelorussia</option><option value="KH">KH-Cambodia</option><option value="CA">CA-Canada</option><option value="CL">CL-Chile</option><option value="CN">CN-China</option><option value="CO">CO-Colombia</option><option value="CD">CD-Congo</option><option value="CR">CR-Costa Rica</option><option value="CI">CI-Cote d' lvoire</option><option value="HR">HR-Croatia</option><option value="CU">CU-Cuba</option><option value="CZ">CZ-CZECH REPUBLIC</option><option value="DK">DK-DENMARK</option><option value="DM">DM-Dominican Repubilic</option><option value="EC">EC-Ecuador</option><option value="EG">EG-EGYPT</option><option value="SV">SV-El salvalor</option><option value="ER">ER-Eritrea</option><option value="EE">EE-ESTONIA</option><option value="ET">ET-ETHlopia</option><option value="FJ">FJ-Fiji</option><option value="FI">FI-FINLAND</option><option value="FR">FR-France</option><option value="MK">MK-FYR Macedonia</option><option value="GE">GE-Georgia</option><option value="DE">DE-Germany</option><option value="GH">GH-Ghana</option><option value="GB">GB-Grate Britain</option><option value="GR">GR-GREECE</option><option value="GT">GT-Guatemala</option><option value="HN">HN-honduras</option><option value="HK">HK-Hong Kong</option><option value="HU">HU-HUNGARY</option><option value="IN">IN-India</option><option value="ID">ID-Indonesia</option><option value="IR">IR-IRAN</option><option value="IQ">IQ-IRAQ</option><option value="IL">IL-ISRAEL</option><option value="IT">IT-Italy</option><option value="JM">JM-Jamaica</option><option value="JP">JP-Japan</option><option value="JO">JO-JORDAN</option><option value="KE">KE-Kenya</option><option value="KW">KW-KUWAIT</option><option value="LA">LA-Laos</option><option value="LV">LV-LATVIA</option><option value="LB">LB-LEBANON</option><option value="LY">LY-LIBYA</option><option value="LU">LU-LUXEMBOURG</option><option value="MG">MG-Madagascar</option><option value="MY">MY-Malaysia</option><option value="MV">MV-Maldives</option><option value="MU">MU-Mauritius</option><option value="MX">MX-Mexico</option><option value="MA">MA-MOROCCO</option><option value="MZ">MZ-Mozambique</option><option value="MM">MM-Myanmar</option><option value="NL">NL-NETHERLANDS</option><option value="NC">NC-New Caledonia</option><option value="NZ">NZ-New Zealand</option><option value="NI">NI-Nicaragua</option><option value="NG">NG-Nigeria</option><option value="KP">KP-North Korea</option><option value="NO">NO-Norway</option><option value="NM">NM-Noumea</option><option value="OM">OM-OMAN</option><option value="PK">PK-Pakistan</option><option value="PG">PG-Papua Nw Guinea</option><option value="PY">PY-Paraguay</option><option value="PE">PE-Peru</option><option value="PH">PH-Philippines</option><option value="PL">PL-POLAND</option><option value="PT">PT-PORTUGAL</option><option value="PR">PR-Puerto Rico</option><option value="QA">QA-Qatar</option><option value="RO">RO-ROMANIA</option><option value="RU">RU-Russia</option><option value="SU">SU-Samoa</option><option value="AS">AS-Samoa American</option><option value="SA">SA-Saudi Arabia</option><option value="RM">RM-Sebia and Montenego</option><option value="SN">SN-Senegal</option><option value="SL">SL-Sierra leone</option><option value="SG">SG-Singapore</option><option value="SK">SK-SLOVAKIA</option><option value="SI">SI-SLOVENIA</option><option value="SB">SB-Solomon Islands</option><option value="SF">SF-South Africa</option><option value="ZA">ZA-South Africa</option><option value="KR">KR-South Korea</option><option value="ES">ES-SPAIN</option><option value="LK">LK-Sri Lanka</option><option value="SE">SE-SWEDEN</option><option value="SZ">SZ-Switzerland</option><option value="SY">SY-SYRIA</option><option value="TW">TW-Taiwan</option><option value="TZ">TZ-Tanzania</option><option value="TH">TH-Thailand</option><option value="TT">TT-Trinidad and tobago</option><option value="TN">TN-TUNISIA</option><option value="TR">TR-Turkey</option><option value="UA">UA-Ukraine</option><option value="AE">AE-Unit.Arab Emir.</option><option value="UK">UK-United Kingdom</option><option value="US">US-United state</option><option value="UY">UY-Uruguay</option><option value="UZ">UZ-Uzbekistan</option><option value="VU">VU-Vanuatu</option><option value="VE">VE-Venezuela</option><option value="VN">VN-Vietnam</option><option value="WS">WS-Western Samoa</option><option value="YE">YE-Yemen</option><option value="ZW">ZW-Zimbabwe</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>LC.NO.</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>

                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}> หน่วยงาน</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="B">ส.ขบ.</option><option value="C">ส.ตท.</option><option value="A">ส.ผจ.</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>CURRENCY</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="AUD">Australian Dollars</option><option value="EUR">European Currency Unit</option><option value="MYR">Malaysian Ringgit</option><option value="THB">Thai Baht</option><option value="USD">US Dollars</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>สถานะ</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="O">Operate</option><option value="M">Completed</option><option value="K">Checked</option><option value="A">Approved</option><option value="N">Actived</option><option value="C">Closed</option><option value="Y">Canceled</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="a.Weight">Weight</option><option value="a.H">H</option><option value="a.B">B</option><option value="a.T1">T1</option><option value="a.T2">T2</option><option value="a.R1">R1</option><option value="a.R2">R2</option><option value="a.D">D</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" > SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </MainCard>
                        <MainCard isOption title="CUSTOMER EMAIL">
                            <Row>
                                <Col className="email-card">
                                    <Button id="btnEdit" variant="warring" className="mr-2 d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary" onClick={e => this.setShowModal(e, "Edit")}><span className="feather icon-edit text-info" /></Button>
                                    <Button id="btnDel" variant="default" className="d-none btn waves-effect waves-light btn-icon btn-rounded btn-outline-secondary sweet-multiple has-ripple" onClick={this.sweetConfirmHandler}><span className="feather icon-trash-2 text-c-red" /></Button>
                                </Col>

                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success">NEW SC DLC.</Button>
                                    <Button size="sm" variant="danger" >ลบเอกสาร</Button>
                                    <Button size="sm" variant="success">SET OPEN</Button>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>NEW SC DLC.</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>CONTRACT NO</th>
                                        <th>CONTRACT DATE</th>
                                        <th>LATEST DELIVERY</th>
                                        <th>EXPIRE</th>
                                        <th>CODE</th>
                                        <th>CUSTOMER</th>
                                        <th>PAYMENT TERM</th>
                                        <th>TON</th>
                                        <th>AMT</th>
                                        <th>CURR.</th>
                                        <th>DLC.NO.</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                            </Table>
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SizeMaster;