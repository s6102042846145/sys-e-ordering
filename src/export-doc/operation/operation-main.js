import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> OPERATION </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/print-inv-export-order">
                                            PRINT INV# EXPORT ORDER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/moveorder-to-ods" >
                                            MOVE ORDER TO ODS
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/be-ben-cert-from" >
                                            BE/BEN.CERT.FROM
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/operation-taxation" >
                                            ใบขอรับเงินชดเชยค่าภาษีอากร
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/est-shippment" >
                                            EST.SHIPPMENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/order-discrepancy" >
                                            ORDER DISCREPANCY
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/print-inv-export-ord-shipment" >
                                            PRINT INV# EXPORT ORD.BY SHIPMENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/redit-document" >
                                            CREDIT DOCUMENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/operation-number" >
                                            กำหนดเลขที่ใบขน
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/update-payment-status" >
                                            UPDATE PAYMENT STATUS
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/payment-receipt" >
                                            ใบรับชำระเงิน
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/operation/short-shipment" >
                                            SHORT SHIPMENT
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>

                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;