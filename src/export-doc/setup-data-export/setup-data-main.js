import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {



    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> SETUP DATA </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/admin-page">
                                            ADMIN PAGE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/end-customer-email" >
                                            END CUSTOMER EMAIL
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/order-email" >
                                            SEND ORDER EMAIL
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/mail-log" >
                                            MAIL LOG
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/op-data" >
                                            OP DATA
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >

                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/customer-email" >
                                            CUSTOMER EMAIL
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/ods-setup" >
                                            ODS SETUP
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/ods-report-authorize" >
                                            ODS REPORT AUTHORIZE
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/scrap-return-master" >
                                            SCRAP RETURN MASTER
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/export-doc/setup-data-export/in-land-cost" >
                                            IN-LAND COST
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>


                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;