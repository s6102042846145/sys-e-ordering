import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col xl={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> PRODUCTION PLAN</Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/production-plan/DomesticYearlyProductionPlan">
                                            Export Yearly Production Plan
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/production-plan/DomesticYearlyProductionPlan">
                                            Export Yearly Production Plan
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>

                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;