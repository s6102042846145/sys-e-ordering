import React from 'react';
import { Row, Col, Form, Button, Table, Modal, Tabs, Tab, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const data1 = [
    {
        "id": 1,
        "Col1": "SYS1-19-R2",
        "Col2": "250 PFC",
        "Col3": "",
        "Col4": "Y",
        "Col5": "",
        "Col6": "Y",
        "Col7": "",
        "Col8": "",
        "Col9": "",
        "Col10": "",
        "Col11": "",
        "Col12": "",
        "Col13": "",
        "Col14": "Y",
        "Col15": "05-Jul-19 11:51 583-2"
    },
    {
        "id": 2,
        "Col1": "SYS1-19-R2",
        "Col2": "300 PFC",
        "Col3": "",
        "Col4": "",
        "Col5": "",
        "Col6": "Y",
        "Col7": "",
        "Col8": "",
        "Col9": "",
        "Col10": "",
        "Col11": "",
        "Col12": "",
        "Col13": "Y",
        "Col14": "",
        "Col15": "05-Jul-19 13:58 584-2"
    },
    {
        "id": 3,
        "Col1": "SYS1-19-R2",
        "Col2": "H 203*203",
        "Col3": "",
        "Col4": "",
        "Col5": "Y",
        "Col6": "Y",
        "Col7": "",
        "Col8": "",
        "Col9": "",
        "Col10": "",
        "Col11": "",
        "Col12": "",
        "Col13": "",
        "Col14": "",
        "Col15": "05-Jul-19 14:10 543-2"
    },
    {
        "id": 4,
        "Col1": "SYS1-19-R2",
        "Col2": "H 254*254",
        "Col3": "",
        "Col4": "",
        "Col5": "Y",
        "Col6": "",
        "Col7": "",
        "Col8": "",
        "Col9": "",
        "Col10": "",
        "Col11": "Y",
        "Col12": "",
        "Col13": "Y",
        "Col14": "",
        "Col15": "05-Jul-19 14:10 543-2"
    }
];

const data2 = [
    {
        "id": 1,
        "Col1": "SYS1-19-R2",
        "Col2": "250 PFC",
        "Col3": "Jun-19",
        "Col4": "05-Jul-19 11:45",
        "Col5": "NOTE",
        "Col6": "ปิดเนื่องจาก 300PFC หลุดเดือน May มา Jun จึงปิด 250PFC",
        "Col7": "actived"
    },
    {
        "id": 2,
        "Col1": "SYS1-19-R2",
        "Col2": "250 PFC",
        "Col3": "Feb-19",
        "Col4": "14-Mar-19 09:33",
        "Col5": "OPEN",
        "Col6": "เปิดเนื่องจาก ปิดมา3 เดือน พร้อมกับมี Enquiry",
        "Col7": "actived"
    },
    {
        "id": 3,
        "Col1": "SYS1-19-R2",
        "Col2": "250 PFC",
        "Col3": "Feb-19",
        "Col4": "14-Mar-19 09:33",
        "Col5": "CLOSE",
        "Col6": "Close เนื่องจากเปิดเดือน Aug / เปิด เดือน เว้นเดือน",
        "Col7": "actived"
    },
    {
        "id": 4,
        "Col1": "SYS1-19-R2",
        "Col2": "250 PFC",
        "Col3": "Mar-19",
        "Col4": "07-Mar-19 15:46",
        "Col5": "OPEN",
        "Col6": "Close เนื่องจากไม่มี Enquiry",
        "Col7": "actived"
    }
];

function atable() {
    let table1 = '#data-table1';
    let table2 = '#data-table2';
    $.fn.dataTable.ext.errMode = 'throw';

    $(table1).DataTable({
        data: data1,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            {
                sortable: false,
                className: "text-center",
                "data": "Col3",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col4",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col5",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col6",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col7",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col8",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col9",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col10",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col11",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col12",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col13",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            {
                sortable: false,
                className: "text-center",
                "data": "Col14",
                "render": function (data, type, row) {
                    if(data === 'Y') {
                        return "<i class='feather icon-check'></i>"
                    }
                    return ""
                }
            },
            { "data": "Col15", render: function (data, type, row) { return data; } }
        ]
    });

    $(table2).DataTable({
        data: data2,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } },
            { "data": "Col6", render: function (data, type, row) { return data; } },
            { "data": "Col7", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class ExportYearlyProductionPlan extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table1 tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table1 tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            <Aux>
                <Row>
                    <Col>
                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col md={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PLANT</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="4951">BDC</option><option value="4941">SOL</option><option value="4921">SR</option><option value="4911">SYS1</option><option value="4931">SYS2</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PLAN YEAR</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>REV</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>PLAN MONTH</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="01">01 - มกราคม</option><option value="02">02 - กุมภาพันธ์</option><option value="03">03 - มีนาคม</option><option value="04">04 - เมษายน</option><option value="05">05 - พฤษภาคม</option><option value="06">06 - มิถุนายน</option><option value="07">07 - กรกฏาคม</option><option value="08">08 - สิงหาคม</option><option value="09">09 - กันยายน</option><option value="10">10 - ตุลาคม</option><option value="11">11 - พฤศจิกายน</option><option value="12">12 - ธันวาคม</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>ROLL SIZE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="14100002">100 PFC</option><option value="11070002">150 PFC</option><option value="11070003">180 PFC</option><option value="11070004">180 UB</option><option value="11070005">200 PFC</option><option value="11070006">230 PFC</option><option value="11070007">250 PFC</option><option value="11070008">300 PFC</option><option value="19040001">380 PFC</option><option value="11070001">Bloom</option><option value="12030001">C 100*50</option><option value="12030002">C 125*65</option><option value="18040001">C 150*75H</option><option value="12030003">C 150*75L</option><option value="11070009">C 180*75</option><option value="11070010">C 200*80</option><option value="11070011">C 200*90</option><option value="11070012">C 250*90H</option><option value="11070013">C 250*90L</option><option value="11070142">C 300*90L</option><option value="11070014">C 300*90M,H</option><option value="11070015">C 380*100H</option><option value="11070016">C 380*100L</option><option value="14100001">C75x40</option><option value="14080001">CKB 200 32.7KG/M</option><option value="14080002">CKB 200 37.2KG/M</option><option value="11070017">H 100*100</option><option value="11070018">H 100*50</option><option value="11070019">H 102*102</option><option value="11070020">H 125*125</option><option value="11070021">H 127*127</option><option value="11070022">H 150*100</option><option value="11070023">H 150*150</option><option value="11070024">H 150*75</option><option value="11070025">H 152*102</option><option value="11070026">H 152*152</option><option value="11070027">H 175*175</option><option value="11070028">H 200*100</option><option value="11070029">H 200*150</option><option value="11070030">H 200*200</option><option value="11070031">H 203*102</option><option value="11070032">H 203*133</option><option value="15060002">H 203*165(ASTM)</option><option value="11070033">H 203*203</option><option value="11070034">H 244*175</option><option value="11070035">H 250*125</option><option value="11070036">H 250*175</option><option value="11070037">H 250*250</option><option value="11070038">H 254*102</option><option value="11070039">H 254*146</option><option value="11070040">H 254*146 ASTM</option><option value="11070041">H 254*254</option><option value="11070042">H 300*150</option><option value="11070043">H 300*200</option><option value="11070044">H 300*300</option><option value="11070045">H 305*102</option><option value="11070046">H 305*165</option><option value="11070047">H 305*165 ASTM</option><option value="11070048">H 305*203</option><option value="13070001">H 305*305H</option><option value="11070049">H 305*305M, L</option><option value="11070050">H 350*175</option><option value="11070051">H 350*250</option><option value="11070052">H 350*350</option><option value="11070053">H 356*127</option><option value="11070054">H 356*171</option><option value="11070055">H 356*254</option><option value="11070056">H 356*368</option><option value="11070057">H 356*406</option><option value="11070058">H 400*200</option><option value="11070059">H 400*300</option><option value="11070060">H 400*400</option><option value="11070061">H 406*140</option><option value="11070062">H 406*178</option><option value="11070063">H 450*200</option><option value="11070064">H 450*300</option><option value="11070065">H 457*152</option><option value="11070066">H 457*191</option><option value="11070067">H 500*200</option><option value="11070068">H 500*300</option><option value="11070069">H 533*165</option><option value="11070070">H 533*210</option><option value="18050001">H 533*312</option><option value="11070071">H 600*200</option><option value="11070072">H 600*300</option><option value="11070073">H 610*178</option><option value="11070074">H 610*229</option><option value="11070075">H 610*305</option><option value="11070076">H 610*324</option><option value="11070077">H 686*254</option><option value="11070078">H 700*300</option><option value="11070079">H 762*267</option><option value="11070080">H 800*300</option><option value="11070081">H 838*292</option><option value="11070082">H 900*300</option><option value="11070083">H125*125</option><option value="15040001">H175x90</option><option value="18060001">H254x203</option><option value="11070084">HEA 600</option><option value="11070085">HEA, B 100</option><option value="11070086">HEA, B 160</option><option value="11070087">HEA, B 180</option><option value="11070088">HEA, B 200</option><option value="15070001">HEA, B 220</option><option value="11070089">HEA, B 260</option><option value="11070090">HEA, B 300</option><option value="15070002">HEA, B 320</option><option value="11070091">HEA, B 360</option><option value="11070092">HEA, B 400</option><option value="11070093">HEA, B 450</option><option value="11070094">HEA, B 500</option><option value="11070111">I 150*75</option><option value="20100003">I 160*88</option><option value="20100002">I 180*94</option><option value="11070112">I 200*100</option><option value="11070113">I 200*150</option><option value="11070114">I 250*125H</option><option value="11070115">I 250*125L</option><option value="18060002">I 25a,b</option><option value="11070116">I 300*150H</option><option value="11070117">I 300*150L</option><option value="11070118">I 300*150M</option><option value="11070119">I 350*150H</option><option value="11070120">I 350*150L</option><option value="11070121">I 400*150H</option><option value="11070122">I 400*150L</option><option value="11070123">I 450*175H</option><option value="11070124">I 450*175L</option><option value="11070125">I 600*190H</option><option value="11070126">I 600*190L</option><option value="20100001">I60X88</option><option value="11070095">IPE 100</option><option value="11070096">IPE 180</option><option value="11070097">IPE 180</option><option value="11070098">IPE 200</option><option value="11070099">IPE 240</option><option value="11070100">IPE 270</option><option value="11070101">IPE 300</option><option value="11070102">IPE 330</option><option value="11070103">IPE 360</option><option value="11070104">IPE 400</option><option value="11070105">IPE 450</option><option value="11070107">IPE 550</option><option value="11070108">IPE 600</option><option value="11070109">IPEA AA</option><option value="11070127">L 100*100</option><option value="11070128">L 120*120</option><option value="11070129">L 130*130</option><option value="11070130">L 150*150</option><option value="11070131">L 152*152</option><option value="11070132">L 175*175</option><option value="18070001">L 200*200*25</option><option value="11070133">L 200*200x20,15</option><option value="11070136">L 250*250*25</option><option value="18070002">L 250*250*35</option><option value="16010001">L75*75</option><option value="16070001">L90*90</option><option value="11070138">Modular</option><option value="14110001">Solution</option><option value="13060001">SP 400*100</option><option value="11070139">SP 400*125</option><option value="11070140">SP 400*150</option><option value="11070141">SP 400*170</option><option value="17100001">SYS2-H152x89</option><option value="14100003">SYS2-H175x175</option><option value="15060001">SYS2-H305x102</option><option value="16020001">SYS2-I200x150</option><option value="11070110">W 16*16</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={9}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={2}>PRODUCT SIZE</Form.Label>
                                                    <Col sm={10}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row}>
                                            <Col sm={3}></Col>
                                            <Col sm={3}></Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>STATUS</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>SYSTEM CODE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control type="text" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>

                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="None">None</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                    this.state.selectValue === 2 ||
                                                    this.state.selectValue === 3 ||
                                                    this.state.selectValue === 4 ||
                                                    this.state.selectValue === 5 ||
                                                    this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                            {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" onClick={e => this.handleSubmit(e)}> SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        
                        </MainCard>
                        <MainCard isOption title="EXPORT YEARLY PRODUCTION PLAN">
                      
                            <Tabs defaultActiveKey="tab1" className="mb-3">
                                <Tab eventKey="tab1" title="EXPORT YEARLY PRODUCTION PLAN">
                                    <Table ref="tbl" striped hover responsive bordered id="data-table1">
                                        <thead>
                                            <tr>
                                                <th><Form.Check id="example-select-all" /></th>
                                                <th>#</th>
                                                <th>PLANT YEAR-REV</th>
                                                <th>PRODUCT SIZE</th>
                                                <th>JAN</th>
                                                <th>FEB</th>
                                                <th>MAR</th>
                                                <th>APR</th>
                                                <th>MAY</th>
                                                <th>JUN</th>
                                                <th>JUL</th>
                                                <th>AUG</th>
                                                <th>SEP</th>
                                                <th>OCE</th>
                                                <th>NOV</th>
                                                <th>DEC</th>
                                                <th>UPDATED DATE</th>
                                            </tr>
                                        </thead>
                                    </Table>
                                </Tab>
                                <Tab eventKey="tab2" title="NOTE(S)">
                                    <Table ref="tbl" striped hover responsive bordered id="data-table2">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>PLANT YEAR-REV</th>
                                                <th>PRODUCT SIZE</th>
                                                <th>YR.-MTH</th>
                                                <th>DATE</th>
                                                <th>TYPE</th>
                                                <th>REASON</th>
                                                <th>STATUS</th>
                                            </tr>
                                        </thead>
                                    </Table>
                                </Tab>
                                <Tab eventKey="tab3" title="UPLOAD">
                                    <Form.Group as={Row}>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={6}>PLAN YEAR</Form.Label>
                                                <Col sm={6}>
                                                    <Form.Control as="select">
                                                        <option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm={4}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={4}>REV</Form.Label>
                                                <Col sm={6}>
                                                    <Form.Control as="select">
                                                        <option value="-1">None</option><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option>                                                        </Form.Control>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Col sm={12}>
                                            <Form.Group as={Row}>
                                                <Form.Label column sm={2}>NEW UPLOAD FILE</Form.Label>
                                                <Col sm={10}>
                                                    <div className="custom-file">
                                                        <Form.Control
                                                            type="file"
                                                            className="custom-file-input"
                                                            onChange={(event) => this.supportedFileHandler(event)}
                                                        />
                                                            <Form.Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Form.Label>
                                                    </div>
                                                </Col>
                                            </Form.Group> 
                                        </Col>
                                    </Form.Group>
                                </Tab>
                            </Tabs>
                        
                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default ExportYearlyProductionPlan;
