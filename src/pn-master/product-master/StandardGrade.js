import React from 'react';
import { Row, Col, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';
import MainCard from "../../App/components/MainCard";
import Aux from "../../hoc/_Aux";
import Dropdown from "../../App/components/Dropdown";
import DropdownEdit from "../../App/components/DropdownEdit";
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require('datatables.net-responsive-bs');

const names = [
    {
        "id": 1,
        "Col1": "0",
        "Col2": "0",
        "Col3": "TIS",
        "Col4": "SS400",
        "Col5": "SS400",
        "Col6": "0",
        "Col7": "000",
        "Col8": "000",
        "Col9": "actived",
        "Col10": "12-Feb-21 17:58 "
    },
    {
        "id": 2,
        "Col1": "0",
        "Col2": "1",
        "Col3": "TIS",
        "Col4": "SS490",
        "Col5": "SS490",
        "Col6": "4",
        "Col7": "000",
        "Col8": "010",
        "Col9": "actived",
        "Col10": "21-Mar-16 14:30"
    },
    {
        "id": 3,
        "Col1": "0",
        "Col2": "1",
        "Col3": "TIS",
        "Col4": "SS540",
        "Col5": "SS540",
        "Col6": "A",
        "Col7": "000",
        "Col8": "020",
        "Col9": "actived",
        "Col10": "21-Mar-16 14:30"
    },
    {
        "id": 4,
        "Col1": "0",
        "Col2": "3",
        "Col3": "TIS",
        "Col4": "SM400",
        "Col5": "SM400",
        "Col6": "2",
        "Col7": "000",
        "Col8": "030",
        "Col9": "actived",
        "Col10": "21-Mar-16 14:30"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "data": "id", "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } },
            { "data": "Col6", render: function (data, type, row) { return data; } },
            { "data": "Col7", render: function (data, type, row) { return data; } },
            { "data": "Col8", render: function (data, type, row) { return data; } },
            { "data": "Col9", render: function (data, type, row) { return data; } },
            { "data": "Col10", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class StandardGrade extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };

    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (

            <Aux>

                <Row>
                    <Col>

                        <Modal size="xl" backdrop="static" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="f-12">
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">STANDARD</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="standard" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">STD CD</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="std-cd" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">GRADE CD</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="grade-cd" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">STD</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="std" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">GRADE</Form.Label>
                                            <Col sm={8}>
                                                <DropdownEdit type="grade" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">PN.GRADE</Form.Label>
                                            <Col sm={8}>
                                            <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">SIZE Cd</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">STD Cd</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={4}>
                                        <Form.Group as={Row}>
                                            <Form.Label className="col-sm-4">GRADE Cd</Form.Label>
                                            <Col sm={8}>
                                                <input type="text" className="form-control-file" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Row>


                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save</Button>
                            </Modal.Footer>
                        </Modal>

                        <MainCard isOption title="SEARCH">
                            <Row>
                                <Col md={12}>
                                    <Form>
                                        <Form.Group as={Row}>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>STANDARD</Form.Label>
                                                    <Col sm={8}>
                                                        <Dropdown type="standard" />
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>STATUS</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">ทั้งหมด</option><option value="N">Actived</option><option value="Y">Canceled</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={4}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>GRADE</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="ALL">All</option><option value="">None</option><option value="3SP">3SP</option><option value="43A">43A</option><option value="43A/S275JR">43A/S275JR</option><option value="43B">43B</option><option value="43C">43C</option><option value="50B">50B</option><option value="50B/S355JR">50B/S355JR</option><option value="50C">50C</option><option value="50E">50E</option><option value="55C">55C</option><option value="5SP">5SP</option><option value="A36">A36</option><option value="A36/SS400">A36/SS400</option><option value="A572 Gr.65">A572 Gr.65</option><option value="A572-GR.42">A572-GR.42</option><option value="A572-GR.50">A572-GR.50</option><option value="A572-GR.55">A572-GR.55</option><option value="A572-GR.60">A572-GR.60</option><option value="A572-GR.65">A572-GR.65</option><option value="A572-GR50/S355J2G3">A572-GR50/S355J2G3</option><option value="A992">A992</option><option value="A992/A572G50">A992/A572G50</option><option value="A992-50">A992-50</option><option value="AS/NZS 3679.1-250">AS/NZS 3679.1-250</option><option value="AS/NZS 3679.1-300">AS/NZS 3679.1-300</option><option value="AS/NZS 3679.1-300L0">AS/NZS 3679.1-300L0</option><option value="AS/NZS 3679.1-300S0">AS/NZS 3679.1-300S0</option><option value="AS/NZS 3679.1-300W">AS/NZS 3679.1-300W</option><option value="AS/NZS 3679.1-350">AS/NZS 3679.1-350</option><option value="AS/NZS 3679.1-350W">AS/NZS 3679.1-350W</option><option value="AS/NZS 3679.1-355D">AS/NZS 3679.1-355D</option><option value="AS/NZS 3679.1-355EM">AS/NZS 3679.1-355EM</option><option value="AS/NZS 3679.1-355EMZ">AS/NZS 3679.1-355EMZ</option><option value="BJ P 41">BJ P 41</option><option value="BJ P 50">BJ P 50</option><option value="BJ P 55">BJ P 55</option><option value="BJ PHC 400">BJ PHC 400</option><option value="BJ PHC 490">BJ PHC 490</option><option value="BJ PHC 540">BJ PHC 540</option><option value="D">D</option><option value="DH32">DH32</option><option value="DH36">DH36</option><option value="DH40">DH40</option><option value="E">E</option><option value="EH32">EH32</option><option value="EH36">EH36</option><option value="EH40">EH40</option><option value="Q235qD">Q235qD</option><option value="S235J0">S235J0</option><option value="S235JR">S235JR</option><option value="S240GP">S240GP</option><option value="S270GP">S270GP</option><option value="S275J0">S275J0</option><option value="S275J2">S275J2</option><option value="S275J2G3">S275J2G3</option><option value="S275JR">S275JR</option><option value="S320GP">S320GP</option><option value="S355GP">S355GP</option><option value="S355J0">S355J0</option><option value="S355J2">S355J2</option><option value="S355J2G3">S355J2G3</option><option value="S355JR">S355JR</option><option value="S355K2">S355K2</option><option value="S390GP">S390GP</option><option value="S430GP">S430GP</option><option value="S450J0">S450J0</option><option value="SM400">SM400</option><option value="SM400A">SM400A</option><option value="SM400B">SM400B</option><option value="SM490">SM490</option><option value="SM490A">SM490A</option><option value="SM490B">SM490B</option><option value="SM490YA">SM490YA</option><option value="SM490YB">SM490YB</option><option value="SM520">SM520</option><option value="SM520B">SM520B</option><option value="SM520C">SM520C</option><option value="SM570">SM570</option><option value="SN">SN</option><option value="SN400YB/SN400B">SN400YB/SN400B</option><option value="SS400">SS400</option><option value="SS400/SM400">SS400/SM400</option><option value="SS490">SS490</option><option value="SS540">SS540</option><option value="ST44-2">ST44-2</option><option value="ST50-2">ST50-2</option><option value="ST52-3">ST52-3</option><option value="SW275A">SW275A</option><option value="SY295">SY295</option><option value="SY295/S270GP">SY295/S270GP</option><option value="SY295:2012">SY295:2012</option><option value="SY295:2012/S270GP">SY295:2012/S270GP</option><option value="SY390">SY390</option><option value="SY390/S390GP">SY390/S390GP</option><option value="SY390:2012">SY390:2012</option><option value="SY390:2012/S390GP">SY390:2012/S390GP</option><option value="test">Test</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                        </Form.Group>
                                        <br />
                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control as="select">
                                                                <option value={0}>Create Date</option>
                                                                <option value={1}>Update Date</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 1 ||
                                                        this.state.selectDate === 2 ||
                                                        this.state.selectDate === 3 ||
                                                        this.state.selectDate === 4 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 5 ||
                                                        this.state.selectDate === 6 ||
                                                        this.state.selectDate === 7 ||
                                                        this.state.selectDate === 8 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>From</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 9 ||
                                                        this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 11 ||
                                                        this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control as="select">
                                                            <option value="None">None</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>From Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Control
                                                            as="select"
                                                            value={this.state.supportedSelect}
                                                            onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                        >
                                                            <option value={0}>None</option>
                                                            <option value={1}>At</option>
                                                            <option value={2}>Between</option>
                                                            <option value={3}>Less than</option>
                                                            <option value={4}>Less than or equal</option>
                                                            <option value={5}>At Month</option>
                                                            <option value={6}>Between Month</option>
                                                            <option value={7}>More than</option>
                                                            <option value={8}>More than or equal</option>
                                                            <option value={9}>At Quater</option>
                                                            <option value={10}>Between Quater</option>
                                                            <option value={11}>At Year</option>
                                                            <option value={12}>Between Year</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectDate === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 6 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={3}>To</Form.Label>
                                                            <Col sm={4}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                            <Col sm={5}>
                                                                <Form.Control as="select">
                                                                    <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 10 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                                {
                                                    this.state.selectDate === 12 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To</Form.Label>
                                                            <Col sm={8}>
                                                                <Form.Control as="select">
                                                                    <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                </Form.Control>
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                            <Col sm={3}>
                                                <Form.Group as={Row}>
                                                    <Form.Label column sm={4}>Criteria</Form.Label>
                                                    <Col sm={8}>
                                                        <Form.Group>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>More than</option>
                                                                <option value={6}>More than or equal</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>
                                            </Col>
                                            <Col sm={3}>
                                                {
                                                    this.state.selectValue === 2 ?

                                                        <Form.Group as={Row}>
                                                            <Form.Label column sm={4}>To Value</Form.Label>
                                                            <Col sm={8}>
                                                                <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                            </Col>
                                                        </Form.Group>

                                                        : ''
                                                }
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row}>
                                            <Col>
                                                <Button className="pull-right" size="sm" onClick={e => this.handleSubmit(e)}> SEARCH </Button>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>

                        </MainCard>
                        <MainCard isOption title="STANDARD - GRADE">

                            <Row>
                                <Col className="btn-page text-left" sm>
                                    <Button id="btnEdit" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit" /></Button>
                                    <Button id="btnDel" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash" /></Button>
                                </Col>
                                <Col className="btn-page text-right" sm>
                                    <Button size="sm" variant="success" >SET ACTIVE</Button>
                                    <Button size="sm" variant="danger" >SET CANCEL</Button>
                                    <Button size="sm" variant="success" href="./GradeMaster" >เพิ่ม STD.-GRADE</Button>
                                    <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                </Col>
                            </Row>
                            <br />
                            <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                <thead>
                                    <tr>
                                        <th><Form.Check id="example-select-all" /></th>
                                        <th>#</th>
                                        <th>STD CD</th>
                                        <th>GRADE CD</th>
                                        <th>STD</th>
                                        <th>GRADE</th>
                                        <th>PN.GRADE</th>
                                        <th>SIZE Cd</th>
                                        <th>STD Cd</th>
                                        <th>GRADE Cd</th>
                                        <th>STATUS</th>
                                        <th>UPDATED DATE</th>
                                    </tr>
                                </thead>
                            </Table>

                        </MainCard>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default StandardGrade;
