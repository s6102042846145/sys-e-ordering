import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col xl={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> PRODUCT MASTER</Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/SectionMaster">
                                            Section Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/SizeMaster" >
                                            Size Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/GradeMaster" >
                                            Grade Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/PnStatus" >
                                            Pn Status
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/StorageNewPn" >
                                            Storage New PN
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/SalesOrgChannelNewPn" >
                                            Sales ORG Channel New Pn
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/SectionHierarchyMaster">
                                            Section Hierarchy Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/StandardMaster" >
                                            Standard Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/StandardGrade" >
                                            Standard - Grade
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/PnStatusSize" >
                                            PN  Status-Size.STD-OLD Code
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/GroupVcMaster" >
                                            Group VC Master
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className='list-group-item list-group-item-action' to="/pn-master/product-master/GroupVcCost" >
                                            Group VC Cost
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;