import React from 'react';
import {
    Row,
    Col,
    Card
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> PRODUCT </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <Row className="card m-15" >
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ProductCodeOpenningMS126">
                                            ใบเปิดรหัสสินค้า (MS126-1)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSBC" >
                                            ยืนยันเปิดรหัสสินค้า (สบช)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSSK" >
                                            ยืนยันเปิดรหัสสินค้า (สสค)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSSR2" >
                                            ยืนยันเปิดรหัสสินค้า (สสร2)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                                <Col sm={6}>
                                    <Row className="card m-15" >
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSGT" >
                                            ยืนยันเปิดรหัสสินค้า (สกธ)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSSJ" >
                                            ยืนยันเปิดรหัสสินค้า (สสจ)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ConfirmOpenProductSSR1" >
                                            ยืนยันเปิดรหัสสินค้า (สสร1)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                        <Link className="list-group-item list-group-item-action" to="/pn-master/product/ItemMastFGSTOCK" >
                                            ITEM MAST (FGSTOCK)
                                            <i className="feather float-right icon-chevron-right mt-1"></i>
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;